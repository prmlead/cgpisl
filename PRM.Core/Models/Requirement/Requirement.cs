﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class Requirement : Entity
    {
        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }
        
        string custFirstName = string.Empty;
        [DataMember(Name = "custFirstName")]
        public string CustFirstName
        {
            get
            {
                if (!string.IsNullOrEmpty(custFirstName))
                { return custFirstName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { custFirstName = value; }
            }
        }
        
        string custLastName = string.Empty;
        [DataMember(Name = "custLastName")]
        public string CustLastName
        {
            get
            {
                if (!string.IsNullOrEmpty(custLastName))
                { return custLastName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { custLastName = value; }
            }
        }

        //[DataMember(Name = "title")]
        //public string Title { get; set; }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        string description = string.Empty;
        [DataMember(Name = "title")]
        public string Description
        {
            get
            {
                if (!string.IsNullOrEmpty(description))
                { return description; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { description = value; }
            }
        }

        [DataMember(Name = "postedOn")]
        public DateTime? PostedOn { get; set; }
        
        string budget = string.Empty;
        [DataMember(Name = "budget")]
        public string Budget
        {
            get
            {
                if (!string.IsNullOrEmpty(budget))
                { return budget; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { budget = value; }
            }
        }
        
        string deliveryLocation = string.Empty;
        [DataMember(Name = "deliveryLocation")]
        public string DeliveryLocation
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryLocation))
                { return deliveryLocation; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryLocation = value; }
            }
        }

        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                { return status; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { status = value; }
            }
        }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }
        
        string urgency = string.Empty;
        [DataMember(Name = "status")]
        public string Urgency
        {
            get
            {
                if (!string.IsNullOrEmpty(urgency))
                { return urgency; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { urgency = value; }
            }
        }

        [DataMember(Name = "category")]
        public string[] Category { get; set; }

        string taxes = string.Empty;
        [DataMember(Name = "taxes")]
        public string Taxes
        {
            get
            {
                if (!string.IsNullOrEmpty(taxes))
                { return taxes; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { taxes = value; }
            }
        }
        
        string paymentTerms = string.Empty;
        [DataMember(Name = "paymentTerms")]
        public string PaymentTerms
        {
            get
            {
                if (!string.IsNullOrEmpty(paymentTerms))
                { return paymentTerms; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { paymentTerms = value; }
            }
        }
        
        string isClosed = string.Empty;
        [DataMember(Name = "isClosed")]
        public string IsClosed
        {
            get
            {
                if (!string.IsNullOrEmpty(isClosed))
                { return isClosed; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { isClosed = value; }
            }
        }

        string attachmentName = string.Empty;
        [DataMember(Name = "attachmentName")]
        public string AttachmentName
        {
            get
            {
                if (!string.IsNullOrEmpty(attachmentName))
                {
                    return attachmentName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    attachmentName = value;
                }
            }
        }

        [DataMember(Name = "auctionVendors")]
        public List<VendorDetails> AuctionVendors { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }

        //[DataMember(Name = "deliveryTime")]
        //public DateTime? DeliveryTime { get; set; }


        string deliveryTime = string.Empty;
        [DataMember(Name = "deliveryTime")]
        public string DeliveryTime
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryTime))
                {
                    return deliveryTime;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    deliveryTime = value;
                }
            }
        }


        [DataMember(Name = "inclusiveTax")]
        public bool InclusiveTax { get; set; }

        [DataMember(Name = "includeFreight")]
        public bool IncludeFreight { get; set; }

        [DataMember(Name = "minBidAmount")]
        public double MinBidAmount { get; set; }

        [DataMember(Name = "minBid")]
        public int MinBid { get; set; }


        [DataMember(Name = "superUserID")]
        public int SuperUserID { get; set; }

        [DataMember(Name = "selectedVendorID")]
        public int SelectedVendorID { get; set; }


        [DataMember(Name = "poLink")]
        public string POLink { get; set; }

        [DataMember(Name = "selectedVendor")]
        public VendorDetails SelectedVendor { get; set; }
        
        string customerCompanyName = string.Empty;
        [DataMember(Name = "customerCompanyName")]
        public string CustomerCompanyName
        {
            get
            {
                if (!string.IsNullOrEmpty(customerCompanyName))
                { return customerCompanyName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { customerCompanyName = value; }
            }
        }

        string subcategories = string.Empty;
        [DataMember(Name = "subcategories")]
        public string Subcategories
        {
            get
            {
                if (!string.IsNullOrEmpty(subcategories))
                { return subcategories; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { subcategories = value; }
            }
        }

        [DataMember(Name = "isStopped")]
        public bool IsStopped { get; set; }

        [DataMember(Name = "savings")]
        public Double Savings { get; set; }

        [DataMember(Name = "checkBoxSms")]
        public bool CheckBoxSms { get; set; }

        [DataMember(Name = "checkBoxEmail")]
        public bool CheckBoxEmail { get; set; }

        [DataMember(Name = "isNegotiationEnded")]
        public int IsNegotiationEnded { get; set; }

        [DataMember(Name = "minReduceAmount")]
        public int MinReduceAmount { get; set; }

        [DataMember(Name = "attachmentBase64")]
        public String AttachmentBase64 { get; set; }

        DateTime quotationFreezTime = DateTime.MaxValue;
        [DataMember(Name = "quotationFreezTime")]
        public DateTime? QuotationFreezTime
        {
            get
            {
                return quotationFreezTime;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    quotationFreezTime = value.Value;
                }
            }
        }

        string currency = "INR";
        [DataMember(Name = "currency")]
        public string Currency
        {
            get
            {
                if (!string.IsNullOrEmpty(currency))
                {
                    return currency;
                }
                else
                {
                    return "INR";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    currency = value;
                }
            }
        }

        [DataMember(Name = "timeZoneID")]
        public int TimeZoneID { get; set; }

        string timeZone = string.Empty;
        [DataMember(Name = "timeZone")]
        public string TimeZone
        {
            get
            {
                if (!string.IsNullOrEmpty(timeZone))
                {
                    return timeZone;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    timeZone = value;
                }
            }
        }


        [DataMember(Name = "listRequirementItems")]
        public List<RequirementItems> ListRequirementItems { get; set; }

        [DataMember(Name = "isTabular")]
        public bool IsTabular { get; set; }

        string reqComments = string.Empty;
        [DataMember(Name = "reqComments")]
        public string ReqComments
        {
            get
            {
                if (!string.IsNullOrEmpty(reqComments))
                {
                    return reqComments;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    reqComments = value;
                }
            }
        }

        [DataMember(Name = "isSubmit")]
        public int IsSubmit { get; set; }

        string negotiationDuration = string.Empty;
        [DataMember(Name = "negotiationDuration")]
        public string NegotiationDuration
        {
            get
            {
                if (!string.IsNullOrEmpty(negotiationDuration))
                {
                    return negotiationDuration;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    negotiationDuration = value;
                }
            }
        }

        [DataMember(Name = "minVendorComparision")]
        public int MinVendorComparision { get; set; }

        [DataMember(Name = "vendorsCount")]
        public int VendorsCount { get; set; }


        string stringCategory = string.Empty;
        [DataMember(Name = "stringCategory")]
        public string StringCategory
        {
            get
            {
                if (!string.IsNullOrEmpty(stringCategory))
                {
                    return stringCategory;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    stringCategory = value;
                }
            }
        }

        [DataMember(Name = "leastVendorPrice")]
        public double LeastVendorPrice { get; set; }

        [DataMember(Name = "intialLeastVendorPrice")]
        public double IntialLeastVendorPrice { get; set; }

        [DataMember(Name = "NegotiationSettings")]
        public NegotiationSettings NegotiationSettings { get; set; }

        [DataMember(Name = "itemsAttachment")]
        public byte[] ItemsAttachment { get; set; }

        [DataMember(Name = "listRequirementTaxes")]
        public List<RequirementTaxes> ListRequirementTaxes { get; set; }

        [DataMember(Name = "itemSNoCount")]
        public int ItemSNoCount { get; set; }

        [DataMember(Name = "taxSNoCount")]
        public int TaxSNoCount { get; set; }

        [DataMember(Name = "reqPDF")]
        public int ReqPDF { get; set; }

        [DataMember(Name = "reqPDFCustomer")]
        public int ReqPDFCustomer { get; set; }


        [DataMember(Name = "noOfVendorsInvited")]
        public int NoOfVendorsInvited { get; set; }

        [DataMember(Name = "noOfVendorsParticipated")]
        public int NoOfVendorsParticipated { get; set; }

        [DataMember(Name = "priceBeforeNegotiation")]
        public double PriceBeforeNegotiation { get; set; }

        [DataMember(Name = "priceAfterNegotiation")]
        public double priceAfterNegotiation { get; set; }

        [DataMember(Name = "reportReq")]
        public int ReportReq { get; set; }

        [DataMember(Name = "reportItemWise")]
        public int ReportItemWise { get; set; }

        [DataMember(Name = "reqType")]
        public string ReqType { get; set; }

        [DataMember(Name = "priceCapValue")]
        public double PriceCapValue { get; set; }

        [DataMember(Name = "customerReqAccess")]
        public bool CustomerReqAccess { get; set; }

        [DataMember(Name = "custCompID")]
        public int CustCompID { get; set; }

        [DataMember(Name = "isQuotationPriceLimit")]
        public bool IsQuotationPriceLimit { get; set; }

        [DataMember(Name = "quotationPriceLimit")]
        public int QuotationPriceLimit { get; set; }

        [DataMember(Name = "noOfQuotationReminders")]
        public int NoOfQuotationReminders { get; set; }

        [DataMember(Name = "remindersTimeInterval")]
        public int RemindersTimeInterval { get; set; }

        [DataMember(Name = "postedUser")]
        public User PostedUser { get; set; }

        [DataMember(Name = "superUser")]
        public User SuperUser { get; set; }

        [DataMember(Name = "isUnitPriceBidding")]
        public int IsUnitPriceBidding { get; set; }

        [DataMember(Name = "deleteQuotations")]
        public bool DeleteQuotations { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }

        [DataMember(Name = "attachment")]
        public byte[] Attachment { get; set; }
    }
}