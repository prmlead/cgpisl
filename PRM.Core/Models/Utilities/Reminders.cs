﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRM.Core.Models
{
    [DataContract]
    public class Reminders : Entity
    {        
        [DataMember(Name = "vendorIDs")]
        public string VendorIDs { get; set; }

        [DataMember(Name = "reminderMessage")]
        public string ReminderMessage { get; set; }

        [DataMember(Name = "sentOn")]
        public DateTime sentOn { get; set; }

        [DataMember(Name = "vendorCompanies")]
        public string VendorCompanies { get; set; }

        [DataMember(Name = "requestType")]
        public string RequestType { get; set; }
    }
}