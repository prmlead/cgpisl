﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class UserDetails : User
    {
        DateTime established = DateTime.Now;

        [DataMember(Name = "achievements")]
        public string Achievements { get; set; }

        [DataMember(Name = "assocWithOEM")]
        public bool AssocWithOEM { get; set; }

        [DataMember(Name = "assocWithOEMFile")]
        public byte[] AssocWithOEMFile { get; set; }

        [DataMember(Name = "assocWithOEMFileName")]
        public string AssocWithOEMFileName { get; set; }

        [DataMember(Name = "clients")]
        public string Clients { get; set; }

        [DataMember(Name = "establishedDate")]
        public DateTime EstablishedDate
        {
            get
            {
                return established;
            }
            set
            {
                established = value;
            }
        }

        [DataMember(Name = "products")]
        public string Products { get; set; }

        [DataMember(Name = "strengths")]
        public string Strengths { get; set; }

        [DataMember(Name = "responseTime")]
        public string ResponseTime { get; set; }

        [DataMember(Name = "files")]
        public List<FileUpload> Files { get; set; }

        [DataMember(Name = "aboutUs")]
        public string AboutUs { get; set; }

        [DataMember(Name = "logoURL")]
        public string LogoURL { get; set; }

        [DataMember(Name = "logoFile")]
        public FileUpload LogoFile { get; set; }

        [DataMember(Name = "oemCompanyName")]
        public string OemName { get; set; }

        [DataMember(Name = "oemKnownSince")]
        public string OemKnownSince { get; set; }

        [DataMember(Name = "registrationScore")]
        public int RegistrationScore { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "role")]
        public string Role { get; set; }

        [DataMember(Name = "category")]
        public string Category { get; set; }

        [DataMember(Name = "profileFile")]
        public byte[] ProfileFile { get; set; }

        [DataMember(Name = "profileFileName")]
        public string ProfileFileName { get; set; }

        [DataMember(Name = "profileFileUrl")]
        public string ProfileFileUrl { get; set; }

        [DataMember(Name = "directors")]
        public string Directors { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "workingHours")]
        public string WorkingHours { get; set; }

        [DataMember(Name = "rating")]
        public string Rating { get; set; }

        [DataMember(Name = "subcategories")]
        public List<CategoryObj> Subcategories { get; set; }

        [DataMember(Name = "creditsLeft")]
        public bool CreditsLeft { get; set; }

        [DataMember(Name = "credits")]
        public int Credits { get; set; }

        [DataMember(Name = "currency")]
        public string Currency { get; set; }

        [DataMember(Name = "timeZone")]
        public string TimeZone { get; set; }

        [DataMember(Name = "NegotiationSettings")]
        public NegotiationSettings NegotiationSettings { get; set; }

        [DataMember(Name = "isValid")]
        public bool IsValid { get; set; }

        [DataMember(Name = "companyId")]
        public int CompanyId { get; set; }

        [DataMember(Name = "username")]
        public string UserName { get; set; }

        [DataMember(Name = "price")]
        public double Price { get; set; }
    }
}