﻿
using System;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class POVendor : ResponseAudit
    {
        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "poID")]
        public int POID { get; set; }

        [DataMember(Name = "poQuantity")]
        public decimal POQuantity { get; set; }

        [DataMember(Name = "vendorPOQuantity")]
        public decimal VendorPOQuantity { get; set; }

        [DataMember(Name = "itemID")]
        public int ItemID { get; set; }

        string productIDorName = string.Empty;
        [DataMember(Name = "productIDorName")]
        public string ProductIDorName
        {
            get
            {
                if (!string.IsNullOrEmpty(productIDorName))
                { return productIDorName; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { productIDorName = value; }
            }
        }

        DateTime expectedDeliveryDate = DateTime.MaxValue;
        [DataMember(Name = "expectedDeliveryDate")]
        public DateTime? ExpectedDeliveryDate
        {
            get
            {
                return expectedDeliveryDate;
            }
            set
            {
                if (value != null && value.HasValue)
                {
                    expectedDeliveryDate = value.Value;
                }
            }
        }

        [DataMember(Name = "reqQuantity")]
        public decimal ReqQuantity { get; set; }
        
        [DataMember(Name = "price")]
        public decimal Price { get; set; }
        
        string comments = string.Empty;
        [DataMember(Name = "comments")]
        public string Comments
        {
            get
            {
                if (!string.IsNullOrEmpty(comments))
                { return comments; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { comments = value; }
            }
        }
        
        string status = string.Empty;
        [DataMember(Name = "status")]
        public string Status
        {
            get
            {
                if (!string.IsNullOrEmpty(status))
                { return status; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { status = value; }
            }
        }
        
        string purchaseID = string.Empty;
        [DataMember(Name = "purchaseID")]
        public string PurchaseID
        {
            get
            {
                if (!string.IsNullOrEmpty(purchaseID))
                { return purchaseID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { purchaseID = value; }
            }
        }
        
        string poLink = string.Empty;
        [DataMember(Name = "poLink")]
        public string POLink
        {
            get
            {
                if (!string.IsNullOrEmpty(poLink))
                { return poLink; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { poLink = value; }
            }
        }

        [DataMember(Name = "poStream")]
        public byte[] POStream { get; set; }
        
        string deliveryAddress = string.Empty;
        [DataMember(Name = "deliveryAddress")]
        public string DeliveryAddress
        {
            get
            {
                if (!string.IsNullOrEmpty(deliveryAddress))
                { return deliveryAddress; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { deliveryAddress = value; }
            }
        }

        [DataMember(Name = "vendor")]
        public UserDetails Vendor { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }
        
        string indentID = string.Empty;
        [DataMember(Name = "indentID")]
        public string IndentID
        {
            get
            {
                if (!string.IsNullOrEmpty(indentID))
                { return indentID; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { indentID = value; }
            }
        }

        string title = string.Empty;
        [DataMember(Name = "title")]
        public string Title
        {
            get
            {
                if (!string.IsNullOrEmpty(title))
                { return title; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { title = value; }
            }
        }

        [DataMember(Name = "poFile")]
        public FileUpload POFile { get; set; }

    }
}