﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class ReportUser : Entity
    {
        [DataMember(Name="userID")]
        public int UserID { get; set; }

        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        [DataMember(Name = "totalRequirements")]
        public int TotalRequirements { get; set; }

        [DataMember(Name = "aboutUs")]
        public string AboutUs { get; set; }

        [DataMember(Name = "companyName")]
        public string CompanyName { get; set; }

        [DataMember(Name = "phoneNum")]
        public string PhoneNum { get; set; }
    }
}