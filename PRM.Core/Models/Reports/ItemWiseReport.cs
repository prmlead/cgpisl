﻿using System.Runtime.Serialization;

namespace PRM.Core.Models
{
    [DataContract]
    public class ItemWiseReport : ResponseAudit
    {
        [DataMember(Name = "leastItemVendor")]
        public User LeastItemVendor { get; set; }

        [DataMember(Name = "leastItemVendorCompany")]
        public Company LeastItemVendorCompany { get; set; }

        [DataMember(Name = "leastVendor")]
        public User LeastVendor { get; set; }

        [DataMember(Name = "leastVendorCompany")]
        public Company LeastVendorCompany { get; set; }

        [DataMember(Name = "leastBidderPrice")]
        public decimal LeastBidderPrice { get; set; }

        [DataMember(Name = "unitPrice")]
        public decimal UnitPrice { get; set; }

        [DataMember(Name = "leastBidderUnitPrice")]
        public decimal LeastBidderUnitPrice { get; set; }

        [DataMember(Name = "itemDetails")]
        public RequirementItems ItemDetails { get; set; }
    }
}