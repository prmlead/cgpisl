﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PRM.Core.Models.Reports
{
    [DataContract]
    public class OpenPR : ResponseAudit
    {
        [DataMember]
        [DataNames("COMP_ID")]
        public string COMP_ID { get; set; }

        [DataMember]
        [DataNames("PLANT")]
        public string PLANT { get; set; }

        [DataMember]
        [DataNames("PURCHASING_GROUP")]
        public string PURCHASING_GROUP { get; set; }

        [DataMember]
        [DataNames("PURCHASE_REQUISITION")]
        public string PURCHASE_REQUISITION { get; set; }

        [DataMember]
        [DataNames("ITEM_OF_REQUISITION")]
        public string ITEM_OF_REQUISITION { get; set; }

        [DataMember]
        [DataNames("REQUISITION_DATE")]
        public string REQUISITION_DATE { get; set; }

        [DataMember]
        [DataNames("MATERIAL")]
        public string MATERIAL { get; set; }

        [DataMember]
        [DataNames("SHORT_TEXT")]
        public string SHORT_TEXT { get; set; }

        [DataMember]
        [DataNames("UNIT_OF_MEASURE")]
        public string UNIT_OF_MEASURE { get; set; }

        [DataMember]
        [DataNames("QUANTITY_REQUESTED")]
        public string QUANTITY_REQUESTED { get; set; }

        [DataMember]
        [DataNames("TOTAL_VALUE")]
        public string TOTAL_VALUE { get; set; }

        [DataMember]
        [DataNames("DELIV_DATE_FROM_TO")]
        public string DELIV_DATE_FROM_TO { get; set; }

        [DataMember]
        [DataNames("VENDOR")]
        public string VENDOR { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER_DATE")]
        public string PURCHASE_ORDER_DATE { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER")]
        public string PURCHASE_ORDER { get; set; }

        [DataMember]
        [DataNames("DELIVERY_DATE")]
        public string DELIVERY_DATE { get; set; }

        [DataMember]
        [DataNames("RELEASE_DATE")]
        public string RELEASE_DATE { get; set; }

        [DataMember]
        [DataNames("STATUSCODE")]
        public string STATUSCODE { get; set; }

        [DataMember]
        [DataNames("DELETION_INDICATOR")]
        public string DELETION_INDICATOR { get; set; }

        [DataMember]
        [DataNames("PURCHASE_ORDER_ITEM")]
        public string PURCHASE_ORDER_ITEM { get; set; }

        [DataMember]
        [DataNames("SUPPLIER")]
        public string SUPPLIER { get; set; }

        [DataMember]
        [DataNames("BLOCKING_INDICATOR")]
        public string BLOCKING_INDICATOR { get; set; }

        [DataMember]
        [DataNames("CLOSED")]
        public string CLOSED { get; set; }

        [DataMember]
        [DataNames("QUANTITY_ORDERED")]
        public string QUANTITY_ORDERED { get; set; }

        [DataMember]
        [DataNames("GOODS_RECEIPT")]
        public string GOODS_RECEIPT { get; set; }

        [DataMember]
        [DataNames("PLANT_NAME")]
        public string PLANT_NAME { get; set; }

        [DataMember]
        [DataNames("PURCHASER")]
        public string PURCHASER { get; set; }

        [DataMember]
        [DataNames("API_NAME")]
        public string API_NAME { get; set; }

        [DataMember]
        [DataNames("LEAD_TIME")]
        public string LEAD_TIME { get; set; }

        [DataMember]
        [DataNames("FILTER_STRING")]
        public string FILTER_STRING { get; set; }


        //Comments
        [DataMember]
        [DataNames("C_QUANTITY")]
        public decimal C_QUANTITY { get; set; }

        [DataMember]
        [DataNames("NEW_DELIVERY_DATE")]
        public DateTime? NEW_DELIVERY_DATE { get; set; }

        [DataMember]
        [DataNames("COMMENTS")]
        public string COMMENTS { get; set; }

        [DataMember]
        [DataNames("COMMENT_CREATED")]
        public DateTime? COMMENT_CREATED { get; set; }

        [DataMember]
        [DataNames("CATEGORY")]
        public string CATEGORY { get; set; }


        [DataMember]
        [DataNames("NWSTATUS")]
        public string NWSTATUS { get; set; }


        [DataMember]
        [DataNames("NEW_DELIVERY_DATE_COMM")]
        public string NEW_DELIVERY_DATE_COMM { get; set; }
    }


    [DataContract]
    public class OpenPO : ResponseAudit
    {
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }
        [DataMember] [DataNames("PURCHASING_GROUP")] public string PURCHASING_GROUP { get; set; }
        [DataMember] [DataNames("PURCHASER")] public string PURCHASER { get; set; }
        [DataMember] [DataNames("PURCHASING_DOCUMENT")] public string PURCHASING_DOCUMENT { get; set; }
        [DataMember] [DataNames("ITEM")] public string ITEM { get; set; }
        [DataMember] [DataNames("CONCAT")] public string CONCAT { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("PO_MONTH")] public string PO_MONTH { get; set; }
        [DataMember] [DataNames("NAME_OF_SUPPLIER")] public string NAME_OF_SUPPLIER { get; set; }
        [DataMember] [DataNames("MATERIAL")] public string MATERIAL { get; set; }
        [DataMember] [DataNames("SHORT_TEXT")] public string SHORT_TEXT { get; set; }
        [DataMember] [DataNames("API_NAME")] public string API_NAME { get; set; }
        [DataMember] [DataNames("ORDER_UNIT")] public string ORDER_UNIT { get; set; }
        [DataMember] [DataNames("ORDER_QUANTITY")] public string ORDER_QUANTITY { get; set; }
        [DataMember] [DataNames("STILL_TO_BE_DELIVERED_QTY")] public string STILL_TO_BE_DELIVERED_QTY { get; set; }
        [DataMember] [DataNames("USER_ID")] public string USER_ID { get; set; }
        [DataMember] [DataNames("MULTIPLE_PR_TEXT")] public string MULTIPLE_PR_TEXT { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_ITM_NO")] public string PR_ITM_NO { get; set; }
        [DataMember] [DataNames("PR_CREATE_DATE")] public DateTime? PR_CREATE_DATE { get; set; }
        [DataMember] [DataNames("PO_DELV_DATE")] public DateTime? PO_DELV_DATE { get; set; }
        [DataMember] [DataNames("IMPORT_LOCAL")] public string IMPORT_LOCAL { get; set; }
        [DataMember] [DataNames("LEAD_TIME")] public string LEAD_TIME { get; set; }
        [DataMember] [DataNames("IDEAL_DELIVERY_DATE")] public DateTime? IDEAL_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_MONTH")] public string DELIVERY_MONTH { get; set; }
        [DataMember] [DataNames("PR_DELV_DATE")] public DateTime? PR_DELV_DATE { get; set; }

        [DataMember] public string STATUS { get; set; }
        [DataMember] public string DAYS_TO_DELIVERY { get; set; }
        [DataMember] public string DELIVERY_RANGE { get; set; }
        [DataMember] public string IDEAL_DELIVERY_MONTH { get; set; }

        //Comments
        [DataMember]
        [DataNames("C_QUANTITY")]
        public decimal C_QUANTITY { get; set; }

        [DataMember]
        [DataNames("NEW_DELIVERY_DATE")]
        public DateTime? NEW_DELIVERY_DATE { get; set; }

        [DataMember]
        [DataNames("COMMENTS")]
        public string COMMENTS { get; set; }

        [DataMember]
        [DataNames("COMMENT_CREATED")]
        public DateTime? COMMENT_CREATED { get; set; }

        [DataMember]
        [DataNames("CATEGORY")]
        public string CATEGORY { get; set; }

        [DataMember]
        [DataNames("NWSTATUS")]
        public string NWSTATUS { get; set; }

        [DataMember]
        [DataNames("PR_QUAN")]
        public decimal PR_QUAN { get; set; }

        [DataMember]
        [DataNames("NEW_DELIVERY_DATE_COMM")]
        public string NEW_DELIVERY_DATE_COMM { get; set; }

        [DataMember]
        [DataNames("DELIVERED_LIST")]
        public List<OpenPO> DELIVERED_LIST { get; set; }
    }

    [DataContract]
    public class OpenPRStg : ResponseAudit
    {

        [DataMember]
        [DataNames("WERKS")]
        public string WERKS { get; set; }

        [DataMember]
        [DataNames("EKGRP")]
        public string EKGRP { get; set; }

        [DataMember]
        [DataNames("BANFN")]
        public string BANFN { get; set; }

        [DataMember]
        [DataNames("BNFPO")]
        public int BNFPO { get; set; }

        [DataMember]
        [DataNames("BADAT")]
        public string BADAT { get; set; }

        [DataMember]
        [DataNames("MATNR")]
        public string MATNR { get; set; }

        [DataMember]
        [DataNames("TXZ01")]
        public string TXZ01 { get; set; }

        [DataMember]
        [DataNames("MEINS")]
        public string MEINS { get; set; }

        [DataMember]
        [DataNames("MENGE")]
        public string MENGE { get; set; }

        [DataMember]
        [DataNames("LV_CAL")]
        public string LV_CAL { get; set; }

        [DataMember]
        [DataNames("LFDAT")]
        public string LFDAT { get; set; }

        [DataMember]
        [DataNames("BLCKT")]
        public string BLCKT { get; set; }

        [DataMember]
        [DataNames("BEDAT")]
        public string BEDAT { get; set; }

        [DataMember]
        [DataNames("EBELN")]
        public string EBELN { get; set; }

        [DataMember]
        [DataNames("LFDAT1")]
        public string LFDAT1 { get; set; }

        [DataMember]
        [DataNames("FRGDT")]
        public string FRGDT { get; set; }

        [DataMember]
        [DataNames("STATU")]
        public string STATU { get; set; }

        [DataMember]
        [DataNames("LOEKZ")]
        public string LOEKZ { get; set; }

        [DataMember]
        [DataNames("EBELP")]
        public int EBELP { get; set; }

        [DataMember]
        [DataNames("LIFNR1")]
        public string LIFNR1 { get; set; }

        [DataMember]
        [DataNames("BLCKD")]
        public string BLCKD { get; set; }

        [DataMember]
        [DataNames("EBAKZ")]
        public string EBAKZ { get; set; }

        [DataMember]
        [DataNames("BSMNG")]
        public double BSMNG { get; set; }

        [DataMember]
        [DataNames("WEPOS")]
        public string WEPOS { get; set; }


    }

    public class OpenPOStg : ResponseAudit
    {
        [DataMember]
        [DataNames("LOEKZ")]
        public string LOEKZ { get; set; }

        [DataMember]
        [DataNames("BSART")]
        public string BSART { get; set; }

        [DataMember]
        [DataNames("WERKS")]
        public string WERKS { get; set; }

        [DataMember]
        [DataNames("FRGKE")]
        public string FRGKE { get; set; }

        [DataMember]
        [DataNames("EKORG")]
        public string EKORG { get; set; }

        [DataMember]
        [DataNames("EKGRP")]
        public string EKGRP { get; set; }

        [DataMember]
        [DataNames("EBELN")]
        public string EBELN { get; set; }

        [DataMember]
        [DataNames("BEDAT")]
        public string BEDAT { get; set; }

        [DataMember]
        [DataNames("LIFNR")]
        public string LIFNR { get; set; }

        [DataMember]
        [DataNames("MATNR")]
        public string MATNR { get; set; }

        [DataMember]
        [DataNames("EBELP")]
        public int EBELP { get; set; }

        [DataMember]
        [DataNames("TXZ01")]
        public string TXZ01 { get; set; }

        [DataMember]
        [DataNames("MENGE")]
        public string MENGE { get; set; }

        [DataMember]
        [DataNames("MEINS")]
        public string MEINS { get; set; }

        [DataMember]
        [DataNames("R_MENGE")]
        public string R_MENGE { get; set; }

        [DataMember]
        [DataNames("NETPR")]
        public double NETPR { get; set; }

        [DataMember]
        [DataNames("NETWR")]
        public string NETWR { get; set; }

        [DataMember]
        [DataNames("WAERS")]
        public string WAERS { get; set; }

        [DataMember]
        [DataNames("PEINH")]
        public string PEINH { get; set; }

        [DataMember]
        [DataNames("LIFNR1")]
        public string LIFNR1 { get; set; }

        [DataMember]
        [DataNames("BSTYP")]
        public string BSTYP { get; set; }

    }

    public class PRGRNStg : ResponseAudit
    {
        [DataMember]
        [DataNames("WERKS")]
        public string WERKS { get; set; }

        [DataMember]
        [DataNames("EKORG")]
        public string EKORG { get; set; }

        [DataMember]
        [DataNames("EKGRP")]
        public string EKGRP { get; set; }

        [DataMember]
        [DataNames("BSART")]
        public string BSART { get; set; }

        [DataMember]
        [DataNames("LIFNR")]
        public string LIFNR { get; set; }

        [DataMember]
        [DataNames("LNAME")]
        public string LNAME { get; set; }

        [DataMember]
        [DataNames("LANDX")]
        public string LANDX { get; set; }

        [DataMember]
        [DataNames("BEDAT")]
        public string BEDAT { get; set; }

        [DataMember]
        [DataNames("ERNAM")]
        public string ERNAM { get; set; }

        [DataMember]
        [DataNames("EBELN")]
        public string EBELN { get; set; }

        [DataMember]
        [DataNames("EBELP")]
        public int EBELP { get; set; }

        [DataMember]
        [DataNames("LFDAT")]
        public string LFDAT { get; set; }

        [DataMember]
        [DataNames("MATNR")]
        public string MATNR { get; set; }

        [DataMember]
        [DataNames("BISMT")]
        public string BISMT { get; set; }

        [DataMember]
        [DataNames("MAKTX")]
        public string MAKTX { get; set; }

        [DataMember]
        [DataNames("MEINS")]
        public string MEINS { get; set; }

        [DataMember]
        [DataNames("BWART")]
        public string BWART { get; set; }

        [DataMember]
        [DataNames("MENGE")]
        public string MENGE { get; set; }

        [DataMember]
        [DataNames("BELNR")]
        public string BELNR { get; set; }

        [DataMember]
        [DataNames("BUZEI")]
        public int BUZEI { get; set; }

        [DataMember]
        [DataNames("BUDAT")]
        public string BUDAT { get; set; }

        [DataMember]
        [DataNames("BLDAT")]
        public string BLDAT { get; set; }

        [DataMember]
        [DataNames("MENGE_G")]
        public double MENGE_G { get; set; }

        [DataMember]
        [DataNames("DMBTR")]
        public double DMBTR { get; set; }

        [DataMember]
        [DataNames("NETPR")]
        public double NETPR { get; set; }

        [DataMember]
        [DataNames("WRBTR")]
        public double WRBTR { get; set; }

        [DataMember]
        [DataNames("BANFN")]
        public string BANFN { get; set; }

        [DataMember]
        [DataNames("BNFPO")]
        public int BNFPO { get; set; }

        [DataMember]
        [DataNames("BADAT")]
        public string BADAT { get; set; }

        [DataMember]
        [DataNames("LFDAT1")]
        public string LFDAT1 { get; set; }

        [DataMember]
        [DataNames("FRGDT")]
        public string FRGDT { get; set; }

        [DataMember]
        [DataNames("TRX01_P")]
        public string TRX01_P { get; set; }

        [DataMember]
        [DataNames("MENGE1")]
        public string MENGE1 { get; set; }

        [DataMember]
        [DataNames("MEINS1")]
        public string MEINS1 { get; set; }

    }

}
