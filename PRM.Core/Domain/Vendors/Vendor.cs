﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Vendors
{
    [Table("vendors")]
   public class Vendor
    {
        [Key]
        [Dapper.Column("V_ID")]
        public int Id { get; set; }

        [Dapper.Column("U_ID")]
        [Dapper.IgnoreUpdate]

        public int User_Id { get; set; }

        [Dapper.Column("COMP_ID")]
        [Dapper.IgnoreUpdate]
        public int Company_Id { get; set; }

        [Dapper.Column("U_FNAME")]
        public string FirstName { get; set; }

        [Dapper.Column("U_LNAME")]
        public string LastName { get; set; }

        [Dapper.Column("U_EMAIL")]
        public string Email { get; set; }

        [Dapper.Column("U_PHONE")]
        public string PhoneNumber { get; set; }


        [Dapper.Column("COMP_NAME")]
        public string CompanyName { get; set; }

        [Dapper.Column("DESIGNATION")]
        public string Designation { get; set; }

        [Dapper.Column("DATE_CREATED")]
        [Dapper.IgnoreUpdate]
        public DateTime? CreatedOnUtc { get; set; }

        [Dapper.IgnoreUpdate]
        [Dapper.Column("CREATED_BY")]
        public int CreatedBy { get; set; }

        [Dapper.Column("MODIFIED_BY")]
        public int UpdatedBy { get; set; }

        [Dapper.Column("DATE_MODIFIED")]
        public DateTime? UpdatedOnUtc { get; set; }

        [Dapper.Column("IS_PRIMARY")]
        public bool IsPrimary { get; set; }

        [Dapper.Column("IS_VALID")]
        public bool IsValid { get; set; }

        //[Dapper.Column("IS_DELETED")]

        //public bool Deleted { get; set; }

        [Dapper.Column("REG_STATUS")]
        public int Status { get; set; }


        [Dapper.Column("REG_REASON")]
        public string Reason { get; set; }

        [Dapper.Column("REG_COMMENTS")]
        public string Comment { get; set; }

        [Dapper.Column("REG_SCORE")]
        public int Score { get; set; }

        [Dapper.Column("ADDITIONAL_INFO")]
        public string AdditionalInfo { get; set; }

        [Dapper.NotMapped]
        public virtual Dictionary<string,object> AdditionalData { get; set; }

    }
}
