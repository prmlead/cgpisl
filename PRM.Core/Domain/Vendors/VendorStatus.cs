﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Core.Domain.Vendors
{
   public enum VendorStatus
    {
        Approved = 1,
        Declined = 2,
        OnHold = 3,
        Pending = 4,
        InActive = 5
    }
}
