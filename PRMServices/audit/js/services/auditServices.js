prmApp.constant('PRMAuditServiceDomain', 'audit/svc/PRMAuditService.svc/REST/');
prmApp
    .service('auditreportingServices', ["PRMAuditServiceDomain", "userService", "httpServices", "$window", function (PRMAuditServiceDomain, userService, httpServices, $window) {
        //var domain = 'http://182.18.169.32/services/';
        var auditreportingService = this;

        auditreportingService.getrequirementaudit = function (reqID) {
            let url = PRMAuditServiceDomain + 'getrequirementaudit?reqid=' + reqID + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };

        return auditreportingService;
    }]);