angular.module('loginModule').constant('domain','/PRMService.svc/REST/');
angular.module('loginModule').constant('version','v1');
angular.module('loginModule').constant('signalR', '~');
angular.module('loginModule').constant('signalRHubName', 'requirementHub');

angular.module('loginModule')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');
    $stateProvider
        .state('resetpassword', {
            url: '/resetpassword/:email/:sessionid',
            templateUrl: '/partials/resetpassword.html',
            controller: 'loginCtrl'
        }).state('login', {
            url: '/login',
            templateUrl: '/partials/login.html',
            controller: 'loginCtrl'
        }).state('register', {
            url: '/register',
            templateUrl: '/partials/register.html',
            controller: 'registerCtrl'
        }).state('forgotpassword', {
            url: '/forgotpassword',
            templateUrl: '/partials/forgotpassword.html',
            controller: 'forgotpasswordCtrl'
        });
        // .state('verify-otp', {
        //     url: '/verify-otp',
        //     templateUrl: '/partials/verify-otp.html',
        //     controller: 'verifyOTPCtrl'
        // });
}]);