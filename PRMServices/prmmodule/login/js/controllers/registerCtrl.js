/*global angular */

/**
 * The main controller for the app. The controller:
 * - retrieves and persists the model via the todoStorage service
 * - exposes the model to the template and provides event handlers
 */
angular = require('angular');

angular.module('loginModule')
    .controller('registerCtrl', ["$state", "$scope", "growlService", "$http", "domain",
        "fileReader", "ngDialog", "$filter", "loginService", function RegisterCtrl($state, $scope, growlService, $http, domain, fileReader, ngDialog, $filter, loginService) {
            'use strict';
            $scope.login = 0;
            $scope.register = 1;
            $scope.otp = 0;
            $scope.otpvalue = 0;
            $scope.verification = 0;
            $scope.verificationObj = {};
            $scope.otpobj = {};
            $scope.register_vendor = 0;
            $scope.forgot = 0;
            $scope.forgotpassword = {};
            $scope.loggedIn = false;
            $scope.userError = {};
            $scope.user = {};
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;
            $scope.vendorregisterobj = $scope.registerobj = {};
            $scope.otpvalueValidation = false;
            $scope.otpvalueValidationEmpty = false;
            $scope.otpvalueValidationError = false;
            $scope.categories = [];
            $scope.subcategories = [];
            $scope.categoriesdata = [];
            $scope.selectedSubcategories = [];
            $scope.selectedCurrency = {};
            $scope.selectedTimezone = {};
            $scope.currencies = [];
            $scope.timezones = [];


            $scope.register = 1;
            $scope.registerobj = {};
            $scope.otp = $scope.forgot = $scope.register_vendor = $scope.login = 0;
            $scope.register_vendor = 1;
            $scope.checkEmailUniqueResult = false;
            $scope.checkPhoneUniqueResult = false;
            $scope.checkCompanyUniqueResult = false;
            $scope.checkPANUniqueResult = false;
            $scope.checkTINUniqueResult = false;
            $scope.checkSTNUniqueResult = false;

            $scope.isMobile = (typeof window.orientation !== 'undefined' ? true : false);

            $scope.EmailValidateVendor = function () {
                var re = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
                var result = re.test($scope.vendorregisterobj.email);
                if (!result) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Email Address");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
            }

            $scope.getCategories = function () {
                loginService.getCategories()
                    .then(function (response) {
                        $scope.categories = _.uniq(_.map(response, 'category'));
                        //$scope.subcategories = response;
                        $scope.categoriesdata = response;

                    })
            }



            $scope.getKeyValuePairs = function (parameter) {

                loginService.getKeyValuePairs(parameter)
                    .then(function (response) {
                        if (parameter == "CURRENCY") {
                            $scope.currencies = response;

                        } else if (parameter == "TIMEZONES") {
                            $scope.timezones = response;

                        }

                        $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                        $scope.selectedCurrency = $scope.selectedCurrency[0];
                        $scope.selectedTimezone = $filter('filter')($scope.timezones, { value: response.timeZone });
                        $scope.selectedTimezone = $scope.selectedTimezone[0];
                    })

            }

            

            $scope.loadSubCategories = function () {
                $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.vendorregisterobj.category });
                /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
            }

            $scope.getCategories();

            $scope.vendorregisterobj.panno = '';
            $scope.vendorregisterobj.vatno = '';
            $scope.vendorregisterobj.taxno = '';

            $scope.sub = {
                selectedSubcategories: []
            }

            $scope.vendorregisterobj.panno = '';
            $scope.vendorregisterobj.vatno = '';
            $scope.vendorregisterobj.taxno = '';

            $scope.RegisterVendor1 = function () {
                if (isNaN($scope.vendorregisterobj.phoneNum)) {
                    $scope.showMessage = true;
                    $scope.msg = $scope.getErrMsg("Please enter a valid Phone Number");
                } else {
                    $scope.showMessage = false;
                    $scope.msg = '';
                }
                if (!$scope.showMessage) {
                    $scope.vendorregisterobj.username = $scope.vendorregisterobj.email;
                    $scope.vendorregisterobj.currency = $scope.vendorregisterobj.currency.key;
                    $scope.vendorregisterobj.timeZone = $scope.vendorregisterobj.timeZone.key;
                    $scope.vendorregisterobj.subcategories = _.map($scope.sub.selectedSubcategories, 'id');
                    if (!$scope.vendorregisterobj.subcategories) {
                        $scope.vendorregisterobj.subcategories = [];
                    }
                    if ($scope.vendorregisterobj.vatno == null) {
                        $scope.vendorregisterobj.vatno = '';
                    }
                    loginService.vendorregistration1($scope.vendorregisterobj).then(function (error) {
                        $scope.loggedIn = loginService.isLoggedIn();
                        if (error.errorMessage != "") {
                            $scope.showMessage = true;
                            $scope.msg = $scope.getErrMsg(error);
                        } else {
                            $scope.otp = 1;
                            $scope.forgot = $scope.login = $scope.register_vendor = $scope.register = 0;
                            $state.go('profile-home');
                        }
                    });
                }
            }



            $scope.checkUserUniqueResult = function (idtype, inputvalue) {
                if (inputvalue == "" || inputvalue == undefined) {
                    return false;
                }
                $scope.checkPhoneUniqueResult = false;
                $scope.checkEmailUniqueResult = false;
                $scope.checkCompanyUniqueResult = false;
                $scope.checkPANUniqueResult = false;
                $scope.checkTINUniqueResult = false;
                $scope.checkSTNUniqueResult = false;
                loginService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                    if (idtype == "PHONE") {
                        $scope.checkPhoneUniqueResult = !response;
                    } else if (idtype == "EMAIL") {
                        $scope.checkEmailUniqueResult = !response;
                    } else if (idtype == "COMPANY") {
                        $scope.checkCompanyUniqueResult = !response;
                    }
                    else if (idtype == "PAN") {
                        $scope.checkPANUniqueResult = !response;
                    }
                    else if (idtype == "TIN") {
                        $scope.checkTINUniqueResult = !response;
                    }
                    else if (idtype == "STN") {
                        $scope.checkSTNUniqueResult = !response;
                    }
                });
            };

            $scope.forgotpasswordfunction = function () {
                loginService.forgotpassword($scope.forgotpassword)
                    .then(function (response) {
                        if (response.data.errorMessage == "") {
                            $scope.forgotpassword = {};
                            swal("Done!", "Password reset link sent to your registerd Email and Mobile Number.", "success");
                            $scope.login = 1;
                            $scope.user = {};
                            $scope.forgot = $scope.register_vendor = $scope.register = 0;
                        } else {
                            swal("Warning", "Please check the Email/Phone you have entered.", "warning");
                        }
                    });
            };



            $scope.clickToOpen = function () {
                ngDialog.open({ template: 'login/termsConditions.html', width: 1000, height: 500 });
            };


            $scope.closeMsg = function () {
                $scope.showMessage = false;
            }

            $scope.getKeyValuePairs('CURRENCY');
            $scope.getKeyValuePairs('TIMEZONES');
        }]);
