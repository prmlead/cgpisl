angular = require('angular');
jQuery = require('jquery');
moment = require('moment');
require('bootstrap');
require('angular-ui-bootstrap');
store = require('angular-storage');
require('angular-ui-router');
require('ng-dialog');
require('loginmodule');
require('commonmodule');
require('profilemodule');
require('../dist/templateCachePartials');

var app = angular.module('storeModule', ['ui.router', 'storePartials', 'angular-storage', 'ngDialog', 'loginModule', 'commonModule', 'profileModule']);

require('./config/index.js');
require('./services/index.js');
require('./directives/index.js');
require('./controllers/index.js');