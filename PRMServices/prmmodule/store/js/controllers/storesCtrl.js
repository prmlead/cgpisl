﻿angular = require('angular');

angular.module('storeModule')
    .controller('storesCtrl', ['$scope', '$state', '$stateParams', 'loginService', 'storeService', 'growlService', function ($scope, $state, $stateParams, loginService, storeService, growlService) {
        $scope.compID = loginService.getUserCompanyId();

        $scope.storeID = 0;
        $scope.companyStores = [];

        $scope.sessionID = loginService.getUserToken();

        $scope.getCompanyStores = function () {
            storeService.getcompanystores($scope.compID)
               .then(function (response) {
                   $scope.companyStores = response;
               })
        }

        $scope.getCompanyStores();

        $scope.goToStoreEdit = function (store, companyStores) {
            $state.go("addnewstore", { "storeID": store.storeID, "storeObj": store, "companyStores": companyStores });
        }


        $scope.deletestore = function (storeId) {

            var params = {
                "storeId": storeId,
                sessionID: $scope.sessionID
            };

            storeService.deletestore(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Store Deleted Successfully.", "success");
                       location.reload();
                   }
               })
        }


    }]);  