﻿angular = require('angular');

angular.module('storeModule')
    .controller('addnewstoreCtrl', ['$scope', '$stateParams', '$state', '$window', 'loginService', 'storeService', 'growlService', function ($scope, $stateParams, $state, $window, loginService, storeService, growlService) {
        $scope.storeID = $stateParams.storeID == "" ? 0 : $stateParams.storeID;

        $scope.companyID = loginService.getUserCompanyId();
        $scope.sessionID = loginService.getUserToken();

        $scope.storeObj = {
            storeID: $scope.storeID,
            companyID: $scope.companyID,
            //isMainBranch: 0,
            mainStoreID: 0,
            //storeCode: '',
            //storeDescription: '',
            //storeInCharge: '',
            //storeDetails: '',
            //isValid: 0,
            //storeAddress: '',
            //storeBranches: 0,
            sessionID: $scope.sessionID
        };

        $scope.companyStores = $stateParams.companyStores;

        if ($scope.storeID > 0) {            
            if (!$stateParams.storeObj) {
                storeService.getstores($scope.storeID, $scope.companyID)
                    .then(function (response) {
                        $scope.storeObj = response[0];
                    });
            }
            else {
                $scope.storeObj = $stateParams.storeObj;
            }            
        }

        


        $scope.saveStore = function () {

            $scope.storeObj.isValid = 1;

            var params = {
                "store": $scope.storeObj
            };

            storeService.savestore(params)
               .then(function (response) {
                   if (response.errorMessage != '') {
                       growlService.growl(response.errorMessage, "inverse");
                   }
                   else {
                       growlService.growl("Store Saved Successfully.", "success");
                       $window.history.back();
                   }
               })
        }

}]);