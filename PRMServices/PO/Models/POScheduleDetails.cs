﻿using PRM.Core.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class POScheduleDetails : ResponseAudit
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_RECEIPT_DATE")] public DateTime? PO_RECEIPT_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_COMPANY")] public string VENDOR_COMPANY { get; set; }
        [DataMember] [DataNames("PO_STATUS")] public string PO_STATUS { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("PO_CLOSED_DATE")] public DateTime? PO_CLOSED_DATE { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("PLANT_NAME")] public string PLANT_NAME { get; set; }
        [DataMember] [DataNames("PLANT_LOCATION")] public string PLANT_LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("GST_NUMBER")] public string GST_NUMBER { get; set; }
        [DataMember] [DataNames("GST_ADDR")] public string GST_ADDR { get; set; }

        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS")] public string PAYMENT_TERMS { get; set; }
        [DataMember] [DataNames("PAYMENT_TERMS_DESC")] public string PAYMENT_TERMS_DESC { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_EMAIL")] public string VENDOR_PRIMARY_EMAIL { get; set; }
        [DataMember] [DataNames("VENDOR_PRIMARY_PHONE_NUMBER")] public string VENDOR_PRIMARY_PHONE_NUMBER { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("PO_RELEASE_DATE")] public DateTime? PO_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("PO_DATE")] public DateTime? PO_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE_STRING")] public string VENDOR_EXPECTED_DELIVERY_DATE_STRING { get; set; }
        [DataMember] [DataNames("STATS_TOTAL_COUNT")] public int STATS_TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("STATS_PO_AWAITING_RECEIPT")] public int STATS_PO_AWAITING_RECEIPT { get; set; }
        [DataMember] [DataNames("IS_PO_ACK")] public int IS_PO_ACK { get; set; }
        [DataMember] [DataNames("STATS_PO_NOT_INITIATED")] public int STATS_PO_NOT_INITIATED { get; set; }
        [DataMember] [DataNames("STATS_PO_PARTIAL_DELIVERY")] public int STATS_PO_PARTIAL_DELIVERY { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("ASN_CODE")] public string ASN_CODE { get; set; }
        [DataMember] [DataNames("ASN_ID")] public int ASN_ID { get; set; }

        [DataMember] [DataNames("GRN_STATUS")] public string GRN_STATUS { get; set; }
        [DataMember] [DataNames("HEADER_TEXT")] public string HEADER_TEXT { get; set; }

        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember(Name = "vendorAttachmentsArray")]
        public List<FileUpload> VendorAttachmentsArray { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_ATTACHEMNTS")] public string VENDOR_ATTACHEMNTS { get; set; }
        [DataMember] [DataNames("INCO_TERMS")] public string INCO_TERMS { get; set; }
        [DataMember] [DataNames("ADDRESS")] public string ADDRESS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }

        [DataMember] [DataNames("SessionID")] public string SessionID { get; set; }

        [DataMember] [DataNames("EMAIL_SENT_DATE")] public DateTime? EMAIL_SENT_DATE { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }
    }

    public class POScheduleDetailsItems : ResponseAudit
    {

        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PRODUCT_CODE")] public string PRODUCT_CODE { get; set; }
        [DataMember] [DataNames("PRODUCT_NAME")] public string PRODUCT_NAME { get; set; }
        [DataMember] [DataNames("PO_MATERIAL_DESC")] public string PO_MATERIAL_DESC { get; set; }
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
        [DataMember] [DataNames("PR_LINE_ITEM")] public string PR_LINE_ITEM { get; set; }
        [DataMember] [DataNames("TAX_CODE")] public string TAX_CODE { get; set; }
        [DataMember] [DataNames("MAT_TYPE")] public string MAT_TYPE { get; set; }
        [DataMember] [DataNames("PR_QTY")] public decimal PR_QTY { get; set; }
        [DataMember] [DataNames("ACK_QTY")] public decimal ACK_QTY { get; set; }
        [DataMember] [DataNames("ACK_DATE")] public DateTime? ACK_DATE { get; set; }
        [DataMember] [DataNames("PR_RELEASE_DATE")] public DateTime? PR_RELEASE_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("CITY")] public string CITY { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM")] public string ALTERNATIVE_UOM { get; set; }
        [DataMember] [DataNames("ALTERNATIVE_UOM_QTY")] public decimal ALTERNATIVE_UOM_QTY { get; set; }
        [DataMember] [DataNames("FREIGHT")] public decimal FREIGHT { get; set; }
        [DataMember] [DataNames("MISC_CHARGES")] public decimal MISC_CHARGES { get; set; }
        [DataMember] [DataNames("PACKING_CHARGES")] public decimal PACKING_CHARGES { get; set; }
        [DataMember] [DataNames("CATEGORY_CODE")] public string CATEGORY_CODE { get; set; }
        [DataMember] [DataNames("ITEM_TEXT_PO")] public string ITEM_TEXT_PO { get; set; }
        [DataMember] [DataNames("REL_IND")] public string REL_IND { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("PO_CONTRACT")] public string PO_CONTRACT { get; set; }
        [DataMember] [DataNames("DELETED_INDICATOR")] public string DELETED_INDICATOR { get; set; }
        [DataMember] [DataNames("DELIVERY_COMPLETED")] public string DELIVERY_COMPLETED { get; set; }
        [DataMember] [DataNames("ORDER_QTY")] public decimal ORDER_QTY { get; set; }
        [DataMember] [DataNames("RECEIVED_QTY")] public decimal RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("LAST_RECEIVED_DATE")] public DateTime? LAST_RECEIVED_DATE { get; set; }
        [DataMember] [DataNames("PO_ITEM_CHANGE_DATE")] public DateTime? PO_ITEM_CHANGE_DATE { get; set; }
        [DataMember] [DataNames("VALID_TO")] public DateTime? VALID_TO { get; set; }
        [DataMember] [DataNames("VALID_FROM")] public DateTime? VALID_FROM { get; set; }
        [DataMember] [DataNames("REJECTED_QTY")] public decimal REJECTED_QTY { get; set; }
        [DataMember] [DataNames("REMAINING_QTY")] public decimal REMAINING_QTY { get; set; }
        [DataMember] [DataNames("NET_PRICE")] public decimal NET_PRICE { get; set; }
        [DataMember] [DataNames("TOTAL_VALUE")] public decimal TOTAL_VALUE { get; set; }
        [DataMember] [DataNames("CURRENCY")] public string CURRENCY { get; set; }
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_STATUS")] public string VENDOR_ACK_STATUS { get; set; }
        [DataMember] [DataNames("VENDOR_ACK_COMMENTS")] public string VENDOR_ACK_COMMENTS { get; set; }
        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE")] public DateTime? VENDOR_EXPECTED_DELIVERY_DATE { get; set; }

        [DataMember] [DataNames("VENDOR_EXPECTED_DELIVERY_DATE_STRING")] public string VENDOR_EXPECTED_DELIVERY_DATE_STRING { get; set; }
        [DataMember] [DataNames("CGST")] public decimal CGST { get; set; }
        [DataMember] [DataNames("SGST")] public decimal SGST { get; set; }
        [DataMember] [DataNames("IGST")] public decimal IGST { get; set; }
        [DataMember] [DataNames("CESS")] public decimal CESS { get; set; }
        [DataMember] [DataNames("TCS")] public decimal TCS { get; set; }
        [DataMember] [DataNames("PO_CREATOR")] public string PO_CREATOR { get; set; }
        [DataMember] [DataNames("PO_CREATOR_NAME")] public string PO_CREATOR_NAME { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("PR_REQUISITIONER")] public string PR_REQUISITIONER { get; set; }
        [DataMember] [DataNames("GRNItem")] public List<GRNItem> GRNItems { get; set; }
        [DataMember] [DataNames("ITEM_MAT_TEXT")] public string ITEM_MAT_TEXT { get; set; }
        [DataMember] [DataNames("ITEM_GROSS_PRICE")] public decimal ITEM_GROSS_PRICE { get; set; }
        [DataMember] [DataNames("ITEM_DISCOUNT_VALUE")] public decimal ITEM_DISCOUNT_VALUE { get; set; }
        [DataMember] [DataNames("ITEM_DISCOUNT_PERCENTAGE")] public decimal ITEM_DISCOUNT_PERCENTAGE { get; set; }

        [DataMember] [DataNames("VENDOR_ACK_REJECT_COMMENTS")] public string VENDOR_ACK_REJECT_COMMENTS { get; set; }

        [DataMember] [DataNames("REMAINING_NET_QTY")] public decimal REMAINING_NET_QTY { get; set; }
    }

    [DataContract]
    public class GRNItem : ResponseAudit
    {
        [DataMember] [DataNames("GRN_NUMBER")] public string GRN_NUMBER { get; set; }
        [DataMember] [DataNames("GRN_LINE_ITEM")] public string GRN_LINE_ITEM { get; set; }
        [DataMember] [DataNames("GRN_RECEIVED_QTY")] public decimal GRN_RECEIVED_QTY { get; set; }
        [DataMember] [DataNames("GRN_REJECTED_QTY")] public decimal GRN_REJECTED_QTY { get; set; }
        [DataMember] [DataNames("GRN_DELIVERY_DATE")] public DateTime? GRN_DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("PO_LINE_ITEM")] public string PO_LINE_ITEM { get; set; }
    }

    [DataContract]
    public class POInvoice : ResponseAudit
    {
        [DataMember] [DataNames("PO_NUMBER")] public string PO_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_NUMBER")] public string INVOICE_NUMBER { get; set; }
        [DataMember] [DataNames("INVOICE_AMOUNT")] public decimal INVOICE_AMOUNT { get; set; }
        [DataMember] [DataNames("VENDOR_ID")] public int VENDOR_ID { get; set; }
        [DataMember] [DataNames("INVOICE_ID")] public int INVOICE_ID { get; set; }
        [DataMember] [DataNames("WF_ID")] public int WF_ID { get; set; }
        [DataMember] [DataNames("VENDOR_CODE")] public string VENDOR_CODE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("STATUS")] public string STATUS { get; set; }
        [DataMember] [DataNames("LOCATION")] public string LOCATION { get; set; }
        [DataMember] [DataNames("VENDOR_COMP_NAME")] public string VENDOR_COMP_NAME { get; set; }
        [DataMember] [DataNames("VENDOR_NAME")] public string VENDOR_NAME { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("DATE_MODIFIED")] public DateTime? DATE_MODIFIED { get; set; }

        [DataMember(Name = "attachmentsArray")]
        public List<FileUpload> AttachmentsArray { get; set; }

        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("SessionID")] public string SessionID { get; set; }

    }
}