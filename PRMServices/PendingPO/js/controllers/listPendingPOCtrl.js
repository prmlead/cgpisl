﻿prmApp
    .controller('listPendingPOCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPOService", "poService",
        "PRMCustomFieldService", "fileReader", "$uibModal", "$filter",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPOService, poService, PRMCustomFieldService, fileReader, $uibModal, $filter) {
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            //$scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;
            $scope.sessionID = userService.getUserToken();
            $scope.compId = userService.getUserCompanyId();
            $scope.myAuctions1 = [];
            $scope.myAuctionsFiltred = [];
            $scope.selectedPRItems = [];
            $scope.prmTemplates = [];
            $scope.selectedTemplate = {};
            $scope.selectedRFP;
            $scope.selectedPR;
            $scope.filteredRequirements = [];
            $scope.onlyContracts = $stateParams.onlyContracts ? 1 : 0;
            $scope.excludeContracts = $stateParams.excludeContracts ? 1 : 0;
            if (!$scope.onlyContracts && !$scope.excludeContracts) {
                $scope.onlyContracts = 0;
                $scope.excludeContracts = 0;
                if (window.location.href && window.location.href.toLowerCase().indexOf('list-pendingcontracts') >= 0) { //Backup on F5.
                    $scope.onlyContracts = 1;
                } else {
                    $scope.excludeContracts = 1;
                }
            }
            $scope.attachmentError = false;
            $scope.pendingPOStats = {
                totalPendingPOs: 0,
                totalPOs: 0,
                totalAwaitingRecipt: 0,
                totalPOsNotInitiated: 0,
                totalPartialDeliverbles: 0
            };
            $scope.prExcelReport = [];
            $scope.invoiceList = [];

            $scope.invoiceDetails = {
                invoiceNumber: '',
                invoiceAmount: '',
                invoiceComments: ''
            };

            $scope.selectedPODetails = [];
            $scope.selectedIndex = 0;
            //$scope.isVendor = userService.getUserType() === "VENDOR" ? true : false;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            $scope.filtersList = {
                departmentList: [],
                categoryidList: [],
                productidList: [],
                supplierList: [],
                poStatusList: [],
                deliveryStatusList: [],
                plantList: [],
                purchaseGroupList: [],
                subUserList: [],
                vendorAckStatus: []
            };

            $scope.filters = {
                department: {},
                categoryid: {},
                productid: {},
                supplier: {},
                poStatus: {},
                deliveryStatus: {},
                plant: {},
                purchaseGroup: {},
                pendingPOFromDate: moment().subtract(30, "days").format("YYYY-MM-DD"),
                pendingPOToDate: moment().format('YYYY-MM-DD'),
                subuser: {},
                ackStatus: {}
            };

            //$scope.filters.pendingPOToDate = moment().format('YYYY-MM-DD');
            //$scope.filters.pendingPOFromDate = moment().subtract(30, "days").format("YYYY-MM-DD");

            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5;

            $scope.subUsers = [];
            $scope.inactiveSubUsers = [];
            $scope.subUsers1 = [];

            $scope.getSubUserData = function () {
                userService.getSubUsersData({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                    .then(function (response) {

                        $scope.subUsers = $filter('filter')(response, { isValid: true });

                        $scope.subUsers.forEach(function (user, userIndex) {
                            subUserListTemp.push({ id: user.userID, name: user.firstName + " " + user.lastName });
                        })
                        $scope.filtersList.subUserList = subUserListTemp;
                    });
            };

            // $scope.getSubUserData();

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
                $scope.getpendingPOlist(($scope.currentPage - 1), 10, $scope.filters.searchKeyword);
            };

            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            $scope.totalItems2 = 0;
            $scope.currentPage2 = 1;
            $scope.itemsPerPage2 = 10;
            $scope.maxSize2 = 5;

            $scope.setPage2 = function (pageNo) {
                $scope.currentPage1 = pageNo;
            };

            $scope.pageChanged2 = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };

            /*PAGINATION CODE*/


            $scope.pendingPOList = [];
            $scope.filteredPendingPOsList = [];
            $scope.pendingPOItems = [];

            $('.selected-items-box').bind('click', function (e) {
                $('.multiple-selection-dropdown .list').slideToggle('fast');
            });
            var isFilter = false;
            $scope.setFilters = function (currentPage) {
                $scope.pendingPOStats.totalPendingPOs = 0;
                $scope.pendingPOStats.totalPOs = 0;
                $scope.pendingPOStats.totalAwaitingRecipt = 0;
                $scope.pendingPOStats.totalPOsNotInitiated = 0;
                $scope.pendingPOStats.totalPartialDeliverbles = 0;

                $scope.filteredPendingPOsList = $scope.pendingPOList;
                $scope.totalItems = $scope.filteredPendingPOsList.length;
                $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);

                if ($scope.filters.searchKeyword || !_.isEmpty($scope.filters.department) || !_.isEmpty($scope.filters.categoryid) || !_.isEmpty($scope.filters.productid) ||
                    !_.isEmpty($scope.filters.supplier) || !_.isEmpty($scope.filters.poStatus) || !_.isEmpty($scope.filters.ackStatus) ||
                    !_.isEmpty($scope.filters.deliveryStatus) || !_.isEmpty($scope.filters.plant) || !_.isEmpty($scope.filters.purchaseGroup) || !_.isEmpty($scope.filters.subuser)) {
                    //$scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);
                } else {

                    if ($scope.initialPendingPOPageArray && $scope.initialPendingPOPageArray.length > 0) {
                        $scope.pendingPOList = $scope.initialPendingPOPageArray;
                        if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                            //$scope.totalItems = $scope.pendingPOList[0].TOTAL_PENDING_PO_COUNT;
                            //$scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                            //$scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].totalPOs;
                            //$scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].totalAwaitingRecipt;
                            //$scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].totalPOsNotInitiated;
                            //$scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].totalPartialDeliverbles;
                            //$scope.filteredPendingPOsList = $scope.pendingPOList;


                            $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                            $scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                            $scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].STATS_TOTAL_COUNT;
                            $scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].STATS_PO_AWAITING_RECEIPT;
                            $scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].STATS_PO_NOT_INITIATED;
                            $scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].STATS_PO_PARTIAL_DELIVERY;
                            $scope.filteredPendingPOsList = $scope.pendingPOList;
                        }

                    }
                }

            };

            $scope.filterByDate = function () {
                $scope.pendingPOStats.totalPendingPOs = 0;
                $scope.pendingPOStats.totalPOs = 0;
                $scope.pendingPOStats.totalAwaitingRecipt = 0;
                $scope.pendingPOStats.totalPOsNotInitiated = 0;
                $scope.pendingPOStats.totalPartialDeliverbles = 0;

                $scope.filteredPendingPOsList = $scope.pendingPOList;
                $scope.totalItems = $scope.filteredPendingPOsList.length;
                $scope.getpendingPOlist(0, 10, $scope.filters.searchKeyword);

            };

            $scope.totalCount = 0;
            $scope.searchString = '';
            $scope.initialPendingPOPageArray = [];

            $scope.getpendingPOlist = function (recordsFetchFrom, pageSize, searchString) {

                var department, categoryid, productid, supplier, poStatus, ackStatus, deliveryStatus, plant, purchaseGroup, pendingPOFromDate, pendingPOToDate, buyer;


                if (_.isEmpty($scope.filters.pendingPOFromDate)) {
                    pendingPOFromDate = '';
                } else {
                    pendingPOFromDate = $scope.filters.pendingPOFromDate;
                }

                if (_.isEmpty($scope.filters.pendingPOToDate)) {
                    pendingPOToDate = '';
                } else {
                    pendingPOToDate = $scope.filters.pendingPOToDate;
                }
                if (_.isEmpty($scope.filters.plant)) {
                    plant = '';
                } else if ($scope.filters.plant && $scope.filters.plant.length > 0) {
                    var plants = _($scope.filters.plant)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    plant = plants.join(',');
                }



                if (_.isEmpty($scope.filters.categoryid)) {
                    categoryid = '';
                } else if ($scope.filters.categoryid && $scope.filters.categoryid.length > 0) {
                    var categories = _($scope.filters.categoryid)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    categoryid = categories.join(',');
                }

                if (_.isEmpty($scope.filters.productid)) {
                    productid = '';
                } else if ($scope.filters.productid && $scope.filters.productid.length > 0) {
                    var productids = _($scope.filters.productid)
                        .filter(item => item.id)
                        .map('id')
                        .value();
                    productid = productids.join(',');
                }

                if (_.isEmpty($scope.filters.supplier)) {
                    supplier = '';
                } else if ($scope.filters.supplier && $scope.filters.supplier.length > 0) {
                    var suppliers = _($scope.filters.supplier)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    supplier = suppliers.join(',');
                }

                if (_.isEmpty($scope.filters.poStatus)) {
                    poStatus = '';
                } else if ($scope.filters.poStatus && $scope.filters.poStatus.length > 0) {
                    var poStatuses = _($scope.filters.poStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    poStatus = poStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.ackStatus)) {
                    ackStatus = '';
                } else if ($scope.filters.ackStatus && $scope.filters.ackStatus.length > 0) {
                    var ackStatuses = _($scope.filters.ackStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    ackStatus = ackStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.deliveryStatus)) {
                    deliveryStatus = '';
                } else if ($scope.filters.deliveryStatus && $scope.filters.deliveryStatus.length > 0) {
                    var deliveryStatuses = _($scope.filters.deliveryStatus)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    deliveryStatus = deliveryStatuses.join(',');
                }

                if (_.isEmpty($scope.filters.subuser)) {
                    buyer = '';
                } else if ($scope.filters.subuser && $scope.filters.subuser.length > 0) {
                    var buyers = _($scope.filters.subuser)
                        .filter(item => item.name)
                        .map('name')
                        .value();
                    buyer = buyers.join(',');
                }

                //if (_.isEmpty($scope.filters.purchaseGroup)) {
                //    purchaseGroup = '';
                //} else if ($scope.filters.purchaseGroup && $scope.filters.purchaseGroup.length > 0) {
                //    var purchaseGroups = _($scope.filters.purchaseGroup)
                //        .filter(item => item.name)
                //        .map('name')
                //        .value();
                //    purchaseGroup = purchaseGroups.join(',');
                //}

                var params = {
                    "compid": $scope.isCustomer ? $scope.compId : 0,
                    "uid": $scope.isCustomer ? 0 : $scope.userID,
                    "search": searchString ? searchString : "",
                    "categoryid": categoryid,
                    "productid": productid,
                    "supplier": supplier,
                    "postatus": poStatus,
                    "deliverystatus": deliveryStatus,
                    "plant": plant,
                    "fromdate": pendingPOFromDate,
                    "todate": pendingPOToDate,
                    "page": recordsFetchFrom * pageSize,
                    "pagesize": pageSize,
                    "onlycontracts": $scope.onlyContracts,
                    "excludecontracts": $scope.excludeContracts,
                    "ackStatus": ackStatus,
                    "buyer": buyer,
                    "purchaseGroup": '',
                    "sessionid": userService.getUserToken()
                };

                $scope.pageSizeTemp = (params.page + 1);
                $scope.NumberOfRecords = ((recordsFetchFrom + 1) * pageSize);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response = _.orderBy(response, ['PO_DATE'], ['desc']);
                            response.forEach(function (item, index) {
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? moment(item.VENDOR_EXPECTED_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_DATE = item.PO_DATE ? moment(item.PO_DATE).format("DD-MM-YYYY") : '-';
                                item.multipleAttachments = [];
                                item.INVOICE_AMOUNT = 0;
                                item.INVOICE_NUMBER = '';
                                item.COMMENTS = '';
                                item.isAcknowledgeOverall = false;
                                item.fontStyle = {};
                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }
                        if (!$scope.downloadExcel) {
                            $scope.pendingPOList = [];
                            $scope.filteredPendingPOsList = [];
                            if (response && response.length > 0) {
                                response.forEach(function (item, index) {
                                    $scope.pendingPOList.push(item);
                                    if ($scope.initialPendingPOPageArray.length <= 9) { // Push Initial 10 Records When Page is Loaded because needed in SetFilters function it's getting called every time (need to modify directive code)  #Crap Code need to remove(should think of another solution)

                                        //var ifExists1 = _.findIndex($scope.initialPendingPOPageArray, function (po) { return po.PO_NUMBER === item.PO_NUMBER });
                                        //if (ifExists1 <= -1) {
                                        //    $scope.initialPendingPOPageArray.push(item);
                                        //}

                                        //$scope.initialPendingPOPageArray.push(item);
                                    }
                                });

                            }

                            if ($scope.pendingPOList && $scope.pendingPOList.length > 0) {
                                $scope.totalItems = $scope.pendingPOList[0].TOTAL_COUNT;
                                $scope.pendingPOStats.totalPendingPOs = $scope.totalItems;
                                $scope.pendingPOStats.totalPOs = $scope.pendingPOList[0].STATS_TOTAL_COUNT;
                                $scope.pendingPOStats.totalAwaitingRecipt = $scope.pendingPOList[0].STATS_PO_AWAITING_RECEIPT;
                                $scope.pendingPOStats.totalPOsNotInitiated = $scope.pendingPOList[0].STATS_PO_NOT_INITIATED;
                                $scope.pendingPOStats.totalPartialDeliverbles = $scope.pendingPOList[0].STATS_PO_PARTIAL_DELIVERY;
                                $scope.filteredPendingPOsList = $scope.pendingPOList;
                            }
                        } else {
                            if (response && response.length > 0) {
                                $scope.pendingPOExcelReport = response;
                                downloadPRExcel()
                            } else {
                                swal("Error!", "No records.", "error");
                                $scope.downloadExcel = false;
                            }
                        }



                    });
            };

            $scope.getpendingPOlist(0, 10, $scope.searchString);
            $scope.filterValues = [];

            $scope.getFilterValues = function () {
                var params =
                {
                    "compid": $scope.isCustomer ? $scope.compId : 0
                };

                let departmentListTemp = [];
                let categoryidListTemp = [];
                let productidListTemp = [];
                let supplierListTemp = [];
                let poStatusListTemp = [];
                let deliveryStatusListTemp = [];
                let plantListTemp = [];
                let purchaseGroupListTemp = [];
                let subUserListTemp = [];
                let vendorAckStatusTemp = [];

                PRMPOService.getPOScheduleFilterValues(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.filterValues = response;
                            if ($scope.filterValues && $scope.filterValues.length > 0) {
                                $scope.filterValues.forEach(function (item, index) {
                                    if (item.name === 'DEPARTMENT') {
                                        item.arrayPair.forEach(function (item) {
                                            departmentListTemp.push({ id: item.key, name: item.value });
                                        });
                                        departmentListTemp.push({ id: item.arrayPair.key, name: item.arrayPair.value });
                                    } else if (item.name === 'CATEGORY') {
                                        item.arrayPair.forEach(function (item) {
                                            categoryidListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'PRODUCT') {
                                        item.arrayPair.forEach(function (item) {
                                            productidListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'VENDORS') {
                                        item.arrayPair.forEach(function (item) {
                                            supplierListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'PO_STATUS') {
                                        item.arrayPair.forEach(function (item) {
                                            poStatusListTemp.push({ id: item.key, name: item.value });
                                        });
                                        //poStatusListTemp.push({ id: item.ID, name: item.NAME });
                                    } else if (item.name === 'PLANT') {
                                        item.arrayPair.forEach(function (item) {
                                            plantListTemp.push({ id: item.key1, name: item.key1 + ' - ' + item.value });
                                        });
                                    } else if (item.name === 'DELIVERY_STATUS') {
                                        deliveryStatusListTemp.push({ id: item.NAME, name: item.ID });
                                    } else if (item.name === 'PURCHASE_GROUP') {
                                        item.arrayPair.forEach(function (item) {
                                            purchaseGroupListTemp.push({ id: item.key, name: item.value });
                                        });

                                    } else if (item.name === 'PO_CREATOR') {
                                        item.arrayPair.forEach(function (item) {
                                            subUserListTemp.push({ id: item.key, name: item.value });
                                        });
                                    } else if (item.name === 'VENDOR_ACK_STATUS') {
                                        item.arrayPair.forEach(function (item) {
                                            vendorAckStatusTemp.push({ id: item.key, name: item.value });
                                        });
                                    }

                                });
                                //vendorAckStatusTemp.forEach(function (item) {
                                //    poStatusListTemp.push({ id: item.id, name: item.name });
                                //})

                                //$scope.filtersList.departmentList = departmentListTemp;
                                $scope.filtersList.plantList = plantListTemp;
                                $scope.filtersList.categoryidList = categoryidListTemp;
                                $scope.filtersList.productidList = productidListTemp;
                                $scope.filtersList.supplierList = supplierListTemp;
                                $scope.filtersList.poStatusList = poStatusListTemp;
                                $scope.filtersList.deliveryStatusList = deliveryStatusListTemp;
                                $scope.filtersList.purchaseGroupList = purchaseGroupListTemp;
                                $scope.filtersList.subUserList = subUserListTemp;
                                $scope.filtersList.vendorAckStatus = vendorAckStatusTemp;

                            }
                        }
                    });

            };
            $scope.getFilterValues();



            $scope.getPendingPOItems = function (pendingPODetails) {
                if (pendingPODetails) {
                    var params = {
                        "ponumber": pendingPODetails.PO_NUMBER,
                        "moredetails": 0,
                        "forasn":false
                    };
                    PRMPOService.getPOScheduleItems(params)
                        .then(function (response) {
                            $scope.pendingPOItems = response;
                            $scope.pendingPOItems.forEach(function (item, index) {
                                item.LAST_RECEIVED_DATE = item.LAST_RECEIVED_DATE ? moment(item.LAST_RECEIVED_DATE).format("DD-MM-YYYY") : '-';
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_ITEM_CHANGE_DATE = item.PO_ITEM_CHANGE_DATE ? moment(item.PO_ITEM_CHANGE_DATE).format("DD-MM-YYYY") : '-';
                                if (pendingPODetails.isAcknowledgeOverall) {
                                    item.isAcknowledge = 1;
                                } else {
                                    item.isAcknowledge = 0;
                                }
                                if (pendingPODetails.isEdit1) {
                                    item.isEditField = true;
                                } else {
                                    item.isEditField = false;
                                }
                                item.isError = false;
                            });

                            pendingPODetails.pendingPOItems = $scope.pendingPOItems;


                        });
                }
            };


            $scope.downloadExcel = false;
            $scope.GetReport = function () {
                $scope.pendingPOList = [];
                $scope.downloadExcel = true;
                $scope.getpendingPOlist(0, 0, $scope.searchString);
            };



            function downloadPRExcel() {
                alasql('SELECT PR_NUMBER as [PR Number],PLANT as [Plant],PLANT_NAME as [Plant Name], ' +
                    'PR_CREATOR_NAME as [PR Creator Name], NEW_PR_STATUS as [Status],RELEASE_DATE as [Release Date], ' +
                    'GMP as [GMP],PURCHASE_GROUP_CODE as [Purchase Group Code], ' +
                    'PURCHASE_GROUP_NAME as [Purchase Group Name]' +

                    'INTO XLSX(?, { headers: true, sheetid: "PR_DETAILS", style: "font-size:15px;background:#233646;color:#FFF;" }) FROM ? ',
                    ["PR Details.xlsx", $scope.pendingPOExcelReport]);
                $scope.downloadExcel = false;
            }


            $scope.scrollWin = function (id, scrollPosition) {

                var elmnt = document.getElementById(id);
                elmnt.scrollIntoView();
                //document.getElementById(id).scrollIntoView();
                if (scrollPosition == 'BOTTOM') {
                    window.scroll({ bottom: elmnt.offsetBottom });
                }
                else {
                    window.scroll({ top: elmnt.offsetTop });
                }

            };

            $scope.viewPO = function (poOrderId) {
                var url = $state.href('list-pendingPOOverall', { "poID": poOrderId });

                $window.open(url, '_blank');
            };

            //$scope.viewGRN = function (poOrderId) {
            //    var url = $state.href('list-grn', { "poOrderId": poOrderId });

            //    $window.open(url, '_blank');
            //};

            $scope.goToDispatchTrackForm = function (poOrderId, dCode) {

                var url = $state.href("asnForm", { "poOrderId": poOrderId, "dCode": dCode });
                //window.open(url, '_self');
                $window.open(url, '_blank');

            };


            $scope.notifyToVendor = function () {
                //auctionsService.savepricecap(params).then(function (req) {
                //    if (req.errorMessage == '') {
                //        setTimeout(function () {
                //            swal({
                //                text: 'Email Will be sent to vendor',
                //                type: "success",
                //                showCancelButton: false,
                //                confirmButtonColor: "#DD6B55",
                //                confirmButtonText: "Ok",
                //                closeOnConfirm: true
                //            },
                //                function () {

                //                    location.reload();
                //                });
                //        }, 1000);
                //    } else {
                //        swal("Error!", req.errorMessage, "error");
                //    }
                //});
            };

            $scope.getPOInvoiceDetails = function (item, index) {
                $scope.selectedPODetails = item;
                $scope.selectedIndex = index;
                $scope.params = {
                    "ponumber": $scope.selectedPODetails.PO_NUMBER,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
                                $scope.filteredPendingPOsList[id].multipleAttachments = [];
                            }

                            var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                            if (ifExists <= -1) {
                                $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
                            }

                        });
                })
            }
            //$scope.getFile1 = function (id, itemid, ext) {
            //    $scope.filesTemp = $("#" + id)[0].files;
            //    $scope.filesTemp = Object.values($scope.filesTemp);
            //    $scope.filesTemp.forEach(function (attach, attachIndex) {
            //        $scope.file = $("#" + id)[0].files[attachIndex];
            //        fileReader.readAsDataUrl($scope.file, $scope)
            //            .then(function (result) {
            //                var fileUpload = {
            //                    fileStream: [],
            //                    fileName: '',
            //                    fileID: 0
            //                };
            //                var bytearray = new Uint8Array(result);
            //                fileUpload.fileStream = $.makeArray(bytearray);
            //                fileUpload.fileName = attach.name;
            //                if (!$scope.filteredPendingPOsList[id].multipleAttachments) {
            //                    $scope.filteredPendingPOsList[id].multipleAttachments = [];
            //                }

            //                var ifExists = _.findIndex($scope.filteredPendingPOsList[id].multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
            //                if (ifExists <= -1) {
            //                    $scope.filteredPendingPOsList[id].multipleAttachments.push(fileUpload);
            //                }

            //            });
            //    })
            //}

            $scope.removeAttach = function (index, item) {
                item.multipleAttachments.splice(index, 1);
            }

            $scope.savePOInvoice = function (item, id) {
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": $scope.userID,
                        "INVOICE_NUMBER": item.INVOICE_NUMBER,
                        "INVOICE_AMOUNT": item.INVOICE_AMOUNT,
                        "attachmentsArray": $scope.filteredPendingPOsList[id].multipleAttachments,
                        "COMMENTS": item.COMMENTS,
                        "STATUS": item.STATUS,
                        "SessionID": $scope.sessionID
                    }
                };
                if (params1.details.attachmentsArray.length == 0) {
                    $scope.attachmentError = true;
                    return;
                }
                PRMPOService.savePOInvoice(params1)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPOInvoiceDetails($scope.selectedPODetails, $scope.selectedIndex);
                            item.INVOICE_NUMBER = '';
                            item.INVOICE_AMOUNT = 0;
                            item.multipleAttachments = [];
                            item.COMMENTS = '';
                            $scope.attachmentError = false;
                        }

                    });
            };


            $scope.deletePOInvoice = function (ponumber, invoiceNumber) {

                $scope.params = {
                    "ponumber": ponumber,
                    "invoicenumber": invoiceNumber,
                    "sessionid": userService.getUserToken()
                }


                PRMPOService.deletePOInvoice($scope.params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Deleted Successfully.", "success");
                            $scope.getPOInvoiceDetails($scope.selectedPODetails, $scope.selectedIndex);

                        }
                    });

            }


            $scope.cancelInvoice = function (item,id) {
                item.INVOICE_NUMBER = '';
                item.INVOICE_AMOUNT = 0;
                item.multipleAttachments = [];
                item.COMMENTS = '';
                $scope.attachmentError = false;
            };

            $scope.acknowledgeDetails = function (details, module, pendingPODetails) {

                if (!details.PRODUCT_NAME && module === 'ACKNOWLEDGE') {
                    //swal("Accept!", "All the items will be approved with ordered Qty for " + details.PO_NUMBER, "warning");
                    swal({
                        title: "Are you sure?",
                        text: "All the items will be approved with ordered Qty for " + details.PO_NUMBER ,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ACKNOWLEDGE!",
                        closeOnConfirm: true
                    },
                        function (isConfirm1) {
                            if (isConfirm1) {
                                var params = {
                                    'ponumber': details.PO_NUMBER,
                                    'poitemline': '',
                                    'status': module,
                                    'isVendPoAck': details.IS_PO_ACK,
                                    'quantity': 0,
                                    'user': $scope.userID,
                                    'sessionid': userService.getUserToken()
                                }
                                poService.SavePOVendorQuantityAck(params)
                                    .then(function (response) {
                                        if (response.errorMessage != '') {
                                            growlService.growl(response.errorMessage, "inverse");
                                        }
                                        else {
                                            growlService.growl("Acknowledged Successfully.", "success");
                                            location.reload();
                                        }
                                    })
                            } else {
                                return;
                            }
                        });
                   
                }

                if (!details.PRODUCT_NAME && module === 'REJECTED') {
                    swal({
                        title: "Are you sure?",
                        text: " Reject all the Items of " + details.PO_NUMBER,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                //swal("Reject!", "Rejected items can not be approved .", "success");
                                swal({
                                    title: "Are you sure?",
                                    text: "Once you click on Reject button, rejected items cannot be approved" + details.PO_NUMBER,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Reject!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': '',
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': 0,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {
                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Rejected Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })
                                        } else {
                                            return;
                                        }
                                    });
                            } else {
                                return;
                            }
                        });
                }

                if (details.PRODUCT_NAME && module === 'ACKNOWLEDGE') {
                    //swal("Accept!", "Item will be approved with ordered Qty for " + details.PRODUCT_NAME, "warning");
                    swal({
                        title: "Are you sure?",
                        text: "Item will be approved with ordered Qty for " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "ACKNOWLEDGE!",
                        closeOnConfirm: true
                    },
                        function (isConfirm1) {
                            if (isConfirm1) {
                                var params = {
                                    'ponumber': details.PO_NUMBER,
                                    'poitemline': details.PO_LINE_ITEM,
                                    'status': module,
                                    'isVendPoAck': details.IS_PO_ACK,
                                    'quantity': details.ORDER_QTY,
                                    'user': $scope.userID,
                                    'sessionid': userService.getUserToken()
                                }
                                poService.SavePOVendorQuantityAck(params)
                                    .then(function (response) {
                                        if (response.errorMessage != '') {
                                            growlService.growl(response.errorMessage, "inverse");
                                        }
                                        else {
                                            growlService.growl("Acknowledged Successfully.", "success");
                                            location.reload();
                                        }
                                    })
                             
                            } else {
                                return;
                            }
                        });
                   
                }

                if (details.PRODUCT_NAME && module == 'REJECTED') {
                    swal({
                        title: "Are you sure?",
                        text: " You want to reject " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Continue!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal({
                                    title: "Are you sure?",
                                    text: " Rejected item can not be approved " + details.PRODUCT_NAME,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Reject!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': details.PO_LINE_ITEM,
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': 0,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {
                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Rejected Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })
                                          
                                        } else {
                                            return;
                                        }
                                    });
                            } else {
                                return;
                            }
                        });

                }

                if (details.PRODUCT_NAME && module == 'EDIT') {
                    if (details.ACK_QTY <=0 ||details.ACK_QTY > details.ORDER_QTY) {
                        details.isError = true;
                        return;
                    }
                    swal({
                        title: "Are you sure?",
                        text: "On clicking on Modify, user will not be able to edit the quantity in future " + details.PRODUCT_NAME,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Modify!",
                        closeOnConfirm: false
                    },
                        function (isConfirm) {
                            if (isConfirm) {
                                swal({
                                    title: "Are you sure?",
                                    text: "On Click on Save following items will be modified " + details.PRODUCT_NAME,
                                    type: "warning",
                                    showCancelButton: true,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Save!",
                                    closeOnConfirm: true
                                },
                                    function (isConfirm1) {
                                        if (isConfirm1) {
                                            var params = {
                                                'ponumber': details.PO_NUMBER,
                                                'poitemline': details.PO_LINE_ITEM,
                                                'status': module,
                                                'isVendPoAck': details.IS_PO_ACK,
                                                'quantity': details.ACK_QTY,
                                                'user': $scope.userID,
                                                'sessionid': userService.getUserToken()
                                            }
                                            poService.SavePOVendorQuantityAck(params)
                                                .then(function (response) {

                                                    if (response.errorMessage != '') {
                                                        growlService.growl(response.errorMessage, "inverse");
                                                    }
                                                    else {
                                                        growlService.growl("Modified Successfully.", "success");
                                                        location.reload();
                                                    }
                                                })

                                        } else {
                                            return;
                                        }
                                    }); 
                            } else {
                                return;
                            }
                        });

                }

                //if (details.PRODUCT_NAME && module == 'EDIT') {

                //    if (details.ACK_QTY > details.ORDER_QTY) {
                //        details.isError = true;
                //        return;
                //    }
                //    //swal("Edit!", details.PRODUCT_NAME + "is Edited", "warning");
                //    swal({
                //        title: "Are you sure?",
                //        text: details.PRODUCT_NAME + "is Edited",
                //        type: "warning",
                //        showCancelButton: true,
                //        confirmButtonColor: "#DD6B55",
                //        confirmButtonText: "Edit!",
                //        closeOnConfirm: true
                //    },
                //        function (isConfirm1) {
                //            if (isConfirm1) {
                //                var params = {
                //                    'ponumber': details.PO_NUMBER,
                //                    'poitemline': details.PO_LINE_ITEM,
                //                    'status': module,
                //                    'isVendPoAck': details.IS_PO_ACK,
                //                    'quantity': details.ACK_QTY,
                //                    'user': $scope.userID,
                //                    'sessionid': userService.getUserToken()
                //                }
                //                poService.SavePOVendorQuantityAck(params)
                //                    .then(function (response) {

                //                        location.reload();
                //                    })
                                
                //            } else {
                //                return;
                //            }
                //        });
                   
                //}
            };

            $scope.goToDispatchTrackFormEdit = function (poNumber) {
                $scope.params = {
                    "ponumber": poNumber,
                    "vendorid": $scope.isCustomer? 0 : userService.getUserId(),
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetailsList($scope.params)
                    .then(function (response) {
                        $scope.asnList = response;
                    });
            }

        }]);