﻿prmApp
    .controller('listPendingPOOverallCtrl', ["$rootScope", "$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService", "PRMPRServices", "poService",
        "PRMCustomFieldService", "PRMPOService", "$uibModal", "fileReader",
        function ($rootScope, $scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMPRServices, poService, PRMCustomFieldService, PRMPOService, $uibModal, fileReader) {
            $scope.poNumber = $stateParams.poID;
            $scope.fromDate = $stateParams.fromDate;
            $scope.toDate = $stateParams.toDate;
            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.poOrderId = $stateParams.poOrderId;
            $scope.dCode = $stateParams.dCode;
            $scope.isCustomer = userService.getUserType() === "CUSTOMER" ? true : false;

            $scope.compID = userService.getUserCompanyId();
            /*PAGINATION CODE*/
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 10;
            $scope.maxSize = 5; //Number of pager buttons to show

            $scope.setPage = function (pageNo) {
                $scope.currentPage = pageNo;
            };
            $scope.selectedIndex = 0;
            $scope.filteredPendingPOsList = [];
            $scope.pageChanged = function () {
                ////console.log('Page changed to: ' + $scope.currentPage);
            };
            $scope.ackPO = { isPOAck : 0};
            $scope.vendAttachments = {
                vendorMultipleAttachments : []
            };
            $scope.approveRejectObj = { isApproveVendorPo: -1, isApproveRejectComments: '' };
            /*PAGINATION CODE*/
            $scope.billedQty = 0;
            $scope.receivedQty = 0;
            $scope.rejectedQty = 0;
            $scope.remainingQty = 0;
            $scope.removeQty = 0;

            $scope.ALTERNATIVE_UOM = '';
            $scope.ALTERNATIVE_UOM_QTY = '';
            $scope.SERVICE_CODE = '';
            $scope.SERVICE_DESCRIPTION = '';
            $scope.MISC_CHARGES = '';

            $scope.getPendingPOOverall = function () {
                $scope.billedQty = 0;
                $scope.receivedQty = 0;
                $scope.rejectedQty = 0;
                $scope.remainingQty = 0;
                $scope.removeQty = 0;

                var params = {
                    "compid": $scope.isCustomer ? $scope.compID : 0,
                    "uid": $scope.isCustomer ? 0 : +$scope.userID,
                    "search": $scope.poNumber,
                    "categoryid": '',
                    "productid": '',
                    "supplier": '',
                    "postatus": '',
                    "deliverystatus": '',
                    "plant": '',
                    "fromdate": '1970-01-01',
                    "todate": '2100-01-01',
                    "page": 0,
                    "pagesize": 10,
                    "ackStatus": '',
                    "buyer": '',
                    "purchaseGroup": '',
                    "sessionid": userService.getUserToken()

                };

                $scope.pageSizeTemp = (params.page + 1);

                PRMPOService.getPOScheduleList(params)
                    .then(function (response) {
                        $scope.pendingPOList = [];
                        $scope.filteredPendingPOsList = [];
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                                item.vendorMultipleAttachments = [];
                                item.vendAttachArray = [];
                                item.PO_DATE = item.PO_DATE ? moment(item.PO_DATE).format("DD-MM-YYYY") : '-';
                                item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_CLOSED_DATE = item.PO_CLOSED_DATE ? moment(item.PO_CLOSED_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RELEASE_DATE = item.PO_RELEASE_DATE ? moment(item.PO_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                                item.PO_RECEIPT_DATE = item.PO_RECEIPT_DATE ? moment(item.PO_RECEIPT_DATE).format("DD-MM-YYYY") : '-';
                                item.vendAttachArray = item.VENDOR_ATTACHEMNTS != null || item.VENDOR_ATTACHEMNTS != undefined ? item.VENDOR_ATTACHEMNTS.split(',') : '';
                                $scope.approveRejectObj.isApproveVendorPo = item.VENDOR_ACK_STATUS == 'APPROVED' ? true : false;
                                $scope.approveRejectObj.isApproveRejectComments = item.VENDOR_ACK_REJECT_COMMENTS; 
                                $scope.ackPO.isPOAck = item.IS_PO_ACK;
                                $scope.pendingPOList.push(item);

                                //item.MODIFIED_DATE = userService.toLocalDate(item.MODIFIED_DATE).split(' ')[0];
                            });
                        }

                        $scope.filteredPendingPOsList = $scope.pendingPOList[0];

                    });


                var params1 = {
                    "ponumber": $scope.poNumber,
                    "moredetails": 0,
                    "forasn": false
                };
                PRMPOService.getPOScheduleItems(params1)
                    .then(function (response) {
                        $scope.pendingPOItems = response;
                        $scope.pendingPOItems.forEach(function (item, index) {
                            item.DELIVERY_DATE = item.DELIVERY_DATE ? moment(item.DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_RELEASE_DATE = item.PR_RELEASE_DATE ? moment(item.PR_RELEASE_DATE).format("DD-MM-YYYY") : '-';
                            item.PR_DELIVERY_DATE = item.PR_DELIVERY_DATE ? moment(item.PR_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            item.VALID_FROM = item.VALID_FROM ? moment(item.VALID_FROM).format("DD-MM-YYYY") : '-';
                            item.VALID_TO = item.VALID_TO ? moment(item.VALID_TO).format("DD-MM-YYYY") : '-';
                            //item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? moment(item.VENDOR_EXPECTED_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                            if (item.VENDOR_EXPECTED_DELIVERY_DATE_STRING == "NOT_ACK_WITH_EXPECTED_DATE") {
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = '-';
                            } else {
                                item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = userService.toOnlyDate(item.VENDOR_EXPECTED_DELIVERY_DATE);
                            }
                          //  item.VENDOR_EXPECTED_DELIVERY_DATE_LOCAL = item.VENDOR_EXPECTED_DELIVERY_DATE ? userService.toOnlyDate(item.VENDOR_EXPECTED_DELIVERY_DATE) : '-';
                            
                            item.PO_ITEM_CHANGE_DATE = item.PO_ITEM_CHANGE_DATE ? moment(item.PO_ITEM_CHANGE_DATE).format("DD-MM-YYYY") : '-';
                            item.ALTERNATIVE_UOM = $scope.ALTERNATIVE_UOM;
                            item.ALTERNATIVE_UOM_QTY = $scope.ALTERNATIVE_UOM_QTY;
                            item.SERVICE_CODE = $scope.SERVICE_CODE;
                            item.SERVICE_DESCRIPTION = $scope.SERVICE_DESCRIPTION;
                            item.MISC_CHARGES = $scope.MISC_CHARGES;

                            $scope.billedQty += item.ORDER_QTY;
                            $scope.receivedQty += item.RECEIVED_QTY;
                            $scope.rejectedQty += item.REJECTED_QTY;
                            $scope.remainingQty += item.REMAINING_QTY;
                            //$scope.removeQty += item.REMOVE_QTY;

                            if (item.GRNItems && item.GRNItems.length > 0) {
                                item.GRNItems.forEach(function (grnItem, index) {
                                    grnItem.GRN_DELIVERY_DATE_LOCAL = grnItem.GRN_DELIVERY_DATE ? moment(grnItem.GRN_DELIVERY_DATE).format("DD-MM-YYYY") : '-';
                                });
                            }
                        });

                        //pendingPODetails.pendingPOItems = $scope.pendingPOItems;


                    });

            };

            $scope.getPendingPOOverall();


            $scope.getPOInvoiceDetails = function () {
                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "sessionID": userService.getUserToken()
                };

                PRMPOService.getPOInvoiceDetails($scope.params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            response.forEach(function (item, index) {
                                item.multipleAttachments = [];
                            });
                        }
                        $scope.invoiceList = response;
                        //$scope.invoiceList.forEach(function (item) {
                        //    var attchArray = item.ATTACHMENTS.split(',');
                        //    attchArray.forEach(function (att, index) {

                        //        var fileUpload = {
                        //            fileStream: [],
                        //            fileName: '',
                        //            fileID: parseInt(att)
                        //        };

                        //        item.multipleAttachments.push(fileUpload);
                        //    });

                        //})


                    });
            };

            $scope.getPOInvoiceDetails()

            $scope.getASNDetails = function () {
                //$scope.params = {
                //    "compid": $scope.isCustomer ? $scope.compID : 0,
                //    "asnid": 0,
                //    "ponumber": $scope.poNumber,
                //    "grncode": 0,
                //    "asncode": 0,
                //    "vendorid": $scope.isCustomer ? 0 : $scope.userID,
                //    "sessionID": userService.getUserToken()
                //};

                //PRMPOService.getASNDetails($scope.params)
                //    .then(function (response) {
                //        if (response && response.length > 0) {
                //            $scope.asnList = response;
                //        }
                //    });

                $scope.params = {
                    "ponumber": $scope.poNumber,
                    "vendorid": 0,
                    "senssionid": userService.getUserToken()
                };

                PRMPOService.getASNDetailsList($scope.params)
                    .then(function (response) {
                        $scope.asnList = response;
                    });
            };

            $scope.getASNDetails();

            $scope.viewPO = function (grnNo) {
                var url = $state.href('list-ViewGRN', { "grnNo": grnNo, "isGRN": false });

                $window.open(url, '_blank');
            };

            //$scope.viewASN = function (asnNo, vendorCode) {
            //    var url = $state.href('viewPendingasn', { "asnNo": asnNo, "vendorCode": vendorCode, "isASN": false });

            //    $window.open(url, '_blank');
            //};

            $scope.viewASN = function (purchaseID, dispatchCode) {
                var url = $state.href("asnForm", { "poOrderId": purchaseID, "dCode": dispatchCode });
                window.open(url, '_blank');
            };




            $scope.saveAcknowledgementForm = function (obj) {
                $scope.ackExpectedDeldateError = '';
                $scope.ackcommentsError = '';
                //var ts = moment(obj.DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";
                //if (obj == undefined || obj == null) {
                //    $scope.ackError = 'Please select expected delivery date and enter comments.';
                //    return;
                //} else {
                //    if (obj.EXPECTED_DELIVERY_DATE == null || obj.EXPECTED_DELIVERY_DATE == undefined || obj.EXPECTED_DELIVERY_DATE == '') {
                //        $scope.ackExpectedDeldateError = 'Please select expected delivery date.';
                //        return
                //    }
                //    if (obj.COMMENTS == null || obj.COMMENTS == undefined || obj.COMMENTS == '') {
                //        $scope.ackcommentsError = 'Please enter comments.';
                //        return
                //    }
                //}

                if (obj.EXPECTED_DELIVERY_DATE == null || obj.EXPECTED_DELIVERY_DATE == undefined || obj.EXPECTED_DELIVERY_DATE == '') {
                    obj.EXPECTED_DELIVERY_DATE_TEMP = 'NOT_ACK_WITH_EXPECTED_DATE';
                } else {
                    obj.EXPECTED_DELIVERY_DATE_TEMP = 'ACK_WITH_EXPECTED_DATE';
                }

                //var ts = moment(obj.EXPECTED_DELIVERY_DATE, "DD-MM-YYYY").valueOf();
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.EXPECTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                //var ts = userService.toUTCTicks(obj.EXPECTED_DELIVERY_DATE);
                //var m = moment(ts);
                //var deliveryDate = new Date(m);
                //var milliseconds = parseInt(deliveryDate.getTime() / 1000.0);
                //obj.EXPECTED_DELIVERY_DATE = "/Date(" + milliseconds + "000+0530)/";

                var params = {
                    "vendordeliverydate": obj.EXPECTED_DELIVERY_DATE,
                    "vendordeliverydateString": obj.EXPECTED_DELIVERY_DATE_TEMP,
                        "poitemline": '',
                        "status": "ACKNOWLEDGE",
                        "ponumber": $scope.poNumber,
                        "user": userService.getUserId(),
                        "comments": obj.COMMENTS,
                    "isVendPoAck": $scope.ackPO.isPOAck,
                    "vendorAttachments": $scope.vendAttachments.vendorMultipleAttachments,
                    "ExistingVendorAttachments": obj.vendAttachArray,
                    "sessionid": $scope.sessionID

                };

                poService.SaveVendorAck(params).then(function (response) {
                    if (!response.errorMessage) {
                        angular.element('#acknowledgeModal').modal('hide');
                        growlService.growl("Vendor Acknowledged Successfully", "success");
                        location.reload();
                    }
                    else
                        growlService.growl("Error saving Acknowledgement.", "inverse");
                });

            };

            $scope.openAckModel = function (obj) {
                if (obj.isPOAck) {
                    $scope.ackExpectedDeldateError = '';
                    $scope.ackcommentsError = '';
                    angular.element('#acknowledgeModal').modal('show');
                }
                
            }

            $scope.closeAcknowledgementForm = function () {
                if ($scope.filteredPendingPOsList) {
                  
                    $scope.filteredPendingPOsList.EXPECTED_DELIVERY_DATE = '';
                    $scope.filteredPendingPOsList.COMMENTS = '';
                    $scope.vendAttachments.vendorMultipleAttachments = [];
                   // $scope.ackPO.isPOAck = 0;
                    angular.element('#acknowledgeModal').modal('hide');
                } else {
                    $scope.vendAttachments.vendorMultipleAttachments = [];
                    //$scope.ackPO.isPOAck = 0;
                    angular.element('#acknowledgeModal').modal('hide');
                }
               
                
            };

            $scope.removeAttach = function (index, item) {
                item.multipleAttachments.splice(index, 1);
            }
            $scope.removeVendAttach = function (index, item) {
                item.vendorAttachmentsArray.splice(index, 1);
            }
            $scope.removeVendAttachPopUp = function (index, item) {
                item.vendorMultipleAttachments.splice(index, 1);
            }
            $scope.removeAttach1 = function (index, item) {
                item.attachmentsArray.splice(index, 1);
            }

            $scope.getFile1 = function (id, itemid, ext) {
                $scope.filesTemp = $("#" + id)[0].files;
                $scope.filesTemp = Object.values($scope.filesTemp);
                $scope.filesTemp.forEach(function (attach, attachIndex) {
                    $scope.file = $("#" + id)[0].files[attachIndex];

                    fileReader.readAsDataUrl($scope.file, $scope)
                        .then(function (result) {
                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: 0
                            };
                            var bytearray = new Uint8Array(result);
                            fileUpload.fileStream = $.makeArray(bytearray);
                            fileUpload.fileName = attach.name;
                            if (id == "vendorAttach") {
                                if ( !$scope.vendAttachments.vendorMultipleAttachments) {
                                    $scope.vendAttachments.vendorMultipleAttachments = [];
                                }

                                var ifExists = _.findIndex($scope.vendAttachments.vendorMultipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.vendAttachments.vendorMultipleAttachments.push(fileUpload);
                                }
                            } else {
                                if (!$scope.filteredPendingPOsList.multipleAttachments) {
                                    $scope.filteredPendingPOsList.multipleAttachments = [];
                                }

                                var ifExists = _.findIndex($scope.filteredPendingPOsList.multipleAttachments, function (attach) { return attach.fileName.toLowerCase() === fileUpload.fileName.toLowerCase() });
                                if (ifExists <= -1) {
                                    $scope.filteredPendingPOsList.multipleAttachments.push(fileUpload);
                                }
                            }
                          

                        });
                })
            }

            $scope.saveAttachments = function (item) {
                if ($scope.filteredPendingPOsList.multipleAttachments.length == 0) {
                    growlService.growl("Please upload attachments.", "inverse");
                    return;
                }
                var params1 = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": item.VENDOR_ID,
                        "attachmentsArray": $scope.filteredPendingPOsList.multipleAttachments,
                        "SessionID": $scope.sessionID
                    }
                };

                PRMPOService.savepoattachments(params1)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPendingPOOverall();
                        }

                    });
            };

            $scope.PoApproval = function (item, approveObj) {
                if (approveObj.isApproveVendorPo == false) {
                    if (approveObj.isApproveRejectComments == '' || approveObj.isApproveRejectComments == null || approveObj.isApproveRejectComments == undefined) {
                        swal("Error!", "Please enter reject comments.", "error");
                        return;
                    }
                }
                var params = {
                    details: {
                        "PO_NUMBER": item.PO_NUMBER,
                        "VENDOR_CODE": item.VENDOR_CODE,
                        "VENDOR_ID": item.VENDOR_ID,
                        "attachmentsArray": $scope.filteredPendingPOsList.multipleAttachments,
                        "SessionID": $scope.sessionID
                    },
                    isPoApprove: approveObj.isApproveVendorPo,
                    isPoRejectComments: approveObj.isApproveRejectComments,
                    currentUserID: $scope.userID
                };

                PRMPOService.poApproval(params)
                    .then(function (response) {

                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("Saved Successfully.", "success");
                            $scope.getPendingPOOverall();
                        }

                    });
            }

        }]);