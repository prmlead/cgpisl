﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class PRItems : Entity
    {

        [DataMember] [DataNames("ITEM_ID")] public int ITEM_ID { get; set; }
        [DataMember] [DataNames("PR_ID")] public int PR_ID { get; set; }
        [DataMember] [DataNames("PR_NUMBER")] public string PR_NUMBER { get; set; }
        [DataMember] [DataNames("ITEM_NAME")] public string ITEM_NAME { get; set; }
        [DataMember] [DataNames("ITEM_CODE")] public string ITEM_CODE { get; set; }
        [DataMember] [DataNames("ITEM_DESC")] public string ITEM_DESC { get; set; }
        [DataMember] [DataNames("ITEM_CODE_CAS")] public string ITEM_CODE_CAS { get; set; }
        [DataMember] [DataNames("ITEM_CODE_MFCD")] public string ITEM_CODE_MFCD { get; set; }
        [DataMember] [DataNames("ITEM_NUM")] public string ITEM_NUM { get; set; }
        //ITEM_NUM
        [DataMember] [DataNames("HSN_CODE")] public string HSN_CODE { get; set; }
        [DataMember] [DataNames("ITEM_DESCRIPTION")] public string ITEM_DESCRIPTION { get; set; }
        [DataMember] [DataNames("BRAND")] public string BRAND { get; set; }
        [DataMember] [DataNames("UNITS")] public string UNITS { get; set; }
        [DataMember] [DataNames("ALTERNATE_UNITS")] public string ALTERNATE_UNITS { get; set; }
        [DataMember] [DataNames("LEAD_TIME")] public string LEAD_TIME { get; set; }
        [DataMember] [DataNames("EXIST_QUANTITY")] public decimal EXIST_QUANTITY { get; set; }
        [DataMember] [DataNames("REQUIRED_QUANTITY")] public decimal REQUIRED_QUANTITY { get; set; }
        [DataMember] [DataNames("ALTERNATE_QUANTITY")] public decimal ALTERNATE_QUANTITY { get; set; }
        [DataMember] [DataNames("UNIT_PRICE")] public decimal UNIT_PRICE { get; set; }
        [DataMember] [DataNames("C_GST_PERCENTAGE")] public decimal C_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("S_GST_PERCENTAGE")] public decimal S_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("I_GST_PERCENTAGE")] public decimal I_GST_PERCENTAGE { get; set; }
        [DataMember] [DataNames("TOTAL_PRICE")] public decimal TOTAL_PRICE { get; set; }
        [DataMember] [DataNames("COMMENTS")] public string COMMENTS { get; set; }
        [DataMember] [DataNames("ATTACHMENTS")] public string ATTACHMENTS { get; set; }
        [DataMember] [DataNames("REQ_ID")] public int REQ_ID { get; set; }
        [DataMember] [DataNames("REQ_NUMBER")] public string REQ_NUMBER { get; set; }
        
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }

        [DataMember] [DataNames("PRODUCT_ID")] public int PRODUCT_ID { get; set; }


        [DataMember] [DataNames("itemAttachment")] public byte[] itemAttachment { get; set; }
        [DataMember] [DataNames("attachmentName")] public string attachmentName { get; set; }

        [DataMember] [DataNames("DEPARTEMENT")] public string DEPARTEMENT { get; set; }
        [DataMember] [DataNames("DEPT_CODE")] public string DEPT_CODE { get; set; }
        [DataMember] [DataNames("PLANT")] public string PLANT { get; set; }
        [DataMember] [DataNames("DOC_TYPE")] public string DOC_TYPE { get; set; }
        [DataMember] [DataNames("DELIVERY_DATE")] public DateTime? DELIVERY_DATE { get; set; }
        [DataMember] [DataNames("REQUESITION_DATE")] public DateTime? REQUESITION_DATE { get; set; }
        [DataMember] [DataNames("PR_CHANGE_DATE")] public DateTime? PR_CHANGE_DATE { get; set; }
        [DataMember] [DataNames("DELIVERY_TEXT")] public string DELIVERY_TEXT { get; set; }
        [DataMember] [DataNames("U_NAME")] public string U_NAME { get; set; }
        [DataMember] [DataNames("CREATED_DATE")] public DateTime? CREATED_DATE { get; set; }
        [DataMember] [DataNames("MODIFIED_DATE")] public DateTime? MODIFIED_DATE { get; set; }        
        [DataMember] [DataNames("PURCHASE_GROUP")] public string PURCHASE_GROUP { get; set; }
        [DataMember] [DataNames("CategoryCode")] public string CategoryCode { get; set; }
        [DataMember] [DataNames("CreatedBy")] public string CreatedBy { get; set; }
        [DataMember] [DataNames("ITEM_TEXT")] public string ITEM_TEXT { get; set; }
        [DataMember] [DataNames("PO_TEXT")] public string PO_TEXT { get; set; }
        [DataMember] [DataNames("ProductId")] public int ProductId { get; set; }

        [DataMember] [DataNames("CategoryName")] public string CategoryName { get; set; }
        [DataMember] [DataNames("IS_VALID")] public int IS_VALID { get; set; }

        [DataMember] [DataNames("CONTRACT_VENDOR")] public int CONTRACT_VENDOR { get; set; }
        
        [DataMember][DataNames("GMP_NGMP")] public string GMP_NGMP { get; set; }
        [DataMember] [DataNames("MATERIAL_TYPE")] public string MATERIAL_TYPE { get; set; }
        [DataMember] [DataNames("PR_NOTE")] public string PR_NOTE { get; set; }
        [DataMember] [DataNames("PR_TYPE")] public string PR_TYPE { get; set; }
        [DataMember] [DataNames("PR_TYPE_DESC")] public string PR_TYPE_DESC { get; set; }
        [DataMember] [DataNames("CASNR")] public string CASNR { get; set; }
        [DataMember] [DataNames("MFCD_NUMBER")] public string MFCD_NUMBER { get; set; }
        [DataMember] [DataNames("ITEM_CATEGORY")] public string ITEM_CATEGORY { get; set; }


        /**** MOVED FROM OVER ALL TO ITEM LEVEL ****/

        [DataMember] [DataNames("REQUISITIONER_ID")] public string REQUISITIONER_ID { get; set; }
        [DataMember] [DataNames("REQUISITIONER_NAME")] public string REQUISITIONER_NAME { get; set; }
        [DataMember] [DataNames("REQUISITIONER_EMAIL")] public string REQUISITIONER_EMAIL { get; set; }
        //[DataMember] [DataNames("PR_CREATOR_NAME")] public string PR_CREATOR_NAME { get; set; }

        /**** MOVED FROM OVER ALL TO ITEM LEVEL ****/


        /**** NEW FIELDS ADDED ****/

        [DataMember] [DataNames("MATERIAL_DESCRIPTION")] public string MATERIAL_DESCRIPTION { get; set; }
        [DataMember] [DataNames("UOM")] public string UOM { get; set; }
        [DataMember] [DataNames("ACCOUNT_ASSIGNMENT_CATEGORY")] public string ACCOUNT_ASSIGNMENT_CATEGORY { get; set; }
        [DataMember] [DataNames("LOEKZ")] public string LOEKZ { get; set; }
        [DataMember] [DataNames("ERDAT")] public string ERDAT { get; set; }
        [DataMember] [DataNames("EBAKZ")] public string EBAKZ { get; set; }
        [DataMember] [DataNames("QTY_REQUIREDFieldSpecified")] public int QTY_REQUIREDFieldSpecified { get; set; }
        [DataMember] [DataNames("ALTERNATE_QTYFieldSpecified")] public int ALTERNATE_QTYFieldSpecified { get; set; }
        [DataMember] [DataNames("SERVICE_QTYFieldSpecified")] public int SERVICE_QTYFieldSpecified { get; set; }

        //[DataMember] [DataNames("PURCHASE_GROUP_CODE")] public string PURCHASE_GROUP_CODE { get; set; }
        //[DataMember] [DataNames("PURCHASE_GROUP_NAME")] public string PURCHASE_GROUP_NAME { get; set; }

        [DataMember] [DataNames("SHORT_TEXT")] public string SHORT_TEXT { get; set; }

        [DataMember] [DataNames("IS_MODIFIED")] public int IS_MODIFIED { get; set; }

        [DataMember] [DataNames("RFQ_ID")] public int RFQ_ID { get; set; }
        [DataMember] [DataNames("PR_ITEM_TYPE")] public string PR_ITEM_TYPE { get; set; }
        

        /**** NEW FIELDS ADDED ****/


        /********  CONSOLIDATE PR ********/

        [DataMember] [DataNames("OVERALL_ITEM_QUANTITY")] public decimal OVERALL_ITEM_QUANTITY { get; set; }
        [DataMember] [DataNames("TOTAL_PR_COUNT")] public int TOTAL_PR_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_PR_ITEM_COUNT")] public int TOTAL_PR_ITEM_COUNT { get; set; }
        /********  CONSOLIDATE PR ********/


        /******** SHUFFLED COLUMNS *******/
        [DataMember] [DataNames("WBS_CODE")] public string WBS_CODE { get; set; }
        [DataMember] [DataNames("PROJECT_DESCRIPTION")] public string PROJECT_DESCRIPTION { get; set; }
        [DataMember] [DataNames("PROFIT_CENTER")] public string PROFIT_CENTER { get; set; }
        [DataMember] [DataNames("SECTION_HEAD")] public string SECTION_HEAD { get; set; }
        [DataMember] [DataNames("PROJECT_TYPE")] public string PROJECT_TYPE { get; set; }
        [DataMember] [DataNames("SUB_VERTICAL")] public string SUB_VERTICAL { get; set; }
        /******** SHUFFLED COLUMNS *******/

    }
}