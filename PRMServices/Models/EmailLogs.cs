﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class EmailLogs : Entity
    {
        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "compID")]
        public int CompID { get; set; }

        [DataMember(Name = "reqID")]
        public int ReqID { get; set; }


        [DataMember(Name = "customerID")]
        public int CustomerID { get; set; }

        [DataMember(Name = "v_User_ID")]
        public int V_User_ID { get; set; }

        [DataMember(Name = "altUserID")]
        public int AltUserID { get; set; }

        string userName = string.Empty;
        [DataMember(Name = "userName")]
        public string UserName
        {
            get
            {
                return userName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    userName = value;
                }
            }
        }

        string firstName = string.Empty;
        [DataMember(Name = "firstName")]
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    firstName = value;
                }
            }
        }

        string lastName = string.Empty;
        [DataMember(Name = "lastName")]
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    lastName = value;
                }
            }
        }

        [DataMember(Name = "email")]
        public string Email { get; set; }

        string phoneNum = string.Empty;
        [DataMember(Name = "phoneNum")]
        public string PhoneNum
        {
            get
            {
                return phoneNum;
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    phoneNum = value;
                }
            }
        }

        //[DataMember(Name = "altEmail")]
        //public string AltEmail { get; set; }

        //[DataMember(Name = "altPhoneNum")]
        //public string AltPhoneNum { get; set; }


        string altEmail = string.Empty;
        [DataMember(Name = "altEmail")]
        public string AltEmail
        {
            get
            {
                if (!string.IsNullOrEmpty(altEmail))
                { return altEmail; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altEmail = value; }
            }
        }

        string altPhoneNum = string.Empty;
        [DataMember(Name = "altPhoneNum")]
        public string AltPhoneNum
        {
            get
            {
                if (!string.IsNullOrEmpty(altPhoneNum))
                { return altPhoneNum; }
                else
                { return ""; }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                { altPhoneNum = value; }
            }
        }

        [DataMember(Name = "company")]
        public string Company { get; set; }

        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "subject")]
        public string Subject { get; set; }

        [DataMember(Name = "reqStatus")]
        public string ReqStatus { get; set; }

        [DataMember(Name = "reqType")]
        public int ReqType { get; set; }

        [DataMember(Name = "requirement")]
        public string Requirement { get; set; }

        [DataMember(Name = "institution")]
        public string Institution { get; set; }

        [DataMember(Name = "messageDate")]
        public DateTime? MessageDate { get; set; }

        string timeZone = string.Empty;
        [DataMember(Name = "timeZone")]
        public string TimeZone
        {
            get
            {
                if (!string.IsNullOrEmpty(timeZone))
                {
                    return timeZone;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    timeZone = value;
                }
            }
        }

        [DataMember(Name = "DATETIME_NOW")]
        public DateTime? DATETIME_NOW { get; set; }

        [DataMember(Name = "CB_END_TIME")]
        public DateTime? CB_END_TIME { get; set; }

        [DataMember(Name = "CB_TIME_LEFT")]
        public long CB_TIME_LEFT { get; set; }

        [DataMember(Name = "startTime")]
        public DateTime? StartTime { get; set; }

        [DataMember(Name = "endTime")]
        public DateTime? EndTime { get; set; }

        [DataMember(Name = "timeLeft")]
        public long TimeLeft { get; set; }

        [DataMember(Name = "IS_CB_ENABLED")]
        public bool IS_CB_ENABLED { get; set; }

        [DataMember(Name = "REQ_NUMBER")]
        public string REQ_NUMBER { get; set; }

        
    }
}