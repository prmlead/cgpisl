﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;
using PRMServices.Models;


namespace PRMServices.Models
{
    public class SAPOEntity
    {
        [DataMember(Name = "COMP_ID")]
        [DataNames("COMP_ID")]
        public int COMP_ID { get; set; }

        [DataMember(Name = "LIFNR")]
        [DataNames("LIFNR")]
        public string LIFNR { get; set; }

        [DataMember(Name = "BUKRS")]
        [DataNames("BUKRS")]
        public string BUKRS { get; set; }

        [DataMember(Name = "EKORG")]
        [DataNames("EKORG")]
        public string EKORG { get; set; }

        [DataMember(Name = "ZTERM")]
        [DataNames("ZTERM")]
        public string ZTERM { get; set; }

        [DataMember(Name = "ERNAM")]
        [DataNames("ERNAM")]
        public string ERNAM { get; set; }

        [DataMember(Name = "INCO1")]
        [DataNames("INCO1")]
        public string INCO1 { get; set; }

        [DataMember(Name = "INCO2")]
        [DataNames("INCO2")]
        public string INCO2 { get; set; }

        [DataMember(Name = "EKGRP")]
        [DataNames("EKGRP")]
        public string EKGRP { get; set; }

        [DataMember(Name = "WAERS")]
        [DataNames("WAERS")]
        public string WAERS { get; set; }

        [DataMember(Name = "MEMORYTYPE")]
        [DataNames("MEMORYTYPE")]
        public string MEMORYTYPE { get; set; }

        [DataMember(Name = "DELIVERY_MODE")]
        [DataNames("DELIVERY_MODE")]
        public string DELIVERY_MODE { get; set; }

        [DataMember(Name = "ZZQANO")]
        [DataNames("ZZQANO")]
        public string ZZQANO { get; set; }

        [DataMember(Name = "ZZMAP_NON_GMP")]
        [DataNames("ZZMAP_NON_GMP")]
        public string ZZMAP_NON_GMP { get; set; }

        [DataMember(Name = "EINDT")]
        [DataNames("EINDT")]
        public string EINDT { get; set; }

        [DataMember(Name = "ITM_SP_INST")]
        [DataNames("ITM_SP_INST")]
        public string ITM_SP_INST { get; set; }

        [DataMember(Name = "WERKS")]
        [DataNames("WERKS")]
        public string WERKS { get; set; }

        [DataMember(Name = "MTART")]
        [DataNames("MTART")]
        public string MTART { get; set; }

        [DataMember(Name = "MATNR")]
        [DataNames("MATNR")]
        public string MATNR { get; set; }

        [DataMember(Name = "EMATN")]
        [DataNames("EMATN")]
        public string EMATN { get; set; }

        [DataMember(Name = "BANFN")]
        [DataNames("BANFN")]
        public string BANFN { get; set; }

        [DataMember(Name = "BNFPO")]
        [DataNames("BNFPO")]
        public string BNFPO { get; set; }

        [DataMember(Name = "MEINS")]
        [DataNames("MEINS")]
        public string MEINS { get; set; }
        [DataMember(Name = "BPRME")]
        [DataNames("BPRME")]
        public string BPRME { get; set; }

        [DataMember(Name = "PEINH")]
        [DataNames("PEINH")]
        public decimal PEINH { get; set; }

        [DataMember(Name = "PEINHFieldSpecified")]
        [DataNames("PEINHFieldSpecified")]
        public bool PEINHFieldSpecified { get; set; }

        [DataMember(Name = "IDNLF")]
        [DataNames("IDNLF")]
        public string IDNLF { get; set; }

        [DataMember(Name = "KONNR")]
        [DataNames("KONNR")]
        public string KONNR { get; set; }

        [DataMember(Name = "KTPNR")]
        [DataNames("KTPNR")]
        public string KTPNR { get; set; }

        [DataMember(Name = "MWSKZ")]
        [DataNames("MWSKZ")]
        public string MWSKZ { get; set; }

        [DataMember(Name = "PBXX")]
        [DataNames("PBXX")]
        public decimal PBXX { get; set; }

        [DataMember(Name = "PBXXFieldSpecified")]
        [DataNames("PBXXFieldSpecified")]
        public bool PBXXFieldSpecified { get; set; }

        [DataMember(Name = "RB00")]
        [DataNames("RB00")]
        public decimal RB00 { get; set; }

        [DataMember(Name = "RB00FieldSpecified")]
        [DataNames("RB00FieldSpecified")]
        public bool RB00FieldSpecified { get; set; }

        [DataMember(Name = "P_26F1")]
        [DataNames("P_26F1")]
        public decimal P_26F1 { get; set; }

        [DataMember(Name = "P_26F1FieldSpecified")]
        [DataNames("P_26F1FieldSpecified")]
        public bool P_26F1FieldSpecified { get; set; }

        [DataMember(Name = "P_26F2")]
        [DataNames("P_26F2")]
        public decimal P_26F2 { get; set; }

        [DataMember(Name = "P_26F2FieldSpecified")]
        [DataNames("P_26F2FieldSpecified")]
        public bool P_26F2FieldSpecified { get; set; }

        [DataMember(Name = "FRA2")]
        [DataNames("FRA2")]
        public decimal FRA2 { get; set; }

        [DataMember(Name = "FRA2FieldSpecified")]
        [DataNames("FRA2FieldSpecified")]
        public bool FRA2FieldSpecified { get; set; }

        [DataMember(Name = "FRA1")]
        [DataNames("FRA1")]
        public decimal FRA1 { get; set; }

        [DataMember(Name = "FRA1FieldSpecified")]
        [DataNames("FRA1FieldSpecified")]
        public bool FRA1FieldSpecified { get; set; }

        [DataMember(Name = "FRB1")]
        [DataNames("FRB1")]
        public decimal FRB1 { get; set; }

        [DataMember(Name = "FRB1FieldSpecified")]
        [DataNames("FRB1FieldSpecified")]
        public bool FRB1FieldSpecified { get; set; }

        [DataMember(Name = "FRB2")]
        [DataNames("FRB2")]
        public decimal FRB2 { get; set; }

        [DataMember(Name = "FRB2FieldSpecified")]
        [DataNames("FRB2FieldSpecified")]
        public bool FRB2FieldSpecified { get; set; }

        [DataMember(Name = "ZMS1")]
        [DataNames("ZMS1")]
        public decimal ZMS1 { get; set; }

        [DataMember(Name = "ZMS1FieldSpecified")]
        [DataNames("ZMS1FieldSpecified")]
        public bool ZMS1FieldSpecified { get; set; }

        [DataMember(Name = "ZMS2")]
        [DataNames("ZMS2")]
        public decimal ZMS2 { get; set; }

        [DataMember(Name = "ZMS2FieldSpecified")]
        [DataNames("ZMS2FieldSpecified")]
        public bool ZMS2FieldSpecified { get; set; }

        [DataMember(Name = "ZMS3")]
        [DataNames("ZMS3")]
        public decimal ZMS3 { get; set; }

        [DataMember(Name = "ZMS3FieldSpecified")]
        [DataNames("ZMS3FieldSpecified")]
        public bool ZMS3FieldSpecified { get; set; }

        [DataMember(Name = "ZMS4")]
        [DataNames("ZMS4")]
        public decimal ZMS4 { get; set; }

        [DataMember(Name = "ZMS4FieldSpecified")]
        [DataNames("ZMS4FieldSpecified")]
        public bool ZMS4FieldSpecified { get; set; }

        [DataMember(Name = "MENGE")]
        [DataNames("MENGE")]
        public decimal MENGE { get; set; }

        [DataMember(Name = "MENGEFieldSpecified")]
        [DataNames("MENGEFieldSpecified")]
        public bool MENGEFieldSpecified { get; set; }

        [DataMember(Name = "KDATB")]
        [DataNames("KDATB")]
        public string KDATB { get; set; }

        [DataMember(Name = "KDATE")]
        [DataNames("KDATE")]
        public string KDATE { get; set; }

        [DataMember(Name = "BSART")]
        [DataNames("BSART")]
        public string BSART { get; set; }

        [DataMember(Name = "J_1NBM")]
        [DataNames("J_1NBM")]
        public string J_1NBM { get; set; }

        [DataMember(Name = "REQ_ID")]
        [DataNames("REQ_ID")]
        public int REQ_ID { get; set; }

        [DataMember(Name = "PO_NO")]
        [DataNames("PO_NO")]
        public string PO_NO { get; set; }

        [DataMember(Name = "PO_TEMPLATE")]
        [DataNames("PO_TEMPLATE")]
        public string PO_TEMPLATE { get; set; }

        [DataMember(Name = "QUOT_NO")]
        [DataNames("QUOT_NO")]
        public string QUOT_NO { get; set; }

        [DataMember(Name = "USER")]
        [DataNames("USER")]
        public int USER { get; set; }

        [DataMember(Name = "DATE_CREATED")]
        [DataNames("DATE_CREATED")]
        public DateTime? DATE_CREATED { get; set; }

        [DataMember(Name = "DATE_MODIFIED")]
        [DataNames("DATE_MODIFIED")]
        public DateTime? DATE_MODIFIED { get; set; }

        [DataMember(Name = "NETPR")]
        [DataNames("NETPR")]
        public decimal NETPR { get; set; }

        [DataMember(Name = "SRVPOS")]
        [DataNames("SRVPOS")]
        public string SRVPOS { get; set; }

        [DataMember(Name = "EXTROW")]
        [DataNames("EXTROW")]
        public string EXTROW { get; set; }

        [DataMember(Name = "STEUC")]
        [DataNames("STEUC")]
        public string STEUC { get; set; }

        [DataMember(Name = "ZMIS")]
        [DataNames("ZMIS")]
        public decimal ZMIS { get; set; }

        [DataMember(Name = "JOFP")]
        [DataNames("JOFP")]
        public decimal JOFP { get; set; }

        [DataMember(Name = "JOFV")]
        [DataNames("JOFV")]
        public decimal JOFV { get; set; }

        [DataMember(Name = "EBTYP")]
        [DataNames("EBTYP")]
        public string EBTYP { get; set; }

        [DataMember(Name = "JINS")]
        [DataNames("JINS")]
        public string JINS { get; set; }

        [DataMember(Name = "ZZBONGING")]
        [DataNames("ZZBONGING")]
        public string ZZBONGING { get; set; }

        [DataMember(Name = "VENDOR_ID")]
        [DataNames("VENDOR_ID")]
        public int VENDOR_ID { get; set; }

        [DataMember(Name = "VENDOR_NAME")]
        [DataNames("VENDOR_NAME")]
        public string VENDOR_NAME { get; set; }

        [DataMember(Name = "INSURANCE_NUMBER")]
        [DataNames("INSURANCE_NUMBER")]
        public string INSURANCE_NUMBER { get; set; }

        [DataMember(Name = "PENALITY_PER_WEEK")]
        [DataNames("PENALITY_PER_WEEK")]
        public decimal PENALITY_PER_WEEK { get; set; }

        [DataMember(Name = "NUMBER_WEEKS")]
        [DataNames("NUMBER_WEEKS")]
        public decimal NUMBER_WEEKS { get; set; }

        [DataMember(Name = "MAX_PENALITY")]
        [DataNames("MAX_PENALITY")]
        public decimal MAX_PENALITY { get; set; }

        [DataMember(Name = "MAX_WEEKS")]
        [DataNames("MAX_WEEKS")]
        public decimal MAX_WEEKS { get; set; }

        [DataMember(Name = "PLANT_NAME")]
        [DataNames("PLANT_NAME")]
        public string PLANT_NAME { get; set; }

        [DataMember(Name = "PURCHASE_GROUP_NAME")]
        [DataNames("PURCHASE_GROUP_NAME")]
        public string PURCHASE_GROUP_NAME { get; set; }

        [DataMember(Name = "TOTAL_PO_COUNT")]
        [DataNames("TOTAL_PO_COUNT")]
        public int TOTAL_PO_COUNT { get; set; }

        [DataMember(Name = "QCS_ID")]
        [DataNames("QCS_ID")]
        public int QCS_ID { get; set; }

        [DataMember(Name = "PO_RAW_JSON")]
        [DataNames("PO_RAW_JSON")]
        public string PO_RAW_JSON { get; set; }

        [DataMember(Name = "QCS_VENDOR_ASSIGNMENT_JSON")]
        [DataNames("QCS_VENDOR_ASSIGNMENT_JSON")]
        public string QCS_VENDOR_ASSIGNMENT_JSON { get; set; }

        [DataMember(Name = "QCS_REQUIREMENT_JSON")]
        [DataNames("QCS_REQUIREMENT_JSON")]
        public string QCS_REQUIREMENT_JSON { get; set; }
        
    }
}