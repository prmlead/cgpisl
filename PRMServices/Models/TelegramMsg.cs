﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class TelegramMsg : Entity
    {
        [DataMember(Name = "message")]
        public string Message { get; set; }

        [DataMember(Name = "moduleName")]
        public string ModuleName { get; set; }

        [DataMember(Name = "group")]
        public string Group { get; set; }
    }
}