﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models
{
    [DataContract]
    public class MACVendorDetails : Entity, ICloneable
    {
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        [DataMember(Name = "vendorID")]
        public int VendorID { get; set; }

        [DataMember(Name = "requirementID")]
        public int RequirementID { get; set; }

        string vendorName = string.Empty;
        [DataMember(Name = "vendorName")]
        public string VendorName
        {
            get
            {
                return this.vendorName;
            }
            set
            {
                this.vendorName = value;
            }
        }
        
        [DataMember(Name = "initialPrice")]
        public double InitialPrice { get; set; }

        [DataMember(Name = "runningPrice")]
        public double RunningPrice { get; set; }

        [DataMember(Name = "totalInitialPrice")]
        public double TotalInitialPrice { get; set; }

        [DataMember(Name = "totalRunningPrice")]
        public double TotalRunningPrice { get; set; }

        [DataMember(Name = "rank")]
        public int Rank { get; set; }

        [DataMember(Name = "city")]
        public string City { get; set; }

        [DataMember(Name = "taxes")]
        public double Taxes { get; set; }

        [DataMember(Name = "quotationUrl")]
        public string QuotationUrl { get; set; }


        [DataMember(Name = "rating")]
        public double Rating { get; set; }

        string companyName = string.Empty;
        [DataMember(Name = "companyName")]
        public string CompanyName
        {
            get
            {
                return this.companyName;
            }
            set
            {
                this.companyName = value;
            }
        }
        

        [DataMember(Name = "initialPriceWithOutTaxFreight")]
        public double InitialPriceWithOutTaxFreight { get; set; }

        [DataMember(Name = "vendorFreight")]
        public double VendorFreight { get; set; }

        [DataMember(Name = "warranty")]
        public string Warranty { get; set; }

        [DataMember(Name = "payment")]
        public string Payment { get; set; }

        [DataMember(Name = "duration")]
        public string Duration { get; set; }

        [DataMember(Name = "validity")]
        public string Validity { get; set; }

        [DataMember(Name = "isQuotationRejected")]
        public int IsQuotationRejected { get; set; }

        [DataMember(Name = "quotationRejectedComment")]
        public string QuotationRejectedComment { get; set; }

        [DataMember(Name = "PO")]
        public RequirementPO PO { get; set; }


        

        //  REV_QUOTATION_URL, REV_PRICE, REV_TAX, REV_VEND_FREIGHT, REV_VEND_TOTAL_PRICE

        [DataMember(Name = "revquotationUrl")]
        public string RevQuotationUrl { get; set; }

        [DataMember(Name = "revPrice")]
        public double RevPrice { get; set; }

        [DataMember(Name = "revVendorFreight")]
        public double RevVendorFreight { get; set; }

        [DataMember(Name = "revVendorTotalPrice")]
        public double RevVendorTotalPrice { get; set; }

        [DataMember(Name = "isRevQuotationRejected")]
        public int IsRevQuotationRejected { get; set; }

        [DataMember(Name = "revQuotationRejectedComment")]
        public string RevQuotationRejectedComment { get; set; }

        [DataMember(Name = "lastActiveTime")]
        public string LastActiveTime { get; set; }

        [DataMember(Name = "discount")]
        public double Discount { get; set; }

        [DataMember(Name = "reductionPercentage")]
        public double ReductionPercentage { get; set; }

        [DataMember(Name = "otherProperties")]
        public string OtherProperties { get; set; }

        [DataMember(Name = "vendor")]
        public User Vendor { get; set; }

        [DataMember(Name = "materialDispachmentLink")]
        public int MaterialDispachmentLink { get; set; }

        [DataMember(Name = "materialReceivedLink")]
        public int MaterialReceivedLink { get; set; }

        [DataMember(Name = "landingPrice")]
        public decimal LandingPrice { get; set; }

        [DataMember(Name = "revLandingPrice")]
        public decimal RevLandingPrice { get; set; }

        [DataMember(Name = "techEvalScore")]
        public decimal TechEvalScore { get; set; }


        [DataMember(Name = "marginRankDiscount")]
        public double marginRankDiscount { get; set; }

        [DataMember(Name = "marginRankRevDiscount")]
        public double marginRankRevDiscount { get; set; }

        [DataMember(Name = "sumOfMargin")]
        public decimal SumOfMargin { get; set; }

        [DataMember(Name = "sumOfInitialMargin")]
        public decimal SumOfInitialMargin { get; set; }

        [DataMember(Name = "gstNumber")]
        public string GSTNumber { get; set; }

        [DataMember(Name = "quotationPrices")]
        public MACQuotationPrices QuotationPrices { get; set; }


        [DataMember(Name = "selectedVendorCurrency")]
        public string SelectedVendorCurrency { get; set; }

        [DataMember(Name = "packingChargesWithTax")]
        public double PackingChargesWithTax { get; set; }

        [DataMember(Name = "installationChargesWithTax")]
        public double InstallationChargesWithTax { get; set; }

        [DataMember(Name = "revinstallationChargesWithTax")]
        public double RevinstallationChargesWithTax { get; set; }

        [DataMember(Name = "revpackingChargesWithTax")]
        public double RevpackingChargesWithTax { get; set; }

        [DataMember(Name = "installationChargesTaxPercentage")]
        public double InstallationChargesTaxPercentage { get; set; }

        [DataMember(Name = "packingChargesTaxPercentage")]
        public double PackingChargesTaxPercentage { get; set; }





        [DataMember(Name = "freightCharges")]
        public double FREIGHT_CHARGES { get; set; }

        [DataMember(Name = "freightChargesTaxPercentage")]
        public double FREIGHT_CHARGES_TAX_PERCENTAGE { get; set; }

        [DataMember(Name = "freightChargesWithTax")]
        public double FREIGHT_CHARGES_WITH_TAX { get; set; }

        [DataMember(Name = "revfreightCharges")]
        public double REV_FREIGHT_CHARGES { get; set; }

        [DataMember(Name = "revfreightChargesWithTax")]
        public double REV_FREIGHT_CHARGES_WITH_TAX { get; set; }

        [DataMember(Name = "revVendorFreightCB")]
        public double RevVendorFreightCB { get; set; }

        [DataMember(Name = "vendorFreightCB")]
        public double VendorFreightCB { get; set; }




        [DataMember(Name = "revpackingChargesCB")]
        public double RevpackingChargesCB { get; set; }

        [DataMember(Name = "revpackingChargesWithTaxCB")]
        public double RevpackingChargesWithTaxCB { get; set; }



        [DataMember(Name = "revinstallationChargesCB")]
        public double RevinstallationChargesCB { get; set; }

        [DataMember(Name = "revinstallationChargesWithTaxCB")]
        public double RevinstallationChargesWithTaxCB { get; set; }

        [DataMember(Name = "revVendorTotalPriceCB")]
        public double RevVendorTotalPriceCB { get; set; }

    }
}