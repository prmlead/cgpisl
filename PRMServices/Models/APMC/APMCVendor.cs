﻿using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class APMCVendor : VendorDetails
    {
        [DataMember(Name = "apmcVendorID")]
        public int APMCVendorID { get; set; }

        [DataMember(Name = "apmcNegotiationID")]
        public int APMCNegotiationID { get; set; }

        [DataMember(Name = "apmcID")]
        public int APMCID { get; set; }        

        [DataMember(Name = "userID")]
        public int UserID { get; set; }

        [DataMember(Name = "company")]
        public Company Company { get; set; }

        [DataMember(Name = "companyID")]
        public int CompanyID { get; internal set; }

        [DataMember(Name = "apmcName")]
        public string APMCName { get; internal set; }
    }
}