﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace PRMServices.Models.Catalog
{
    [DataContract]
    public class EntityExt
    {
        string eMessage = string.Empty;
        [DataMember(Name = "errorMessage")]
        public string ErrorMessage
        {
            get
            {
                return this.eMessage;
            }
            set
            {
                this.eMessage = value;
            }
        }

        string sessionId = string.Empty;
        [DataMember(Name = "sessionID")]
        public string SessionID
        {
            get
            {
                return this.sessionId;
            }
            set
            {
                this.sessionId = value;
            }
        }

       // [DataMember(Name = "DateCreated")]
       // public DateTime DateCreated { get; set; }

        DateTime dateCreated = DateTime.MaxValue;
        [DataMember(Name = "dateCreated")]
        public DateTime DateCreated
        {
            get
            {
                return this.dateCreated;
            }
            set
            {
                this.dateCreated = value;
            }
        }

        DateTime dateModified = DateTime.MaxValue;
        [DataMember(Name = "dateModified")]
        public DateTime DateModified
        {
            get
            {
                return this.dateModified;
            }
            set
            {
                this.dateModified = value;
            }
        }

        [DataMember(Name = "CreatedBy")]
        public int CreatedBy { get; set; }

        [DataMember(Name = "ModifiedBy")]
        public int ModifiedBy { get; set; }


    }
}