﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class PaybackPeriodAnalysis : Entity
    {
        [DataMember(Name = "expectedNoOfProcedure")]
        public int ExpectedNoOfProcedure { get; set; }

        [DataMember(Name = "proposedTariffPerProcedure")]
        public int ProposedTariffPerProcedure { get; set; }

        [DataMember(Name = "approxRunningCost")]
        public int ApproxRunningCost { get; set; }

        [DataMember(Name = "paybackPeriod")]
        public int PaybackPeriod { get; set; }

    }
}