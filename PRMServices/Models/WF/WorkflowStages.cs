﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class WorkflowStages : ResponseAudit
    {
        [DataMember(Name = "stageID")]
        public int StageID { get; set; }

        [DataMember(Name = "workflowID")]
        public int WorkflowID { get; set; }

        [DataMember(Name = "approver")]
        public CompanyDesignations Approver { get; set; }

        [DataMember(Name = "department")]
        public CompanyDepartments Department { get; set; }

        [DataMember(Name = "order")]
        public int Order { get; set; }

        [DataMember(Name = "time")]
        public int Time { get; set; }

        [DataMember(Name = "approverRange")]
        public int ApproverRange { get; set; }
    }
}