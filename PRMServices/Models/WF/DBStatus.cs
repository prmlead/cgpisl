﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace PRMServices.Models
{
    [DataContract]
    public class DBStatus : ResponseAudit
    {
        [DataMember(Name = "noOfReqClosed")]
        public int NoOfReqClosed { get; set; }


        [DataMember(Name = "noOfCapexClosed")]
        public int NoOfCapexClosed { get; set; }

        [DataMember(Name = "noOfNonCapexClosed")]
        public int NoOfNonCapexClosed { get; set; }

        [DataMember(Name = "noOfWorkorderClosed")]
        public int NoOfWorkorderClosed { get; set; }


        [DataMember(Name = "noOfCapexPending")]
        public int NoOfCapexPending { get; set; }

        [DataMember(Name = "noOfNonCapexPending")]
        public int NoOfNonCapexPending { get; set; }

        [DataMember(Name = "noOfWorkorderPending")]
        public int NoOfWorkorderPending { get; set; }

    }
}