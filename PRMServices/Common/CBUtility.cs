﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using OfficeOpenXml;
using PRMServices.Models;
using PRMServices;

namespace PRMServices.Common
{

    public static class CBUtility
    {
        public static Response SaveCBPricesAlerts(VendorDetails auctionVendor, int userID, int isCustomer, string sessionID, string bidComments, int vendorID) {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            string ScreenName = "";

            Requirement reqVendor = prmservices.GetRequirementData(auctionVendor.RequirementID, vendorID, sessionID);
            UserInfo customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);
            UserInfo vendor = prmservices.GetUser(vendorID, sessionID);
            List<CBPrices> CBPricesAudit = new List<CBPrices>();
            CBPricesAudit = prmservices.GetCBPricesAudit(auctionVendor.RequirementID, vendorID, sessionID);

            int count = 0;
            CBPricesAudit = CBPricesAudit.Where(cba => cba.REQ_ID == auctionVendor.RequirementID && cba.V_ID == vendorID && cba.CREATED_BY != vendorID).ToList();
            count = CBPricesAudit.Count;

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;

            reqVendor.Module = ScreenName;



            if (isCustomer == 1)
            {

                if (count > 1)
                {
                    ScreenName = "CB_PRICE_UPDATED_CUSTOMER";
                    reqVendor.Module = ScreenName;
                    subject = " A COUNTER BID PRICE HAS BEEN UPDATED.";
                    bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBPricesVendorEMAIL");
                    bodySMS = prmservices.GenerateEmailBody("UpdateCBPricesVendorSMS");
                    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                    actionTelegaram = "COUNTER PRICE UPDATED";
                }
                else
                {
                    ScreenName = "CB_PRICE_OFFERED_CUSTOMER";
                    reqVendor.Module = ScreenName;
                    subject = "A NEW COUNTER BID HAS BEEN OFFERED";
                    bodyEMAIL = prmservices.GenerateEmailBody("SaveCBPricesVendorEMAIL");
                    bodySMS = prmservices.GenerateEmailBody("SaveCBPricesVendorSMS");
                    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                    actionTelegaram = "COUNTER PRICE OFFERED";
                }

                bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution, bidComments);
                prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                      "Req Number: " + reqVendor.RequirementNumber + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID,vendorID,reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution, bidComments);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]), reqVendor.RequirementID, vendorID, reqVendor.Module,
                //        sessionID).ConfigureAwait(false);

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, customer.Institution, vendor.Institution, "");

            }
            else {

                ScreenName = "CB_PRICE_OFFERED_VENDOR";
                reqVendor.Module = ScreenName;
                subject = "NEW OFFER FROM THE " + vendor.Institution + " RECEIVED";
                bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBPricesCustomerEMAIL");
                bodySMS = prmservices.GenerateEmailBody("UpdateCBPricesCustomerSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "COUNTER PRICE OFFERED";

                bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution, bidComments);
                prmservices.SendEmail(customer.Email + "," + customer.AltEmail,
                                      "Req Number: " + reqVendor.RequirementNumber + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(customer.UserID), reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution, bidComments);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]),
                //        sessionID).ConfigureAwait(false);

                //if (reqVendor.CustomerID != reqVendor.SuperUserID) {

                //    User superCustomer = new User();
                //    superCustomer = reqVendor.SuperUser;

                //    bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBPricesCustomerEMAIL");
                //    bodySMS = prmservices.GenerateEmailBody("UpdateCBPricesCustomerSMS");

                //    bodyEMAIL = String.Format(bodyEMAIL, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, bidComments);
                //    prmservices.SendEmail(superCustomer.Email + "," + superCustomer.AltEmail,
                //                          "ReqID: " + auctionVendor.RequirementID + " - " + subject,
                //                          bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(superCustomer.UserID), reqVendor.Module,
                //                          sessionID).ConfigureAwait(false);

                //    bodySMS = String.Format(bodySMS, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, bidComments);
                //    bodySMS = bodySMS.Replace("<br/>", "");

                //}

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, vendor.Institution, customer.Institution, "");

            }

            SendCBTelegramAlerts(bodyTELEGRAM);

            return response;

        }

        public static Response CBStopQuotationsAlerts(int userID, int reqID, string sessionID)
        {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            Requirement reqVendor = new Requirement();
            UserInfo customer = new UserInfo();
            UserInfo vendor = new UserInfo();

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;

            List<CBPrices> CBPricesAudit = new List<CBPrices>();

            reqVendor = prmservices.GetRequirementData(reqID, userID, sessionID);
            customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);

            reqVendor.AuctionVendors = reqVendor.AuctionVendors.Where(v => string.IsNullOrEmpty(v.QuotationUrl)).ToList();

            foreach (VendorDetails v in reqVendor.AuctionVendors)
            {
                vendor = prmservices.GetUser(v.VendorID, sessionID);

                subject = customer.Institution + " IS NO MORE ACCEPTING QUOTATIONS";
                bodyEMAIL = prmservices.GenerateEmailBody("CBStopQuotationsVendorEMAIL");
                bodySMS = prmservices.GenerateEmailBody("CBStopQuotationsVendorSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "STOP QUOTATIONS";

                bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution);
                prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                        "REQ-ID: " + reqID + " - " + subject,
                                        bodyEMAIL, reqVendor.RequirementID, v.VendorID,
                                        sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]),
                //        sessionID).ConfigureAwait(false);

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementID, actionTelegaram, customer.Institution, vendor.Institution, "");
                SendCBTelegramAlerts(bodyTELEGRAM);
            }

            return response;

        }

        public static Response UpdateCBTimeAlerts(DateTime cbEndTime, int userID, int reqID, string sessionID)
        {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            string ScreenName = "";

            Requirement reqVendor = new Requirement();
            UserInfo customer = new UserInfo();
            UserInfo vendor = new UserInfo();

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;

            List<CBPrices> CBPricesAudit = new List<CBPrices>();

            reqVendor = prmservices.GetRequirementData(reqID, userID, sessionID);
            customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);

            reqVendor.AuctionVendors = reqVendor.AuctionVendors.Where(v => v.IsQuotationRejected == 0).ToList();

            foreach (VendorDetails v in reqVendor.AuctionVendors)
            {
                vendor = prmservices.GetUser(v.VendorID, sessionID);

                if (vendor.Institution != "PRICE_CAP")
                {
                    ScreenName = "CB_UPDATE_ENDTIME";
                    reqVendor.Module = ScreenName;
                    subject = "END TIME HAS BEEN UPDATED.";
                    bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBTimeVendorEMAIL");
                    bodySMS = prmservices.GenerateEmailBody("UpdateCBTimeVendorSMS");
                    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                    actionTelegaram = "UPDATED END TIME";

                    //bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution, cbEndTime);
                    bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution, prmservices.toLocal(cbEndTime));
                    prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                            "REQ-Number: " + reqVendor.RequirementNumber + " - " + subject,
                                            bodyEMAIL, reqVendor.RequirementID, v.VendorID, reqVendor.Module,
                                            sessionID).ConfigureAwait(false);

                    //bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution, cbEndTime);
                    bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementID, reqVendor.Title, customer.Institution, prmservices.toLocal(cbEndTime));
                    bodySMS = bodySMS.Replace("<br/>", "");
                    //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                    //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                    //        StringSplitOptions.None)[0]), reqVendor.RequirementID, v.VendorID, reqVendor.Module,
                    //        sessionID).ConfigureAwait(false);

                    bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementID, actionTelegaram, customer.Institution, vendor.Institution, "");
                    SendCBTelegramAlerts(bodyTELEGRAM);
                }

                //#region TO CUSTOMER SUB USER
                //subject = "END TIME HAS BEEN UPDATED.";
                //bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBTimeCustomerEMAIL");
                //bodySMS = prmservices.GenerateEmailBody("UpdateCBTimeCustomerSMS");
                //bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                //actionTelegaram = "CB END TIME UPDATE";

                //bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, prmservices.toLocal(cbEndTime));
                //prmservices.SendEmail(customer.Email + "," + customer.AltEmail,
                //                        "REQ-ID: " + reqID + " - " + subject,
                //                        bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(customer.UserID), "CB_UPDATE_ENDTIME",
                //                        sessionID).ConfigureAwait(false);

                //bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, prmservices.toLocal(cbEndTime));
                //bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]), reqVendor.RequirementID, Convert.ToInt32(customer.UserID), "CB_UPDATE_ENDTIME",
                //        sessionID).ConfigureAwait(false);
                //#endregion TO CUSTOMER SUB USER

                //#region TO CUSTOMER SUPER USER
                //if (reqVendor.CustomerID != reqVendor.SuperUserID)
                //{
                //    User superCustomer = new User();
                //    superCustomer = reqVendor.SuperUser;

                //    subject = "END TIME HAS BEEN UPDATED.";
                //    bodyEMAIL = prmservices.GenerateEmailBody("UpdateCBTimeCustomerEMAIL");
                //    bodySMS = prmservices.GenerateEmailBody("UpdateCBTimeCustomerSMS");
                //    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                //    actionTelegaram = "CB END TIME UPDATE";

                //    bodyEMAIL = String.Format(bodyEMAIL, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, prmservices.toLocal(cbEndTime));
                //    prmservices.SendEmail(superCustomer.Email + "," + superCustomer.AltEmail,
                //                            "REQ-ID: " + reqID + " - " + subject,
                //                            bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(reqVendor.SuperUserID), "CB_UPDATE_ENDTIME",
                //                            sessionID).ConfigureAwait(false);

                //    bodySMS = String.Format(bodySMS, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution, prmservices.toLocal(cbEndTime));
                //    bodySMS = bodySMS.Replace("<br/>", "");
                //    prmservices.SendSMS(string.Empty, superCustomer.PhoneNum + "," + superCustomer.AltPhoneNum,
                //            System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //            StringSplitOptions.None)[0]), reqVendor.RequirementID, Convert.ToInt32(reqVendor.SuperUserID), "CB_UPDATE_ENDTIME",
                //            sessionID).ConfigureAwait(false);
                //}
                //#endregion TO CUSTOMER SUPER USER
            }

            return response;

        }

        public static Response FreezeCounterBidAlerts(int reqID, int vendorID, string sessionID, string freezedBy)
        {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            string ScreenName = "";

            Requirement reqVendor = prmservices.GetRequirementData(reqID, vendorID, sessionID);
            UserInfo customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);
            UserInfo vendor = prmservices.GetUser(vendorID, sessionID);

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;
                    
                        
            if (freezedBy == "CUSTOMER")
            {
                ScreenName = "CB_FREEZE_CUSTOMER";
                reqVendor.Module = ScreenName;
                subject = customer.Institution + " IS NO MORE ACCEPTING NEW PRICES.";
                bodyEMAIL = prmservices.GenerateEmailBody("FreezeCounterBidVendorEMAIL");
                bodySMS = prmservices.GenerateEmailBody("FreezeCounterBidVendorSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "FREEZE COUNTER BID";

                bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                      "Req Number: " + reqVendor.RequirementNumber + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID, vendorID, reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]),
                //        sessionID).ConfigureAwait(false);

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, customer.Institution, vendor.Institution, "");

            }
            else
            {
                ScreenName = "CB_FREEZE_VENDOR";
                reqVendor.Module = ScreenName;
                subject = vendor.Institution + " SUBMITTED HIS FINAL PRICES.";
                bodyEMAIL = prmservices.GenerateEmailBody("FreezeCounterBidCustomerEMAIL");
                bodySMS = prmservices.GenerateEmailBody("FreezeCounterBidCustomerSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "SUBMITTED FINAL PRICE";

                bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution);
                prmservices.SendEmail(customer.Email + "," + customer.AltEmail,
                                      "Req Number: " + reqVendor.RequirementNumber + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID,Convert.ToInt32(customer.UserID), reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]),
                //        sessionID).ConfigureAwait(false);


                //if (reqVendor.CustomerID != reqVendor.SuperUserID)
                //{

                //    User superCustomer = new User();
                //    superCustomer = reqVendor.SuperUser;

                //    bodyEMAIL = prmservices.GenerateEmailBody("FreezeCounterBidCustomerEMAIL");
                //    bodySMS = prmservices.GenerateEmailBody("FreezeCounterBidCustomerSMS");
                //    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                //    actionTelegaram = "FREEZE COUNTER BID";

                //    bodyEMAIL = String.Format(bodyEMAIL, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                //    prmservices.SendEmail(superCustomer.Email + "," + superCustomer.AltEmail,
                //                          "ReqID: " + reqVendor.RequirementID + " - " + subject,
                //                          bodyEMAIL, reqVendor.RequirementID,vendorID,
                //                          sessionID).ConfigureAwait(false);

                //    bodySMS = String.Format(bodySMS, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
                //    bodySMS = bodySMS.Replace("<br/>", "");
                //}

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, vendor.Institution, customer.Institution, "");

            }

            SendCBTelegramAlerts(bodyTELEGRAM);

            return response;

        }

        public static Response UnFreezeCounterBidAlerts(int reqID, int vendorID, string sessionID, string freezedBy)
        {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            string ScreenName = "";

            Requirement reqVendor = prmservices.GetRequirementData(reqID, vendorID, sessionID);
            UserInfo customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);
            UserInfo vendor = prmservices.GetUser(vendorID, sessionID);

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;

            if (freezedBy == "CUSTOMER")
            {
                ScreenName = "CB_UNFREEZE_CUSTOMER";
                reqVendor.Module = ScreenName;
                subject = "RFQ bid process re-opened.";
                bodyEMAIL = prmservices.GenerateEmailBody("UnFreezeCounterBidVendorEMAIL");
                bodySMS = prmservices.GenerateEmailBody("UnFreezeCounterBidVendorSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "UNFREEZE COUNTER BID";

                bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                      "Req Number: " + reqVendor.RequirementNumber + " - " + subject,
                                      bodyEMAIL, reqVendor.RequirementID, vendorID, reqVendor.Module,
                                      sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]),
                //        sessionID).ConfigureAwait(false);

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, customer.Institution, vendor.Institution, "");

            }
            
            SendCBTelegramAlerts(bodyTELEGRAM);

            return response;

        }

        public static Response CBMarkAsCompleteAlerts(int isCBCompleted, int userID, int reqID, string sessionID)
        {

            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            Requirement reqVendor = new Requirement();
            UserInfo customer = new UserInfo();
            UserInfo vendor = new UserInfo();

            List<CBPrices> CBPricesAudit = new List<CBPrices>();

            string subject = string.Empty;
            string bodyEMAIL = string.Empty;
            string bodySMS = string.Empty;
            string bodyTELEGRAM = string.Empty;
            string actionTelegaram = string.Empty;

            reqVendor = prmservices.GetRequirementData(reqID, userID, sessionID);
            customer = prmservices.GetUser(reqVendor.CustomerID, sessionID);

            reqVendor.AuctionVendors = reqVendor.AuctionVendors.Where(v => v.IsQuotationRejected == 0).ToList();

            foreach (VendorDetails v in reqVendor.AuctionVendors)
            {
                vendor = prmservices.GetUser(v.VendorID, sessionID);

                subject = customer.Institution + " IS NO MORE ACCEPTING QUOTATIONS.";
                bodyEMAIL = prmservices.GenerateEmailBody("CBMarkAsCompleteVendorEMAIL");
                bodySMS = prmservices.GenerateEmailBody("CBMarkAsCompleteVendorSMS");
                bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
                actionTelegaram = "MARK AS COMPLETE";

                bodyEMAIL = String.Format(bodyEMAIL, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                prmservices.SendEmail(vendor.Email + "," + vendor.AltEmail,
                                        "REQ-NUMBER: " + reqVendor.RequirementNumber + " - " + subject,
                                        bodyEMAIL, reqVendor.RequirementID, v.VendorID,
                                        sessionID).ConfigureAwait(false);

                bodySMS = String.Format(bodySMS, vendor.FirstName, vendor.LastName, reqVendor.RequirementNumber, reqVendor.Title, customer.Institution);
                bodySMS = bodySMS.Replace("<br/>", "");
                //prmservices.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum,
                //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
                //        StringSplitOptions.None)[0]), reqVendor.RequirementID, v.VendorID,
                //        sessionID).ConfigureAwait(false);

                bodyTELEGRAM = string.Format(bodyTELEGRAM, reqVendor.RequirementNumber, actionTelegaram, customer.Institution, vendor.Institution, "");
                SendCBTelegramAlerts(bodyTELEGRAM);

            }

            #region TO CUSTOMER SUB USER
            subject = "REQUIREMENT IS MARKED AS COMPLETED.";
            bodyEMAIL = prmservices.GenerateEmailBody("CBMarkAsCompleteCustomerEMAIL");
            bodySMS = prmservices.GenerateEmailBody("CBMarkAsCompleteCustomerSMS");
            bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
            actionTelegaram = "MARK AS COMPLETE";

            bodyEMAIL = String.Format(bodyEMAIL, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution);
            prmservices.SendEmail(customer.Email + "," + customer.AltEmail,
                                    "REQ-NUMBER: " + reqVendor.RequirementNumber + " - " + subject,
                                    bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(customer.UserID),
                                    sessionID).ConfigureAwait(false);

            bodySMS = String.Format(bodySMS, customer.FirstName, customer.LastName, reqVendor.RequirementNumber, reqVendor.Title, vendor.Institution);
            bodySMS = bodySMS.Replace("<br/>", "");
            //prmservices.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum,
            //        System.Web.HttpUtility.UrlEncode(bodySMS.Split(new string[] { "Thank You" },
            //        StringSplitOptions.None)[0]),
            //        sessionID).ConfigureAwait(false);
            #endregion TO CUSTOMER SUB USER

            #region TO CUSTOMER SUPER USER
            //if (reqVendor.CustomerID != reqVendor.SuperUserID)
            //{
            //    User superCustomer = new User();
            //    superCustomer = reqVendor.SuperUser;

            //    subject = "REQUIREMENT IS MARKED AS COMPLETED.";
            //    bodyEMAIL = prmservices.GenerateEmailBody("CBMarkAsCompleteCustomerEMAIL");
            //    bodySMS = prmservices.GenerateEmailBody("CBMarkAsCompleteCustomerSMS");
            //    bodyTELEGRAM = prmservices.GenerateEmailBody("UniqeTemplateTELEGRAM");
            //    actionTelegaram = "MARK AS COMPLETE";

            //    bodyEMAIL = String.Format(bodyEMAIL, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
            //    prmservices.SendEmail(superCustomer.Email + "," + superCustomer.AltEmail,
            //                            "REQ-ID: " + reqID + " - " + subject,
            //                            bodyEMAIL, reqVendor.RequirementID, Convert.ToInt32(superCustomer.UserID),
            //                            sessionID).ConfigureAwait(false);

            //    bodySMS = String.Format(bodySMS, superCustomer.FirstName, superCustomer.LastName, reqVendor.RequirementID, reqVendor.Title, vendor.Institution);
            //    bodySMS = bodySMS.Replace("<br/>", "");
            //}
            #endregion TO CUSTOMER SUPER USER

            //SendCBTelegramAlerts(bodyTELEGRAM);

            return response;

        }

        public static Response SendCBTelegramAlerts(string bodyTelegram)
        {
            Response response = new Response();
            PRMServices prmservices = new PRMServices();

            bodyTelegram = bodyTelegram.Replace("<br/>", "");
            bodyTelegram = bodyTelegram.Replace("<br />", "");

            TelegramMsg tgMsg = new TelegramMsg();
            tgMsg.Message = bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0];
            prmservices.SendTelegramMsg(tgMsg);

            return response;
        }
    }
}