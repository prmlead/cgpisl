﻿using System;
using System.Runtime.Serialization;
using PRM.Core.Common;

namespace PRMServices.Models
{
    [DataContract]
    public class ExcelUpload : Entity
    {
        [DataMember] [DataNames("UI_COLUMN_NAME")] public string UI_COLUMN_NAME { get; set; }
        [DataMember] [DataNames("DB_COLUMN_NAME")] public string DB_COLUMN_NAME { get; set; }
        [DataMember] [DataNames("COLUMN_DATA_TYPE")] public string COLUMN_DATA_TYPE { get; set; }
        [DataMember] [DataNames("IS_MANDATE")] public int IS_MANDATE { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("TEMPLATE_NAME")] public string TEMPLATE_NAME { get; set; }
        [DataMember] [DataNames("FETCH_QUERY")] public string FETCH_QUERY { get; set; }
        [DataMember] [DataNames("SHEET_NAME")] public string SHEET_NAME { get; set; }
        [DataMember] [DataNames("DATA_LENGTH")] public string DATA_LENGTH { get; set; }
        [DataMember] [DataNames("IS_DROP_DOWN_REQUIRED")] public int IS_DROP_DOWN_REQUIRED { get; set; }
        [DataMember] [DataNames("DROP_DOWN_VALUES")] public string DROP_DOWN_VALUES { get; set; }
        [DataMember] [DataNames("IS_JOB_ID_REQUIRED")] public int IS_JOB_ID_REQUIRED { get; set; }
        [DataMember] [DataNames("IS_PHONE")] public int IS_PHONE { get; set; }

    }

    public class ExcelErrorUpload : Entity
    {
        [DataMember] [DataNames("JOB_ID")] public Guid JOB_ID { get; set; }
        [DataMember] [DataNames("REASON")] public string REASON { get; set; }
        [DataMember] [DataNames("TEMPLATE_NAME")] public string TEMPLATE_NAME { get; set; }
        [DataMember] [DataNames("COLUMN_NAME")] public string COLUMN_NAME { get; set; }
        [DataMember] [DataNames("COLUMN_DATA_TYPE")] public string COLUMN_DATA_TYPE { get; set; }
        [DataMember] [DataNames("COMP_ID")] public int COMP_ID { get; set; }
        [DataMember] [DataNames("U_ID")] public int U_ID { get; set; }
        [DataMember] [DataNames("ROW_NUMBER")] public int ROW_NUMBER { get; set; }
        [DataMember] [DataNames("DATE_CREATED")] public DateTime? DATE_CREATED { get; set; }
        [DataMember] [DataNames("SUCCESS_COUNT")] public int SUCCESS_COUNT { get; set; }
        [DataMember] [DataNames("FAILED_COUNT")] public int FAILED_COUNT { get; set; }
        [DataMember] [DataNames("TOTAL_COUNT")] public int TOTAL_COUNT { get; set; }
        [DataMember] [DataNames("IS_SUCCESS")] public bool IS_SUCCESS { get; set; }
        [DataMember] [DataNames("IS_NO_RECORDS")] public bool IS_NO_RECORDS { get; set; }
    }
}