﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;
using System.Data;
using PRMServices.Models.Catalog;
using System.IO;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.SQLHelper;
using PRMName = PRMServices.Models;
using CORE = PRM.Core.Common;
using System.Net.Mail;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMRealTimePriceService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMRealTimePriceService.svc or PRMRealTimePriceService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCatalogService : IPRMCatalogService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        private List<Category> Getcategories(int compId, int catId, string catCode, string catName, int PageSize = 0, int NumberOfRecords = 0)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_getcategories", new List<string>() { "P_COMP_ID", "P_CAT_ID", "P_CAT_CODE", "P_CAT_NAME", "P_PAGE", "P_PAGE_SIZE" }, new List<object>() { compId, catId, catCode, catName, PageSize, NumberOfRecords});
            List<Category> details = FillCategoryModel(ds);
            List<Category> data = new List<Category>();
            GetTreeData(details, data, 0);
            return data;
        }

        public List<Category> GetCategories(int compId, string sessionId, int PageSize = 0, int NumberOfRecords = 0)
        {
            int isValidSessin = Utilities.ValidateSession(sessionId);
            return Getcategories(compId, 0, "", "", PageSize, NumberOfRecords);
        }

        public Category GetCategoryById(int compId, int catId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, catId, string.Empty, string.Empty);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public Category GetCategoryByName(int compId, string catName, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, 0, string.Empty, catName);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public Category GetCategoryByCode(int compId, string catCode, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            details = Getcategories(compId, 0, catCode, string.Empty);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Category();
            }
        }

        public List<Category> SearchCategories(string searchString, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            DataSet ds = CatalogUtility.GetResultSet("cm_SearchCategories", new List<string>() { "searchText" }, new List<object>() { searchString });
            return FillCategoryModel(ds);
        }

        public CatalogResponse AddCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", 0);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_NAME", reqCategory.CategoryName);
                sd.Add("P_CAT_CODE", string.Empty);
                sd.Add("P_CAT_DESC", reqCategory.CategoryDesc == null ? string.Empty : reqCategory.CategoryDesc);
                sd.Add("P_CAT_ORDER", reqCategory.CategoryOrder);
                sd.Add("P_CAT_PARENTID", reqCategory.CatParentId);
                sd.Add("P_CAT_DEPTS", null == reqCategory.Departments? string.Empty: reqCategory.Departments);
                sd.Add("P_CAT_ISVALID", 1);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_savecategory", sd);
            }
            catch(Exception ex)
            {
                catalogResponse.ErrorMessage = ex.Message;
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (catalogResponse.ResponseId == -99)
                {
                    catalogResponse.ErrorMessage = "\""+ reqCategory.CategoryName + "\" already exists";
                }
            }
            catalogResponse.SessionID = sessionid;

            return catalogResponse;
        }

        public CatalogResponse DeleteCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", reqCategory.CategoryId);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_CODE", reqCategory.CategoryCode);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_deletecategory", sd);
            }
            catch(Exception ex)
            {
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if(ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public CatalogResponse UpdateCategory(Category reqCategory, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_CAT_ID", reqCategory.CategoryId);
                sd.Add("P_COMP_ID", reqCategory.CompanyId);
                sd.Add("P_CAT_NAME", reqCategory.CategoryName);
                sd.Add("P_CAT_CODE", reqCategory.CategoryCode == null ? string.Empty : reqCategory.CategoryCode);
                sd.Add("P_CAT_DESC", reqCategory.CategoryDesc == null ? string.Empty : reqCategory.CategoryDesc);
                sd.Add("P_CAT_ORDER", reqCategory.CategoryOrder);
                sd.Add("P_CAT_PARENTID", reqCategory.CatParentId);
                sd.Add("P_CAT_DEPTS", null == reqCategory.Departments ? string.Empty : reqCategory.Departments);
                sd.Add("P_CAT_ISVALID", 1);
                sd.Add("P_USER", reqCategory.ModifiedBy);
                ds = sqlHelper.SelectList("cm_savecategory", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (catalogResponse.ResponseId == -99)
                {
                    catalogResponse.ErrorMessage = "\"" + reqCategory.CategoryName + "\" alreadt exists";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<Category> GetSubCategories(int parentCatId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_getsubcategories", new List<string>() { "P_PARENT_ID", "P_COMP_ID" }, new List<object>() { parentCatId, companyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }
            return details;
        }

        public List<Category> GetProductSubCategories(int prodId, int parentCatId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetProductCategories", new List<string>() { "P_COMP_ID", "P_CAT_PARENT_ID", "P_PROD_ID" }, new List<object>() { companyId, parentCatId, prodId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }
            List<Category> data = new List<Category>();
            GetTreeData(details, data, parentCatId);
            return data;
        }

        public List<Category> GetVendorCategories(int vendorId, int parentCatId, int companyId, string sessionId, string type, int PageSize = 0, int NumberOfRecords = 0, string searchString = null)
        {
            Utilities.ValidateSession(sessionId);
            List<Category> details = new List<Category>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetVendorCategories", new List<string>() { "P_VENDOR_ID", "P_CAT_PARENT_ID", "P_COMP_ID", "P_TYPE", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING" }, new List<object>() { vendorId, parentCatId, companyId, type, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }

                    details.Add(category);
                }
            }

            List<Category> data = new List<Category>();
            GetTreeData(details, data, 0);
            return data;
        }

        public List<Product> GetVendorProducts(int vendorId, int companyId, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Product> details = new List<Product>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetVendorProducts", new List<string>() { "P_VENDOR_ID", "P_COMP_ID" }, new List<object>() { vendorId, companyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Product product = new Product();
                    try
                    {
                        CatalogUtility.SetItemFromRow(product, dr);
                    }
                    catch (Exception ex)
                    {
                        product = new Product();
                        product.ErrorMessage = ex.Message;
                    }

                    details.Add(product);
                }
            }
            //foreach(Product prod in details)
            //{
            //    if(prod.ProductSelected > 0)
            //    {
            //        prod.ProductChecked = true;
            //    }
            //    else
            //    {
            //        prod.ProductChecked = false;
            //    }
            //}
            return details;
        }


        public List<PRMName.ProductVendorDetails> GetProductVendors(int ProductID, string sessionID)
        {
            List<PRMName.ProductVendorDetails> getAssignedVendors = new List<PRMName.ProductVendorDetails>();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                string query = string.Format("SELECT * FROM cm_vendorproducts cvp "+
                               "inner join vendors v on v.U_ID = cvp.VendorId where cvp.ProductId = {0} and cvp.IsValid = 1;", ProductID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        PRMName.ProductVendorDetails prmVendDet = new PRMName.ProductVendorDetails();
                        prmVendDet.VendorID = row["VendorId"] != DBNull.Value ? Convert.ToInt32(row["VendorId"]) : 0;
                        // prmVendDet.VendorID = row["VendorId"] != DBNull.Value ? Convert.ToInt32(row["VendorId"]) : 0;
                        prmVendDet.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                        prmVendDet.VendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                        getAssignedVendors.Add(prmVendDet);
                    }
                }

            }
            catch (Exception ex)
            {
                PRMName.ProductVendorDetails v = new PRMName.ProductVendorDetails();
                v.ErrorMessage = ex.Message;
                getAssignedVendors.Add(v);
            }

            return getAssignedVendors;
        }


        private void GetTreeData(List<Category> rawDetails, List<Category> parent, int id)
        {
            var items = rawDetails.Where(a => a.CatParentId == id && a.CategoryId > 0).Select(p => p).ToList();
            foreach (var item in items)
            {
                if(item.catSelected > 0)
                {
                    item.nodeChecked = true;
                }
                else
                {
                    item.nodeChecked = false;
                }
                parent.Add((Category)item);
            }
            foreach (var item in parent)
            {
                item.subCategories = new List<Category>();
                GetTreeData(rawDetails, item.subCategories, item.CategoryId);
            }
        }

        private List<Product> GetProducts(string sessionid, int compId, int prodId, string prodCode, string prodName, int PageSize = 0, int NumberOfRecords = 0, string searchString = null)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getproducts", new List<string>() { "P_COMP_ID", "P_PROD_ID", "P_PROD_CODE", "P_PROD_NAME", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING" }, new List<object>() { compId, prodId, prodCode, prodName, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null });
            return FillProductModel(ds);
        }

        private List<Product> FillProductModel(DataSet ds)
        {
            List<Product> details = new List<Product>();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Product product = new Product();
                    try
                    {
                        product.ContractManagement = new List<ContractManagementDetails>();
                        CatalogUtility.SetItemFromRow(product, dr);
                        if (ds.Tables.Count > 1)
                        {
                            DataTable dt1 = ds.Tables[1];
                            foreach (DataRow dr1 in dt1.Rows)
                            {
                                ContractManagementDetails ContractManagement = new ContractManagementDetails();
                                CatalogUtility.SetItemFromRow(ContractManagement, dr1);
                                product.ContractManagement.Add(ContractManagement);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        product = new Product();
                        product.ErrorMessage = ex.Message;
                    }

                    if (string.IsNullOrEmpty(product.CasNumber))
                    {
                        product.CasNumber = product.ProductCode;
                    }

                    if (string.IsNullOrEmpty(product.MfcdCode))
                    {
                        product.MfcdCode = product.ProductNo;
                    }

                    details.Add(product);
                }
            }
            return details;
        }

        public List<Product> GetProducts(int compId, string sessionid, int PageSize = 0, int NumberOfRecords = 0, string searchString = null)
        {
            return GetProducts(sessionid, compId, 0, "", "", PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : searchString = null);
        }

        public List<Product> GetUserProducts(int compId,int userId, string sessionid, int PageSize = 0, int NumberOfRecords = 0, string searchString = null, bool isCas = false, bool isMfcd = false, bool isnamesearch = false)
        {
            int SearchCas = 0;
            int SearchMfcd = 0;
            int nameSearch = 0;
            if (isCas || isMfcd) {
                SearchCas = 1;
                SearchMfcd = 1;
            }

            if (isnamesearch)
            {
                nameSearch = 1;
            }

            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getuserproducts", new List<string>() { "P_COMP_ID", "P_USER", "P_PAGE", "P_PAGE_SIZE", "P_SEARCH_STRING", "P_SEARCH_CAS", "P_SEARCH_MFCD", "P_IS_NAME_SEARCH" }, 
                new List<object>() { compId, userId, PageSize, NumberOfRecords, !string.IsNullOrEmpty(searchString) ? searchString : null, SearchCas, SearchMfcd, nameSearch });
            return FillProductModel(ds);
        }

        public List<Product> GetNonCoreProducts(int compId, int userId, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_GetNonCoreProducts", new List<string>() { "P_COMP_ID", "P_USER" }, new List<object>() { compId, userId });
            return FillProductModel(ds);
        }

        public List<Product> GetMaterialProducts(int compId, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            DataSet ds = CatalogUtility.GetResultSet("cm_getmaterialproducts", new List<string>() { "P_COMP_ID" }, new List<object>() { compId });
            return FillProductModel(ds);
        }

        public List<VendorProductData> GetProdDataReport(string reportType, int catitemid, string sessionid)
        {
            List<VendorProductData> details = new List<VendorProductData>();
            List<VendorProductDetails> details1 = new List<VendorProductDetails>();

            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };

                if (reportType.ToUpper().Contains("MATERIAL_REPORT"))
                {
                    sd.Add("P_C_I_ID", catitemid);
                    CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                    var dataset = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details = mapper.Map(dataset.Tables[0]).ToList();

                    CORE.DataNamesMapper<VendorProductDetails> mapper1 = new CORE.DataNamesMapper<VendorProductDetails>();
                    var dataset1 = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details1 = mapper1.Map(dataset1.Tables[1]).ToList();

                    foreach (VendorProductData data in details)
                    {
                        data.VEND_PROD_DETAILS = details1.Where(d => d.REQ_ID == data.REQ_ID).ToList();

                    }

                }

                if (reportType.ToUpper().Contains("PPS_REPORT"))
                {
                    sd.Add("P_C_I_ID", catitemid);
                    CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                    var dataset = sqlHelper.SelectList("cp_GetProdDataReport", sd);
                    details = mapper.Map(dataset.Tables[2]).ToList();
                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<Product> GetProductsByFilters(int compId, string sessionId, List<ProductFilters> prodFilters)
        {
            var innerQuery = string.Empty;
            string attrQuery = string.Empty;
            string attrIsInQuery = string.Empty;
            string attrIsNotInQuery = string.Empty;
            string catQuery = string.Empty;
            string prodQuery = string.Empty;

            //var attrIsDefined = prodFilters.Where(p => (p.FilterType.Equals("attr") && (p.FilterCond.ToLower().Equals("is defined")))).Select(p => p.FilterField).ToList();
            //if(attrIsDefined != null)
            //{
            //    attrIsInQuery += " cp.propName in ( " + string.Join(",", attrIsDefined) + " ) ";
            //}

            //var attrIsNotDefined = prodFilters.Where(p => (p.FilterType.Equals("attr") && (p.FilterCond.ToLower().Equals("is defined")))).Select(p => p.FilterField).ToList();
            //if (attrIsNotDefined != null)
            //{
            //    attrIsNotInQuery += " cp.propName in ( " + string.Join(",", attrIsNotDefined) + " ) ";
            //}


            foreach (ProductFilters filter in prodFilters)
            {
                
                if (filter.FilterType.Equals("attr"))
                {
                    string condStr = string.Empty;
                    switch (filter.FilterCond.ToLower())
                    {
                        case "is defined":
                            attrIsInQuery = " cp.propName = '" + filter.FilterField + "' ";
                            break;
                        case "is not defined":
                            attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' ";
                            break;
                        case "contains":
                            attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue  like % '" + filter.FilterValue + "'";
                            break;

                        case "is equals to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) = Date('" + filter.FilterValue + "') ";
                            }
                            else if (filter.filterFieldDataType.ToLower().Equals("int") || filter.filterFieldDataType.ToLower().Equals("decimal"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = " + filter.FilterValue ;
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = '" + filter.FilterValue + "' ";
                            }
                            
                            break;
                        case "is not equals to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) = Date('" + filter.FilterValue + "') ";
                            }
                            else if (filter.filterFieldDataType.ToLower().Equals("int") || filter.filterFieldDataType.ToLower().Equals("decimal"))
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = " + filter.FilterValue;
                            }
                            else
                            {
                                attrIsNotInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue = '" + filter.FilterValue + "' ";
                            }
                            break;
                        
                        case "is greater than":
                            if(filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) > Date('" + filter.FilterValue + "')";
                            }
                            else 
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue > " + filter.FilterValue;
                            }
                            break;
                        case "is greater or equal to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) >= Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue >= " + filter.FilterValue;
                            }
                            break;
                        case "is less than":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) < Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue < " + filter.FilterValue;
                            }
                            break;
                        case "is less or equal to":
                            if (filter.filterFieldDataType.ToLower().Equals("date"))
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and Date(ep.propValue) <= Date('" + filter.FilterValue + "')";
                            }
                            else
                            {
                                attrIsInQuery = " cp.propName = '" + filter.FilterField + "' and ep.propValue <= " + filter.FilterValue;
                            }
                            break;
                    }
                    if (!string.IsNullOrEmpty(attrIsInQuery))
                    {
                        if (string.IsNullOrEmpty(innerQuery))
                        {
                        innerQuery = " and ProductId in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsInQuery + ")";
                        }
                        else
                        {
                            innerQuery += " and ProductId in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsInQuery + ")";

                        }
                    }
                    if (!string.IsNullOrEmpty(attrIsNotInQuery))
                    {
                        if (string.IsNullOrEmpty(innerQuery))
                        {
                            innerQuery = " and ProductId not in "
                            + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                            " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsNotInQuery + ")";
                        }
                        else
                        {
                            innerQuery += " and ProductId not in "
                        + "(select entityId from cm_catalogproperties cp, cm_entityproperties ep " +
                        " where cp.propId = ep.propertyId and cp.propType = 0 and cp.companyId = ep.CompanyId and cp.IsValid = 1 and ep.IsValid = 1 and " + attrIsNotInQuery + ")";

                        }
                    }
                }
                else if (filter.FilterType.Equals("cat"))
                {
                    string condStr = string.Empty;
                    switch (filter.FilterCond.ToLower())
                    {
                        //case "is defined":
                        //    condStr = "is not null";
                        //    break;
                        //case "is not defined":
                        //    condStr = "is null";
                        //    break;
                        case "equals to":
                            condStr = "= " + filter.FilterValue;
                            break;
                        case "contains":
                            condStr = "like % '" + filter.FilterValue + "'";
                            break;
                    }
                    if (string.IsNullOrEmpty(catQuery))
                    {
                        catQuery = " cc.CategoryName = '" + condStr;
                    }
                    else
                    {
                        catQuery += " and cc.CategoryName = '" + condStr;
                    }
                }
                else if (filter.FilterType.Equals("prod"))
                {
                    string condStr = string.Empty;
                    if (filter.FilterField.ToLower().Equals("product name"))
                    {
                        condStr = " ProductName like '%" + filter.FilterValue + "%' ";
                    }
                    else if (filter.FilterField.ToLower().Equals("created") || filter.FilterField.ToLower().Equals("last modified"))
                    {
                        string columnName = "DateCreated";
                        if(filter.FilterField.ToLower().Equals("last modified"))
                        {
                            columnName = "DateModified";
                        }
                        switch (filter.FilterCond.ToLower()) {
                            case "is equals to":
                                condStr = " Date("+ columnName + ") = Date('" + filter.FilterValue + "') ";
                                break;
                            case "is not equals to":
                                condStr = " Date(" + columnName + ") <> Date('" + filter.FilterValue + "') ";
                                break;
                            case "is greater than":
                                condStr = " Date(" + columnName + ") > Date('" + filter.FilterValue + "') ";
                                break;
                            case "is greater or equal to":
                                condStr = " Date(" + columnName + ") >= Date('" + filter.FilterValue + "') ";
                                break;
                            case "is less than":
                                condStr = " Date(" + columnName + ") < Date('" + filter.FilterValue + "') ";
                                break;
                            case "is less or equal to":
                                condStr = " Date(" + columnName + ") <= Date('" + filter.FilterValue + "') ";
                                break;
                        }
                    }
                    if (!string.IsNullOrEmpty(condStr))
                    {
                        if (string.IsNullOrEmpty(prodQuery))
                        {
                            prodQuery = condStr;
                        }
                        else
                        {
                            prodQuery += " and " + condStr;
                        }
                    }
                }
            }
            

            string queryString = "select * from cm_product where IsValid = 1 and CompanyId = " + compId.ToString();

            if (!string.IsNullOrEmpty(innerQuery))
            {
                queryString += innerQuery;
            }
            if (!string.IsNullOrEmpty(prodQuery))
            {
                queryString += " and " + prodQuery;
            }

            queryString += " order by ProductName";


            DataSet ds = new DataSet();

            try
            {
                Utilities.ValidateSession(sessionId);
                ds = sqlHelper.ExecuteQuery(queryString);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return FillProductModel(ds);
        }

        public Product GetProductById(int compId, int prodId, string sessionId)
        {
            List<Product> details = new List<Product>();
            details = GetProducts(sessionId, compId, prodId, "", "");
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Product();
            }
        }

        public List<Product> GetAllProductsByCategories(int compId, string catIds, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            List<Product> details = new List<Product>();
            DataSet ds = CatalogUtility.GetResultSet( "cm_getproductsByCategories", new List<string>() { "P_COMP_ID", "P_CAT_IDS", "P_FROM", "P_TO" }, new List<object>() { compId, catIds, 0, 100000 });
            details = FillProductModel(ds);
            return details;
        }


        public Product GetProductByName(int compId, string prodName, string sessionId)
        {
            List<Product> details = new List<Product>();
            details = GetProducts(sessionId, compId, 0, string.Empty, prodName);
            if (details != null && details.Count > 0)
            {
                return details[0];
            }
            else
            {
                return new Product();
            }
        }

        public List<Product> SearchProducts(int compId, string searchString, string sessionId)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_SearchProducts", new List<string>() { "P_COMP_ID", "P_SEARCH_TEXT" }, new List<object>() { compId, searchString });
            return FillProductModel(ds);
        }

        public List<Product> GetProductsByCategory(int compId, int catIds, string sessionId)
        {
            DataSet ds = CatalogUtility.GetResultSet("cm_CategoryProducts", new List<string>() { "P_COMP_ID", "P_CAT_IDS" }, new List<object>() { Convert.ToString(compId), catIds });
            return FillProductModel(ds);
        }

        public PRMName.Response GetUserMyCatalogFileId(int userid,string sessionid)
        {
            PRMName.Response response = new PRMName.Response();
            string query = string.Format("SELECT * FROM  UserData WHERE U_ID = {0};", userid);
            DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                response.Message = ds.Tables[0].Rows[0]["MY_CATALOG_FILE"] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0]["MY_CATALOG_FILE"]) : string.Empty;
            }

            return response;
        }

        public CatalogResponse AddProduct(Product reqProduct, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            PRMServices prm = new PRMServices();
            //List<Product> data = new List<Product>();
            DataSet ds = new DataSet();


            string fileName = string.Empty;
            var filenameTemp = string.Empty;
            if (reqProduct.MultipleAttachments != null && reqProduct.MultipleAttachments.Count > 0)
            {
                foreach (PRMName.FileUpload fd in reqProduct.MultipleAttachments)
                {

                    if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                    {

                        var attachName = string.Empty;

                        long tick = DateTime.UtcNow.Ticks;
                        attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName);
                        prm.SaveFile(attachName, fd.FileStream);
                        //SaveFileAsync(fileName, attachment);

                        attachName = "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName;

                        PRMName.Response res = prm.SaveAttachment(attachName);
                        if (res.ErrorMessage != "")
                        {
                           
                        }

                        fd.FileID = res.ObjectID;


                        if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid, fd.FileStream, filenameTemp);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }
                        }

                    }
                    else if (fd.FileID > 0)
                    {
                        Attachment singleAttachment = null;
                        var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }

                    fileName += Convert.ToString(fd.FileID) + ",";

                }

                fileName = fileName.Substring(0, fileName.Length - 1);

            }


            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", 0);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? string.Empty : reqProduct.ProductCode);
                sd.Add("P_PROD_NAME", reqProduct.ProductName);
                sd.Add("P_PROD_NO", reqProduct.ProductNo == null ? string.Empty : reqProduct.ProductNo);
                sd.Add("P_PROD_HSN", reqProduct.ProductHSNCode == null ? string.Empty : reqProduct.ProductHSNCode.Replace("'", ""));
                sd.Add("P_PROD_QTY", reqProduct.ProdQty);
                sd.Add("P_PROD_DESC", reqProduct.ProductDesc == null ? string.Empty : reqProduct.ProductDesc.Replace("'", ""));
                sd.Add("P_PROD_ISVALID", 1);
                sd.Add("P_PROD_ALTERNATIVE_UNITS", reqProduct.ProdAlternativeUnits == null ? string.Empty : reqProduct.ProdAlternativeUnits);
                sd.Add("P_UNIT_CONVERSION", reqProduct.UnitConversion == null ? string.Empty : reqProduct.UnitConversion);
                sd.Add("P_PRODUCT_VOLUME", reqProduct.ProductVolume == null ? string.Empty : reqProduct.ProductVolume);
                sd.Add("P_SHELF_LIFE", reqProduct.ShelfLife == null ? string.Empty : reqProduct.ShelfLife);
                
                sd.Add("P_PROD_GST", reqProduct.ProductGST);
                sd.Add("P_PREF_BRAND", reqProduct.PrefferedBrand == null ? string.Empty : reqProduct.PrefferedBrand.Replace("'", ""));
                sd.Add("P_ALTER_BRAND", reqProduct.AlternateBrand == null ? string.Empty : reqProduct.AlternateBrand.Replace("'", ""));
                sd.Add("P_TOT_PURCH_QTY", reqProduct.TotPurchaseQty);
                sd.Add("P_IN_TRANSIT", reqProduct.InTransit);
                sd.Add("P_LEAD_TIME", reqProduct.LeadTime == null ? string.Empty : reqProduct.LeadTime);
                sd.Add("P_DEPARTMENTS", reqProduct.Departments == null ? string.Empty : reqProduct.Departments);
                sd.Add("P_DELIVER_TREMS", reqProduct.DeliveryTerms);
                sd.Add("P_TERMS_CONDITIONS", reqProduct.TermsConditions);
                sd.Add("P_USER", reqProduct.ModifiedBy);
                sd.Add("P_ITEM_ATTACHMENTS", fileName);
                sd.Add("P_CAS_NUMBER", reqProduct.CasNumber == null ? string.Empty : reqProduct.CasNumber);
                sd.Add("P_MFCD_CODE", reqProduct.MfcdCode == null ? string.Empty : reqProduct.MfcdCode);
                //data.Add();
                ds = sqlHelper.SelectList("cm_saveproduct", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                catalogResponse.ObjectId = (ds.Tables[0].Rows[0][2] != DBNull.Value && !string.IsNullOrWhiteSpace(ds.Tables[0].Rows[0][2].ToString())) ? Convert.ToInt32(ds.Tables[0].Rows[0][2].ToString()) : -1;
                if (catalogResponse.ResponseId >=1 && reqProduct.ListVendorDetails != null && reqProduct.ListVendorDetails.Count > 0)
                {
                    //List<Product> ListVendorProducts = new List<Product>();
                    //foreach(ProductVendorDet)
                    //  SaveVendorCatalog(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId);

                    DataSet dataset = new DataSet();
                    try
                    {
                        foreach (ContractManagementDetails contract in reqProduct.ContractManagement)
                        {
                            SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                            sd1.Add("P_PC_ID", catalogResponse.PcId);
                            sd1.Add("P_ProductId", catalogResponse.ResponseId);
                            sd1.Add("P_U_ID", contract.VendorId);
                            sd1.Add("P_number", contract.Number);
                            sd1.Add("P_value", contract.Value);
                            sd1.Add("P_quantity", contract.Quantity);
                            sd1.Add("P_availedQuantity", contract.AvailedQuantity);
                            sd1.Add("P_document", contract.Document);
                            sd1.Add("P_startTime", contract.StartTime);
                            sd1.Add("P_endTime", contract.EndTime);
                            sd1.Add("P_companyName", contract.CompanyName);
                            sd1.Add("P_isValid", 1);

                            dataset = sqlHelper.SelectList("cm_saveContractDetails", sd1);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }

                    try
                    {
                        string deleteVendorProducts = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", catalogResponse.ResponseId);
                        sqlHelper.ExecuteNonQuery_IUD(deleteVendorProducts);
                        string vendorProduct = string.Empty;
                        foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                        {
                            vendorProduct += string.Format("INSERT INTO cm_VendorProducts(VendorId, ProductId,CompanyId, " +
                                "IsValid, ApprovalStatus, DateCreated, DateModified, CreatedBy, ModifiedBy) values({0}, {1}, {2}, {3}, {4}, {5}, " +
                                "{6}, {7}, {8})", pvd.VendorID, catalogResponse.ResponseId, reqProduct.CompanyId, 1, "'PENDING'", "NOW()", "NOW()",
                                reqProduct.ModifiedBy, reqProduct.ModifiedBy) + ";";
                        }

                        sqlHelper.ExecuteNonQuery_IUD(vendorProduct);

                    }
                    catch (Exception ex)
                    {
                        //response.ErrorMessage = ex.Message;
                    }
                }
                if(catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "Invalid";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<ContractManagementDetails> GetPendingContracts(int compid, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            List<ContractManagementDetails> details = new List<ContractManagementDetails>();

            //string query =$@"select PC.*, P.ProductCode, P.ProductName From productcontractdetails PC INNER JOIN cm_product P ON P.ProductId= PC.ProductID 
            // WHERE PC.IsValid = 1 AND PC.Quantity <> PC.AvailedQuantity AND P.CompanyID = {compid}";

            string query = $@"select po.req_id,PC.*, p.ProductCode, P.ProductName,v.U_PHONE,concat(v.U_FNAME,' ',v.U_LNAME) as SUPPLIER_NAME, 
                            (select categoryname from cm_Category where categoryid = cpc.categoryid) as Categories, po.PURCHASE_ORDER_ID 
                            From productcontractdetails PC inner JOIN cm_product p ON P.ProductId = PC.ProductId 
                            LEFT JOIN requirementitems ri on ri.CATALOGUE_ITEM_ID = pc.ProductId 
                            LEFT JOIN poinformation po on po.ITEM_ID = ri.ITEM_ID 
                            LEFT JOIN vendors v on v.V_ID = po.VENDOR_ID 
                            LEFT JOIN cm_productcategory cpc on cpc.ProductId = P.ProductId 
                            WHERE PC.IsValid = 1 AND p.CompanyID = {compid}";

            DataSet ds = sqlHelper.ExecuteQuery(query);
            if(ds!=null & ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach(var row in ds.Tables[0].AsEnumerable())
                {
                    ContractManagementDetails detail = new ContractManagementDetails();

                    detail.AvailedQuantity = row["AvailedQuantity"] != DBNull.Value ? Convert.ToInt32(row["AvailedQuantity"]) : 0;
                    detail.Quantity = row["Quantity"] != DBNull.Value ? Convert.ToInt32(row["Quantity"]) : 0;
                    detail.StartTime = row["StartTime"] != DBNull.Value ? Convert.ToDateTime(row["StartTime"]) : DateTime.MaxValue;
                    detail.EndTime = row["EndTime"] != DBNull.Value ? Convert.ToDateTime(row["EndTime"]) : DateTime.MaxValue;
                    detail.ProductId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                    detail.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                    detail.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    detail.ContractNumber =row["Number"] != DBNull.Value ? Convert.ToString(row["Number"]) : string.Empty;
                    detail.ContractValue = row["Value"] != DBNull.Value ? Convert.ToDecimal(row["Value"]) : 0;
                    detail.Categories = row["Categories"] != DBNull.Value ? Convert.ToString(row["Categories"]) : string.Empty;
                    detail.Document = row["document"] != DBNull.Value ? Convert.ToString(row["document"]) : string.Empty;
                    detail.PhoneNumber = row["U_PHONE"] != DBNull.Value ? Convert.ToInt32(row["U_PHONE"]) : 0;
                    detail.SupplierName = row["SUPPLIER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPPLIER_NAME"]) : string.Empty;
                    detail.PONumber = row["PURCHASE_ORDER_ID"] != DBNull.Value ? Convert.ToString(row["PURCHASE_ORDER_ID"]) : string.Empty;
                    details.Add(detail);
                }
            }
            
            
            return details;
        }

        public List<ContractManagementDetails> GetProductContracts(string productids, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            if (!string.IsNullOrWhiteSpace(productids))
            {
                productids = string.Join(",", productids.Split(',').ToList().Distinct());
            }
            List<ContractManagementDetails> details = new List<ContractManagementDetails>();

            string query = $@"select PC.*, C.COMPANY_NAME, P.ProductCode, P.ProductName From productcontractdetails PC INNER JOIN cm_product P ON P.ProductId= PC.ProductID 
                            INNER JOIN company C ON C.COMP_ID = dbo.GetCompanyId(PC.U_ID) WHERE PC.IsValid = 1 AND PC.Quantity <> PC.AvailedQuantity AND P.productid IN ({productids}) AND PC.isValid = 1 AND EndTime >= NOW()";

            DataSet ds = sqlHelper.ExecuteQuery(query);
            if (ds != null & ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                foreach (var row in ds.Tables[0].AsEnumerable())
                {
                    ContractManagementDetails detail = new ContractManagementDetails();

                    detail.AvailedQuantity = row["AvailedQuantity"] != DBNull.Value ? Convert.ToInt32(row["AvailedQuantity"]) : 0;
                    detail.Price = row["Value"] != DBNull.Value ? Convert.ToInt32(row["Value"]) : 0;
                    detail.Quantity = row["Quantity"] != DBNull.Value ? Convert.ToInt32(row["Quantity"]) : 0;
                    detail.StartTime = row["StartTime"] != DBNull.Value ? Convert.ToDateTime(row["StartTime"]) : DateTime.MaxValue;
                    detail.EndTime = row["EndTime"] != DBNull.Value ? Convert.ToDateTime(row["EndTime"]) : DateTime.MaxValue;
                    detail.ProductId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                    detail.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                    detail.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    detail.ADDRESS = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    detail.CREATED_DATE = row["CREATED_DATE"] != DBNull.Value ? Convert.ToDateTime(row["CREATED_DATE"]) : DateTime.MaxValue;


                    //detail.PC_ID = row["PC_ID"] != DBNull.Value ? Convert.ToInt32(row["PC_ID"]) : 0;
                    //detail.AvailedQuantity = row["AvailedQuantity"] != DBNull.Value ? Convert.ToInt32(row["AvailedQuantity"]) : 0;
                    //detail.Price = row["Value"] != DBNull.Value ? Convert.ToDouble(row["Value"]) : 0;
                    //detail.Value = row["Value"] != DBNull.Value ? Convert.ToInt32(row["Value"]) : 0;
                    //detail.IsValid = row["IsValid"] != DBNull.Value ? Convert.ToInt32(row["IsValid"]) : 0;
                    //detail.Quantity = row["Quantity"] != DBNull.Value ? Convert.ToInt32(row["Quantity"]) : 0;
                    //detail.StartTime = row["StartTime"] != DBNull.Value ? Convert.ToDateTime(row["StartTime"]) : DateTime.MaxValue;
                    //detail.EndTime = row["EndTime"] != DBNull.Value ? Convert.ToDateTime(row["EndTime"]) : DateTime.MaxValue;
                    //detail.ProductId = row["ProductId"] != DBNull.Value ? Convert.ToInt32(row["ProductId"]) : 0;
                    //detail.ProductName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]) : string.Empty;
                    //detail.CompanyName = row["CompanyName"] != DBNull.Value ? Convert.ToString(row["CompanyName"]) : string.Empty;
                    //detail.Document = row["document"] != DBNull.Value ? Convert.ToString(row["document"]) : string.Empty;
                    detail.Number = row["Number"] != DBNull.Value ? Convert.ToString(row["Number"]) : string.Empty;
                    //detail.VendorId = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    //detail.U_ID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    //detail.SelectedVendorCode = row["VENDOR_CODE"] != DBNull.Value ? Convert.ToString(row["VENDOR_CODE"]) : string.Empty;
                    //detail.PAYMENT_TERMS = row["PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["PAYMENT_TERMS"]) : string.Empty;
                    //detail.INCO_TERMS = row["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row["INCO_TERMS"]) : string.Empty;
                    //detail.DeliveryTerms = row["DELIVERY_TERMS"] != DBNull.Value ? Convert.ToString(row["DELIVERY_TERMS"]) : string.Empty;
                    //detail.GeneralTerms = row["TERMS"] != DBNull.Value ? Convert.ToString(row["TERMS"]) : string.Empty;
                    //detail.PlantCode = row["PLANT_CODE"] != DBNull.Value ? Convert.ToString(row["PLANT_CODE"]) : string.Empty;

                    //detail.DeliveryFrom = row["DELIVERY_FROM"] != DBNull.Value ? Convert.ToString(row["DELIVERY_FROM"]) : string.Empty;
                    //detail.DeliveryAt = row["DELIVERY_AT"] != DBNull.Value ? Convert.ToString(row["DELIVERY_AT"]) : string.Empty;
                    //detail.LeadTime = row["LEAD_TIME"] != DBNull.Value ? Convert.ToString(row["LEAD_TIME"]) : string.Empty;
                    //detail.Warranty = row["WARRANTY"] != DBNull.Value ? Convert.ToString(row["WARRANTY"]) : string.Empty;
                    //detail.SpecialInstructions = row["SPECIAL_INSTRUCTIONS"] != DBNull.Value ? Convert.ToString(row["SPECIAL_INSTRUCTIONS"]) : string.Empty;
                    //detail.DiscountEligibility = row["DISCOUNT_ELIGIBILITY"] != DBNull.Value ? Convert.ToString(row["DISCOUNT_ELIGIBILITY"]) : string.Empty;
                    //detail.ReconciliationTimelines = row["RECONCILIATION_TIMELINES"] != DBNull.Value ? Convert.ToString(row["RECONCILIATION_TIMELINES"]) : string.Empty;
                    //detail.CURRENCY = row["CURRENCY"] != DBNull.Value ? Convert.ToString(row["CURRENCY"]) : string.Empty;
                    //detail.EXCH_RATE = row["EXCH_RATE"] != DBNull.Value ? Convert.ToDecimal(row["EXCH_RATE"]) : 0;
                    //detail.POCurrency = row["PO_CURRENCY"] != DBNull.Value ? Convert.ToString(row["PO_CURRENCY"]) : string.Empty;
                    //detail.GST = row["GST"] != DBNull.Value ? Convert.ToDecimal(row["GST"]) : 0;
                    //detail.CustomDuty = row["CUSTOM_DUTY"] != DBNull.Value ? Convert.ToDecimal(row["CUSTOM_DUTY"]) : 0;
                    //detail.Discount = row["DISCOUNT"] != DBNull.Value ? Convert.ToDecimal(row["DISCOUNT"]) : 0;
                    //detail.NetPrice = row["NET_PRICE"] != DBNull.Value ? Convert.ToDecimal(row["NET_PRICE"]) : 0;
                    details.Add(detail);
                }
            }

            return details;
        }

        //public CatalogResponse saveProductVendorDetails(int productId,List<PRMName.ProductVendorDetails> vendorDetails) {
        ////    Utilities.ValidateSession(sessionid, null);
        // //   CatalogResponse catalogResponse = new CatalogResponse();
        //      CatalogResponse catalogVendResp = new CatalogResponse();
        //      DataSet ds = new DataSet();

        //    int loopcount = 0;

        //    string queryValues = string.Empty;
        //    int count = 0;
        //    string delete = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", productId);
        //    string query = delete + "INSERT INTO cm_VendorProducts(VendorId, ProductId, CompanyId, IsValid, DateCreated, DateModified, CreatedBy," +
        //        " ModifiedBy, ApprovalStatus) Values";

        //    foreach (Product vendDet in vendorDetails)
        //    {
        //        string columns = "({0},{1},{2},~$^{3}~$^,~$^{4}~$^,~$^{5}~$^,~$^{6},~$^{7},~$^{8}),";

        //        queryValues = queryValues + string.Format(columns,vendDet.ListVendorDetails);


        //        count++;

        //    }




        //    return catalogVendResp;
        //}


        public CatalogResponse UpdateProduct(Product reqProduct, string sessionid)
        {
            Utilities.ValidateSession(sessionid);
            CatalogResponse catalogResponse = new CatalogResponse();
            PRMServices prm = new PRMServices();
            DataSet ds = new DataSet();

            string fileName = string.Empty;
            var filenameTemp = string.Empty;
            if (reqProduct.MultipleAttachments != null && reqProduct.MultipleAttachments.Count > 0)
            {
                foreach (PRMName.FileUpload fd in reqProduct.MultipleAttachments)
                {

                    if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                    {

                        var attachName = string.Empty;

                        long tick = DateTime.UtcNow.Ticks;
                        attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName);
                        prm.SaveFile(attachName, fd.FileStream);
                        //SaveFileAsync(fileName, attachment);

                        attachName = "req" + tick + "_user" + reqProduct.CreatedBy + "_" + fd.FileName;

                        PRMName.Response res = prm.SaveAttachment(attachName);
                        if (res.ErrorMessage != "")
                        {

                        }

                        fd.FileID = res.ObjectID;


                        if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid, fd.FileStream, filenameTemp);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }
                        }

                    }
                    else if (fd.FileID > 0)
                    {
                        Attachment singleAttachment = null;
                        var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionid);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }

                    fileName += Convert.ToString(fd.FileID) + ",";

                }

                fileName = fileName.Substring(0, fileName.Length - 1);

            }

            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? "" : reqProduct.ProductCode);
                sd.Add("P_PROD_NAME", reqProduct.ProductName);
                sd.Add("P_PROD_DESC", reqProduct.ProductDesc == null ? string.Empty : reqProduct.ProductDesc);
                sd.Add("P_PROD_NO", reqProduct.ProductNo);
                sd.Add("P_PROD_HSN", reqProduct.ProductHSNCode);
                sd.Add("P_PROD_QTY", reqProduct.ProdQty == null ? string.Empty : reqProduct.ProdQty);
                sd.Add("P_PROD_ISVALID", reqProduct.IsValid);
                sd.Add("P_USER", reqProduct.ModifiedBy);
                sd.Add("P_PROD_ALTERNATIVE_UNITS", reqProduct.ProdAlternativeUnits == null?string.Empty: reqProduct.ProdAlternativeUnits);
                sd.Add("P_UNIT_CONVERSION", reqProduct.UnitConversion == null ? string.Empty : reqProduct.UnitConversion);
                sd.Add("P_PRODUCT_VOLUME", reqProduct.ProductVolume == null ? string.Empty : reqProduct.ProductVolume);
                sd.Add("P_SHELF_LIFE", reqProduct.ShelfLife == null ? string.Empty : reqProduct.ShelfLife);

                sd.Add("P_PROD_GST", reqProduct.ProductGST);
                sd.Add("P_PREF_BRAND", reqProduct.PrefferedBrand == null ? string.Empty : reqProduct.PrefferedBrand);
                sd.Add("P_ALTER_BRAND", reqProduct.AlternateBrand == null ? string.Empty : reqProduct.AlternateBrand);
                sd.Add("P_TOT_PURCH_QTY", reqProduct.TotPurchaseQty);
                sd.Add("P_IN_TRANSIT", reqProduct.InTransit);
                sd.Add("P_LEAD_TIME", reqProduct.LeadTime == null ? string.Empty : reqProduct.LeadTime);
                sd.Add("P_DEPARTMENTS", reqProduct.Departments == null ? string.Empty : reqProduct.Departments);
                sd.Add("P_DELIVER_TREMS", reqProduct.DeliveryTerms == null ? string.Empty : reqProduct.DeliveryTerms);
                sd.Add("P_TERMS_CONDITIONS", reqProduct.TermsConditions == null ? string.Empty : reqProduct.TermsConditions);

                sd.Add("P_CAS_NUMBER", reqProduct.CasNumber == null ? string.Empty : reqProduct.CasNumber);
                sd.Add("P_MFCD_CODE", reqProduct.MfcdCode == null ? string.Empty : reqProduct.MfcdCode);
                sd.Add("P_ITEM_ATTACHMENTS", fileName);

                ds = sqlHelper.SelectList("cm_saveproduct", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                //int delResp = DeleteVendorCatalog(catalogResponse.ResponseId, sessionid);
                //if (delResp == 0)
                //{
                //foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                //    {
                //    // saveProductVendorDetails(reqProduct.ProductId, pvd);
                //      SaveVendorCatalog(pvd.VendorID, reqProduct.CompanyId, "", catalogResponse.ResponseId.ToString(), Convert.ToUInt16(reqProduct.ModifiedBy), sessionid);//

                //    }
                //}

                DataSet dataset = new DataSet();
                try
                {
                    //   reqProduct.ContractManagement = new ContractManagement();

                    foreach (ContractManagementDetails contract in reqProduct.ContractManagement)
                    {
                        SortedDictionary<object, object> sd1 = new SortedDictionary<object, object>() { };
                        sd1.Add("P_PC_ID", contract.PC_ID);
                        sd1.Add("P_ProductId", catalogResponse.ResponseId);
                        sd1.Add("P_U_ID", contract.U_ID);
                        sd1.Add("P_number", contract.Number);
                        sd1.Add("P_value", contract.Value);
                        sd1.Add("P_quantity", contract.Quantity);
                        sd1.Add("P_availedQuantity", contract.AvailedQuantity);
                        sd1.Add("P_document", contract.Document);
                        sd1.Add("P_startTime", contract.StartTime);
                        sd1.Add("P_endTime", contract.EndTime);
                        sd1.Add("P_companyName", contract.CompanyName);
                        sd1.Add("P_isValid", contract.IsValid);

                        dataset = sqlHelper.SelectList("cm_saveContractDetails", sd1);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }


                try
                {
                    string deleteVendorProducts = string.Format("DELETE FROM cm_VendorProducts WHERE ProductId = {0};", catalogResponse.ResponseId);
                    sqlHelper.ExecuteNonQuery_IUD(deleteVendorProducts);
                    string newVendorQuery = string.Empty;
                    foreach (PRMName.ProductVendorDetails pvd in reqProduct.ListVendorDetails)
                    {
                        newVendorQuery += string.Format("INSERT INTO cm_VendorProducts(VendorId, ProductId,CompanyId, " +
                            "IsValid, ApprovalStatus, DateCreated, DateModified, CreatedBy, ModifiedBy) values({0}, {1}, {2}, {3}, {4}, {5}, " +
                            "{6}, {7}, {8})", pvd.VendorID, catalogResponse.ResponseId, reqProduct.CompanyId, 1, "'PENDING'", "NOW()", "NOW()",
                            reqProduct.ModifiedBy, reqProduct.ModifiedBy)+ ";";
                    }

                    sqlHelper.ExecuteNonQuery_IUD(newVendorQuery);

                }
                catch (Exception ex)
                {
                    //response.ErrorMessage = ex.Message;
                }

                if (catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }


        public int DeleteVendorCatalog(int ProductID, string sessionID)
        {
            int a = -1;

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                string query = string.Format("DELETE FROM cm_vendorproducts where ProductId = {0};", ProductID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
               // ds.
                if (ds != null)
                {
                    a = 0;
                }

            }
            catch (Exception ex)
            {
                
            }

            return a;
        }

        public CatalogResponse updateproductcategories(int prodId, int compId, string catIds, int user, int statusCheck, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", prodId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);

                sd.Add("P_CHECKED", statusCheck);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_saveproductcategories", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }

        public CatalogResponse DeleteProduct(Product reqProduct, string sessionid)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_PROD_CODE", reqProduct.ProductCode == null ? "" : reqProduct.ProductCode);
                sd.Add("P_ISVALID", reqProduct.IsValid);
                sd.Add("P_USER", reqProduct.CreatedBy);
                ds = sqlHelper.SelectList("cm_deleteproduct", sd);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if(ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public CatalogResponse IsProdudtEditAllowed(Product reqProduct, string sessionid)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROD_ID", reqProduct.ProductId);
                sd.Add("P_COMP_ID", reqProduct.CompanyId);
                sd.Add("P_USER", reqProduct.CreatedBy);
                ds = sqlHelper.SelectList("cm_isproducteditallowed", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                if (ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                {
                    catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "";
                }
            }

            catalogResponse.SessionID = sessionid;
            return catalogResponse;
        }

        public List<PropertyModel> GetProperties(int CompanyId, int entityId, int entityType, string sessionId)
        {
            List<PropertyModel> Properties = new List<PropertyModel>();
            DataSet ds = CatalogUtility.GetResultSet("cm_GetProperties", new List<string>() { "P_COMP_ID", "P_ENTITY_ID", "P_ENITY_TYPE" }, new List<object>() { CompanyId, entityId, entityType });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    PropertyModel property = new PropertyModel();
                    try
                    {
                        CatalogUtility.SetItemFromRow(property, dr);
                    }
                    catch (Exception ex)
                    {
                        property = new PropertyModel();
                        property.ErrorMessage = ex.Message;
                    }

                    Properties.Add(property);
                }
            }

            return Properties;
        }

        public List<AttributeModel> GetAttribute(int CompanyId, string sessionId)
        {
            List<AttributeModel> Prop = new List<AttributeModel>();
            DataSet ds = CatalogUtility.GetResultSet("cm_getattributes", new List<string>() { "P_COMP_ID" }, new List<object>() { CompanyId });
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    AttributeModel property = new AttributeModel();
                    try
                    {
                        CatalogUtility.SetItemFromRow(property, dr);
                    }
                    catch (Exception ex)
                    {
                        property = new AttributeModel();
                        property.ErrorMessage = ex.Message;
                    }

                    Prop.Add(property);
                }
            }

            return Prop;
        }

        public CatalogResponse SaveCatalogProperty(PropertyModel property, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROP_ID", property.propId);
                sd.Add("P_COMP_ID", property.CompanyId);
                sd.Add("P_PROP_NAME", property.propName);
                sd.Add("P_PROP_DESC", property.propDesc == null ? "": property.propDesc);
                sd.Add("P_PROP_TYPE", property.propType);
                sd.Add("P_PROP_DATATYPE", property.propDataType);
                sd.Add("P_PROP_OPTIONS", property.propOptions == null ? "" : property.propOptions);
                sd.Add("P_PROP_DEFAULTVALUE", property.propDefaultValue == null ? "" : property.propDefaultValue);
                sd.Add("P_ISVALID", property.IsValid);
                sd.Add("P_USER", property.U_Id);
                ds = sqlHelper.SelectList("cm_savecatalogproperty", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (catalogResponse.ResponseId == -1 && ds.Tables[0].Columns.Count > 1)
                    {
                        catalogResponse.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1]) : "unknowwn error";
                    }
                }

                catalogResponse.SessionID = sessionId;
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
            }

            return catalogResponse;
        }

        public CatalogResponse SaveEntityProperties(List<PropertyEntityModel> propertyobj, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();

            string query = string.Empty;
            foreach (PropertyEntityModel pem in propertyobj)
            {
                query += $"CALL cm_saveentityproperty({pem.entityId}, {pem.propId}, {pem.companyId}, '{pem.propValue}', {pem.IsValid}, {pem.U_Id});";
            }

            sqlHelper.ExecuteNonQuery_IUD(query);
            catalogResponse.ResponseId = propertyobj.Count;
            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }


        public CatalogResponse ImportEntity(ImportEntity entity, int compId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }
            }
            if (string.Compare(entity.EntityName, "products", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
            {
                if (!sheetName.Equals("products", StringComparison.InvariantCultureIgnoreCase))
                {
                    return catalogResponse;
                }
            }

            DataSet ds = new DataSet();
            int count = 0;
            if (currentData.Rows.Count > 0)
            {
                foreach (DataRow row in currentData.Rows)
                {
                    string itemName = Convert.ToString(row["ProductName"]);
                    string itemNo = Convert.ToString(row["ProductNo"]);
                    string itemCode = Convert.ToString(row["ProductCode"]);
                    string itemHSN = Convert.ToString(row["HSNCode"]);
                    string itemUnits = Convert.ToString(row["QuantityUnits"]);
                    string itemDesc = Convert.ToString(row["ProductDescription"]);
                    string itemCategories = Convert.ToString(row["ProductCategories"]);
                    string itemUnitConversion = "";
                    string itemProductVolume = "";
                    // string itemProperties = Convert.ToString(row["ITEM_PROPERTIES"]);

                    if (!(string.IsNullOrEmpty(itemName)) && !(string.IsNullOrEmpty(itemUnits)))// || string.IsNullOrEmpty(itemNo) || string.IsNullOrEmpty(itemUnits)))
                    {
                        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                        try
                        {
                            sd.Add("P_PROD_ID", 0);
                            sd.Add("P_COMP_ID", compId);
                            sd.Add("P_PROD_CODE", itemCode);
                            sd.Add("P_PROD_NAME", itemName);
                            sd.Add("P_PROD_NO", null == itemNo ? "" : itemNo);
                            sd.Add("P_PROD_HSN", null == itemHSN ? "" : itemHSN);
                            sd.Add("P_PROD_QTY", null == itemUnits ? "" : itemUnits);
                            sd.Add("P_PROD_DESC", null == itemDesc ? "" : itemDesc);
                            sd.Add("P_PROD_ISVALID", 1);
                            sd.Add("P_PROD_ALTERNATIVE_UNITS", Convert.ToString(row["AlternativeQuantityUnits"]));
                            sd.Add("P_UNIT_CONVERSION", itemUnitConversion);
                            sd.Add("P_PRODUCT_VOLUME", itemProductVolume);
                            sd.Add("P_SHELF_LIFE", Convert.ToString(row["ShelfLife"]));
                            sd.Add("P_USER", user);

                            sd.Add("P_PROD_GST", 0.00);
                            sd.Add("P_PREF_BRAND", "");
                            sd.Add("P_ALTER_BRAND", "");
                            sd.Add("P_TOT_PURCH_QTY", 0);
                            sd.Add("P_IN_TRANSIT", 0);
                            sd.Add("P_LEAD_TIME", "");
                            sd.Add("P_DEPARTMENTS", "");
                            sd.Add("P_DELIVER_TREMS", "");
                            sd.Add("P_TERMS_CONDITIONS", "");
                            sd.Add("P_ITEM_ATTACHMENTS", "");
                            sd.Add("P_CAS_NUMBER", "");
                            sd.Add("P_MFCD_CODE", "");

                            ds = sqlHelper.SelectList("cm_saveproduct", sd);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                            {
                                int itemId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                                if (itemId > 0)
                                {
                                    count++;
                                    if (!string.IsNullOrWhiteSpace(itemCategories))
                                    {
                                        try
                                        {
                                            SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };
                                            sdCat.Add("P_COMP_ID", compId);
                                            sdCat.Add("P_CAT_NAMES", itemCategories);
                                            sdCat.Add("P_PROD_ID", itemId);
                                            sdCat.Add("P_USER", user);
                                            DataSet dsCat = sqlHelper.SelectList("cm_importItemCategory", sdCat);
                                        }
                                        catch (Exception ex)
                                        {
                                            logger.Error(ex.Message);
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
                    else
                    {
                        catalogResponse.ErrorMessage = "ProductName/ProductQuantityUnits should not be Empty";
                    }
                }
            }
            else
            {
                catalogResponse.ErrorMessage = "Please Fill All the Mandatory Fields";
            }
            catalogResponse.ResponseId = count;
            return catalogResponse;
        }

        public CatalogResponse ImportCatalogueCategories(ImportEntity entity, int compId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                    using (ExcelPackage package = new ExcelPackage(ms))
                    {
                        currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                        sheetName = package.Workbook.Worksheets[1].Name;
                    }
                }
            }
            if (string.Compare(entity.EntityName, "categories", StringComparison.InvariantCultureIgnoreCase) == 0 && currentData.Rows.Count > 0)
            {
                if (!sheetName.Equals("categories", StringComparison.InvariantCultureIgnoreCase))
                {
                    catalogResponse.ErrorMessage = "sheet name should be categories";
                    return catalogResponse;
                }
            }

            int count = 0;
            foreach (DataRow row in currentData.Rows)
            {
                string itemName = Convert.ToString(row["CategoryName"]);
                string itemDepts = Convert.ToString(row["CategoryDepartments"]);

                if (!string.IsNullOrEmpty(itemName))
                {
                    SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };
                   
                    try
                    {
                        sdCat.Add("P_COMP_ID", compId);
                        sdCat.Add("P_CAT_NAMES", itemName);
                        sdCat.Add("P_CAT_DEPTS", itemDepts);
                        sdCat.Add("P_USER", user);
                        DataSet dsCat = sqlHelper.SelectList("cm_importCatalogueCategory", sdCat);

                        if (dsCat != null && dsCat.Tables.Count > 0 && dsCat.Tables[0].Rows.Count > 0 && dsCat.Tables[0].Rows[0][0] != null)
                        {
                            int itemId = dsCat.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dsCat.Tables[0].Rows[0][0].ToString()) : -1;
                            if (itemId > 0)
                            {
                                count++;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.Message);
                    }
                }
            }
            catalogResponse.ResponseId = count;
            return catalogResponse;
        }

        public CatalogResponse ImportVendorItemCategories(ImportEntity entity, int compId, int vendorId, int user, string sessionId)
        {
            Utilities.ValidateSession(sessionId);
            string moreInfo = string.Empty;
            CatalogResponse catalogResponse = new CatalogResponse();
            string sheetName = string.Empty;
            DataTable currentData = new DataTable();

            if (entity.Attachment != null)
            {
                if(!string.Equals(entity.EntityName, "MyCategories", StringComparison.InvariantCultureIgnoreCase))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(entity.Attachment, 0, entity.Attachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = StoreUtility.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }
                }
            }

            if (string.Equals(entity.EntityName, "VendorItemCategories", StringComparison.InvariantCultureIgnoreCase) && currentData.Rows.Count > 0)
            {
                
                sqlHelper.ExecuteQuery("DELETE FROM cm_vendorcategory WHERE vendorid= 0;DELETE FROM cm_vendorproducts WHERE vendorid= 0;");

                int count = 0;
                foreach (DataRow row in currentData.Rows)
                {
                    string itemName = row["ProductName"] != DBNull.Value ? Convert.ToString(row["ProductName"]).Trim() : string.Empty;
                    string itemCategory = row["CategoryName"] != DBNull.Value ? Convert.ToString(row["CategoryName"]).Trim() : string.Empty;

                    if (!string.IsNullOrEmpty(itemName))
                    {
                        SortedDictionary<object, object> sdCat = new SortedDictionary<object, object>() { };

                        try
                        {
                            sdCat.Add("P_COMP_ID", compId);
                            sdCat.Add("P_VENDOR_ID", vendorId);
                            sdCat.Add("P_CAT_NAMES", itemCategory);
                            sdCat.Add("P_PROD_NAME", itemName);
                            sdCat.Add("P_USER", user);
                            DataSet dsCat = sqlHelper.SelectList("cm_importVendorItemCategory", sdCat);

                            if (dsCat != null && dsCat.Tables.Count > 0 && dsCat.Tables[0].Rows.Count > 0 && dsCat.Tables[0].Rows[0][0] != null)
                            {
                                int itemId = dsCat.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(dsCat.Tables[0].Rows[0][0].ToString()) : -1;
                                if (itemId > 0)
                                {
                                    count++;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
                }
                catalogResponse.ResponseId = count;
                return catalogResponse;
            }
            else if (string.Equals(entity.EntityName, "MyCategories", StringComparison.InvariantCultureIgnoreCase))
            {
                if (entity.Attachment != null)
                {
                    long tick = DateTime.Now.Ticks;
                    string attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "mycatalog_" + tick + entity.UserID + "_" + Path.GetExtension(entity.AttachmentFileName));
                    string fileName = "mycatalog_" + tick + entity.UserID + "_" + Path.GetExtension(entity.AttachmentFileName);
                    Utilities.SaveFile(attachName, entity.Attachment);
                    PRMName.Response response = Utilities.SaveAttachment(fileName);
                    if(response !=null && response.ObjectID > 0)
                    {
                        string query = string.Format("UPDATE UserData SET MY_CATALOG_FILE = {0} WHERE U_ID = {1};", response.ObjectID , entity.UserID);
                        sqlHelper.ExecuteQuery(query);
                    }
                }
                catalogResponse.ResponseId = 1;
                return catalogResponse;
            }
            else
            {
                catalogResponse.ErrorMessage = "sheet name should be VendorItemCategories";
                return catalogResponse;
            }
        }

        private List<Category> FillCategoryModel(DataSet ds)
        {
            List<Category> details = new List<Category>();
            if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                foreach (DataRow dr in dt.Rows)
                {
                    Category category = new Category();
                    try
                    {
                        CatalogUtility.SetItemFromRow(category, dr);
                    }
                    catch (Exception ex)
                    {
                        category = new Category();
                        category.ErrorMessage = ex.Message;
                    }
                    details.Add(category);
                }
            }
            return details;
        }

        public CatalogResponse SaveVendorCatalog(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);
                sd.Add("P_PROD_IDS", string.IsNullOrEmpty(prodIds)?"0": prodIds);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_savevendorcatalog", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }



        public CatalogResponse SaveVendorCatalog1(int vendorId, int compId, string catIds, string prodIds, int user, string sessionId)
        {
            CatalogResponse catalogResponse = new CatalogResponse();
            DataSet ds = new DataSet();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_VENDOR_ID", vendorId);
                sd.Add("P_COMP_ID", compId);
                sd.Add("P_CAT_IDS", catIds);
                sd.Add("P_PROD_IDS", string.IsNullOrEmpty(prodIds) ? "0" : prodIds);
                sd.Add("P_USER", user);
                ds = sqlHelper.SelectList("cm_savevendorcatalog1", sd);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
            {
                catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
            }

            catalogResponse.SessionID = sessionId;
            return catalogResponse;
        }

        //public CatalogResponse SaveProductVendors(int vendorIds, int compId, string catIds, string prodId, int user, string sessionId)
        //{
        //    CatalogResponse catalogResponse = new CatalogResponse();
        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
        //        sd.Add("P_PROD_ID", prodId);
        //        sd.Add("P_COMP_ID", compId);
        //        sd.Add("P_CAT_IDS", catIds);
        //        sd.Add("P_VENDOR_IDS", string.IsNullOrEmpty(vendorIds) ? "0" : vendorIds);
        //        sd.Add("P_USER", user);
        //        ds = sqlHelper.SelectList("cm_saveproductvendors", sd);
        //    }
        //    catch (Exception ex)
        //    {
        //        logger.Error(ex.Message);
        //    }

        //    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
        //    {
        //        catalogResponse.ResponseId = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
        //    }

        //    catalogResponse.SessionID = sessionId;
        //    return catalogResponse;
        //}

        public List<CompanyConfiguration> GetCompanyConfiguration(int compID, string configKey, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<CompanyConfiguration> listCompanyConfiguration = new List<CompanyConfiguration>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_COMP_ID", compID);
                sd.Add("P_CONFIG_KEY", configKey);
                DataSet ds = sqlHelper.SelectList("cp_GetCompanyConfiguration", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        CompanyConfiguration companyconfiguration = new CompanyConfiguration();
                        companyconfiguration.CompConfigID = row["C_CONFIG_ID"] != DBNull.Value ? Convert.ToInt16(row["C_CONFIG_ID"]) : 0;
                        companyconfiguration.CompID = row["COMP_ID"] != DBNull.Value ? Convert.ToInt16(row["COMP_ID"]) : 0;
                        companyconfiguration.ConfigKey = row["CONFIG_KEY"] != DBNull.Value ? Convert.ToString(row["CONFIG_KEY"]) : string.Empty;
                        companyconfiguration.ConfigValue = row["CONFIG_VALUE"] != DBNull.Value ? Convert.ToString(row["CONFIG_VALUE"]) : string.Empty;
                        companyconfiguration.ConfigText = row["CONFIG_TEXT"] != DBNull.Value ? Convert.ToString(row["CONFIG_TEXT"]) : string.Empty;
                        if (string.IsNullOrEmpty(companyconfiguration.ConfigText))
                        {
                            companyconfiguration.ConfigText = companyconfiguration.ConfigValue;
                        }

                        companyconfiguration.IsValid = row["IS_VALID"] != DBNull.Value ? (Convert.ToInt32(row["IS_VALID"]) == 1 ? true : false) : false;
                        companyconfiguration.UserID = row["MODIFIED_BY"] != DBNull.Value ? Convert.ToInt16(row["MODIFIED_BY"]) : 0;
                        listCompanyConfiguration.Add(companyconfiguration);
                    }
                }
            }
            catch (Exception ex)
            {
                CompanyConfiguration companyconfiguration = new CompanyConfiguration();
                companyconfiguration.ErrorMessage = ex.Message;
                listCompanyConfiguration.Add(companyconfiguration);
            }

            return listCompanyConfiguration;
        }



        public List<VendorProductData> GetProdVendorData(int vendorid, int catitemid, string sessionid)
        {
            List<VendorProductData> details = new List<VendorProductData>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_V_ID", vendorid);
                sd.Add("P_C_I_ID", catitemid);
                CORE.DataNamesMapper<VendorProductData> mapper = new CORE.DataNamesMapper<VendorProductData>();
                var dataset = sqlHelper.SelectList("cp_GetProdVendorData", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public CatalogResponse SaveProductQuotationTemplate(ProductQuotationTemplate productquotationtemplate, string sessionid)
        {
            CatalogResponse details = new CatalogResponse();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_T_ID", productquotationtemplate.T_ID);
                sd.Add("P_PRODUCT_ID", productquotationtemplate.PRODUCT_ID);
                sd.Add("P_NAME", productquotationtemplate.NAME);
                sd.Add("P_DESCRIPTION", productquotationtemplate.DESCRIPTION);
                sd.Add("P_HAS_SPECIFICATION", productquotationtemplate.HAS_SPECIFICATION);
                sd.Add("P_HAS_PRICE", productquotationtemplate.HAS_PRICE);
                sd.Add("P_HAS_QUANTITY", productquotationtemplate.HAS_QUANTITY);
                sd.Add("P_CONSUMPTION", productquotationtemplate.CONSUMPTION);
                sd.Add("P_UOM", productquotationtemplate.UOM);
                sd.Add("P_HAS_TAX", productquotationtemplate.HAS_TAX);
                sd.Add("P_IS_VALID", productquotationtemplate.IS_VALID);
                sd.Add("P_U_ID", productquotationtemplate.U_ID);
                CORE.DataNamesMapper<CatalogResponse> mapper = new CORE.DataNamesMapper<CatalogResponse>();
                var dataset = sqlHelper.SelectList("cm_SaveProductQuotationTemplate", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


        public List<ProductQuotationTemplate> GetProductQuotationTemplate(int catitemid, string sessionid)
        {
            List<ProductQuotationTemplate> details = new List<ProductQuotationTemplate>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_C_I_ID", catitemid);
                CORE.DataNamesMapper<ProductQuotationTemplate> mapper = new CORE.DataNamesMapper<ProductQuotationTemplate>();
                var dataset = sqlHelper.SelectList("cm_GetProductQuotationTemplate", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }


    }


}
