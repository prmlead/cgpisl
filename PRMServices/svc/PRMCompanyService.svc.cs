﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ServiceModel.Activation;
using PRMServices.Models;
using PRMServices.SQLHelper;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMCompanyService : IPRMCompanyService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();

        #region Services

        public List<Branch> GetCompanyBranch(int compid, int branchid, string sessionid)
        {
            List<Branch> details = new List<Branch>();
            try
            {
                Utilities.ValidateSession(sessionid);
                CORE.DataNamesMapper<Branch> mapper = new CORE.DataNamesMapper<Branch>();
                string query = string.Format("SELECT * FROM Branch WHERE COMP_ID = {0} AND IsValid = 1", compid);
                if (branchid > 0)
                {
                    query = query + " AND BRANCH_ID = " + branchid;
                }

                var dataset = sqlHelper.ExecuteQuery(query);
                details = mapper.Map(dataset.Tables[0]).ToList();                
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<Project> GetBranchProjects(int compid, int branchid, string sessionid)
        {
            List<Project> details = new List<Project>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_COMP_ID", compid);
                sd.Add("P_BRANCH_ID", branchid);
                //sd.Add("P_PROJECT_ID", projectid);
                CORE.DataNamesMapper<Project> mapper = new CORE.DataNamesMapper<Project>();
                var dataset = sqlHelper.SelectList("cp_GetBranchProjects", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<Branch> GetUserBranches(int uid, string sessionid)
        {
            List<Branch> details = new List<Branch>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", uid);
                CORE.DataNamesMapper<Branch> mapper = new CORE.DataNamesMapper<Branch>();
                var dataset = sqlHelper.SelectList("cp_GetUserBranches", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public List<Project> GetUserProjects(int uid, string sessionid)
        {
            List<Project> details = new List<Project>();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", uid);
                CORE.DataNamesMapper<Project> mapper = new CORE.DataNamesMapper<Project>();
                var dataset = sqlHelper.SelectList("cp_GetUserProjects", sd);
                details = mapper.Map(dataset.Tables[0]).ToList();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveBranch(Branch item)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(item.SessionID);

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_BRANCH_ID", item.BRANCH_ID);
                sd.Add("P_COMP_ID", item.COMP_ID);
                sd.Add("P_BRANCH_CODE", item.BRANCH_CODE);
                sd.Add("P_BRANCH_NAME", item.BRANCH_NAME);
                sd.Add("P_BRANCH_ADDRESS", item.BRANCH_ADDRESS);
                sd.Add("P_USER", item.MODIFIED_BY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cp_SaveBranch", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response DeleteBranch(Branch item)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(item.SessionID);

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_BRANCH_ID", item.BRANCH_ID);
                sd.Add("P_COMP_ID", item.COMP_ID);
                sd.Add("P_USER", item.MODIFIED_BY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var ds = sqlHelper.SelectList("cp_deletecompanybranch", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                    {
                        details.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "Unknown error";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        public Response SaveProject(Project item)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(item.SessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", item.PROJECT_ID);
                sd.Add("P_COMP_ID", item.COMP_ID);
                sd.Add("P_PROJECT_CODE", item.PROJECT_CODE);
                sd.Add("P_PROJECT_NAME", item.PROJECT_NAME);
                sd.Add("P_PROJECT_DETAILS", item.PROJECT_DETAILS);
                sd.Add("P_PROJECT_HEAD", item.PROJECT_HEAD);
                sd.Add("P_PROJECT_COMMENTS", item.PROJECT_COMMENTS == null ? "": item.PROJECT_COMMENTS);
                sd.Add("P_BRANCHES", item.SEL_BRANCHES == null ? "" : item.SEL_BRANCHES);
                sd.Add("P_USER", item.MODIFIED_BY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var dataset = sqlHelper.SelectList("cp_SaveProject", sd);
                details = mapper.Map(dataset.Tables[0]).FirstOrDefault();

                //if(details.ObjectID > 0)
                //{
                //    foreach(var branch in item.Branches)
                //    {
                //        sd = new SortedDictionary<object, object>() { };
                //        sd.Add("P_PROJECT_ID", item.PROJECT_ID);
                //        sd.Add("P_BRANCH_ID", branch.BRANCH_ID);
                //        sqlHelper.SelectList("cp_SaveProjectBranch", sd);
                //    }
                //}
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response DeleteProject(Project item)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(item.SessionID);

                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", item.PROJECT_ID);
                sd.Add("P_COMP_ID", item.COMP_ID);
                sd.Add("P_USER", item.MODIFIED_BY);

                CORE.DataNamesMapper<Response> mapper = new CORE.DataNamesMapper<Response>();
                var ds = sqlHelper.SelectList("cp_deletecompanyproject", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    details.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (ds.Tables[0].Columns.Count > 1 && Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString()) == -1)
                    {
                        details.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? ds.Tables[0].Rows[0][1].ToString() : "Unknown error";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveUserBranch(int uid, int[] branches, int user, string sessionid)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(sessionid);
                //foreach(var branch in branches)
                //{
                    SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                    sd.Add("P_BRANCHES", string.Join(",", branches));
                    sd.Add("P_U_ID", uid);
                    sd.Add("P_USER", user);
                    sqlHelper.SelectList("cp_SaveUserBranch", sd);
                //}

                details.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        public Response SaveProjectRequirement(Project item)
        {
            Response details = new Response();
            try
            {
                Utilities.ValidateSession(item.SessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_PROJECT_ID", item.PROJECT_ID);
                sd.Add("P_REQ_ID", item.requirement.RequirementID);
                sd.Add("P_USER", item.MODIFIED_BY);
                sqlHelper.SelectList("cp_SaveProjectRequirement", sd);
                details.ObjectID = 1;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }
        #endregion Services

    }

}