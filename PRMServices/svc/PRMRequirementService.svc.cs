﻿using Newtonsoft.Json;
using PRM.Core;
using PRM.Core.Domain.Requirments;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRM.Services.Acl;
using PRM.Services.Requirements;
using PRMServices.Infrastructure.Mapper;
using PRMServices.Models.Common;
using PRMServices.Models.Requirements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Activation;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PRMRequirementService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select PRMRequirementService.svc or PRMRequirementService.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMRequirementService : IPRMRequirementService
    {


        public PRMRequirementService(
           IRequirementTemplateService RequirementTemplateService,
       IWorkContext workContext,

       IAclMappingService aclMappingService

           )
        {

            _requirementTemplateService = RequirementTemplateService;
            _workContext = workContext;

            _aclMappingService = aclMappingService;
        }

        private readonly IRequirementTemplateService _requirementTemplateService;
        private readonly IWorkContext _workContext;

        private readonly IAclMappingService _aclMappingService;

        #region Requirement Template

        public Response InsertRequirementTemplate(RequirementTemplateModel model)
        {
            var entity = model.ToEntity();

            entity.Company_Id = _workContext.CurrentCompany;

            _requirementTemplateService.InsertRequirementTemplate(entity);


            return new Response() { Message = "Contact detail created" };
        }

        public Response UpdateRequirementTemplate(RequirementTemplateModel model)
        {

            var entity = _requirementTemplateService.GetRequirementTemplateById(model.Id);

            entity = model.ToEntity(entity);

            _requirementTemplateService.UpdateRequirementTemplate(entity);
            return new Response() { Message = "Contact detail updated" };
        }

        public dynamic GetByPageRequirementTemplate(Pager pager)
        {

            var lst = _requirementTemplateService.GetRequirementTemplateList(pager, _workContext.CurrentCompany);
            return JsonConvert.SerializeObject(new PagedResponse() { Data = lst, HasNextPage = lst.HasNextPage, HasPreviousPage = lst.HasPreviousPage, PageIndex = lst.PageIndex, PageSize = lst.PageSize, TotalPages = lst.TotalPages, TotalCount = lst.TotalCount });
        }


        public RequirementTemplate GetByIdRequirementTemplate(string id)
        {
            return _requirementTemplateService.GetRequirementTemplateById(Convert.ToInt32(id));
        }


        public Response DeleteRequirementTemplate(int id)
        {
            var entity = _requirementTemplateService.GetRequirementTemplateById(Convert.ToInt32(id));
            entity.Deleted = true;
            _requirementTemplateService.UpdateRequirementTemplate(entity);

            return new Response() { Message = "Contact detail deleted" };

        }

        public List<RequirementTemplate> GetRequirementTemplate()
        {
            return _requirementTemplateService.GetRequirementTemplateList(new { Company_Id = _workContext.CurrentCompany, Deleted = false, Active = true }).ToList();
        }


        #endregion
    }
}
