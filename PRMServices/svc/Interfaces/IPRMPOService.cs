﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using PRMServices.Models;
using SendGrid.Helpers.Mail;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMPOService
    {
        //[WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinformation?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        //List<POInformation> GetPOInformation(int reqID, int userID, string sessionID);

        //[OperationContract]
        //[WebInvoke(Method = "POST",
        //RequestFormat = WebMessageFormat.Json,
        //ResponseFormat = WebMessageFormat.Json,
        //BodyStyle = WebMessageBodyStyle.WrappedRequest,
        //UriTemplate = "savepoinfo")]List<PaymentInfo> GetPendingPayments(int compid, string sessionid)
        //Response SavePOInfo(POInformation[] poList, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdespoinfo?reqid={reqID}&userid={userID}&sessionid={sessionID}")]
        POVendor GetDesPoInfo(int reqID, int userID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendors?reqid={reqID}&sessionid={sessionID}")]
        List<UserDetails> GetVendors(int reqID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdescdispatch?poid={poID}&dtid={dtID}&sessionid={sessionID}")]
        DispatchTrack GetDescDispatch(int poID, int dtID, string sessionID);
        
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpolist?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoList(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getvendorpoinfo?reqid={reqID}&userid={userID}&poid={poID}&sessionid={sessionID}")]
        VendorPO GetVendorPoInfo(int reqID, int userID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtracklist?poorderid={poorderid}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrackList(string poorderid, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getdispatchtrack?poorderid={poorderid}&dcode={dcode}&sessionid={sessionID}")]
        List<DispatchTrack> GetDispatchTrack(string poorderid, string dcode, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpaymenttrack?vendorid={vendorID}&poid={poID}&sessionid={sessionID}")]
        PaymentTrack GetPaymentTrack(int vendorID, int poID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedule?ponumber={ponumber}&sessionid={sessionID}")]
        List<POSchedule> GetPOScheduler(string ponumber, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getAssignedPO?userid={userId}&sessionid={sessionID}")]
        List<PendingPO> GetAssignedPO(int userId, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getCompanyPendingPOS?compid={compId}&sessionid={sessionID}&isVendor={IsVendor}&userId={UserId}")]
        List<PendingPO> GetCompanyPendingPOS(int compId, string sessionID, bool IsVendor, int UserId);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetFiltersOnLoadData?compid={compId}&sessionid={sessionID}")]
        List<Filter> GetFiltersOnLoadData(int compId, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getDeliverySchedules?poNumbers={poNumbers}&sessionid={sessionID}")]
        List<POSchedule> GetDeliverySchedules(string poNumbers,string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpendingpayments?compid={compid}&sessionid={sessionid}")]
        List<PaymentInfo> GetPendingPayments(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulefilters?compid={compid}&sessionid={sessionid}")]
        List<CArrayKeyValue> GetPOScheduleFilters(int compid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposchedulelist?compid={compid}&uid={uid}&search={search}" +
           "&categoryid={categoryid}&productid={productid}&supplier={supplier}&postatus={postatus}&deliverystatus={deliverystatus}&plant={plant}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
           "&onlycontracts={onlycontracts}&excludecontracts={excludecontracts}&ackStatus={ackStatus}&buyer={buyer}&purchaseGroup={purchaseGroup}&sessionid={sessionid}")]
        List<POScheduleDetails> GetPOScheduleList(int compid, int uid, string search, string categoryid, string productid, string supplier, string postatus, string deliverystatus, string plant,
           string fromdate, string todate, int page, int pagesize, int onlycontracts, int excludecontracts,string ackStatus, string buyer,string purchaseGroup, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getposcheduleitems?ponumber={ponumber}&moredetails={moredetails}&forasn={forasn}&sessionid={sessionid}")]
        List<POScheduleDetailsItems> GetPOScheduleItems(string ponumber, int moredetails, bool forasn, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoinvoicedetails?ponumber={ponumber}&sessionid={sessionid}&vendorID={VendorID}&invoiceID={InvoiceID}")]
        POInvoice[] GetPOInvoiceDetails(string ponumber, string sessionid, int VendorID = 0, int InvoiceID = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getgrndetailslist?compid={compid}&uid={uid}&search={search}" +
            "&supplier={supplier}&fromdate={fromdate}&todate={todate}&page={page}&pagesize={pagesize}" +
            "&sessionid={sessionid}")]
        List<GRNDetails> GetGRNDetailsList(int compid, string uid, string search, string supplier, string fromdate, string todate, int page, int pagesize, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetails?compid={compid}&asnid={asnid}&ponumber={ponumber}&grncode={grncode}&asncode={asncode}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetails(int compid, int asnid, string asncode, string ponumber, string grncode, int vendorid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getasndetailslist?ponumber={ponumber}&vendorid={vendorid}&sessionid={sessionid}")]
        List<ASNDetails> GetASNDetailsList(string ponumber, int vendorid, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "sendpoemails?sessionid={sessionid}")]
        Response SendPOEmails(string sessionid);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "saveasndetails")]
        Response SaveASNDetails(ASNDetails[] detailsarray);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "savepoinvoice")]
        Response SavePOInvoice(POInvoice details);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "editPOInvoice")]
        Response EditPOInvoice(POInvoice details);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "savepoattachments")]
        Response SavePOAttachments(POScheduleDetails details);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "poapproval")]
        Response PoApproval(POScheduleDetails details,bool isPoApprove,string isPoRejectComments,int currentUserID);
        
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "deletepoinvoice?ponumber={ponumber}&invoicenumber={invoicenumber}&invoiceId={invoiceId}&wfId={wfId}&sessionid={sessionid}")]
        Response DeletePOInvoice(string ponumber, string invoicenumber, int invoiceId, int wfId, string sessionid);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "saveposchedule")]
        Response SavePOSchedule(POSchedule details);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepovendorack")]
        Response SavePOVendorAck(string ponumber, string poitemline, string status, string vendordeliverydate,string vendordeliverydateString, string comments, int user,int isVendPoAck, List<FileUpload> vendorAttachments,List<string> ExistingVendorAttachments, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "savepovendorquantityack")]
        Response SavePOVendorQuantityAck(string ponumber, string poitemline, string status, string comments, int isVendPoAck, decimal quantity, int user, string sessionid);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "checkuniqueifexists")]
        bool CheckUniqueIfExists(string param, string idtype, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedescpoinfo")]
        Response SaveDescPoInfo(POVendor povendor);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "updatepostatus")]
        Response UpdatePOStatus(int poID, string status, string sessionID);
        
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedesdispatchtrack")]
        Response SaveDesDispatchTrack(DispatchTrack dispatchtrack, POVendor povendor);        

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savedispatchtrack")]
        Response SaveDispatchTrack(DispatchTrack dispatchtrack, string requestType);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savevendorpoinfo")]
        Response SaveVendorPOInfo(VendorPO vendorpo);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        UriTemplate = "savepaymentinfo")]
        Response SavePaymentInfo(PaymentInfo paymentinfo);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpogeneratedetails?compid={compid}&template={template}&vendorid={vendorid}&status={status}&creator={creator}" +
            "&plant={plant}&purchasecode={purchasecode}&search={search}&sessionid={sessionid}&page={page}&pagesize={pagesize}")]
        SAPOEntity[] GetPOGenerateDetails(int compid, string template, int vendorid, string status, string creator,
            string plant, string purchasecode, string search, string sessionid, int page = 0, int pagesize = 0);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getpoitems?ponumber={ponumber}&quotno={quotno}&sessionid={sessionid}")]
        SAPOEntity[] GetPOItems(string ponumber, string quotno, string sessionid);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getFilterValues?compid={compID}&sessionid={sessionID}")]
        List<POFieldMapping> GetFilterValues(int compID, string sessionID);

        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getrequirementpo?compid={compid}&reqid={reqid}&sessionid={sessionid}")]
        SAPOEntity[] GetRequirementPO(int compid, int reqid, string sessionid);
    }
}
