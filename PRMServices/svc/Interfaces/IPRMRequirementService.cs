﻿using PRM.Core.Domain.Requirments;
using PRM.Core.Models;
using PRM.Core.Pagers;
using PRMServices.Models.Requirements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPRMRequirementService" in both code and config file together.
    [ServiceContract]
    public interface IPRMRequirementService
    {
        #region Contact details
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "requirement_template/insert")]
        Response InsertRequirementTemplate(RequirementTemplateModel model);


        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "requirement_template/update")]
        Response UpdateRequirementTemplate(RequirementTemplateModel model);



        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.Bare,
       UriTemplate = "requirement_template/getbypage")]
        dynamic GetByPageRequirementTemplate(Pager model);

        [OperationContract]
        [WebInvoke(Method = "GET",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "requirement_template/getbyid/{id}")]
        RequirementTemplate GetByIdRequirementTemplate(string id);

        [OperationContract]
        [WebInvoke(Method = "POST",
       RequestFormat = WebMessageFormat.Json,
       ResponseFormat = WebMessageFormat.Json,
       BodyStyle = WebMessageBodyStyle.WrappedRequest,
       UriTemplate = "requirement_template/delete")]
        Response DeleteRequirementTemplate(int id);


        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        BodyStyle = WebMessageBodyStyle.Bare,
        UriTemplate = "requirement_template/get")]
        List<RequirementTemplate> GetRequirementTemplate();


        #endregion
    }
}
