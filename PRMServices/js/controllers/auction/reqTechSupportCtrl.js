prmApp
    .controller('reqTechSupportCtrl', ["$scope", "$state", "$stateParams", "$log", "userService", "auctionsService", "fileReader", "growlService",
        function ($scope, $state, $stateParams, $log, userService, auctionsService, fileReader, growlService) {


        $scope.message = 'Team, We have posted the following requirement in the system. Please train the vendors and get the quotations on time.'

        auctionsService.getrequirementdata({ "reqid": $stateParams.reqId, "sessionid": userService.getUserToken(), 'userid': userService.getUserId() })
            .then(function (response) {
                $scope.auctionItem = response;

                $scope.reqShare = {
                    requirement: $scope.auctionItem,
                    message: $scope.message,
                    expectedBidding: '',
                    minReductionAmount: $scope.auctionItem.minBidAmount,
                    vendorRankComparision: $scope.auctionItem.minVendorComparision,
                    negotiationDuration: $scope.auctionItem.negotiationDuration,
                    quotationFreezTime: $scope.auctionItem.quotationFreezTime
                };

                $scope.quotationFreezTime = userService.toLocalDate($scope.auctionItem.quotationFreezTime);
        });

        

        $scope.reqTechSupport = function () {

            $scope.reqShare.sessionID = userService.getUserToken();

            var params = {
                reqShare: $scope.reqShare
            };

            auctionsService.reqTechSupport(params)                          
                .then(function (response) {
                    if (response.errorMessage == "") {
                        growlService.growl("Requirement Details have been sent Successfully to PRM360 Tech Support Team.", "success");;
                    }
                });

        };
        
    }]);