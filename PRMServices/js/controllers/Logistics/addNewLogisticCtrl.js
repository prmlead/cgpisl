prmApp
    // =========================================================================
    // COMMON FORMS
    // =========================================================================

    .controller('addNewLogisticCtrl', ["$state", "$stateParams", "$scope", "auctionsService", "userService", "$http", "$window", "domain", "fileReader", "growlService",
        "$log", "$filter", "ngDialog", "techevalService", "fwdauctionsService", "logisticServices", "catalogService",
        function ($state, $stateParams, $scope, auctionsService, userService, $http, $window, domain, fileReader, growlService,
            $log, $filter, ngDialog, techevalService, fwdauctionsService, logisticServices, catalogService) {

        // #region DECLARATIONS

            $scope.abc = 'aaaaa';

        if ($stateParams.Id) {
            $scope.stateParamsReqID = $stateParams.Id;
        } else {
            $scope.stateParamsReqID = 0;
        };
        $scope.isTechEval = false;
        $scope.isForwardBidding = false;
        $scope.allCompanyVendors = [];
        var curDate = new Date();
        var today = moment();
        var tomorrow = today.add('days', 1);
        //var dateObj = $('.datetimepicker').datetimepicker({
        //    format: 'DD/MM/YYYY',
        //    useCurrent: false,
        //    minDate: tomorrow,
        //    keepOpen: false
        //});
        $scope.subcategories = [];
        $scope.sub = {
            selectedSubcategories: [],
        }
        $scope.selectedCurrency = {};
        $scope.currencies = [];
        $scope.questionnaireList = [];
        $scope.postRequestLoding = false;
        $scope.selectedSubcategories = [];
        $scope.selectVendorShow = true;
        $scope.isEdit = false;
        //Input Slider
        this.nouisliderValue = 4;
        this.nouisliderFrom = 25;
        this.nouisliderTo = 80;
        this.nouisliderRed = 35;
        this.nouisliderBlue = 90;
        this.nouisliderCyan = 20;
        this.nouisliderAmber = 60;
        this.nouisliderGreen = 75;
        //Color Picker
        this.color = '#03A9F4';
        this.color2 = '#8BC34A';
        this.color3 = '#F44336';
        this.color4 = '#FFC107';
        $scope.Vendors = [];
        $scope.VendorsTemp = [];
        $scope.categories = [];
        $scope.selectedA = [];
        $scope.selectedB = [];
        $scope.showCategoryDropdown = false;
        $scope.checkVendorPhoneUniqueResult = false;
        $scope.checkVendorEmailUniqueResult = false;
        $scope.checkVendorPanUniqueResult = false;
        $scope.checkVendorTinUniqueResult = false;
        $scope.checkVendorStnUniqueResult = false;
        $scope.showFreeCreditsMsg = false;
        $scope.showNoFreeCreditsMsg = false;
        $scope.formRequest = {
            isTabular: true,
            auctionVendors: [],
            listRequirementItems: [],
            isQuotationPriceLimit: false,
            quotationPriceLimit: 0,
            quotationFreezTime: '',
            deleteQuotations: false,
            expStartTimeName: '',
            isDiscountQuotation: 0,
            multipleAttachments: []
        };
        $scope.Vendors.city = "";
        $scope.Vendors.quotationUrl = "";
        $scope.vendorsLoaded = false;
        $scope.requirementAttachment = [];
        $scope.sessionid = userService.getUserToken();
        $scope.selectedQuestionnaire == {}
        $scope.formRequest.indentID = 0;
        $scope.cijList = [];
        $scope.indentList = [];
        $scope.compID = userService.getUserCompanyId();
        $scope.showSimilarNegotiations = false;
        $scope.CompanyLeads = {};
        $scope.searchstring = '';
        $scope.isRequirementPosted = 0;
        $scope.newVendor = {};
        $scope.Attaachmentparams = {};
        $scope.newVendor.panno = "";
        $scope.newVendor.vatNum = "";
        $scope.newVendor.serviceTaxNo = "";
        $scope.formRequest.checkBoxEmail = true;
        $scope.formRequest.checkBoxSms = true;
        $scope.postRequestLoding = false;
        $scope.formRequest.urgency = '';
        $scope.formRequest.deliveryLocation = '';
        $scope.formRequest.paymentTerms = '';
        $scope.formRequest.isSubmit = 0;
        $scope.titleValidation = $scope.DANoValidation = $scope.attachmentNameValidation = false;
        $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
        $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.selectedCurrencyValidation =
            $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;
        $scope.ItemFile = '';
        $scope.itemSNo = 1;
        $scope.ItemFileName = '';
        $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
        $scope.pageNo = 1;
        $scope.reqDepartments = [];
        $scope.userDepartments = [];
        $scope.noDepartments = false;
        $scope.reqDeptDesig = [];
        $scope.searchvendorstring = '';
        $scope.formRequest.category = '';
        $scope.requirementItems = {
                productSNo: $scope.itemSNo++,
                ItemID: 0,
                productIDorName: '',                
                productDescription: '',
                productQuantity: 0,     
                typeOfPacking: '',
                modeOfShipment: '',
                natureOfGoods: 0,
                storageCondition: '',
                portOfLanding: '',
                finalDestination: '',
                deliveryLocation: '',
                preferredAirlines: '',
                isDeleted: 0,
                itemAttachment: '',
                attachmentName: '',
                netWeight: 0,
                ispalletize: 1,
                palletizeQty: 0,
                palletizeLength: 0,
                palletizeBreadth: 0,
                palletizeHeight: 0
        }
        $scope.selectedSubCategoriesList = [];
        $scope.formRequest.listRequirementItems.push($scope.requirementItems);

        $scope.itemPreviousPrice = [];
        $scope.itemPreviousPrice.lastPrice = -1;
        $scope.bestPriceEnable = 0;

        // #endregion DECLARATIONS

        // #region FUNCTION

        $scope.getPreviousItemPrice = function (itemDetails, index) {
            $scope.itemPreviousPrice = [];
            $scope.itemPreviousPrice.lastPrice = -1;
            $scope.bestPriceEnable = index;
            $log.info(itemDetails);
            itemDetails.sessionID = userService.getUserToken();
            itemDetails.compID = userService.getUserCompanyId();
            //itemDetails.netWeight = $scope.requirementItems.netWeight;
            logisticServices.getPreviousItemPrice(itemDetails)
                .then(function (response) {
                    $scope.itemPreviousPrice = response;
                    $scope.itemPreviousPrice.forEach(function (item, itemindex) {
                        $scope.itemPreviousPrice.lastPrice = Number(item.initialPrice);
                        item.currentTime = new moment(item.currentTime).format("DD-MM-YYYY HH:mm");
                        $scope.itemPreviousPrice.lastPriceVendor = item.companyName;
                        $scope.itemPreviousPrice.lastPriceAirline = item.airline;
                        
                    })
                    if ($scope.itemPreviousPrice.length == 0) {
                        return $scope.getbestprice = "No previous records found";
                    } else {
                        return $scope.getbestprice = "";
                    }
                    //if (response && response.errorMessage == '') {
                    //    $scope.itemPreviousPrice.lastPrice = Number(response.initialPrice);
                    //    $scope.itemPreviousPrice.lastPriceDate = new moment(response.currentTime).format("DD-MM-YYYY HH:mm");
                    //    $scope.itemPreviousPrice.lastPriceVendor = response.companyName;
                    //    $scope.itemPreviousPrice.lastPriceAirline = response.airline;
                    //    $log.info($scope.itemPreviousPrice);
                    //}
                });
        };

         


        $scope.budgetValidate = function () {
            if ($scope.formRequest.budget != "" && (isNaN($scope.formRequest.budget) || $scope.formRequest.budget.indexOf('.') > -1)) {
                $scope.postRequestLoding = false;
                swal({
                    title: "Error!",
                    text: "Please enter valid budget, budget should be greater than 1,00,000.",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                },
                    function () {

                    });

                $scope.formRequest.budget = "";
            }
        };

        $scope.clickToOpen = function () {
            ngDialog.open({ template: 'login/termsAddNewReq.html', width: 1000 });
        };

        $scope.changeCategory = function () {

            $scope.selectedSubCategoriesList = [];
            $scope.formRequest.auctionVendors = [];
            $scope.loadSubCategories();
            $scope.getvendors();
        }

        $scope.getCreditCount = function () {
            userService.getProfileDetails({ "userid": userService.getUserId(), "sessionid": userService.getUserToken() })
                .then(function (response) {
                    $scope.userDetails = response;
                    $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                    $scope.selectedCurrency = $scope.selectedCurrency[0];
                    if (response.creditsLeft) {
                        $scope.showFreeCreditsMsg = true;
                    } else {
                        $scope.showNoFreeCreditsMsg = true;
                    }
                });
        }
            
        $scope.getvendors = function () {
            $scope.vendorsLoaded = false;
            var category = [];

            category.push($scope.formRequest.category);
            if ($scope.formRequest.category != undefined) {
                var params = { 'Categories': category, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                $http({
                    method: 'POST',
                    url: domain + 'getvendors',
                    encodeURI: true,
                    headers: { 'Content-Type': 'application/json' },
                    data: params
                }).then(function (response) {
                    if (response && response.data) {
                        if (response.data.length > 0) {
                            $scope.Vendors = response.data;
                            $scope.vendorsLoaded = true;
                            for (var j in $scope.formRequest.auctionVendors) {
                                for (var i in $scope.Vendors) {
                                    if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                        $scope.Vendors.splice(i, 1);
                                    }
                                }
                            }
                            
                            $scope.VendorsTemp = $scope.Vendors;
                            $scope.searchVendors('');

                        }
                    } else {
                    }
                }, function (result) {
                });
            }

        };

        $scope.getReqQuestionnaire = function () {
            

        }

        $scope.getQuestionnaireList = function () {
            techevalService.getquestionnairelist(0)
                .then(function (response) {
                    $scope.questionnaireList = $filter('filter')(response, { reqID: 0 });
                    if ($stateParams.Id && $stateParams.Id > 0) {
                        techevalService.getreqquestionnaire($stateParams.Id, 1)
                            .then(function (response) {
                                $scope.selectedQuestionnaire = response;

                                if ($scope.selectedQuestionnaire && $scope.selectedQuestionnaire.evalID > 0) {
                                    $scope.isTechEval = true;
                                }

                                $scope.questionnaireList.push($scope.selectedQuestionnaire);
                            })
                    }
                })
        };

        $scope.getCategories = function () {
            $http({
                method: 'GET',
                url: domain + 'getcategories?userid=' + userService.getUserId() + '&sessionid=' + userService.getUserToken(),
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {

                    if (response.data.length > 0) {
                        $scope.categories = _.uniq(_.map(response.data, 'category'));
                        $scope.categoriesdata = response.data;
                        $scope.showCategoryDropdown = true;
                    }
                } else {
                }
            }, function (result) {
            });
            $http({
                method: 'GET',
                url: domain + 'getkeyvaluepairs?parameter=CURRENCY',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' }
            }).then(function (response) {
                if (response && response.data) {
                    if (response.data.length > 0) {
                        $scope.currencies = response.data;
                        $scope.getCreditCount();
                    }
                } else {
                }
            }, function (result) {
            });
        };

        $scope.getData = function () {
            
            $scope.getCategories();

            if ($stateParams.Id > 0) {
                var id = $stateParams.Id;
                $scope.isEdit = true;

                logisticServices.GetRequirementData({ "reqid": $stateParams.Id, "sessionid": userService.getUserToken(), "userid": userService.getUserId() })
                    .then(function (response) {

                        var category = response.category[0];
                        response.category = category;
                        response.taxes = parseInt(response.taxes);
                        $scope.formRequest = response;

                        $scope.itemSNo = $scope.formRequest.itemSNoCount;



                        if (!$scope.formRequest.multipleAttachments) {
                            $scope.formRequest.multipleAttachments = [];
                        }
                        $scope.formRequest.attFile = response.attachmentName;
                        if ($scope.formRequest.attFile != '' && $scope.formRequest.attFile != null && $scope.formRequest.attFile != undefined)
                        {

                       
                        var attchArray = $scope.formRequest.attFile.split(',');

                        attchArray.forEach(function (att, index) {

                            var fileUpload = {
                                fileStream: [],
                                fileName: '',
                                fileID: att
                            };

                            $scope.formRequest.multipleAttachments.push(fileUpload);
                        })
                        
                        }
                        

                        $scope.formRequest.checkBoxEmail = true;
                        $scope.formRequest.checkBoxSms = true;
                        $scope.loadSubCategories();

                        $scope.isRequirementPosted = $scope.formRequest.auctionVendors.length;


                        $scope.selectedSubcategories = response.subcategories.split(",");
                        for (i = 0; i < $scope.selectedSubcategories.length; i++) {
                            for (j = 0; j < $scope.subcategories.length; j++) {
                                if ($scope.selectedSubcategories[i] == $scope.subcategories[j].subcategory) {
                                    $scope.subcategories[j].ticked = true;
                                }
                            }
                        }
                        $scope.selectSubcat();
                        
                        $scope.formRequest.quotationFreezTime = new moment($scope.formRequest.quotationFreezTime).format("DD-MM-YYYY HH:mm");
                        
                        $scope.formRequest.expStartTime = new moment($scope.formRequest.expStartTime).format("DD-MM-YYYY HH:mm");

                        $scope.SelectedVendors = $scope.formRequest.auctionVendors;
                    });
            }

        };

        $scope.showSimilarNegotiationsButton = function (value, searchstring) {
            $scope.showSimilarNegotiations = value;
            if (!value) {
                $scope.CompanyLeads = {};
            }
            if (value) {
                if (searchstring.length < 3) {
                    $scope.CompanyLeads = {};
                }
                if (searchstring.length > 2) {
                    $scope.searchstring = searchstring;
                    $scope.GetCompanyLeads(searchstring);
                }
            }
            return $scope.showSimilarNegotiations;
        }

        $scope.GetCompanyLeads = function (searchstring) {
            if (searchstring.length < 3) {
                $scope.CompanyLeads = {};
            }
            if ($scope.showSimilarNegotiations && searchstring.length > 2) {
                $scope.searchstring = searchstring;
                var params = { "userid": userService.getUserId(), "searchstring": $scope.searchstring, "searchtype": 'Title', "sessionid": userService.getUserToken() };
                auctionsService.GetCompanyLeads(params)
                    .then(function (response) {
                        $scope.CompanyLeads = response;
                        $scope.CompanyLeads.forEach(function (item, index) {
                            item.postedOn = new moment(item.postedOn).format("DD-MM-YYYY HH:mm");
                        })
                    });
            }
        }

        $scope.changeScheduledAuctionsLimit = function () {
            $scope.scheduledLimit = 8;
            $scope.getMiniItems();
        }

        $scope.loadSubCategories = function () {
            $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.formRequest.category });
            /*$scope.subcategories = _.map($scope.subcategories, 'subcategory');*/
        }

        $scope.selectSubcat = function (subcat) {

            $scope.selectedSubCategoriesList = [];

            if (!$scope.isEdit) {
                $scope.formRequest.auctionVendors = [];
            }
            $scope.vendorsLoaded = false;
            var category = [];
            var count = 0;
            var succategory = "";
            $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
            selectedcount = $scope.sub.selectedSubcategories.length;
            if (selectedcount > 0) {
                succategory = _.map($scope.sub.selectedSubcategories, 'id');
                category.push(succategory);

                $scope.selectedSubCategoriesList = succategory;
                
                if ($scope.formRequest.category != undefined) {
                    var params = { 'Categories': succategory, 'sessionID': userService.getUserToken(), 'count': selectedcount, 'uID': userService.getUserId(), evalID: $scope.selectedQuestionnaire ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbycatnsubcat',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                $scope.searchVendors('');

                            }
                        } else {
                        }
                    }, function (result) {
                    });
                }
            } else {
                $scope.getvendors();
            }

        }

        $scope.checkVendorUniqueResult = function (idtype, inputvalue) {
          

            if (idtype == "PHONE") {
                $scope.checkVendorPhoneUniqueResult = false;
            } else if (idtype == "EMAIL") {
                $scope.checkVendorEmailUniqueResult = false;
            }
            else if (idtype == "PAN") {
                $scope.checkVendorPanUniqueResult = false;
            }
            else if (idtype == "TIN") {
                $scope.checkVendorTinUniqueResult = false;
            }
            else if (idtype == "STN") {
                $scope.checkVendorStnUniqueResult = false;
            }

            if (inputvalue == "" || inputvalue == undefined) {
                return false;
            }

            userService.checkUserUniqueResult(inputvalue, idtype).then(function (response) {
                if (idtype == "PHONE") {
                    $scope.checkVendorPhoneUniqueResult = !response;
                } else if (idtype == "EMAIL") {
                    $scope.checkVendorEmailUniqueResult = !response;
                }
                else if (idtype == "PAN") {
                    $scope.checkVendorPanUniqueResult = !response;
                }
                else if (idtype == "TIN") {
                    $scope.checkVendorTinUniqueResult = !response;
                }
                else if (idtype == "STN") {
                    $scope.checkVendorStnUniqueResult = !response;
                }
            });
        };

        $scope.selectForA = function (item) {
            var index = $scope.selectedA.indexOf(item);
            if (index > -1) {
                $scope.selectedA.splice(index, 1);
            } else {
                $scope.selectedA.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedA.length; i++) {
                $scope.formRequest.auctionVendors.push($scope.selectedA[i]);
                $scope.Vendors.splice($scope.Vendors.indexOf($scope.selectedA[i]), 1);
                $scope.VendorsTemp.splice($scope.VendorsTemp.indexOf($scope.selectedA[i]), 1);
            }
            $scope.reset();
        }

        $scope.selectForB = function (item) {
            var index = $scope.selectedB.indexOf(item);
            if (index > -1) {
                $scope.selectedB.splice(index, 1);
            } else {
                $scope.selectedB.splice($scope.selectedA.length, 0, item);
            }
            for (i = 0; i < $scope.selectedB.length; i++) {
                $scope.Vendors.push($scope.selectedB[i]);
                $scope.VendorsTemp.push($scope.selectedB[i]);
                $scope.formRequest.auctionVendors.splice($scope.formRequest.auctionVendors.indexOf($scope.selectedB[i]), 1);
            }
            $scope.reset();
        }

        $scope.AtoB = function () {

        }

        $scope.BtoA = function () {

        }

        $scope.reset = function () {
            $scope.selectedA = [];
            $scope.selectedB = [];
        }

        $scope.multipleAttachments = [];

        $scope.getFile = function () {
            $scope.progress = 0;

            //$scope.file = $("#attachement")[0].files[0];
            $scope.multipleAttachments = $("#attachement")[0].files;

            $scope.multipleAttachments = Object.values($scope.multipleAttachments)
            

            $scope.multipleAttachments.forEach(function (item, index) {

                fileReader.readAsDataUrl(item, $scope)
                    .then(function (result) {

                        var fileUpload = {
                            fileStream: [],
                            fileName: '',
                            fileID: 0
                        };

                        var bytearray = new Uint8Array(result);
                        
                        fileUpload.fileStream = $.makeArray(bytearray);
                        fileUpload.fileName = item.name;

                        if (!$scope.formRequest.multipleAttachments) {
                            $scope.formRequest.multipleAttachments = [];
                        }

                        $scope.formRequest.multipleAttachments.push(fileUpload);

                    });

            })
            
        };

        $scope.deleteAttachment = function (reqid) {
            $scope.Attaachmentparams = {
                reqID: reqid,
                userID: userService.getUserId()
            }
            auctionsService.deleteAttachment($scope.Attaachmentparams)
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("Attachment deleted Successfully", "inverse");
                        $scope.getData();
                    }
                });
        }

        $scope.addVendor = function () {
            $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
            $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
            var addVendorValidationStatus = false;
            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
            if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
                $scope.companyvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
                $scope.firstvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
                $scope.lastvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                $scope.contactvalidation = true;
                addVendorValidationStatus = true;
            }
            else if ($scope.newVendor.contactNum.length != 10) {
                $scope.contactvalidationlength = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
                $scope.emailvalidation = true;
                addVendorValidationStatus = true;
            }
            else if (!$scope.emailRegx.test($scope.newVendor.email)) {
                $scope.emailregxvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.vendorcurrency == "" || $scope.newVendor.vendorcurrency === undefined) {
                $scope.vendorcurrencyvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.panno != "" && $scope.newVendor.panno != undefined && !$scope.panregx.test($scope.newVendor.panno)) {
                $scope.panregxvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.newVendor.vatNum != "" && $scope.newVendor.vatNum != undefined && $scope.newVendor.vatNum.length != 11) {
                $scope.tinvalidation = true;
                addVendorValidationStatus = true;
            }

            if ($scope.newVendor.serviceTaxNo != "" && $scope.newVendor.serviceTaxNo != undefined && $scope.newVendor.serviceTaxNo.length != 15) {
                $scope.stnvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.formRequest.category == "" || $scope.formRequest.category === undefined) {
                $scope.categoryvalidation = true;
                addVendorValidationStatus = true;
            }
            if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
                addVendorValidationStatus = true;
            }
            if (addVendorValidationStatus) {
                return false;
            }
            var vendCAtegories = [];
            $scope.newVendor.category = $scope.formRequest.category;
            vendCAtegories.push($scope.newVendor.category);
            var params = {
                "register": {
                    "firstName": $scope.newVendor.firstName,
                    "lastName": $scope.newVendor.lastName,
                    "email": $scope.newVendor.email,
                    "phoneNum": $scope.newVendor.contactNum,
                    "username": $scope.newVendor.contactNum,
                    "password": $scope.newVendor.contactNum,
                    "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                    "isOTPVerified": 0,
                    "category": $scope.newVendor.category,
                    "userType": "VENDOR",
                    "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                    "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                    "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                    "referringUserID": userService.getUserId(),
                    "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                    "errorMessage": "",
                    "sessionID": "",
                    "userID": 0,
                    "department": "",
                    "currency": $scope.newVendor.vendorcurrency.key,
                    "altPhoneNum": $scope.newVendor.altPhoneNum,
                    "altEmail": $scope.newVendor.altEmail,
                    "subcategories": $scope.selectedSubCategoriesList
                }
            };
            $http({
                method: 'POST',
                url: domain + 'register',
                encodeURI: true,
                headers: { 'Content-Type': 'application/json' },
                data: params
            }).then(function (response) {
                if ((response && response.data && response.data.errorMessage == "") || response.data.errorMessage == 'User assigned to Company.') {
                    $scope.formRequest.auctionVendors.push({ vendorName: $scope.newVendor.firstName + " " + $scope.newVendor.lastName, companyName: $scope.newVendor.companyName, vendorID: response.data.objectID });
                    $scope.newVendor = null;
                    $scope.newVendor = {};
                    $scope.addVendorShow = false;
                    $scope.selectVendorShow = true;
                    $scope.newVendor = {};
                    $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                    growlService.growl("Vendor Added Successfully.", 'inverse');
                } else if (response && response.data && response.data.errorMessage) {
                    growlService.growl(response.data.errorMessage, 'inverse');
                } else {
                    growlService.growl('Unexpected Error Occurred', 'inverse');
                }
            });
        }

        $scope.postRequest = function (isSubmit, pageNo, navigateToView) {
            $scope.titleValidation = $scope.DANoValidation = $scope.attachmentNameValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.expStartTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.questionnaireValidation = false;

            $scope.postRequestLoding = true;
            $scope.formRequest.isSubmit = isSubmit;

            if (isSubmit == 1 || pageNo == 1) {
                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.daNo == null || $scope.formRequest.daNo == '') {
                    $scope.DANoValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if (isSubmit == 1) {
                    if ($scope.formRequest.isTabular) {
                        $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                            if (!item.attachmentName || item.attachmentName == '') {
                                $scope.attachmentNameValidation = true;
                                return false;
                            }
                        })

                    }
                    if (!$scope.formRequest.isTabular) {
                        if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                            $scope.descriptionValidation = true;
                            $scope.postRequestLoding = false;
                            return false;
                        }
                    }
                }
            }

            if (isSubmit == 1) {
                //if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                //    $scope.deliveryLocationValidation = true;
                //    $scope.postRequestLoding = false;
                //    return false;
                //}
                if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    $scope.deliveryTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    $scope.paymentTermsValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
            }

            if (isSubmit == 1) {
                if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                    $scope.urgencyValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                    $scope.quotationFreezTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                    $scope.expStartTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= '') && $scope.formRequest.isQuotationPriceLimit == true) {
                    $scope.quotationPriceLimitValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.isQuotationPriceLimit == false) {
                    $scope.formRequest.quotationPriceLimit = 0;
                }
                if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                    $scope.noOfQuotationRemindersValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                    $scope.remindersTimeIntervalValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                    $scope.questionnaireValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
            }


            if (pageNo != 4) {
                $scope.textMessage = "Save as Draft.";
            }

            if ($scope.formRequest.checkBoxEmail == true && $scope.formRequest.checkBoxSms == true && pageNo == 4) {
                $scope.textMessage = "This will send an email and sms invite to all the vendors selected above.";
            }
            else if ($scope.formRequest.checkBoxEmail == true && pageNo == 4) {
                $scope.textMessage = "This will send an email invite to all the vendors selected above.";
            }
            else if ($scope.formRequest.checkBoxSms == true && pageNo == 4) {
                $scope.textMessage = "This will send an SMS invite to all the vendors selected above.";
            }
            else if (pageNo == 4) {
                $scope.textMessage = "This will not send an SMS or EMAIL the vendors selected above.";
            }
            $scope.formRequest.currency = $scope.selectedCurrency.value;
            $scope.formRequest.timeZoneID = 190;
            swal({
                title: "Are you sure?",
                text: $scope.textMessage,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "OK",
                closeOnConfirm: true
            }, function () {
                $scope.postRequestLoding = true;

                var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationDate = new Date(m);
                var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                $scope.formRequest.quotationFreezTime = "/Date(" + milliseconds + "000+0530)/";

                var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationDate = new Date(m);
                var milliseconds = parseInt(quotationDate.getTime() / 1000.0);
                $scope.formRequest.expStartTime = "/Date(" + milliseconds + "000+0530)/";

                $scope.formRequest.requirementID = $stateParams.Id ? $stateParams.Id : -1;
                $scope.formRequest.customerID = userService.getUserId();
                $scope.formRequest.customerFirstname = userService.getFirstname();
                $scope.formRequest.customerLastname = userService.getLastname();
                $scope.formRequest.isClosed = "NOTSTARTED";
                $scope.formRequest.endTime = "";
                $scope.formRequest.sessionID = userService.getUserToken();
                $scope.formRequest.subcategories = "";
                $scope.formRequest.budget = 100000;
                $scope.formRequest.custCompID = userService.getUserCompanyId();
                $scope.formRequest.customerCompanyName = userService.getUserCompanyId();

                for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    $scope.formRequest.subcategories += $scope.sub.selectedSubcategories[i].subcategory + ",";
                }
                var category = [];
                category.push($scope.formRequest.category);
                $scope.formRequest.category = category;
                if (!$scope.formRequest.isForwardBidding) {
                    $scope.formRequest.cloneID = 0;
                    if ($stateParams.reqObj && $stateParams.reqObj.cloneId && $stateParams.reqObj.cloneId !== "") {

                        $scope.formRequest.auctionVendors = _.filter($scope.formRequest.auctionVendors, function (x) { return x.companyName !== 'PRICE_CAP'; });

                        $scope.formRequest.cloneID = $stateParams.reqObj.cloneId;
                    }

                    
                    var params = {};
                    var myDate = new Date(); // Your timezone!
                    var myEpoch = parseInt(myDate.getTime() / 1000);
                    $scope.formRequest.minBidAmount = 0;
                    $scope.formRequest.postedOn = "/Date(" + myEpoch + "000+0000)/";
                    $scope.formRequest.timeLeft = -1;
                    $scope.formRequest.price = -1;
                    params = {
                        "requirement": {
                            "title": $scope.formRequest.title,
                            "daNo": $scope.formRequest.daNo,
                            "description": $scope.formRequest.description,
                            "category": $scope.formRequest.category,
                            "subcategories": $scope.formRequest.subcategories,
                            "urgency": $scope.formRequest.urgency,
                            "budget": $scope.formRequest.budget,
                            "attachmentName": $scope.formRequest.attachmentName,
                            "deliveryLocation": $scope.formRequest.deliveryLocation,
                            "taxes": $scope.formRequest.taxes,
                            "paymentTerms": $scope.formRequest.paymentTerms,
                            "requirementID": $scope.formRequest.requirementID,
                            "customerID": $scope.formRequest.customerID,
                            "isClosed": $scope.formRequest.isClosed,
                            "endTime": null,
                            "sessionID": $scope.formRequest.sessionID,
                            "errorMessage": "",
                            "timeLeft": -1,
                            "price": -1,
                            "auctionVendors": $scope.formRequest.auctionVendors,
                            "startTime": null,
                            "status": "",
                            "postedOn": "/Date(" + myEpoch + "000+0000)/",
                            "custLastName": $scope.formRequest.customerLastname,
                            "custFirstName": $scope.formRequest.customerFirstname,
                            "deliveryTime": $scope.formRequest.deliveryTime,
                            "includeFreight": $scope.formRequest.includeFreight,
                            "inclusiveTax": $scope.formRequest.inclusiveTax,
                            "minBidAmount": 0,
                            "checkBoxEmail": $scope.formRequest.checkBoxEmail,
                            "checkBoxSms": $scope.formRequest.checkBoxSms,
                            "quotationFreezTime": $scope.formRequest.quotationFreezTime,
                            "currency": $scope.formRequest.currency,
                            "timeZoneID": $scope.formRequest.timeZoneID,
                            "listRequirementItems": $scope.formRequest.listRequirementItems,
                            "isTabular": $scope.formRequest.isTabular,
                            "reqComments": $scope.formRequest.reqComments,
                            "itemsAttachment": $scope.formRequest.itemsAttachment,
                            "isSubmit": $scope.formRequest.isSubmit,
                            "isQuotationPriceLimit": $scope.formRequest.isQuotationPriceLimit,
                            "quotationPriceLimit": $scope.formRequest.quotationPriceLimit,
                            "noOfQuotationReminders": $scope.formRequest.noOfQuotationReminders,
                            "remindersTimeInterval": $scope.formRequest.remindersTimeInterval,
                            "custCompID": $scope.formRequest.custCompID,
                            "customerCompanyName": $scope.formRequest.customerCompanyName,
                            "deleteQuotations": $scope.formRequest.deleteQuotations,
                            "indentID": $scope.formRequest.indentID,
                            "expStartTime": $scope.formRequest.expStartTime,
                            "cloneID": $scope.formRequest.cloneID,
                            "isDiscountQuotation": $scope.formRequest.isDiscountQuotation,
                            "multipleAttachments": $scope.formRequest.multipleAttachments,                            
                        }, "attachment": $scope.formRequest.attachment
                    };

                    
                    logisticServices.RequirementSave(params)
                        .then(function (response) {
                            if (response.objectID != 0) {

                                $scope.SaveReqDepartments(response.objectID);                                

                                if ($scope.selectedQuestionnaire != null && $scope.selectedQuestionnaire != undefined && $scope.selectedQuestionnaire.evalID != undefined) {
                                    techevalService.getquestionnaire($scope.selectedQuestionnaire.evalID, 1)
                                        .then(function (response1) {
                                            $scope.selectedQuestionnaire = response1;

                                            if ($scope.isTechEval) {
                                                $scope.selectedQuestionnaire.reqID = response.objectID;
                                            }
                                            else {
                                                $scope.selectedQuestionnaire.reqID = 0;
                                            }
                                            $scope.selectedQuestionnaire.sessionID = userService.getUserToken();
                                            var params = {
                                                questionnaire: $scope.selectedQuestionnaire
                                            }
                                            techevalService.assignquestionnaire(params)
                                                .then(function (response) {
                                                })
                                        })
                                }
                                setTimeout(function () {
                                    //do what you need here
                                    swal({
                                    title: "Done!",
                                    text: "Requirement Saved Successfully",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                        function () {
                                        $scope.postRequestLoding = false;
										
                                        if (navigateToView) {
                                            $scope.goToCustomerLogistic(response.objectID);
                                            
                                        } else {
                                            if ($scope.stateParamsReqID > 0) {
                                                location.reload();
                                            }
                                            else {
                                                $scope.editRequirement(response.objectID);
                                            }
                                        }
                                    });
                                }, 1000);
                            }
                        });
                } else {
                    fwdauctionsService.postrequirementdata($scope.formRequest)
                        .then(function (response) {
                            if (response.objectID != 0) {
                                
                                $scope.SaveReqDeptDesig(response.objectID);

                                swal({
                                    title: "Done!",
                                    text: "Requirement Created Successfully",
                                    type: "success",
                                    showCancelButton: false,
                                    confirmButtonColor: "#DD6B55",
                                    confirmButtonText: "Ok",
                                    closeOnConfirm: true
                                },
                                    function () {
                                        $scope.postRequestLoding = false;
                                        $state.go('fwd-view-req', { 'Id': response.objectID });
                                    });
                            }
                        });
                }
            });
            $scope.postRequestLoding = false;
        };

        $scope.AddItem = function () {
            $scope.itemnumber = $scope.formRequest.listRequirementItems.length;
            $scope.requirementItems =
                {
                    //productSNo: $scope.itemSNo++,
                    //ItemID: 0,
                    //productIDorName: '',
                    //productNo: '',
                    //productDescription: '',
                    //productQuantity: 0,
                    //productBrand: '',
                    //othersBrands: '',
                    //isDeleted: 0,
                    //itemAttachment: '',
                    //attachmentName: '',
                    //netWeight: 0,
                    //ispalletize: 0,
                    //palletizeQty: 0,
                    //palletizeLength: 0,
                    //palletizeBreadth: 0,
                    //palletizeHeight: 0


                    productSNo: $scope.itemSNo++,
                    ItemID: 0,
                    productIDorName: '',
                    productDescription: '',
                    productQuantity: 0,
                    typeOfPacking: '',
                    modeOfShipment: '',
                    natureOfGoods: 0,
                    storageCondition: '',
                    portOfLanding: '',
                    finalDestination: '',
                    deliveryLocation: '',
                    preferredAirlines: '',
                    isDeleted: 0,
                    itemAttachment: '',
                    attachmentName: '',
                    netWeight: 0,
                    ispalletize: 1,
                    palletizeQty: 0,
                    palletizeLength: 0,
                    palletizeBreadth: 0,
                    palletizeHeight: 0
                }
            $scope.formRequest.listRequirementItems.push($scope.requirementItems);
            console.log($scope.formRequest.listRequirementItems);
        };

        $scope.deleteItem = function (SNo, index) {
            if ($scope.formRequest.listRequirementItems && $scope.formRequest.listRequirementItems.length > 1) {
                //$scope.formRequest.listRequirementItems = _.filter($scope.formRequest.listRequirementItems, function (x) { return x.productSNo !== SNo; });
                $scope.formRequest.listRequirementItems.splice(index, 1);
            };
        };

        $scope.getFile1 = function (id, itemid, ext) {
            $scope.file = $("#" + id)[0].files[0];

            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "itemsAttachment") {
                        if (ext != "xlsx") {
                            swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                            return;
                        }
                        var bytearray = new Uint8Array(result);
                        $scope.formRequest.itemsAttachment = $.makeArray(bytearray);
                        if (!$scope.isEdit) {
                            $scope.formRequest.listRequirementItems = [];
                        }

                        $scope.postRequest(0, 1, false);
                    }

                    if (id != "itemsAttachment") {
                        var bytearray = new Uint8Array(result);
                        var arrayByte = $.makeArray(bytearray);
                        var ItemFileName = $scope.file.name;
                        var index = _.indexOf($scope.formRequest.listRequirementItems, _.find($scope.formRequest.listRequirementItems, function (o) { return o.productSNo == id; }));
                        var obj = $scope.formRequest.listRequirementItems[index];
                        obj.itemAttachment = arrayByte;
                        obj.attachmentName = ItemFileName;
                        $scope.formRequest.listRequirementItems.splice(index, 1, obj);
                    }
                });
        }

        $scope.nextpage = function (pageNo) {
            $scope.tableValidation = false;
            $scope.deliveryLocationValidation = $scope.paymentTermsValidation = $scope.deliveryTimeValidation = departmentsValidation = false;
            $scope.urgencyValidation = $scope.quotationFreezTimeValidation = $scope.expStartTimeValidation = $scope.selectedCurrencyValidation = $scope.quotationPriceLimitValidation = $scope.noOfQuotationRemindersValidation = $scope.remindersTimeIntervalValidation = $scope.questionnaireValidation = false;

            if (pageNo == 1 || pageNo == 4) {
                if ($scope.formRequest.title == null || $scope.formRequest.title == '') {
                    $scope.titleValidation = true;
                    
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.daNo == null || $scope.formRequest.daNo == '') {
                    $scope.DANoValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.isTabular) {
                    $scope.selectedProducts = [];
                    $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                        if (item.productIDorName == null || item.productIDorName == '') {
                            $scope.tableValidation = true;
                        }
                        else {
                            $scope.selectedProducts.push(item.CATALOGUE_ID);
                        }
                        if (item.productQuantity <= 0 || item.productQuantity == undefined || item.productQuantity == '') {
                            $scope.tableValidation = true;
                        }
                        if (item.productQuantityIn == null || item.productQuantityIn == '') {
                            $scope.tableValidation = true;
                        }
                    });


                    if ($scope.tableValidation) {
                        return false;
                    }



                }
                if (!$scope.formRequest.isTabular) {
                    if ($scope.formRequest.description == null || $scope.formRequest.description == '') {
                        $scope.descriptionValidation = true;
                        $scope.postRequestLoding = false;
                        return false;
                    }
                }
            }

            if (pageNo == 2 || pageNo == 4) {
                //if ($scope.formRequest.deliveryLocation == null || $scope.formRequest.deliveryLocation == '') {
                //    $scope.deliveryLocationValidation = true;
                //    $scope.postRequestLoding = false;
                //    return false;
                //}
                
                if ($scope.formRequest.deliveryTime == null || $scope.formRequest.deliveryTime == '') {
                    $scope.deliveryTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.paymentTerms == null || $scope.formRequest.paymentTerms == '') {
                    $scope.paymentTermsValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }

                if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                    var d = new Date();
                    d.setHours(18, 00, 00);
                    d = new moment(d).format("DD-MM-YYYY HH:mm");
                    $scope.formRequest.quotationFreezTime = d;

                }


                if ($scope.formRequest.expStartTime == null || $scope.formRequest.expStartTime == '') {
                    var dt = new Date();
                    var tomorrowNoon = new Date(dt.getFullYear(), dt.getMonth(), dt.getDate() + 1, 12, 0, 0);
                    tomorrowNoon = new moment(tomorrowNoon).format("DD-MM-YYYY HH:mm");
                    $scope.formRequest.expStartTime = tomorrowNoon;

                }

                if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                    $scope.formRequest.noOfQuotationReminders = 3;
                }
                if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                    $scope.formRequest.remindersTimeInterval = 2;
                }
                if ($scope.reqDepartments.length > 0) {

                    $scope.departmentsValidation = true;

                    $scope.reqDepartments.forEach(function (item, index) {
                        if (item.isValid == true || $scope.noDepartments == true)
                            $scope.departmentsValidation = false;
                    });

                    if ($scope.departmentsValidation == true) {
                        return false;
                    };

                }
            }

            if (pageNo == 3 || pageNo == 4) {
                if ($scope.formRequest.urgency == null || $scope.formRequest.urgency == '') {
                    $scope.urgencyValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.quotationFreezTime == null || $scope.formRequest.quotationFreezTime == '') {
                    $scope.quotationFreezTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }                
               
                var ts = moment($scope.formRequest.quotationFreezTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var quotationFreezTime = new Date(m);
                var CurrentDate = moment(new Date());

                var ts = moment($scope.formRequest.expStartTime, "DD-MM-YYYY HH:mm").valueOf();
                var m = moment(ts);
                var expStartTime = new Date(m);
                var CurrentDate = moment(new Date());

                if (quotationFreezTime < CurrentDate) {
                    $scope.quotationFreezTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;

                }

                if (quotationFreezTime >= expStartTime) {
                    $scope.expStartTimeValidation = true;
                    $scope.postRequestLoding = false;
                    return false;

                }


                if (($scope.formRequest.quotationPriceLimit == null || $scope.formRequest.quotationPriceLimit == '' || $scope.formRequest.quotationPriceLimit <= 0) && $scope.formRequest.isQuotationPriceLimit == true) {
                    $scope.quotationPriceLimitValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.noOfQuotationReminders == null || $scope.formRequest.noOfQuotationReminders == '' || $scope.formRequest.noOfQuotationReminders <= 0 || $scope.formRequest.noOfQuotationReminders > 5) {
                    $scope.noOfQuotationRemindersValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.formRequest.remindersTimeInterval == null || $scope.formRequest.remindersTimeInterval == '' || $scope.formRequest.remindersTimeInterval <= 0) {
                    $scope.remindersTimeIntervalValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                if ($scope.isTechEval == true && (!$scope.selectedQuestionnaire || $scope.selectedQuestionnaire.evalID <= 0)) {
                    $scope.questionnaireValidation = true;
                    $scope.postRequestLoding = false;
                    return false;
                }
                $scope.getproductvendors();
            }

            if (pageNo == 4) {
                $scope.formRequest.subcategoriesView = '';
                for (i = 0; i < $scope.sub.selectedSubcategories.length; i++) {
                    $scope.formRequest.subcategoriesView += $scope.sub.selectedSubcategories[i].subcategory + ",";
                }
            }

            $scope.pageNo = $scope.pageNo + 1;
        }

        $scope.previouspage = function () {
            $scope.pageNo = $scope.pageNo - 1;
        }

        $scope.gotopage = function (pageNo) {
            $scope.pageNo = pageNo;
            return $scope.pageNo;
        }

        $scope.GetReqDepartments = function () {
            $scope.reqDepartments = [];
            auctionsService.GetReqDepartments(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.reqDepartments = response;

                        $scope.noDepartments = true;

                        $scope.reqDepartments.forEach(function (item, index) {
                            if (item.isValid == true) {
                                $scope.noDepartments = false;
                            }
                        });

                    }
                });
        }

        $scope.SaveReqDepartments = function (reqID) {

            $scope.reqDepartments.forEach(function (item, index) {
                item.reqID = reqID;
                item.userID = userService.getUserId();
            })

            var params = {
                "listReqDepartments": $scope.reqDepartments,
                sessionID: userService.getUserToken()
            };
            if ($scope.formRequest.isForwardBidding) {
                fwdauctionsService.SaveReqDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                        }
                    });
            } else {
                auctionsService.SaveReqDepartments(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                        }
                    });
            }
            

        };

        $scope.noDepartmentsFunction = function (value) {

            $scope.departmentsValidation = false;

            if (value == 'NA') {
                $scope.reqDepartments.forEach(function (item, index) {
                    item.isValid = false;
                });
            }
            else {
                $scope.reqDepartments.forEach(function (item, index) {
                    if (item.isValid == true)
                        $scope.noDepartments = false;
                });
            }
        };

        $scope.GetReqDeptDesig = function () {
            $scope.reqDeptDesig = [];
            auctionsService.GetReqDeptDesig(userService.getUserId(), $scope.stateParamsReqID, userService.getUserToken())
                .then(function (response) {
                    if (response && response.length > 0) {
                        $scope.reqDeptDesig = response;
                        $scope.noDepartments = true;
                        $scope.reqDeptDesig.forEach(function (item, index) {
                            if (item.isValid == true) {
                                $scope.noDepartments = false;
                            }
                        });
                    }
                });
        };

        $scope.SaveReqDeptDesig = function (reqID) {

            $scope.reqDeptDesig.forEach(function (item, index) {
                item.reqID = reqID;
                item.userID = userService.getUserId();
            })

            var params = {
                "listReqDepartments": $scope.reqDeptDesig,
                sessionID: userService.getUserToken()
            };

            if ($scope.formRequest.isForwardBidding) {
                fwdauctionsService.SaveReqDeptDesig(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            
                        }
                    });
            } else {
                auctionsService.SaveReqDeptDesig(params)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                        }
                    });
            }


        };

        $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {

            if ($scope.newVendor.altPhoneNum == undefined) {
                $scope.newVendor.altPhoneNum = '';
            }
            if ($scope.newVendor.altEmail == undefined) {
                $scope.newVendor.altEmail = '';
            }

            var params = {
                userID: userService.getUserId(),
                vendorPhone: vendorPhone,
                vendorEmail: vendorEmail,
                sessionID: userService.getUserToken(),
                category: $scope.formRequest.category,
                subCategory: $scope.selectedSubCategoriesList,
                altPhoneNum: $scope.newVendor.altPhoneNum,
                altEmail: $scope.newVendor.altEmail
            };

            auctionsService.AssignVendorToCompany(params)
                .then(function (response) {

                    $scope.vendor = response;

                    if ($scope.vendor.errorMessage == '' || $scope.vendor.errorMessage == 'VENDOR ADDED SUCCESSFULLY') {
                            $scope.formRequest.auctionVendors.push({ vendorName: $scope.vendor.firstName + ' ' + $scope.vendor.lastName, companyName: $scope.vendor.email, vendorID: $scope.vendor.userID });
                            $scope.newVendor = {};
                            $scope.checkVendorPhoneUniqueResult = false;
                            $scope.checkVendorEmailUniqueResult = false;
                            $scope.checkVendorPanUniqueResult = false;
                            $scope.checkVendorTinUniqueResult = false;
                            $scope.checkVendorStnUniqueResult = false;
                            $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl('Vendor Added Successfully', "success");
                        }                                                    
                    else {
                        $scope.newVendor = {};
                        $scope.checkVendorPhoneUniqueResult = false;
                        $scope.checkVendorEmailUniqueResult = false;
                        $scope.checkVendorPanUniqueResult = false;
                        $scope.checkVendorTinUniqueResult = false;
                        $scope.checkVendorStnUniqueResult = false;
                        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.knownsincevalidation = $scope.vendorcurrencyvalidation = false;
                            growlService.growl($scope.vendor.errorMessage, "inverse");
                        }
                });
        };

        $scope.searchVendors = function (value) {                        
            $scope.Vendors = _.filter($scope.VendorsTemp, function (item) { return item.companyName.toUpperCase().indexOf(value.toUpperCase()) > -1;});
            
        }
            
        $scope.exportItemsToExcel = function () {
            var mystyle = {
                sheetid: 'RequirementDetails',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql('SELECT itemID as [ItemID], productIDorName as [ProductName], productNo as [ProductNumber], productDescription as [Description], productQuantity as [Quantity], productQuantityIn as [Units], productBrand as PreferredBrand, othersBrands as OtherBrands INTO XLSX(?,{headers:true,sheetid: "RequirementDetails", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["RequirementDetails.xlsx", $scope.formRequest.listRequirementItems]);
        }
            
        $scope.cancelClick = function () {
            $window.history.back();
        }

        $scope.goToCustomerLogistic = function (id) {
            $state.go('customerlogistic', { 'Id': id });
        };

        $scope.editRequirement = function (id) {
            $state.go('form.addnewlogistic', { 'Id': id });
        };
            
        // #endregion FUNCTION

        // #region FUNCTION CALLS IN ORDER

        $scope.getQuestionnaireList();
        $scope.getData();
        
        $scope.GetReqDepartments();
        $scope.GetReqDeptDesig();

        // #endregion FUNCTION CALLS IN ORDER


        $scope.removeAttach = function (index) {
            $scope.formRequest.multipleAttachments.splice(index, 1);
        }











            //#region Catalog
            $scope.selectedProducts = [];
            $scope.productsList = [];
            $scope.getProducts = function () {
                catalogService.getProducts($scope.compID)
                    .then(function (response) {
                        $scope.productsList = response;
                    });
            }
            $scope.getProducts();
            $scope.autofillProduct = function (prodName, index) {
                $scope['ItemSelected_' + index] = false;
                var output = [];
                if (prodName && prodName.trim() != '') {
                    angular.forEach($scope.productsList, function (prod) {
                        if (prod.prodName.toLowerCase().indexOf(prodName.trim().toLowerCase()) >= 0) {
                            output.push(prod);
                        }
                    });
                }
                $scope["filterProducts_" + index] = output;
            }
            $scope.fillTextbox = function (selProd, index) {
                $scope['ItemSelected_' + index] = true;
                $scope.formRequest.listRequirementItems[index].productIDorName = selProd.prodName;
                //$scope.formRequest.listRequirementItems[index].ITEM_CODE = selProd.prodCode;
                //$scope.formRequest.listRequirementItems[index].ITEM_NUM = selProd.prodNo;
                //ITEM_NUM
                $scope.formRequest.listRequirementItems[index].CATALOGUE_ID = selProd.prodId;
                $scope.formRequest.listRequirementItems[index].productDescription = selProd.prodDesc;
                $scope['filterProducts_' + index] = null;

                //$scope.loadUserDepartments(selProd.prodId, index);

            }

            $scope.onBlurProduct = function (index) {
                if ($scope['ItemSelected_' + index] == false) {
                    $scope.formRequest.listRequirementItems[index].productIDorName = "";
                }
            }


            $scope.getproductvendors = function () {
                $scope.vendorsLoaded = false;
                $scope.Vendors = [];

                //products.push($scope.formRequest.category);
                //$scope.formRequest.category = category;
                if ($scope.selectedProducts.length > 0) {
                    var params = { 'products': $scope.selectedProducts, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
                    $http({
                        method: 'POST',
                        url: domain + 'getvendorsbyproducts',
                        encodeURI: true,
                        headers: { 'Content-Type': 'application/json' },
                        data: params
                    }).then(function (response) {
                        if (response && response.data) {
                            if (response.data.length > 0) {
                                $scope.Vendors = response.data;
                                // if (allVendors === 1) {
                                // $scope.allCompanyVendors = response.data;
                                // allVendors = 0;
                                // }  
                                $scope.vendorsLoaded = true;
                                for (var j in $scope.formRequest.auctionVendors) {
                                    for (var i in $scope.Vendors) {
                                        if ($scope.Vendors[i].vendorName == $scope.formRequest.auctionVendors[j].vendorName) {
                                            $scope.Vendors.splice(i, 1);
                                        }
                                    }
                                }

                                $scope.VendorsTemp = $scope.Vendors;
                                $scope.searchVendors('');

                            }
                            //$scope.formRequest.auctionVendors =[];
                        } else {
                        }
                    }, function (result) {
                    });
                }

            };

        








            //#endregion Catalog



//$scope.getproductvendors();


            
    }]);