﻿prmApp
    .controller('CatalogCompleteViewCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService",
        "storeService", "growlService", "PRMPRServices", "catalogReportsServices",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService,
            storeService, growlService, PRMPRServices, catalogReportsServices) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();

            $scope.catalog = [
                {
                    name: 'Dept 1',
                    categories: [
                        {
                            name: 'Cat 1 > D1',
                            products: [
                                {
                                    name: 'Prod 1 > C1 > D1',
                                    id: 1
                                },
                                {
                                    name: 'Prod 2 > C1 > D1',
                                    id: 2
                                },
                                {
                                    name: 'Prod 3 > C1 > D1',
                                    id: 3
                                },
                                {
                                    name: 'Prod 4 > C1 > D1',
                                    id: 4
                                },
                                {
                                    name: 'Prod 5 > C1 > D1',
                                    id: 5
                                }
                            ],
                            id: 1
                        },
                        {
                            name: 'Cat 2 > D1',
                            products: [
                                {
                                    name: 'Prod 1 > C2 > D1',
                                    id: 1
                                },
                                {
                                    name: 'Prod 2 > C2 > D1',
                                    id: 2
                                },
                                {
                                    name: 'Prod 3 > C2 > D1',
                                    id: 3
                                },
                                {
                                    name: 'Prod 4 > C2 > D1',
                                    id: 4
                                },
                                {
                                    name: 'Prod 5 > C2 > D1',
                                    id: 5
                                }
                            ],
                            id: 2
                        }
                    ],
                    id: 1
                },
                {
                    name: 'Dept 2',
                    categories: [
                        {
                            name: 'Cat 1 > D2',
                            products: [
                                {
                                    name: 'Prod 1 > C1 > D2',
                                    id: 1
                                },
                                {
                                    name: 'Prod 2 > C1 > D2',
                                    id: 2
                                },
                                {
                                    name: 'Prod 3 > C1 > D2',
                                    id: 3
                                },
                                {
                                    name: 'Prod 4 > C1 > D2',
                                    id: 4
                                },
                                {
                                    name: 'Prod 5 > C1 > D2',
                                    id: 5
                                }
                            ],
                            id: 1
                        },
                        {
                            name: 'Cat 2 > D2',
                            products: [
                                {
                                    name: 'Prod 1 > C2 > D2',
                                    id: 1
                                },
                                {
                                    name: 'Prod 2 > C2 > D2',
                                    id: 2
                                },
                                {
                                    name: 'Prod 3 > C2 > D2',
                                    id: 3
                                },
                                {
                                    name: 'Prod 4 > C2 > D2',
                                    id: 4
                                },
                                {
                                    name: 'Prod 5 > C2 > D2',
                                    id: 5
                                }
                            ],
                            id: 2
                        }
                    ],
                    id: 2
                }

            ];

            $scope.categories = [];
            $scope.products = [];

            $scope.selectedStyle = {
                'font-weight': 'bold',
                'background-color': 'lightgray',
                'border-radius': '25px'
            }

            $scope.hoverDept = function (val, id) {
                $scope.categories = val;
                $scope.deptId = id;
                //if (val && val.length > 0 && val[0].products) {
                //    $scope.products = val[0].products;
                //    $scope.catId = val[0].id;
                //}

                $scope.products = [];
                $scope.catId = 0;

                $scope.prodId = 0;
            };

            $scope.hoverCat = function (val, id) {
                $scope.products = val;
                $scope.catId = id;

                $scope.prodId = 0;
            };

            $scope.hoverProd = function (id) {
                $scope.prodId = id;
            };



            $scope.cr_UserDepartments = [];
            $scope.cr_DepartmentCategories = [];
            $scope.cr_DepartmentCategories1 = [];
            $scope.cr_CategoryProducts = [];


            $scope.GetUserDepartments = function () {
                $scope.cr_UserDepartments = [];
                $scope.cr_DepartmentCategories = [];
                $scope.cr_DepartmentCategories1 = [];
                $scope.cr_CategoryProducts = [];
                var params = {
                    "u_id": userService.getUserId(),
                    "is_super_user": 1,
                    "sessionid": userService.getUserToken()
                };
                catalogReportsServices.GetUserDepartments(params)
                    .then(function (response) {
                        $scope.cr_UserDepartments = response;
                    })

            };

            $scope.GetUserDepartments();

            $scope.GetDepartmentCategories = function (deptId) {
                $scope.deptId = deptId;
                $scope.catId = 0;
                $scope.subCatId = 0;
                $scope.prodId = 0;
                $scope.cr_DepartmentCategories = [];
                $scope.cr_DepartmentCategories1 = [];
                $scope.cr_CategoryProducts = [];
                var params = {
                    "U_ID": userService.getUserId(),
                    "DEPT_IDS": deptId,
                    "sessionid": userService.getUserToken()
                };
                catalogReportsServices.GetDepartmentCategories(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.cr_DepartmentCategories = response;
                        } else {

                        }
                    })
            };

            $scope.GetCategorySubcategories = function (catId) {
                $scope.catId = catId;
                $scope.subCatId = 0;
                $scope.prodId = 0;
                $scope.cr_DepartmentCategories1 = [];
                $scope.cr_CategoryProducts = [];
                var params = {
                    "U_ID": userService.getUserId(),
                    "CAT_IDS": catId,
                    "sessionid": userService.getUserToken()
                };
                catalogReportsServices.GetCategorySubcategories(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.cr_DepartmentCategories1 = response;
                        } else {
                            $scope.GetCategoryProducts(catId);
                        }
                    })
            };

            $scope.GetCategoryProducts = function (catId) {
                $scope.subCatId = catId;
                $scope.prodId = 0;
                $scope.cr_CategoryProducts = [];
                var params = {
                    "U_ID": userService.getUserId(),
                    "CAT_IDS": catId,
                    "sessionid": userService.getUserToken()
                };
                catalogReportsServices.GetCategoryProducts(params)
                    .then(function (response) {
                        if (response && response.length > 0) {
                            $scope.cr_CategoryProducts = response;
                        }
                    });
            };

            $scope.GetProductDetails = function (prodId) {
                $scope.prodId = prodId;
            };

            $scope.ProductAnalysis = function (prodId) {
                var url = $state.href('productAlalysis', { "productId": prodId });
                $window.open(url, '_self');
            };



        }]);