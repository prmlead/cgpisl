﻿prmApp
    .controller('profileVendorCatalogCtrl',
    function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService, fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, SettingService, catalogService, fileReader) {
        $scope.ExcelProducts = [];
        $scope.compId = userService.getUserCompanyId();
        $scope.catalogCompId = userService.getUserCatalogCompanyId();;
        $scope.parentNodes = [];
        $scope.selectedProducts = [];
        $scope.vendorCatalogFile = '';
        $scope.productSearchString = '';
        $scope.categorySearchString = '';
        $scope.sessionid = userService.getUserToken();
        $scope.getcatalogcategories = function () {
            var catNodes = [];
            catalogService.getVendorCategories(userService.getUserId(), 0, $scope.catalogCompId)
                .then(function (response) {
                    $scope.parentNodes = response;
                    if ($scope.parentNodes && $scope.parentNodes.length > 0) {
                        $scope.parentNodes.forEach(function (tempNode, index) {
                            tempNode.isFiltered = true;
                        });
                    }

                    $scope.collapseAll();
                    $scope.getvendorproducts();
                });
        };

        $scope.getcatalogcategories();
        $scope.getUserMyCatalogFile = function () {
            catalogService.getUserMyCatalogFile(userService.getUserId())
                .then(function (response) {
                    console.log(response);
                    $scope.vendorCatalogFile = response.message;
                });
        };
            $scope.getUserMyCatalogFile();

            $scope.getvendorproducts = function () {
                $scope.products = [];
                $scope.productsInitial = $scope.products;
                $scope.productsInitial = [];
                catalogService.getVendorProducts(userService.getUserId(), $scope.catalogCompId)
                        .then(function (response) {
                            var varproducts = [];
                            var prodArray = response;
                            prodArray.forEach(function (item, index) {

                                var product = {
                                    "compId": item.compId,
                                    "prodId": item.prodId,
                                    "prodName": item.prodName,
                                    "nodeChecked": true
                                }
                                varproducts.push(product);
                                $scope.selectedProducts.push(item.prodId);
                            });
                            $scope.products = varproducts;
                            $scope.productsInitial = $scope.products;
                            $scope.getSelectedNodes();
                            $scope.getProducts($scope.selectedCategories);

                        });
            }

            $scope.selectParentNode = function (ischecked, selectedNodeScope) {
                if (selectedNodeScope) {
                    var parentScope = selectedNodeScope.$parent;
                    if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                        if (ischecked) {
                            parentScope.node.nodeChecked = ischecked;
                        }
                        else {
                            if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                                if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                                    parentScope.node.nodeChecked = ischecked;
                                }
                            }
                            else {
                                parentScope.node.nodeChecked = ischecked;
                            }
                        }

                        $scope.selectParentNode(ischecked, parentScope);
                    }
                }
            }

            $scope.selectChildNodes = function (childNode, ischecked) {
                childNode.nodeChecked = ischecked;
                childNode.nodes.forEach(function (item, index) {
                    $scope.selectChildNodes(item, ischecked);
                });
            }

            $scope.vendorcheckChanged = function (selectedNode, selectedNodeScope) {
                $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
                $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
                $scope.getSelectedNodes();
                $scope.getProducts($scope.selectedCategories);
            }

            $scope.getSelectedNodes = function () {
                $scope.selectedCategories = [];

                $scope.parentNodes.forEach(function (item, index) {
                    $scope.getSelectedNodesIteration(item);
                })
            }

            $scope.getSelectedNodesIteration = function (selectedNode) {
                if (selectedNode.nodeChecked) {
                    $scope.selectedCategories.push(selectedNode.catId);
                }
                if (selectedNode.nodes != null && selectedNode.nodes.length > 0) {
                    selectedNode.nodes.forEach(function (item, index) {
                        if (item.nodeChecked) {
                            $scope.getSelectedNodesIteration(item);
                        }

                    })
                }
            }

            $scope.products = [];
            $scope.productsInitial = $scope.products;
        $scope.productsBkp = [];
        $scope.VendorCheckedProducts = [];
            $scope.getProducts = function (nodeData) {
                $scope.productsBkp = $scope.products;
                var varproducts = [];
                if (nodeData.length > 0) {
                    catalogService.GetAllProductsByCategories($scope.catalogCompId, nodeData.join(','))
                        .then(function (response) {
                            var prodArray = response;
                            prodArray.forEach(function (item, index) {

                                var product = {
                                    "compId": item.compId,
                                    "prodId": item.prodId,
                                    "prodName": item.prodName,
                                    "nodeChecked": false
                                }
                                var obj = $filter('filter')($scope.productsBkp, { prodId: item.prodId }, true)[0];
                                if (obj) {
                                    product.nodeChecked = obj.nodeChecked;
                                }
                                varproducts.push(product);
                            })
                        });
                }
                $scope.products = varproducts;
                $scope.productsInitial = $scope.products;
                $scope.VendorCheckedProducts = varproducts;
            }

            $scope.vendorproductcheckChanged = function () {
                $scope.selectedProducts = [];
                $scope.products.forEach(function (item, index) {
                    if (item.nodeChecked) {
                        $scope.selectedProducts.push(item.prodId);
                    }
                    //else {
                    //    var index = $scope.selectedProducts.indexOf(item.prodId);
                    //    if (index > 0) {
                    //        $scope.selectedProducts.splice(index, 1);
                    //    }
                    //}
                });
            }

            $scope.vendortoggle = function (scope) {
                scope.toggle()
            };

            $scope.collapseAll = function () {
                $scope.$broadcast('angular-ui-tree:collapse-all');
            };

            $scope.expandAll = function () {
                $scope.$broadcast('angular-ui-tree:expand-all');
            };

        $scope.SaveVendorCatalog = function () {


                var param = {
                    vendorId: userService.getUserId(),
                    compId: $scope.catalogCompId,
                    catIds: $scope.selectedCategories.join(','),
                    prodIds: $scope.selectedProducts.join(','),
                    user: userService.getUserId(),
                    sessionId: userService.getUserToken()
                }

                catalogService.saveVendorCatalog(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                           
                            growlService.growl("Vendor catalogue updated Successfully.", 'success');
                            $state.reload();
                        }
                    });

        }


        $scope.SaveVendorCatalog1 = function () {

            $scope.selectedCategories1 = [];

            $scope.selectedProducts1 = [];

            $scope.parentNodes.forEach(function (item,index) {
                if (item.nodeChecked == true) {
                    $scope.selectedCategories1.push(item.catId);
                    $scope.VendorCheckedProducts.filter(function (item1, index1) {
                        if (item1.nodeChecked == true) {
                            $scope.selectedProducts1.push(item1.prodId);
                        } else {

                        }
                    })
                } else {

                }
            })


            var param = {
                vendorId: userService.getUserId(),
                compId: $scope.catalogCompId,
                catIds: $scope.selectedCategories1.join(','),
                prodIds: $scope.selectedProducts1.join(','),
                user: userService.getUserId(),
                sessionId: userService.getUserToken()
            }


            catalogService.saveVendorCatalog1(param)
                .then(function (response) {
                    if (response.errorMessage != '') {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else {
                        growlService.growl("Vendor catalogue updated Successfully.", 'success');
                        $state.reload();
                    }
                });

        }


        $scope.exportTemplateToExcel = function () {
            var mystyle = {
                sheetid: 'VendorItemCategories',
                headers: true,
                column: {
                    style: 'font-size:15px;background:#233646;color:#FFF;'
                }
            };

            alasql('SELECT "ProductName" as [ProductName] ,"CategoryName" as [CategoryName] INTO XLSX(?,{headers:true,sheetid: "VendorItemCategories", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["VendorItemCategories.xlsx", $scope.ExcelProducts]);
        }

        $scope.entityObj = {
            entityName: 'VendorItemCategories',
            attachment: [],
            userid: userService.getUserId(),
            sessionID: userService.getUserToken()
        };

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.file = $("#" + id)[0].files[0];
            var docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id == "excelquotationproducts") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.entityObj.attachmentFileName = $scope.file.name;
                        $scope.importVendorCatalogue();
                    }

                    if (id == "excelquotationproducts1") {
                        var bytearray = new Uint8Array(result);
                        $scope.entityObj.entityName = "MyCategories"
                        $scope.entityObj.attachment = $.makeArray(bytearray);
                        $scope.entityObj.attachmentFileName = $scope.file.name;
                        $scope.importMyCatalogue();
                    }
                });
        }

        $scope.importMyCatalogue = function () {
            var params = {
                "entity": $scope.entityObj,
                compId: $scope.catalogCompId,
                vendorId: userService.getUserId(),
                user: userService.getUserId(),
                sessionId: userService.getUserToken()
            };

            catalogService.importvendoritemcategories(params)
            .then(function (response) {

                if (response.errorMessage != '') {
                    alert(response.errorMessage);
                }
                if (response.repsonseId > 0) {
                    growlService.growl(response.repsonseId + " File uploaded successfully.", "success");
                    //$scope.getcatalogcategories(userService.getUserId());
                    location.reload();
                }
            });
        }

        $scope.importVendorCatalogue = function () {

            var params = {
                "entity": $scope.entityObj,
                compId: $scope.catalogCompId,
                vendorId: userService.getUserId(),
                user: userService.getUserId(),
                sessionId: userService.getUserToken()
            };

            catalogService.importvendoritemcategories(params)
                .then(function (response) {

                    if (response.errorMessage != '') {
                        alert(response.errorMessage);
                    }
                    if (response.repsonseId > 0) {
                        growlService.growl(response.repsonseId + " items updated successfully.", "success");
                        $scope.getcatalogcategories(userService.getUserId());
                        //location.reload();
                    }
                })
        };
        
        $scope.searchProducts = function (str) {
            str = String(str).toUpperCase();
            $scope.products = $scope.productsInitial.filter(function (product) {
                return (String(product.prodName).toUpperCase().includes(str) == true || String(product.prodNo).toUpperCase().includes(str) == true);
            });
        };

        $scope.searchCategory = function (str) {
            if (str && str.trim()) {
                str = String(str).toUpperCase();
                var selectedNodes = $scope.parentNodes.filter(function (category) {
                    return (String(category.catName).toUpperCase().includes(str) === true || String(category.catCode).toUpperCase().includes(str) === true);
                });

                $scope.parentNodes.forEach(function (tempNode, index) {
                    tempNode.isFiltered = false;
                });

                if (selectedNodes && selectedNodes.length > 0) {
                    selectedNodes.forEach(function (tempNode, index) {
                        tempNode.isFiltered = true;
                    });
                }
            } else {
                if ($scope.parentNodes && $scope.parentNodes.length > 0) {
                    $scope.parentNodes.forEach(function (tempNode, index) {
                        tempNode.isFiltered = true;
                    });
                }
            }
        };
    });