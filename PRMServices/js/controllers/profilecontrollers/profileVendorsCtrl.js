﻿prmApp.controller('profileVendorCtrl', function ($timeout, $uibModal, $state, $window, $scope, growlService, userService, auctionsService,
    fwdauctionsService, $http, domain, $rootScope, fileReader, $filter, $log, reportingService, SettingService, catalogService, $stateParams, workflowService) {
    $scope.ExcelProducts = [];

    var self = this;
    $scope.uId = userService.getUserId();
    $scope.stateParamVendorId = $stateParams.Id;
    $scope.compId = userService.getUserCompanyId();
    $scope.isCustomer = userService.getUserType();
    $scope.sessionid = userService.getUserToken();
    $scope.vendorsList = [];
    $scope.selectAllProductsValue = false;
    $scope.productSearchString = '';
    $scope.categorySearchString = '';
    $scope.inactiveVendorsList = [];
    $scope.userObj = {};
    $scope.isValidToSubmit = true;
    $scope.companyGSTInfo = [];
    $scope.multipleLoginDetails = [];
    $scope.supplierType = ['Dealer', 'Importer', 'SSI', 'PSU', 'Govt. Agency', 'Others'];
    $scope.newGSTInfo = {
        companyGSTId: 0,
        gstNumber: '',
        companyId: 0,
        gstAddr: '',
        vendorCode: ''
    };
    $scope.newVendor = {
        lastName: '',
        vendorStatus: 1,
        additionalVendorInformation: {
            fax: 0,
            websiteAddress: '',
            busineesType: '',
            businessIncorportaionDate: '',
            ownerDirectorName: '',
            ownerDirectorAadharCard: '',
            iec: '',
            payeeName: '',
            bankAccountType: '',
            primaryContactName: '',
            primaryContactDesignation: '',
            primaryContactMobile: '',
            primaryContactLandLine: '',
            primaryContactEmailId: '',
            secondaryContactName: '',
            secondaryContactDesignation: '',
            secondaryContactMobile: '',
            secondaryContactLandLine: '',
            secondaryContactEmailId: '',
            clientDealtBefore: '',
            msmedAct: '',
            clientDealt: '',
            nameContactDetails: '',
            materialOrServiceName: '',
            materialOrService: '',
            importFrom: '',
            customerNameAndLocation: '',
            customerNatureOfBusiness: '',
            customerContactPersonName: '',
            customerDersignation: '',
            customerEmailId: '',
            customerPhoneNumber: '',
            //customerCopyLetter: '',
            requestor: '',
            plant: '',
            department: '',
            vendorType: '',
            paymentTerms: '',
            supplier:''
        },
        paymentTermCode: '',
        paymentTermDesc: '',
        incoTerm: '',
        sapVendorCode: ''
    };

    /*region start WORKFLOW*/
    $scope.workflowList = [];
    $scope.unblockWorkflowList = [];
    $scope.itemWorkflow = [];
    $scope.workflowObj = {};
    $scope.workflowObj.workflowID = 0;
    $scope.workflowObj.unblockWorkflowID = 0;
    $scope.currentStep = 0;
    $scope.orderInfo = 0;
    $scope.assignToShow = '';
    $scope.isWorkflowCompleted = false;
    $scope.WorkflowModule = 'VENDOR';
    $scope.UnblockWorkflowModule = 'UNBLOCK_VENDOR';
    $scope.disableWFSelection = false;
    /*region end WORKFLOW*/

    $scope.newVendor.panno = "";
    $scope.newVendor.vatNum = "";
    $scope.newVendor.serviceTaxNo = "";
    userService.getUserDataNoCache()
    .then(function (response) {
        $scope.userObj = response;
    });

    $scope.selectedSubCategoriesList = [];
    $scope.subcategories = [];
    $scope.sub = {
        selectedSubcategories: [],
    };

    $scope.selectedSubcategories = [];
    $scope.totalSubcats = [];
    $scope.selectedCurrency = {};
    $scope.currencies = [];
    $scope.totalVendors = 0;
    $scope.totalInactiveVendors = 0;
    $scope.currentPage = 1;
    $scope.currentPage2 = 1;
    $scope.itemsPerPage = 5;
    $scope.itemsPerPage2 = 5;
    $scope.maxSize = 5;
    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };


    $scope.temporaryObj = {
            email: '',
            phone: '',
            sapVendCode :''
    }

    $scope.isSaveDisabled = false;

    $scope.pageChanged = function () { };
    /*pagination code*/

    $scope.userObj = userService.getUserObj();
    var loginUserData = userService.getUserObj();
    this.userId = userService.getUserId();
    $scope.isOTPVerified = loginUserData.isOTPVerified;
    $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
    $rootScope.$on("CallProfileMethod", function () {
        $scope.updateUserDataFromService();
    });

    $scope.selectedProducts = [];
    this.addVendor = 0;
    var date = new Date();
    $scope.isEditVendor = 1;

    $scope.showIsSuperUser = function () {
        if (userService.getUserObj().isSuperUser) {
            return true;
        } else {
            return false;
        }
    };
    $scope.getCatalogCategories = function () {
        auctionsService.getcatalogCategories(userService.getUserId())
            .then(function (response) {
                var responseTemp = [];
                var obj = {
                    CreatedBy: 0,ModifiedBy: 0,dateCreated: "",dateModified: "",errorMessage: "",sessionID: "",catCode: "ALL",catDesc: null,catId: 548,catName: "ALL",catOrder: 0,catParentId: 0,
                    catPath: null,catSelected: 0,compId: 4545,departments: null,isValid: 0,nodeChecked: false,nodes: null,subCatCount: 0,totalCategories: 0
                }
                responseTemp.push(obj);
                response.forEach(function (cat) {
                    responseTemp.push(cat);
                })
                $scope.companyCatalogueList = responseTemp;

                $scope.catName = $scope.companyCatalogueList[0];
                $scope.searchCategoryVendors($scope.catName);

            });
    };
   // $scope.getCatalogCategories();
        // Pagination For Overall Vendors//
        $scope.page = 0;
        $scope.PageSize = 50;
        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
        $scope.totalCount = 0;
        // Pagination For Overall Vendors//
        

    $scope.GetCompanyVendors = function (IsPaging, searchString) {
        // , Categories
        //if ($scope.catName.catName != 'ALL') {
        //    Categories = $scope.catName.catCode;
        //}
        if (searchString)
        {
            $scope.fetchRecordsFrom = 0;
        }
        //if (Categories == undefined || Categories == null) {
        //    Categories = '';
        //}
        $scope.isOTPVerified = loginUserData.isOTPVerified;
        $scope.isEmailOTPVerified = loginUserData.isEmailOTPVerified;
        $scope.params = { "userID": userService.getUserId(), "sessionID": userService.getUserToken(), "PageSize": $scope.fetchRecordsFrom, "NumberOfRecords": $scope.PageSize, "searchString": searchString ? searchString : "" };
        // , "Categories": Categories
        if ($scope.isOTPVerified && $scope.isEmailOTPVerified) {
            if ($scope.downloadExcel) {
                $scope.params.PageSize = 0;
                $scope.params.NumberOfRecords = 0;
                userService.GetCompanyVendors($scope.params)
                    .then(function (response) {
                        response.forEach(function (item) {
                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                        })
                        if (response && response.length > 0) {
                            $scope.vendorsListExcel = response;
                            exportVendorsToExcel()
                        } else {
                            swal("Error!", "No records.", "error");
                            $scope.downloadExcel = false;
                        }
                    })

            } else {
                if (IsPaging === 1) {
                    if ($scope.page > -1) {
                        $scope.page++;
                        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                        $scope.loaderMore = true;
                        $scope.LoadMessage = ' Loading page ' + $scope.fetchRecordsFrom + ' of ' + $scope.PageSize + ' data...';
                        $scope.result = "color-green";
                        $scope.params.PageSize = $scope.fetchRecordsFrom;
                        $scope.params.searchString = searchString ? searchString : "";

                        if ($scope.totalCount != $scope.vendorsList.length) {
                            userService.GetCompanyVendors($scope.params)
                                .then(function (response) {
                                    //$scope.vendorsList = response;
                                    if (response) {
                                        response.forEach(function (item) {
                                            item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                                        })
                                        for (var a in response) {
                                            $scope.vendorsList.push(response[+a]);
                                        }
                                        $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                        $scope.totalCount = $scope.vendorsList[0].totalVendors;
                                    }

                                    //$scope.searchingVendors = '';
                                    // $scope.getCatalogCategories();
                                });
                        }
                    }
                } else {
                    userService.GetCompanyVendors($scope.params)
                        .then(function (response) {
                            response.forEach(function (item) {
                                item.companyVendorCodes = _.uniq(item.companyVendorCodes.split(',').map(function (item) { return item.trim() })).join(',');
                            })
                            $scope.vendorsList = response;

                            if ($scope.vendorsList.length > 0 && $scope.vendorsList) {
                                $scope.vendorDisplayCollecton = [].concat($scope.vendorsList);
                                $scope.totalCount = $scope.vendorsList[0].totalVendors;
                            }
                            // $scope.searchingVendors = '';
                            // $scope.getCatalogCategories();
                        });
                }
            }
                
            }
    };

    $scope.GetCompanyVendors();


    $scope.searchCategoryVendors = function (str) {
        var category = '';

        category = str.catCode;
        if (category == 'ALL') {
            $scope.GetCompanyVendors();
        } else {
           // $scope.getvendors(str);
            $scope.GetCompanyVendors(0, '', category);
        }
        

    };

    $scope.VendorsList = [];
    $scope.VendorsTempList1 = [];

    $scope.getvendors = function (catObj) {
        let countryFilter = '';

       
        $scope.ShowDuplicateVendorsNames = [];
        $scope.VendorsList = [];
        $scope.VendorsTempList1 = [];
        $scope.vendorsLoaded = false;
        var category = '';

        category = catObj.catCode;

        var params = { 'Categories': category, 'locations': countryFilter, 'sessionID': userService.getUserToken(), 'uID': userService.getUserId(), evalID: $scope.isTechEval ? $scope.selectedQuestionnaire.evalID : 0 };
        $http({
            method: 'POST',
            url: domain + 'getvendors',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data) {
                if (response.data.length > 0) {
                    // filter vendors who has contracts
                   

                    $scope.VendorsList = response.data;
                    $scope.VendorsTempList = $scope.VendorsList;
                    $scope.vendorDisplayCollecton = [];
                    $scope.VendorsList.forEach(function (item, index) {
                        item.email = item.vendor.email;
                        item.phoneNum = item.vendor.phoneNum;
                        //$scope.VendorsTempList.forEach(function (item1, index1) {
                        //    if (item.vendorID == item1.vendorID) {
                        //        $scope.ShowDuplicateVendorsNames.push(item1);
                        //        $scope.VendorsList.splice(index1, 1);
                        //    }
                        //})
                        $scope.vendorDisplayCollecton.push(item);
                    });

                    //$scope.vendorDisplayCollecton = $scope.VendorsList;

                    $scope.VendorsTempList1 = $scope.VendorsList;

                   

                } else {
                    $scope.VendorsList = [];
                }

                


            } else {

            }
        }, function (result) {
        });
    };
    
    $scope.searchTable = function (str)
    {
        var filterText = angular.lowercase(str);
        $scope.searchingVendors = filterText;
        $scope.GetCompanyVendors(0, $scope.searchingVendors);
    }


    ///1111
    $scope.activatecompanyvendor = function (vendorID, isValid) {
        userService.activatecompanyvendor({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": isValid })
            .then(function (response) {
                if (response.errorMessage != "") {
                    growlService.growl(response.errorMessage, "inverse");
                }
                else if (response.errorMessage == "") {
                    $scope.GetCompanyVendors();
                    if (isValid == 0) {
                        growlService.growl("Vendor De-Activated Successfully", "inverse");
                    }
                    else if (isValid == 1) {
                        growlService.growl("Vendor Activated Successfully", "success");
                    }
                }
            });
    };

    $scope.ProdVendorData = [];
    $scope.isLoadingPrices = '';
    $scope.productNameData = '';

    $scope.GetProdVendorData = function (catItemId, vendorId, name) {
        $scope.ProdVendorData = [];
        $scope.isLoadingPrices = 'Loading Data';
        $scope.productNameData = name;
        var params = {
            vendorid: $scope.stateParamVendorId,
            catitemid: catItemId,
            sessionid: userService.getUserToken()
        };

        catalogService.GetProdVendorData(params)
            .then(function (response) {
                if (response) {
                    $scope.ProdVendorData = response;
                    if ($scope.ProdVendorData.length > 0) {
                        $scope.isLoadingPrices = '';
                    } else {
                        $scope.isLoadingPrices = 'No data availble';
                    }
                    $scope.ProdVendorData.forEach(function (data, dataIndex) {
                        data.LAST_MODIFIED_DATE = new moment(data.LAST_MODIFIED_DATE).format("DD-MM-YYYY");
                    })
                } else {
                    $scope.isLoadingPrices = 'No data availble';
                }
            })
    };

    $scope.getCategories = function () {
        auctionsService.getCategories(userService.getUserId())
            .then(function (response) {
                $scope.categories = response;
                $scope.addVendorCats = _.uniq(_.map(response, 'category'));
                $scope.categoriesdata = response;
                $scope.showCategoryDropdown = true;
            })
    };

    $scope.getCategories();

    $scope.loadSubCategories = function () {
        $scope.subcategories = _.filter($scope.categoriesdata, { category: $scope.newVendor.category });
    };

    $scope.changeCategory = function () {
        $scope.selectedSubCategoriesList = [];
        $scope.loadSubCategories();
    };

    $scope.selectSubcat = function () {
        $scope.selectedSubCategoriesList = [];
        $scope.vendorsLoaded = false;
        var category = [];
        var count = 0;
        var succategory = "";
        $scope.sub.selectedSubcategories = $filter('filter')($scope.subcategories, { ticked: true });
        selectedcount = $scope.sub.selectedSubcategories.length;
        if (selectedcount > 0) {
            succategory = _.map($scope.sub.selectedSubcategories, 'id');
            category.push(succategory);
            $scope.selectedSubCategoriesList = succategory;
        }
    };

    $scope.selectSubcat();

    $scope.getKeyValuePairs = function (parameter) {
        auctionsService.getKeyValuePairs(parameter, $scope.compId)
            .then(function (response) {
                if (parameter == "CURRENCY") {
                    $scope.currencies = response;
                } else if (parameter == "TIMEZONES") {
                    $scope.timezones = response;
                }
                $scope.selectedCurrency = $filter('filter')($scope.currencies, { value: response.currency });
                $scope.selectedCurrency = $scope.selectedCurrency[0];
            })
    };

    $scope.getKeyValuePairs('CURRENCY');
    $scope.getKeyValuePairs('TIMEZONES');

    $scope.getFile = function () {
        $scope.progress = 0;
        $scope.file = $("#tindoc")[0].files[0];
        fileReader.readAsDataUrl($scope.file, $scope)
            .then(function (result) {

            });
    };



    $scope.checkUserUniqueResult = function (idtype, inputvalue) {
        $scope.isValidToSubmit = true;
        if (!$scope.checkVendorPhoneUniqueResult) {
            $scope.checkVendorPhoneUniqueResult = false;
        }

        if (!$scope.checkVendorEmailUniqueResult) {
            $scope.checkVendorEmailUniqueResult = false;
        }

        if (!$scope.checkSapVendorUniqueResult) {
            $scope.checkSapVendorUniqueResult = false;
        }

        if (idtype === "PHONE") {
            if (inputvalue && $scope.temporaryObj.phone != inputvalue) {
                $scope.sendUniqueCall = true;
            } else {
                $scope.sendUniqueCall = false;
                $scope.checkVendorPhoneUniqueResult = false;
                $scope.isSaveDisabled = false;
            }
        }

        if (idtype === "EMAIL") {
            if (inputvalue && $scope.temporaryObj.email !== inputvalue) {
                $scope.sendUniqueCall = true;
            } else {
                $scope.sendUniqueCall = false;
                $scope.checkVendorEmailUniqueResult = false;
                $scope.isSaveDisabled = false;
            }
        }

        if (idtype === "SAP_VENDOR_CODE") {
            if (inputvalue && $scope.temporaryObj.sapVendCode != inputvalue) {
                $scope.sendUniqueCall = true;
            } else {
                $scope.sendUniqueCall = false;
                $scope.checkSapVendorUniqueResult = false;
                $scope.isSaveDisabled = false;
            }
        }

        if (!inputvalue) {
            return false;
        }
        
        if ($scope.sendUniqueCall) {
            userService.checkUserUniqueResult(inputvalue, idtype, false, true).then(function (response) {
                if (idtype === "PHONE") {
                    if ($scope.checkVendorPhoneUniqueResult = !response) {
                        $scope.checkVendorPhoneUniqueResult = !response;
                        $scope.submitphoneDisabled = true;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.submitphoneDisabled = false;
                        $scope.isSaveDisabled = false;
                    }


                } else if (idtype === "EMAIL") {
                    if ($scope.checkVendorEmailUniqueResult = !response) {
                        $scope.checkVendorEmailUniqueResult = !response;
                        $scope.submitemailDisabled = true;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.submitemailDisabled = false;
                        $scope.isSaveDisabled = false;
                    }

                }
                else if (idtype === 'SAP_VENDOR_CODE') {
                    if ($scope.checkSapVendorUniqueResult = !response) {
                        $scope.checkSapVendorUniqueResult = !response;
                        $scope.isSaveDisabled = true;
                    } else {
                        $scope.isSaveDisabled = false;
                    }
                }
            });
        }
    };

    $scope.companyConfigStr = '';
        $scope.companyConfigurationArr = ['BANK_CODES', 'STATE_CODE', 'COUNTRY', 'COUNTRY_CODE'];
        $scope.companyConfigStr = $scope.companyConfigurationArr.join(',').toString();
        $scope.companyConfigList = [];
        $scope.companyBankUnits = [];
        $scope.companyStateUnits = [];
        $scope.companyCountryUnits = [];
        $scope.companyCountryCodeUnits = [];

        if ($scope.companyConfigStr != null || $scope.companyConfigStr != '') {
            auctionsService.GetCompanyConfigurationForLogin(0, $scope.companyConfigStr, userService.getUserToken())
                .then(function (unitResponse) {
                    $scope.companyConfigList = unitResponse;

                    $scope.companyConfigList.forEach(function (item, index) {
                        if (item.configKey == 'BANK_CODES') {
                            $scope.companyBankUnits.push(item);
                        } else if (item.configKey == 'STATE_CODE') {
                            $scope.companyStateUnits.push(item);
                        } else if (item.configKey == 'COUNTRY') {
                            $scope.companyCountryUnits.push(item);
                        } else if (item.configKey == 'COUNTRY_CODE') {
                            $scope.companyCountryCodeUnits.push(item);
                        }
                    })
                });
        }

        $scope.populateBankName = function (value) {
            $scope.vendorregisterobj.BankName = value.configValue;
        }

        $scope.companyStateUnitsTemp = [];
        $scope.displayStateVal = true;

        $scope.populateState = function (value) {
            if (value == 'India') {
                $scope.displayStateVal = false;
                $scope.companyStateUnitsTemp = $scope.companyStateUnits;
                $scope.companyStateUnitsTemp.forEach(function (item, index) {
                    item.configValue = item.configValue.split(':')[0];
                })
            } else {
                $scope.displayStateVal = true;
            }
        }

        $scope.cCode = '';

    $scope.populateCCCode = function (value) {
        $scope.cCode = '';
        $scope.companyCountryCodeUnits.forEach(function (item, index) {
            if (value == item.configText) {
                $scope.cCode = item.configValue;
                return item;
            }
        });
    };




    $scope.addVendorValidation = function () {
        $scope.emailRegx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.mobileRegx = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        $scope.panregx = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;

        $scope.addVendorValidationStatus = false;
        $scope.firstvalidation = $scope.companyvalidation = $scope.lastvalidation = $scope.contactvalidation = $scope.emailvalidation = $scope.categoryvalidation = $scope.emailregxvalidation = $scope.contactvalidationlength = $scope.panregxvalidation = $scope.tinvalidation = $scope.stnvalidation = $scope.currencyvalidation = $scope.knownSincevalidation = false;

        if ($scope.newVendor.firstName == "" || $scope.newVendor.firstName === undefined) {
            $scope.firstvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        }
        //if ($scope.newVendor.lastName == "" || $scope.newVendor.lastName === undefined) {
        //    $scope.lastvalidation = true;
        //    $scope.addVendorValidationStatus = true;
        //    return;
        //}
        if ($scope.preferredLogin === 'PHONE') {
            if ($scope.newVendor.contactNum == "" || $scope.newVendor.contactNum === undefined || isNaN($scope.newVendor.contactNum)) {
                $scope.contactvalidation = true;
                $scope.addVendorValidationStatus = true;
                return;
            } else if (!$scope.mobileRegx.test($scope.newVendor.contactNum)) {
                $scope.contactvalidationlength = true;
                $scope.addVendorValidationStatus = true;
                return;
            }
        }
        if ($scope.newVendor.email == "" || $scope.newVendor.email === undefined) {
            $scope.emailvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        } else if (!$scope.emailRegx.test($scope.newVendor.email)) {
            $scope.emailregxvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        }
        if ($scope.checkVendorEmailUniqueResult || $scope.checkVendorEmailUniqueResult) {
            $scope.addVendorValidationStatus = true;
            return;
        }

        if ($scope.newVendor.companyName == "" || $scope.newVendor.companyName === undefined) {
            $scope.companyvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        }

        //if ($scope.newVendor.category == "" || $scope.newVendor.category === undefined) {
        //    $scope.categoryvalidation = true;
        //    $scope.addVendorValidationStatus = true;
        //    return;
        //}
        if ($scope.newVendor.currency == "" || $scope.newVendor.currency === undefined) {
            $scope.currencyvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        }

        //$scope.selectedProducts = [];
        $scope.products.forEach(function (item, index) {
            if (item.nodeChecked) {
                $scope.selectedProducts.push(item.prodId);
            }
        });
        $scope.productsInitial = $scope.products;
        //if ($scope.selectedProducts.length <= 0) {
        //    $scope.catalogvalidation = true;
        //    $scope.addVendorValidationStatus = true;
        //    return;
        //}

        if ($scope.selectedCategories.length <= 0) {
            $scope.catalogvalidation = true;
            $scope.addVendorValidationStatus = true;
            return;
        } else {
            $scope.catalogvalidation = false;
            $scope.addVendorValidationStatus = false;
        }
    };

    



    $scope.AddVendorsExcel = function () {
        var params = {
            "entity": {
                attachment: $scope.ImportEntity.attachment,
                userid: parseInt(userService.getUserId()),
                companyId: $scope.compId,
                entityName: 'AddVendor',
                sessionid: userService.getUserToken()
            }
        };

        userService.AddVendorsExcel(params)
            .then(function (response) {
                if (response.errorMessage == "") {
                    swal({
                        title: "Thanks!",
                        text: "Vendors Added Successfully",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });
                } else {
                    swal({
                        title: "Failed!",
                        text: response.errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            location.reload();
                        });
                }
            })
    };

    $scope.addPhone = function () {
        $scope.phonesLength = $scope.multiplePhones.length;
        $scope.singlePhone =
            {
                contactNo: '',
                phoneSNo: $scope.phoneSNo++
            }
        $scope.multiplePhones.push($scope.singlePhone);
    };

    $scope.deletePhone = function (phoneSNo) {
        $scope.phonesLength = $scope.multiplePhones.length;
        //if ($scope.phonesLength > 1) {
        $scope.multiplePhones = _.filter($scope.multiplePhones, function (x) { return x.phoneSNo !== phoneSNo; });
        //}
    };

    $scope.phoneSNo = 1;
    $scope.multiplePhones = [];
    $scope.singlePhone =
        {
            contactNo: '',
            phoneSNo: $scope.phoneSNo++
        }
    $scope.phones = [];
    $scope.editPhones = function (altPhoneNum) {
        $scope.phones = [];
        $scope.multiplePhones = [];
        $scope.phones = altPhoneNum.split(",");

        if ($scope.phones.length > 0) {
            $scope.phones.forEach(function (item, index) {
                $scope.singlePhone =
                    {
                        contactNo: item,
                        phoneSNo: $scope.phoneSNo++
                    }
                $scope.multiplePhones.push($scope.singlePhone);
            });
        }
    };

    $scope.SaveMultiPhones = function () {
        $scope.phoneError = false;
        $scope.multiplePhones.forEach(function (item, index) {
            item.error = "";
            if (item.contactNo.toString() == "") {
                item.error = "Phone Number cannot be empty.";
                $scope.phoneError = true;
            }
            else if (isNaN(item.contactNo)) {
                item.error = "Please Enter correct Mobile number.";
                $scope.phoneError = true;
            }
            else if (item.contactNo.toString().length != 10) {
                item.error = "Phone Number Must be 10 digits.";
                $scope.phoneError = true;
            }
        });

        if (!$scope.phoneError) {
            var phone = "";
            if ($scope.multiplePhones && $scope.multiplePhones.length > 0) {
                $scope.multiplePhones.forEach(function (item, index) {
                    if (item.contactNo == undefined) {
                        item.contactNo = '';
                    };
                    phone += item.contactNo + ',';
                });
                phone = phone.slice(0, -1);
            };
            $scope.userObj.altPhoneNum = phone;
            $(function () {
                $('#multiplePhones').modal('toggle');
            });
            self.updateUserInfo();
        }
    };

    $scope.addEmail = function () {
        $scope.emailsLength = $scope.multipleEmails.length;
        $scope.singleEmail =
            {
                email: '',
                emailSNo: $scope.emailSNo++
            }
        $scope.multipleEmails.push($scope.singleEmail);
    };

    $scope.deleteEmail = function (emailSNo) {
        $scope.emailsLength = $scope.multipleEmails.length;
        //if ($scope.emailsLength > 1) {
        $scope.multipleEmails = _.filter($scope.multipleEmails, function (x) { return x.emailSNo !== emailSNo; });
        //}
    };

    $scope.emailSNo = 1;
    $scope.multipleEmails = [];
    $scope.singleEmail = {
        email: '',
        emailSNo: $scope.emailSNo++
    };
    $scope.emails = [];

    $scope.editEmails = function (altEmail) {
        $scope.emails = [];
        $scope.multipleEmails = [];
        $scope.emails = altEmail.split(",");

        if ($scope.emails.length > 0) {
            $scope.emails.forEach(function (item, index) {
                $scope.singleEmail =
                    {
                        email: item,
                        emailSNo: $scope.emailSNo++
                    }
                $scope.multipleEmails.push($scope.singleEmail);
            });
        }
    };

    $scope.SaveMultiEmails = function () {
        $scope.emailError = false;
        $scope.multipleEmails.forEach(function (item, index) {
            item.error = "";
            if (item.email.toString() == "") {
                item.error = "Email cannot be empty.";
                $scope.emailError = true;
            }
        });

        if (!$scope.emailError) {
            var email = "";
            if ($scope.multipleEmails && $scope.multipleEmails.length > 0) {
                $scope.multipleEmails.forEach(function (item, index) {
                    if (item.email == undefined) {
                        item.email = '';
                    };
                    email += item.email + ',';
                });
                email = email.slice(0, -1);
            };
            $scope.userObj.altEmail = email;
            $(function () {
                $('#multipleEmails').modal('toggle');
            });
            self.updateUserInfo();
        }
    };

    $scope.AssignVendorToCompany = function (vendorPhone, vendorEmail) {
        if ($scope.newVendor.altPhoneNum == undefined) {
            $scope.newVendor.altPhoneNum = '';
        }
        if ($scope.newVendor.altEmail == undefined) {
            $scope.newVendor.altEmail = '';
        }
        var params = {
            userID: userService.getUserId(),
            vendorPhone: vendorPhone,
            vendorEmail: vendorEmail,
            sessionID: userService.getUserToken(),
            category: $scope.newVendor.category,
            subCategory: $scope.selectedSubCategoriesList,
            altPhoneNum: $scope.newVendor.altPhoneNum,
            altEmail: $scope.newVendor.altEmail
        };

        auctionsService.AssignVendorToCompany(params)
            .then(function (response) {
                $scope.vendor = response;
                if ($scope.vendor.errorMessage == '') {
                    swal({
                        title: "Done!",
                        text: "Vendor Added Successfully",
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            $state.reload();
                        });
                }
                else {
                    swal({
                        title: "Done!",
                        text: $scope.vendor.errorMessage,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    },
                        function () {
                            $state.reload();
                        });
                }
            });
    };

    $scope.goTocategory = function () {
        var url = $state.href('category', {});
        $window.open(url, '_blank');
    };

    $scope.downloadExcel = false;
    $scope.GetVendorReport = function () {
        $scope.vendorsListExcel = [];
        $scope.downloadExcel = true;
        $scope.GetCompanyVendors();
    };

    function exportVendorsToExcel() {

        var mystyle = {
            sheetid: 'Vendors',
            headers: true,
            column: {
                style: 'font-size:15px;background:#233646;color:#FFF;'
            }
        };

        //alasql.fn.getstoreid = function (x) {
        //    return x.storeID;
        //}

        alasql('SELECT firstName as [First Name], lastName as [Last Name], companyName as [Company Name], phoneNum as [Phone], email as [Email], companyVendorCodes as [Vendor Codes], vendorGSTNumbers as [GST Numbers] INTO XLSX(?,{headers:true,sheetid: "Vendors", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["Vendors.xlsx", $scope.vendorsListExcel]);
        $scope.downloadExcel = false;
    };

    $scope.goToVendorDetails = function (vendorID) {
        var url = $state.href('viewProfile', { "Id": vendorID });
        $window.open(url, '_blank');
    };


    $scope.saveVendor = function (type, global) {

        var invalidElements = $('.ng-invalid');

        if (invalidElements.length) {
            $('html, body').animate({
                scrollTop: (invalidElements.first().offset().top)
            }, 500);
        }

        if (type == 'REQUIREMENT') {
            $scope.selectedRequirementProducts = [];
            $scope.selectedRequirementProductsTemp = [];
            $scope.selectedRequirementProductCategories = [];
            $scope.formRequest.listRequirementItems.forEach(function (item, index) {
                if (item.catalogueItemID > 0 && item.CategoryId != null && item.CategoryId.length) {
                    item.nodeChecked = true;
                    item.prodId = item.catalogueItemID;
                    $scope.selectedRequirementProducts.push(item);
                    var tempArray = item.CategoryId.split(",");
                    tempArray.forEach(function (tempItem, tempIndex) {
                        $scope.selectedRequirementProductCategories.push(+tempItem);
                    });
                }
            });

            $scope.products = $scope.selectedRequirementProducts;
            $scope.selectedCategories = $scope.selectedRequirementProductCategories;
        } else if (!type) {
            type = '';
        }


        if ($scope.isEditVendor) {
            EditVendorDetails(type, global);
        } else {
            addNewVendor();
        }
    };

    function addNewVendor(form) {
        $scope.companyGSTInfo = [];
        $scope.isSaveDisabled = true;
        $scope.addVendorValidation();
        if ($scope.addVendorValidationStatus) {
            $scope.isSaveDisabled = false;
            return false;
        }
        var vendCAtegories = [];
        $scope.newVendor.category = $scope.newVendor.category;
        vendCAtegories.push($scope.newVendor.category);
        var params = {
            "register": {
                "firstName": $scope.newVendor.firstName,
                "lastName": $scope.newVendor.lastName,
                "email": $scope.newVendor.email,
                "phoneNum": $scope.newVendor.contactNum,
                "altEmail": $scope.newVendor.altEmail,
                "altPhoneNum": $scope.newVendor.altPhoneNum,
                "username": $scope.newVendor.contactNum,
                "password": $scope.newVendor.contactNum,
                "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                "vendorCode": $scope.newVendor.vendorCode ? $scope.newVendor.vendorCode : "",
                "sapVendorCode": $scope.newVendor.sapVendorCode,
                "isOTPVerified": 0,
                "category": $scope.newVendor.category,
                "userType": "VENDOR",
                "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                "referringUserID": userService.getUserId(),
                "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                "errorMessage": "",
                "sessionID": "",
                "userID": 0,
                "department": "",
                "currency": $scope.newVendor.currency.key,
                "Country": $scope.newVendor.Country,
                "subcategories": $scope.selectedSubCategoriesList,
                "additionalData": JSON.stringify($scope.newVendor.additionalData),
                "attachments": JSON.stringify($scope.Attachements),
                'vendorStatus': $scope.newVendor.vendorStatus,
                "oldEmail": $scope.newVendor.oldEmail,
                "oldPhone": $scope.newVendor.oldPhone,
                'paymentTermCode': $scope.newVendor.paymentTermCode,
                "paymentTermDesc": $scope.newVendor.paymentTermDesc,
                "incoTerm": $scope.newVendor.incoTerm
            },
            "altUsers": $scope.altUsers
        };
        $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            $scope.isSaveDisabled = true;
            if (response && response.data && response.data.errorMessage == "") {
                $scope.SaveVendorCatalog(response.data.objectID, '', '');
                $scope.newVendor = null;
                $scope.newVendor = {};
                $scope.addVendorShow = false;
                growlService.growl("Vendor Added Successfully.", 'inverse');
                //$state.reload();
                this.addVendor = 0;
                $scope.canceEditVendor(form);
            } else if (response && response.data && response.data.errorMessage) {
                growlService.growl(response.data.errorMessage, 'inverse');
                $scope.isSaveDisabled = false;
                //$state.reload();
            } else {
                growlService.growl('Unexpected Error Occurred', 'inverse');
                $scope.isSaveDisabled = false;
            }
        });
    }

    function EditVendorDetails(type, global) {
        $scope.isSaveDisabled = true;
        $scope.addVendorValidation();
        if ($scope.addVendorValidationStatus) {
            $scope.isSaveDisabled = false;
            return false;
        }
        var vendCAtegories = [];
        //$scope.newVendor.category = $scope.newVendor.category;
        vendCAtegories.push($scope.newVendor.category);
        var params = {
            "register": {
                "firstName": $scope.newVendor.firstName,
                "lastName": $scope.newVendor.lastName,
                "email": $scope.newVendor.email,
                "phoneNum": $scope.newVendor.contactNum,
                "altEmail": $scope.newVendor.altEmail,
                "altPhoneNum": $scope.newVendor.altPhoneNum,
                "username": $scope.newVendor.contactNum,
                "password": $scope.newVendor.contactNum,
                "companyName": $scope.newVendor.companyName ? $scope.newVendor.companyName : "",
                "isOTPVerified": 0,
                "category": "",//$scope.newVendor.category,
                "userType": "VENDOR",
                "panNumber": ("panno" in $scope.newVendor) ? $scope.newVendor.panno : "",
                "stnNumber": ("serviceTaxNo" in $scope.newVendor) ? $scope.newVendor.serviceTaxNo : "",
                "vatNumber": ("vatNum" in $scope.newVendor) ? $scope.newVendor.vatNum : "",
                "referringUserID": userService.getUserId(),
                "knownSince": ("knownSince" in $scope.newVendor) ? $scope.newVendor.knownSince : "",
                "errorMessage": "",
                "sessionID": userService.getUserToken(),
                "userID": $scope.newVendor.userID,
                "department": "",
                "currency": $scope.newVendor.currency ? $scope.newVendor.currency.key : '317',
                //"subcategories": $scope.selectedSubCategoriesList,
                "additionalData": JSON.stringify($scope.newVendor.additionalData),
                "attachments": JSON.stringify($scope.Attachements),
                "vendorCode": $scope.newVendor.vendorCode,
                "Address1": $scope.newVendor.Address1,
                "Street1": $scope.newVendor.Street1,
                "Street2": $scope.newVendor.Street2,
                "Street3": $scope.newVendor.Street3,
                "Country": $scope.newVendor.Country,
                "State": $scope.displayStateVal ? $scope.newVendor.State : $scope.newVendor.State,
                "PinCode": $scope.newVendor.PinCode,
                "City": $scope.newVendor.City,
                "altMobile": $scope.newVendor.altMobile,
                "landLine": $scope.newVendor.landLine,
                "salesPerson": $scope.newVendor.salesPerson,
                "salesContact": $scope.newVendor.salesContact,
                "BankName": $scope.newVendor.BankName,
                "BankAddress": $scope.newVendor.BankAddress,
                "accntNumber": $scope.newVendor.accntNumber,
                "Ifsc": $scope.newVendor.Ifsc,
                "cancelledCheque": $scope.newVendor.cancelledCheque,
                "isSelf": false,
                "sapVendorCode": $scope.newVendor.sapVendorCode,
                "BussinessDet": $scope.newVendor.BussinessDet,
                "businessattachments": $scope.businessObject.fileStream,
                "businessattachmentName": $scope.businessObject.name,
                "msmeAttachments": $scope.msmeObject.fileStream,
                "msmeAttachmentName": $scope.msmeObject.name,
                "msmeAttach": $scope.newVendor.msmeAttach,
                "businessAttach": $scope.newVendor.businessAttach,
                "ccAttach": $scope.newVendor.cancelledCheque,
                "additionalVendorInformation": JSON.stringify($scope.newVendor.additionalVendorInformation),
                "customerCopyLetterAttachments": $scope.copyLetterObject.fileStream,
                "customerCopyLetterAttachmentName": $scope.copyLetterObject.name,
                'vendorStatus': $scope.newVendor.vendorStatus,
                "oldEmail": $scope.newVendor.oldEmail,
                "oldPhone": $scope.newVendor.oldPhone,
                'paymentTermCode': $scope.newVendor.paymentTermCode,
                "paymentTermDesc": $scope.newVendor.paymentTermDesc,
                "incoTerm": $scope.newVendor.incoTerm
            },
            "altUsers": $scope.altUsers
        };
        $http({
            method: 'POST',
            url: domain + 'register',
            encodeURI: true,
            headers: { 'Content-Type': 'application/json' },
            data: params
        }).then(function (response) {
            if (response && response.data && response.data.errorMessage == "") {
                $scope.SaveVendorCatalog(response.data.objectID, type, global);
                if (type == 'REQUIREMENT') {
                    angular.element('#poItemDetails').modal('hide');
                }

            } else if (response && response.data && response.data.errorMessage) {
                growlService.growl(response.data.errorMessage, 'inverse');
            } else {
                growlService.growl('Unexpected Error Occurred', 'inverse');
            }


        });
    }

    $scope.downloadTemplate = function () {
        reportingService.downloadTemplate('VENDOR_QUOTATION', userService.getUserId(), 0);
    };








    $scope.businessObject = {};
    $scope.msmeObject = {};
    $scope.copyLetterObject = {};
    $scope.ImportEntity = {};

        $scope.getFile1 = function (id, doctype, ext) {
            $scope.progress = 0;
            $scope.file = $("#" + id)[0].files[0];
            $scope.docType = doctype + "." + ext;
            fileReader.readAsDataUrl($scope.file, $scope)
                .then(function (result) {
                    if (id != "assocWithOEMFile" && id != 'storeLogo') {
                        var bytearray = new Uint8Array(result);
                        var fileobj = {};
                        fileobj.fileStream = $.makeArray(bytearray);
                        fileobj.fileType = $scope.docType;
                        fileobj.isVerified = 0;
                        if (doctype == "PAN") {
                            fileobj.credentialID = $scope.pannumber;
                            $scope.panObject = fileobj;
                        }
                        else if (doctype == "TIN") {
                            fileobj.credentialID = $scope.vatnumber;
                            $scope.tinObject = fileobj;
                        }
                        else if (doctype == "STN") {
                            fileobj.credentialID = $scope.taxnumber;
                            $scope.stnObject = fileobj;
                        }else if (doctype == "BUSINESSDET") {
                            $scope.businessObject = fileobj;
                            $scope.businessObject.name = $scope.file.name;
                        }
                        else if (doctype == "MSME") {
                            $scope.msmeObject = fileobj;
                            $scope.msmeObject.name = $scope.file.name;
                        }
                        else if (doctype == "COPYLETTER") {
                            $scope.copyLetterObject = fileobj;
                            $scope.copyLetterObject.name = $scope.file.name;
                        }
                    }

                    if (id == "storeLogo") {
                        var bytearray = new Uint8Array(result);
                        $scope.logoFile.fileStream = $.makeArray(bytearray);
                        $scope.logoFile.fileName = $scope.file.name;
                    }

                    if (id == "profileFile") {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.profileFile = $.makeArray(bytearray);
                        $scope.userDetails.profileFileName = $scope.file.name;
                    }


                    if (id == "vendorsAttachment") {
                        if (ext != "xlsx") {
                            swal("Error!", "File type should be XSLX. Please download the template and  fill values accordingly.", "error");
                            return;
                        }
                        var bytearray = new Uint8Array(result);
                        $scope.ImportEntity.attachment = $.makeArray(bytearray);
                        $scope.ImportEntity.attachmentFileName = $scope.file.name;
                        $scope.AddVendorsExcel();
                    }

                    else if (id == "assocWithOEMFile") {
                        var bytearray = new Uint8Array(result);
                        $scope.userDetails.assocWithOEMFile = $.makeArray(bytearray);
                        $scope.userDetails.assocWithOEMFileName = $scope.file.name;
                    }

                });
    };
    $scope.exportTemplateToExcel = function () {
        var mystyle = {
            sheetid: 'VendorItemCategories',
            headers: true,
            column: {
                style: 'font-size:15px;background:#233646;color:#FFF;'
            }
        };

        alasql('SELECT "ProductName" as [ProductName] ,"CategoryName" as [CategoryName] INTO XLSX(?,{headers:true,sheetid: "VendorItemCategories", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["VendorItemCategories.xlsx", $scope.ExcelProducts]);
    }

    $scope.entityObj = {
        entityName: 'VendorItemCategories',
        attachment: [],
        userid: userService.getUserId(),
        sessionID: userService.getUserToken()
    };

    $scope.getFile = function () {
        $scope.file = $("#excelquotationproducts")[0].files[0];
        fileReader.readAsDataUrl($scope.file, $scope)
            .then(function (result) {
                var bytearray = new Uint8Array(result);
                $scope.entityObj.attachment = $.makeArray(bytearray);
                $scope.importVendorCatalogue();
            });
    }


    $scope.importVendorCatalogue = function () {

        var params = {
            "entity": $scope.entityObj,
            compId: $scope.compId,
            vendorId: $scope.currentVendorId,
            user: userService.getUserId(),
            sessionId: userService.getUserToken()
        };

        catalogService.importvendoritemcategories(params)
            .then(function (response) {

                if (response.errorMessage != '') {
                    alert(response.errorMessage);
                }
                if (response.repsonseId > 0) {
                    growlService.growl(response.repsonseId + " items updated successfully.", "success");
                    $scope.getcatalogcategories($scope.currentVendorId,'EXCEL',0);
                    //location.reload();
                }
            })
    }






    //Registration Additional field
        $scope.RegistrationAdditonalField = [];

        $scope.getRegAddtionalFields = function () {
            SettingService.getRegistrationFields().then(function (response) {
                if (response != null && response.length > 0) {
                    $scope.RegistrationAdditonalField = response;
                }
            });
        };

        $scope.getRegAddtionalFields();

        $scope.Attachements = [];
        $scope.onFileSelect = function ($files, $item, $modal) {

            var obj = {
                Field: $item.Name,
                Files: []
            }
            $scope.Attachements.push()
            for (var i in $files) {
                fileReader.readAsDataUrl($files[i], $scope)
              .then(function (result) {
                  var bytearray = new Uint8Array(result);
                  var fileobj = {};
                  fileobj.fileStream = $.makeArray(bytearray);
                  fileobj.fileType = $files[i].type;
                  fileobj.name = $files[i].name
                  fileobj.isVerified = 0;
                  //$scope.verificationObj.attachmentName=$scope.file.name;
                  obj.Files.push(fileobj);
              });
            }
            $scope.Attachements.push(obj)


        }

        $scope.resetPwd = function (userID) {
            userService.resetPwd({ "userID": userID, "updatedby": userService.getUserId() })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User password reset successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        };

        $scope.deleteVendor = function (userid) {
            userService.deleteUser({ "userID": userid, "referringUserID": userService.getUserId(), "isVendor": 1 })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    } else {
                        growlService.growl("User deleted Successfully", "inverse");
                        $scope.getSubUserData();
                    }
                });
        }
    //View addtional data
        $scope.viewAdditionalData = function (row) {
            $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'VendorAdditionalModal.html',
                controller: function ($uibModalInstance, vendor, userService) {
                    var _modalCtrl = this;
                    Declined = 2;
                    _modalCtrl.Vendor = vendor;
                    if (_modalCtrl.Vendor.additionalInfo) {
                        _modalCtrl.addtionalInforMationData = JSON.parse(_modalCtrl.Vendor.additionalInfo);
                    }

                    _modalCtrl.sessionid = userService.getUserToken();
                    _modalCtrl.close = function () {
                        $uibModalInstance.close();
                    };


                },
                controllerAs: '_modalCtrl',
                appendTo: 'body',
                resolve: {
                    vendor: row
                }
            })
        }

    //vendor status
    $scope.StatusList = [
                    {
                        id: 1,
                        name: "Approved"
                    },
                   {
                       id: 2,
                       name: "Blocked"
                   },
                   {
                       id: 3,
                       name: "On Hold"
                   }
                   ,
                   {
                       id: 4,
                       name: "Un Approved"
                   },                   
                   {
                       id: 5,
                       name: "Inactive"
                   }
                   ];
    //Update vendor status
    $scope.approvedVendor = function (row) {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'VendorStatusModal.html',
            controller: VendorStatusModelCtrl,
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                data: {
                    vendor: row,
                    StatusList: $scope.StatusList

                }
            }
        }).result.then(function (data) {
            $scope.GetCompanyVendors();
        });

    };

        

        //editVendor
    $scope.canceEditVendor = function (form) {
        $scope.newVendor = {};
        $scope.newVendor.panno = "";
        $scope.newVendor.vatNum = "";
        $scope.newVendor.serviceTaxNo = "";
        $scope.isEditVendor = 0;
        $scope.addVendorShow = false;
        $scope.reset(form);
        $scope.vendorID = 0;
    };


    $scope.reset = function (form) {
        form.$setPristine();
        form.$setUntouched();
        $scope.resetCount++;
    };

    $scope.AddNewVendor = function (vendor) {
        $scope.isEditVendor = 0;
        $scope.newVendor.BankName = "";
        $scope.newVendor.Country = "";
        $scope.newVendor.currency = "";
    };

        $scope.vendorID = 0;


    $scope.editVendor = function (vendor) {
        if ($scope.selectedCategories.length >= 0) {
            $scope.catalogvalidation = false;
        } else if ($scope.selectedCategories.length < 0) {
            $scope.catalogvalidation = true;
        }

        userService.getProfileDetails({ "userid": vendor.userID, "sessionid": userService.getUserToken() })
            .then(function (response) {
                if (response) {
                    $scope.addVendor = 1;
                    $scope.addVendorShow = true;
                    $scope.vendorID = vendor.userID;
                    $scope.newVendor = vendor;
                    $scope.newVendor.vendorStatus = vendor.vendorStatus;
                    //$scope.newVendor.vendorCode = response.vendorCode;
                    $scope.temporaryObj.email = vendor.email;
                    $scope.temporaryObj.phone = vendor.phoneNum;
                    $scope.temporaryObj.sapVendCode = vendor.sapVendCode;
                    $scope.newVendor.vendorStatus = response.vendorStatus;
                    $scope.newVendor.Address1 = response.Address1;
                    $scope.newVendor.Street1 = response.Street1;
                    $scope.newVendor.Street2 = response.Street2;
                    $scope.newVendor.Street3 = response.Street3;
                    $scope.newVendor.Country = response.Country;
                    $scope.newVendor.State = response.State;
                    $scope.newVendor.PinCode = response.PinCode;
                    $scope.newVendor.City = response.City;
                    $scope.newVendor.altMobile = +response.altMobile;
                    $scope.newVendor.landLine = +response.landLine;
                    $scope.newVendor.salesPerson = response.salesPerson;
                    $scope.newVendor.salesContact = response.salesContact;
                    $scope.newVendor.BankName = response.BankName;
                    $scope.newVendor.BankAddress = response.BankAddress;
                    $scope.newVendor.accntNumber = response.accntNumber;
                    $scope.newVendor.Ifsc = response.Ifsc;
                    $scope.newVendor.cancelledCheque = response.cancelledCheque;
                    $scope.newVendor.gstAttach = response.gstAttach;
                    $scope.newVendor.panAttach = response.panAttach;
                    $scope.newVendor.msmeAttach = response.msmeAttach;
                    $scope.newVendor.businessAttach = response.businessAttach;
                    $scope.newVendor.customerCopyLetterAttach = response.customerCopyLetterAttach;
                    $scope.newVendor.stn = response.stn;
                    $scope.newVendor.pan = response.pan;
                    $scope.newVendor.BussinessDet = response.BussinessDet;
                    $scope.newVendor.sapVendorCode = response.sapVendorCode;
                    $scope.changeCategory();
                    $scope.subcategories = response.subcategories;
                    $scope.newVendor.additionalData = response.additionalInfo !== "" ? angular.fromJson(response.additionalInfo) : "";
                    $scope.newVendor.additionalVendorInformation = vendor.additionalVendorInformation !== "" ? angular.fromJson(vendor.additionalVendorInformation) : "";
                    $scope.newVendor.additionalVendorInformation.customerPhoneNumber = (+$scope.newVendor.additionalVendorInformation.customerPhoneNumber);
                    $scope.sub.selectedSubcategories = response.subcategories;
                    $scope.newVendor.userID = response.userID;
                    $scope.newVendor.companyId = response.companyId;
                    //let temp = _.filter($scope.currencies, function (x) { return x.value == response.currency; });
                    $scope.newVendor.currency = _.filter($scope.currencies, function (x) { return x.value == response.currency; })[0];
                    //$scope.currency = $scope.currencies.filter(function (e) { return e.value == response.currency }).map(function (e) { return e.name })[0]
                    //$scope.newVendor.contactNum = response.phoneNum.toString().length == 10 ? Number(response.phoneNum) : '';
                    $scope.newVendor.contactNum = response.phoneNum ? +response.phoneNum : '';
                    $scope.newVendor.paymentTermCode = response.paymentTermCode;
                    $scope.newVendor.paymentTermDesc = response.paymentTermDesc;
                    $scope.newVendor.incoTerm = response.incoTerm;
                    $scope.newVendor.oldEmail = $scope.temporaryObj.email;
                    $scope.newVendor.oldPhone = $scope.temporaryObj.phone;
                    $scope.subcategories.forEach(function (item, index) {
                        item.ticked = true;
                    });
                    $scope.selectSubcat();


                    $scope.getItemWorkflow();
                    $scope.getUnBlockItemWorkflow();
                    $scope.GetCompanyGSTInfo();

                }
            });

        $scope.isEditVendor = 1;
    };

        //View vendor score
    $scope.vendorScore = function (row) {
        $uibModal.open({
            ariaLabelledBy: 'modal-title',
            ariaDescribedBy: 'modal-body',
            templateUrl: 'VendorScoreModal.html',
            controller: function ($uibModalInstance, data, userService, growlService) {
                var _modalCtrl = this;

                _modalCtrl.Vendor = data.vendor
                _modalCtrl.StatusList = data.StatusList

                _modalCtrl.close = function () {
                    $uibModalInstance.close();
                };
                _modalCtrl.Model = {
                    Score: _modalCtrl.Vendor.regScore,
                    User_Id: _modalCtrl.Vendor.userID,
                    Scores: []
                }

                _modalCtrl.calculateScore = function () {
                    userService.calculateScore(_modalCtrl.Vendor.userID).then(function (response) {
                        _modalCtrl.Model.Score = response.objectID;
                    })
                }

                _modalCtrl.getVendorScore = function () {
                    userService.getVendorScore(_modalCtrl.Vendor.userID).then(function (response) {
                        _modalCtrl.Model.Scores = response
                    })
                }
                _modalCtrl.updateScore = function () {
                    var model = angular.copy(_modalCtrl.Model)
                    userService.updateVendorScore(model).then(function (response) {
                        if (response.errorMessage === "") {
                            $uibModalInstance.close(_modalCtrl.Vendor);
                            growlService.growl(response.message, "inverse");
                        }
                        else
                            growlService.growl(response.errorMessage, "inverse");
                    })
                }



            },
            controllerAs: '_modalCtrl',
            appendTo: 'body',
            resolve: {
                data: {
                    vendor: row,
                }
            }
        }).result.then(function (data) {
            $scope.GetCompanyVendors();
        });
    };

         //Get status
    $scope.getStatus = function (id) {
        //return $scope.StatusList.filter(function (e) { return e.id == id }).map(function (e) { return e.name })[0]
        return $scope.StatusList.filter(function (e) { return e.id == id }).map(function (e) { return e.name })[0];
    };

        $scope.altUsers = [];
        //$scope.altUser = {
        //    firstName: '',
        //    lastName: '',
        //    phoneNum: '',
        //    email: '',
        //    userID: 0,
        //    altUserID: 0
        //};

        $scope.GetAltUsers = function (newVendor) {
            auctionsService.GetAltUsers(userService.getUserId(), newVendor.userID, userService.getUserToken())
                .then(function (response) {
                    $scope.altUsers = response;
                })
        };

        $scope.addAltUser = function () {
            $scope.altUser = {
                firstName: '',
                lastName: '',
                phoneNum: '',
                email: '',
                userID: 0,
                altUserID: 0
            };
            $scope.altUsers.push($scope.altUser);
        };

        $scope.delAltUser = function (index) {
            if ($scope.altUsers && $scope.altUsers.length > 1)
            {
                $scope.altUsers.splice(index, 1);
            }
    };

    //// catalog management

    // Pagination For Vendor Level Categories//
    $scope.vendCatalogPage = 0;
    $scope.vendCatalogPageSize = 100;
    $scope.vendCatalogfetchRecordsFrom = $scope.page * $scope.PageSize;
    $scope.vendCatalogtotalCount = 0;
    //({{companyCatalog.length}}/{{totalCount}}
    // Pagination For Vendor Level Categories//
    $scope.parentNodes = null;
    $scope.isparentNodesLoading = false;
    $scope.getcatalogcategories = function (vendorId, type, IsPaging, searchString) {
        if (type == 'undefined' || type == "" || type == null) {
            type = '';
        }
        if (vendorId) {
            $scope.currentVendorId = vendorId;
        } else {
            $scope.currentVendorId = $scope.vendorID;
        }

        if (searchString) {
            $scope.fetchRecordsFrom = 0;
        }

        var catNodes = [];
            // checking for catalogue category checked issue
        if (IsPaging === 1) {
                if (vendorId == 'undefined' || vendorId == "" || vendorId == null) {
                    vendorId = $scope.vendorID;
                }
                if ($scope.vendCatalogPage > -1) {
                    $scope.vendCatalogPage++;
                    $scope.vendCatalogfetchRecordsFrom = $scope.vendCatalogPage * $scope.vendCatalogPageSize;
                    if ($scope.totalCategories != $scope.parentNodes.length) {
                        catalogService.getVendorCategories(vendorId, 0, $scope.compId, type, $scope.vendCatalogfetchRecordsFrom, $scope.vendCatalogPageSize, searchString ? searchString : "")
                            .then(function (response) {
                                if (response.length > 0) {
                                    for (var a in response) {
                                        $scope.parentNodes.push(response[+a]);
                                    }
                                  //  $scope.vendCatalogtotalCount = $scope.parentNodes[0].totalCategories;
                                }
                                //$scope.parentNodes = response;
                                if ($scope.parentNodes && $scope.parentNodes.length > 0) {
                                    $scope.parentNodes.forEach(function (tempNode, index) {
                                        tempNode.isFiltered = true;
                                    });

                                }
                                $scope.getvendorproducts();
                                $scope.collapseAll();
                            });

                    }
                }
            }//unlimited load data while scroll
        else {
            catalogService.getVendorCategories(vendorId, 0, $scope.compId, type, $scope.vendCatalogfetchRecordsFrom, $scope.vendCatalogPageSize, searchString ? searchString : "")
                            .then(function (response) {
                                $scope.parentNodes = response;
                                if ($scope.parentNodes && $scope.parentNodes.length > 0) {
                                    $scope.parentNodes.forEach(function (tempNode, index) {
                                        tempNode.isFiltered = true;
                                    });

                                   // $scope.vendCatalogtotalCount = $scope.parentNodes[0].totalCategories;

                                }
                                $scope.getvendorproducts();
                                $scope.collapseAll();
                            });

            }//default load data while pageload
    };

    $scope.getvendorproducts = function () {
        $scope.products = [];
        $scope.productsInitial = $scope.products;
        $scope.productsInitial = [];
        if ($scope.currentVendorId > 0) {
            catalogService.getVendorProducts($scope.currentVendorId, $scope.compId)
                .then(function (response) {
                    var varproducts = [];
                    var prodArray = response;
                    prodArray.forEach(function (item, index) {

                        var product = {
                            "compId": item.compId,
                            "prodId": item.prodId,
                            "prodName": item.prodName,
                            "prodNo": item.prodNo,
                            "nodeChecked": true
                        }
                        varproducts.push(product);
                        $scope.selectedProducts.push(item.prodId);
                    });
                    $scope.products = varproducts;
                    $scope.productsInitial = $scope.products;
                    $scope.getSelectedNodes();
                    $scope.getProducts($scope.selectedCategories);

                });
        }
        else {
            $scope.getSelectedNodes();
            //catalogService.getVendorProducts($scope.currentVendorId, $scope.compId)
            //    .then(function (response) {
            //        var varproducts1 = [];
            //        var prodArray1 = response;
            //        prodArray1.forEach(function (item, index) {

            //            var product = {
            //                "compId": item.compId,
            //                "prodId": item.prodId,
            //                "prodName": item.prodName,
            //                "prodNo": item.prodNo,
            //                "nodeChecked": true
            //            }
            //            varproducts1.push(product);
            //        //    $scope.selectedProducts.push(item.prodId);
            //        });
            //        $scope.products = varproducts1;

            //    });

            $scope.getProducts($scope.selectedCategories);
        }
    };

    $scope.getsub = function (parentData) {
        $scope.productDetails = null;
        var nodeData = parentData;

        var subNodes = [];

        if (nodeData && nodeData.id && (nodeData.nodes == null || nodeData.nodes.length <= 0)) {
            catalogService.getSubCatagories(nodeData.id, $scope.compId)
                .then(function (response) {
                    var subCatArray = response;
                    subCatArray.forEach(function (item, index) {
                        var subCat = {
                            "comId": item.compId,
                            "parentID": item.catParentId,
                            "id": item.catId,
                            "title": item.catName,
                            "catdesc": item.catDesc,
                            "subCatCount": item.subCatCount,
                            "childCollapsed": true,
                            "nodeChecked": parentData.nodeChecked,
                            "nodes": []
                        }
                        subNodes.push(subCat);
                    })
                });

            nodeData.nodes = subNodes;
        }
    }

    $scope.products = [];
    $scope.productsInitial = $scope.products;
    $scope.productsBkp = [];
    $scope.getProducts = function (nodeData) {
        $scope.productsBkp = $scope.products;
        var varproducts = [];
        var prodArray = [];
        if (nodeData.length > 0) {

            catalogService.GetAllProductsByCategories($scope.compId, nodeData.join(','))
                .then(function (response) {

                    prodArray = response;
                    //  $scope.productsBkp = prodArray;

                    prodArray.forEach(function (item, index) {
                        var product = {
                            "compId": item.compId,
                            "prodId": item.prodId,
                            "prodName": item.prodName,
                            "prodNo": item.prodNo,
                            "nodeChecked": false
                        };

                        // #region $filter 20-30 sec approx for 9k record
                        //var obj = $filter('filter')($scope.productsBkp, { prodId: item.prodId }, true)[0];
                        //if (obj) {
                        //    product.nodeChecked = obj.nodeChecked;
                        //}
                        // #endregion $filter 20-30 sec approx for 9k record

                        // #region _.filter 8-10 sec approx for 9k record
                        //var obj = _.filter($scope.productsBkp, function (pro) {
                        //    return pro.prodId == item.prodId && pro.nodeChecked == true;
                        //});
                        //if (obj && obj[0]) {
                        //    $log.info(obj);
                        //    product.nodeChecked = obj[0].nodeChecked;
                        //}
                        // #endregion _.filter 8-10 sec approx for 9k record

                        // #region forEach 3-5 sec approx for 9k record
                        $scope.productsBkp.forEach(function (pro, proIndex) {
                            if (pro && pro.prodId == item.prodId && pro.nodeChecked == true) {
                                product.nodeChecked = true;
                            }
                        });
                        // #endregion forEach 3-5 sec approx for 9k record

                        varproducts.push(product);
                    });

                    $scope.products = varproducts;
                    $scope.productsInitial = $scope.products;
                    //$scope.loopProducts(prodArray, $scope.productsLimit + 100);
                });
        }

    };

    $scope.selectedCategories = [];
    
    $scope.selectChildNodes = function (childNode, ischecked) {
        childNode.nodeChecked = ischecked;
        childNode.nodes.forEach(function (item, index) {
            $scope.selectChildNodes(item, ischecked);
        });
    };

    $scope.getSelectedNodes = function () {
        //$scope.selectedCategories = [];

        $scope.parentNodes.forEach(function (item, index) {
            $scope.getSelectedNodesIteration(item);
        });
    };

    $scope.getSelectedNodesIteration = function (selectedNode) {
        if (selectedNode.nodeChecked) {
            $scope.selectedCategories.push(selectedNode.catId);
        }
        if (selectedNode.nodes != null && selectedNode.nodes.length > 0) {
            selectedNode.nodes.forEach(function (item, index) {
                if (item.nodeChecked) {
                    $scope.getSelectedNodesIteration(item);
                }

            });
        }
    };

    $scope.selectParentNode = function (ischecked, selectedNodeScope) {
        if (selectedNodeScope) {
            var parentScope = selectedNodeScope.$parent;
            if (parentScope && parentScope.node && parentScope.node.catId > 0) {
                if (ischecked) {
                    parentScope.node.nodeChecked = ischecked;
                }
                else {
                    if (parentScope.node.nodes && parentScope.node.nodes.length > 0) {
                        if ($filter('filter')(parentScope.node.nodes, { 'nodeChecked': true }).length <= 0) {
                            parentScope.node.nodeChecked = ischecked;
                        }
                    }
                    else {
                        parentScope.node.nodeChecked = ischecked;
                    }
                }

                $scope.selectParentNode(ischecked, parentScope);
            }
        }
    };

    $scope.checkChanged = function (selectedNode, selectedNodeScope) {
        $scope.selectChildNodes(selectedNode, selectedNode.nodeChecked);
        $scope.selectParentNode(selectedNode.nodeChecked, selectedNodeScope);
        //$scope.getSelectedNodes();
        
        if (!$scope.selectedCategories || $scope.selectedCategories.length === 0) {
            $scope.selectedCategories = [];
        }

        if (selectedNode.nodeChecked) {
            $scope.selectedCategories.push(selectedNode.catId);
        } else {
            $scope.selectedCategories = _.filter($scope.selectedCategories, (v) => v !== selectedNode.catId);
        }

        if ($scope.selectedCategories.length > 0) {
            $scope.catalogvalidation = false;
        } else {
            $scope.catalogvalidation = true;
        }

        $scope.getProducts($scope.selectedCategories);

        console.log($scope.selectedCategories);
    };

    $scope.productChecked = function (product) {
        if (product.nodeChecked) {
            $scope.selectedProducts.push(product.prodId);
        } else {
            $scope.selectedProducts = _.filter($scope.selectedProducts, (v) => v !== product.prodId);
        }

        console.log($scope.selectedProducts);
    };


    $scope.toggle = function (scope) {
        scope.toggle();``
    };

    $scope.collapseAll = function () {
        $scope.$broadcast('angular-ui-tree:collapse-all');
    };

    $scope.expandAll = function () {
        $scope.$broadcast('angular-ui-tree:expand-all');
    };

    //$scope.getcatalogcategories();

    $scope.SaveVendorCatalog = function (p_vendor, type, global) {

        $scope.selectedCategories = _.uniq($scope.selectedCategories);
        $scope.selectedProducts = _.uniq($scope.selectedProducts);
        console.log($scope.selectedCategories);
        console.log($scope.selectedProducts);
        var param = {
            vendorId: p_vendor,
            compId: $scope.compId,
            catIds: $scope.selectedCategories.join(','),
            prodIds: $scope.selectedProducts.join(','),
            user: userService.getUserId(),
            sessionId: userService.getUserToken()
        };

        catalogService.saveVendorCatalog(param)
            .then(function (response) {
                if (response.errorMessage != '') {
                    growlService.growl(response.errorMessage, "inverse");
                }
                else {
                    if (type == 'REQUIREMENT') {
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        $scope.addVendorShow = false;
                        $scope.getproductvendors(type, p_vendor);
                        growlService.growl("Vendor Added Successfully.", 'success');
                        this.addVendor = 0;
                    } else {
                        $scope.newVendor = null;
                        $scope.newVendor = {};
                        $scope.addVendorShow = false;
                        growlService.growl("Vendor Added Successfully.", 'success');
                        $state.reload();
                        this.addVendor = 0;
                    }

                }
            });

    };






    function VendorStatusModelCtrl($uibModalInstance, data, userService, growlService, auctionsService, $scope) {
        var _modalCtrl = this;
        _modalCtrl.Vendor = data.vendor
        _modalCtrl.StatusList = data.StatusList
        _modalCtrl.close = function () {
            $uibModalInstance.close();
        };
        _modalCtrl.Model = {
            Status: _modalCtrl.Vendor.regStatus.toString(),
            Reason: _modalCtrl.Vendor.regReason,
            Comment: _modalCtrl.Vendor.regComment,
            User_Id: _modalCtrl.Vendor.userID,
            SubCategories: []
        }
        _modalCtrl.sub = {};
        _modalCtrl.selectedSubCategoriesList = [];
        _modalCtrl.subcategories = [];

        _modalCtrl.getCategories = function () {
            auctionsService.getCategories(userService.getUserId())
                .then(function (response) {
                    _modalCtrl.categories = response;
                    _modalCtrl.addVendorCats = _.uniq(_.map(response, 'category'));
                    _modalCtrl.showCategoryDropdown = true;
                    _modalCtrl.Model.Category = _modalCtrl.Vendor.category;

                    _modalCtrl.getVendorSubCategories();

                })
        };

        _modalCtrl.getVendorSubCategories = function () {
            userService.getVendorSubCategories(_modalCtrl.Vendor.userID).then(function (resposne) {
                $timeout(function () {
                    _modalCtrl.Model.SubCategories = angular.copy(_modalCtrl.categories.filter(function (item) {
                        if (resposne.indexOf(item.id) !== -1) {
                            item.ticked = true;
                            return true;
                        }
                        else {
                            item.ticked = false;
                            return false;
                        }

                    }));
                }, 0);

            })
        }

        $scope.$watch("_modalCtrl.Model.Category", function (newVal) {

            if (newVal != undefined && newVal != null && newVal != "") {
                $timeout(function () {
                    _modalCtrl.loadSubCat(newVal)

                }, 0)
            }
            else {
                _modalCtrl.subcategories = [];
                _modalCtrl.Model.SubCategories = [];
            }

        })

        _modalCtrl.loadSubCat = function (cat) {
            _modalCtrl.subcategories = _.filter(_modalCtrl.categories, { category: cat });
        }

        _modalCtrl.selectSubcat = function (data) {

        };

        _modalCtrl.ReasonList = ["Doesn't meet our requirments", "Fake vendor"]

        _modalCtrl.updateStatus = function (form) {
            form.$submitted = true;
            if (form.$valid) {
                var model = angular.copy(_modalCtrl.Model)
                model.SubCategories = _.map(model.SubCategories, 'id');
                userService.saveVendorStatus(model).then(function (response) {
                    if (response.errorMessage === "") {
                        $uibModalInstance.close(_modalCtrl.Vendor);

                        if (model.Status == 1) {
                            $scope.activatecompanyvendor(model.User_Id, 1);
                        }
                        else if (model.Status == 2) {
                            $scope.activatecompanyvendor(model.User_Id, 2);
                        }
                        else if (model.Status == 3) {
                            $scope.activatecompanyvendor(model.User_Id, 3);
                        }
                        else if (model.Status == 4) {
                            $scope.activatecompanyvendor(model.User_Id, 4);
                        }
                        else if (model.Status == 5) {
                            $scope.activatecompanyvendor(model.User_Id, 5);
                        }

                        //growlService.growl(response.message, "inverse");
                    }
                    else
                        growlService.growl(response.errorMessage, "inverse");
                })
            }
        }
        ////222
        $scope.activatecompanyvendor = function (vendorID, isValid) {
            userService.activatecompanyvendor({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": isValid })
                .then(function (response) {
                    if (response.errorMessage != "") {
                        growlService.growl(response.errorMessage, "inverse");
                    }
                    else if (response.errorMessage == "") {
                        //$scope.GetCompanyVendors();
                        if (isValid == 1) {
                            growlService.growl("Vendor Activated Successfully", "success");
                        }
                        else if (isValid == 2) {
                            growlService.growl("Vendor Declined Successfully", "inverse");
                        }
                        else if (isValid == 3) {
                            growlService.growl("Vendor is on Hold", "inverse");
                        }
                        else if (isValid == 4) {
                            growlService.growl("Vendor is on Pending", "inverse");
                        }
                        else if (isValid == 5) {
                            growlService.growl("Vendor is Inactive", "inverse");
                        }
                    }
                });
        };

    };

    $scope.searchProducts = function (str) {
        str = String(str).toUpperCase();
        $scope.products = $scope.productsInitial.filter(function (product) {
            return (String(product.prodName).toUpperCase().includes(str) == true || String(product.prodNo).toUpperCase().includes(str) == true);
        });
    };

    $scope.searchCategory = function (str) {
        if (str && str.trim()) {
            //$scope.isparentNodesLoading = true;
            //str = String(str).toUpperCase();
            //var selectedNodes = $scope.parentNodes.filter(function (category) {
            //    return (String(category.catName).toUpperCase().includes(str) === true || String(category.catCode).toUpperCase().includes(str) === true);
            //});

            //$scope.parentNodes.forEach(function (tempNode, index) {
            //    tempNode.isFiltered = false;
            //});

            //if (selectedNodes && selectedNodes.length > 0) {
            //    selectedNodes.forEach(function (tempNode, index) {
            //        tempNode.isFiltered = true;
            //    });
            //}

            $scope.searchingVendorCategories = angular.lowercase(str);
            $scope.getcatalogcategories($scope.vendorID, '', 0, $scope.searchingVendorCategories);

            var selectedNodes = $scope.parentNodes.forEach(function (tempNode, index) {
                tempNode.isFiltered = false;
            });

            if (selectedNodes && selectedNodes.length > 0) {
                selectedNodes.forEach(function (tempNode, index) {
                    tempNode.isFiltered = true;
                });
            }
            //$scope.isparentNodesLoading = false;
        } else {
            $scope.isparentNodesLoading = true;
            $scope.searchingVendorCategories = '';
            $scope.getcatalogcategories($scope.vendorID, '', 0, $scope.searchingVendorCategories);
            if ($scope.parentNodes && $scope.parentNodes.length > 0) {
                $scope.parentNodes.forEach(function (tempNode, index) {
                    tempNode.isFiltered = true;
                });
            }
           // $scope.isparentNodesLoading = false;
        }
    };

    $scope.selectAllProducts = function () {
        $scope.selectAllProductsValue = !$scope.selectAllProductsValue;
        if ($scope.selectAllProductsValue) {
            $scope.products.forEach(function (product, index) {
                product.nodeChecked = true;
            });
        } else {
            $scope.products.forEach(function (product, index) {
                product.nodeChecked = false;
            });
        }
    };


    $scope.limit1 = 100;
    $scope.productsLimit = 100;


    $scope.newToggle = function (val) {        
        $scope.limit1 = $scope.limit1 + 10;
    };


    $scope.loopProducts = function (prodArray, productsLimit) {
        var varproducts = [];
        $log.info('-->Before--forEach-->');
        prodArray.forEach(function (item, index) {
            if (index > productsLimit) {
                var product = {
                    "compId": item.compId,
                    "prodId": item.prodId,
                    "prodName": item.prodName,
                    "nodeChecked": false
                }
                //var obj = $filter('filter')($scope.productsBkp, { prodId: item.prodId }, true)[0];
                //if (obj) {
                //    product.nodeChecked = obj.nodeChecked;
                //}
                $scope.products.push(product);
                $scope.productsInitial = $scope.products;
            }
        })
        $log.info('-->After--forEach-->');
    };

    //document.body.addEventListener("keydown", function (event) {
    //    console.log(event.key);
    //    console.log(event.code);
    //    console.log(event.keyCode);
    //});


    $scope.showEmailValid = false;
    $scope.showEmailValidMessage = '';
    $scope.EmailValidate1 = function () {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var result = re.test($scope.newVendor.email);
        if (!result) {
            $scope.showEmailValid = true;
            $scope.showEmailValidMessage = "Please enter a valid Email Address";
            return false;
        } else {
            $scope.showEmailValid = false;
            $scope.showEmailValidMessage = '';
        }
    }


    /*region start WORKFLOW*/

    $scope.getWorkflows = function () {
        workflowService.getWorkflowList()
            .then(function (response) {
                $scope.workflowList = [];
                $scope.unblockWorkflowList = [];
                $scope.workflowListTemp = response;
                $scope.workflowListTemp.forEach(function (item, index) {
                    if (item.WorkflowModule == $scope.WorkflowModule) {
                        $scope.workflowList.push(item);
                    } else if (item.WorkflowModule == $scope.UnblockWorkflowModule) {
                        $scope.unblockWorkflowList.push(item);
                        $scope.unblockWorkflowList.forEach(function (item11, index11) {
                           // alert(item11);
                        })
                    }
                });

                if (userService.getUserObj().isSuperUser) {
                    $scope.workflowList = $scope.workflowList;
                    $scope.unblockWorkflowList = $scope.unblockWorkflowList;
                }
                else {
                    $scope.workflowList = $scope.workflowList.filter(function (item) {
                        return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                    });

                    $scope.unblockWorkflowList = $scope.unblockWorkflowList.filter(function (item) {
                        return item.deptID == userService.getSelectedUserDepartmentDesignation().deptID;

                    });
                 }



            });
    };

    $scope.unblockVendorId = 0;
    $scope.unblockErrorMessage = '';

    $scope.displayUnBlockVendorPopup = function (vendorId)
    {
        $scope.unblockVendorId = vendorId;
        $scope.unblockErrorMessage = '';
        $scope.disableUnblockSave = false;
        $scope.unblockExistingVendors = [];
        $scope.unblockExistingVendors.push(vendorId);
    }

    $scope.showBlocekdVendors = function () {
        $scope.disableUnblockedVendors = [];
        $scope.disableUnblockSave = false;
        $scope.showUnBlockedErrorMessage = '';
        $scope.message1 = '';
        var vendorsString1 = '';
        vendorsString1 = $scope.unblockExistingVendors.join(',');

        var params =
        {
            userID: userService.getUserId(),
            vendorIDs: vendorsString1,
            sessionID: userService.getUserToken()
        }

        auctionsService.GetUnBlockedVendors(params)
            .then(function (response) {
                //$scope.auctionItemTemporary.emailLinkVendors
                $scope.disableUnblockedVendors = response;

                $scope.disableUnblockedVendors.forEach(function (item, index) {
                    if (item.vendorID == $scope.unblockExistingVendors[0]) {
                        $scope.message1 = 'Vendor has already UnBlocked by';
                        $scope.showUnBlockedErrorMessage = item.errorMessage;
                        $scope.disableUnblockSave = true;
                    }
                })
            });

    }



    $scope.getWorkflows();

    $scope.getItemWorkflow = function () {
        workflowService.getItemWorkflow(0, $scope.vendorID, $scope.WorkflowModule)
            .then(function (response) {
                $scope.itemWorkflow = response;
                if ($scope.itemWorkflow && $scope.itemWorkflow.length > 0 && $scope.itemWorkflow[0].WorkflowTracks.length > 0) {
                    $scope.currentStep = 0;

                    var count = 0;

                    $scope.itemWorkflow[0].WorkflowTracks.forEach(function (track) {

                        if (track.status == 'APPROVED' || track.status == 'HOLD') {
                            $scope.isFormdisabled = true;
                        }

                        if (track.status == 'APPROVED') {
                            $scope.isWorkflowCompleted = true;
                            $scope.orderInfo = track.order;
                            $scope.assignToShow = track.status;

                        }
                        else {
                            $scope.isWorkflowCompleted = false;
                        }



                        if (track.status == 'REJECTED' && count == 0) {
                            count = count + 1;
                        }

                        if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                            count = count + 1;
                            $scope.IsUserApproverForStage(track.approverID);
                            $scope.currentAccess = track.order;
                        }

                        if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                            $scope.currentStep = track.order;
                            return false;
                        }
                    });
                }
            });

    };


    $scope.getUnBlockItemWorkflow = function () {
        workflowService.getItemWorkflow(0, $scope.vendorID, $scope.UnblockWorkflowModule)
            .then(function (response) {
                $scope.itemunBlockWorkflow = response;
                if ($scope.itemunBlockWorkflow && $scope.itemunBlockWorkflow.length > 0 && $scope.itemunBlockWorkflow[0].WorkflowTracks.length > 0) {
                    $scope.currentStep = 0;

                    var count = 0;

                    $scope.itemunBlockWorkflow[0].WorkflowTracks.forEach(function (track) {

                        if (track.status == 'APPROVED' || track.status == 'HOLD') {
                            $scope.isFormdisabled = true;
                        }

                        if (track.status == 'APPROVED') {
                            $scope.isWorkflowCompleted = true;
                            $scope.orderInfo = track.order;
                            $scope.assignToShow = track.status;

                        }
                        else {
                            $scope.isWorkflowCompleted = false;
                        }



                        if (track.status == 'REJECTED' && count == 0) {
                            count = count + 1;
                        }

                        if ((track.status == 'PENDING' || track.status == 'HOLD') && count == 0) {
                            count = count + 1;
                            $scope.IsUserApproverForStage(track.approverID);
                            $scope.currentAccess = track.order;
                        }

                        if ((track.status == 'PENDING' || track.status == 'HOLD' || track.status == 'REJECTED') && $scope.currentStep == 0) {
                            $scope.currentStep = track.order;
                            return false;
                        }
                    });
                }
            });

    };



    $scope.updateTrack = function (step, status) {
        //$scope.disableAssignPR = true;
        $scope.commentsError = '';

        var tempArray = $scope.itemWorkflow[0].WorkflowTracks[$scope.itemWorkflow[0].WorkflowTracks.length - 1];
        if (step.order == tempArray.order && status == 'APPROVED') {
            $scope.ApproveVendor(step.moduleID);
        } else {
            //$scope.disableAssignPR = true;
        }

        if ($scope.isReject) {
            $scope.commentsError = 'Please Save Rejected Items/Qty';
            return false;
        }

        if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
            $scope.commentsError = 'Please enter comments';
            return false;
        }

        step.status = status;
        step.sessionID = userService.getUserToken();
        step.modifiedBy = userService.getUserId();

        step.moduleName = $scope.WorkflowModule;

        workflowService.SaveWorkflowTrack(step)
            .then(function (response) {
                if (response.errorMessage != '') {
                    growlService.growl(response.errorMessage, "inverse");
                }
                else {
                    $scope.getItemWorkflow();
                    //location.reload();
                    //  $state.go('list-pr');
                }
            })
    };



    $scope.updateUnblockTrack = function (step, status) {
        //$scope.disableAssignPR = true;
        $scope.commentsError = '';

        var tempArray = $scope.itemunBlockWorkflow[0].WorkflowTracks[$scope.itemunBlockWorkflow[0].WorkflowTracks.length - 1];
        if (step.order == tempArray.order && status == 'APPROVED') {
          //  $scope.ApproveVendor(step.moduleID);
        } else {
            //$scope.disableAssignPR = true;
        }

        if ($scope.isReject) {
            $scope.commentsError = 'Please Save Rejected Items/Qty';
            return false;
        }

        if (status == 'REJECTED' && (step.comments == null || step.comments == "")) {
            $scope.commentsError = 'Please enter comments';
            return false;
        }

        step.status = status;
        step.sessionID = userService.getUserToken();
        step.modifiedBy = userService.getUserId();

        step.moduleName = $scope.UnblockWorkflowModule;

        workflowService.SaveWorkflowTrack(step)
            .then(function (response) {
                if (response.errorMessage != '') {
                    growlService.growl(response.errorMessage, "inverse");
                }
                else {
                    $scope.getUnBlockItemWorkflow();
                    //location.reload();
                    //  $state.go('list-pr');
                }
            })
    };

    $scope.assignWorkflow = function (moduleID) {
        workflowService.assignWorkflow(({ wID: $scope.workflowObj.workflowID, moduleID: moduleID, user: userService.getUserId(), sessionID: $scope.sessionID }))
            .then(function (response) {
                if (response.errorMessage != '') {
                    growlService.growl(response.errorMessage, "inverse");
                    $scope.isSaveDisable = false;
                }
                else {
                    //  $state.go('list-pr');
                }
            })
    };

    $scope.IsUserApprover = false;

    $scope.functionResponse = false;

    $scope.IsUserApproverForStage = function (approverID) {
        workflowService.IsUserApproverForStage(approverID, userService.getUserId())
            .then(function (response) {
                $scope.IsUserApprover = response;
            });
    };

    $scope.isApproverDisable = function (index) {

        var disable = true;

        var previousStep = {};

        $scope.itemWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

            if (index == stepIndex) {
                if (stepIndex == 0) {
                    if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                        (step.status == 'PENDING' || step.status == 'HOLD')) {
                        disable = false;
                    }
                    else {
                        disable = true;
                    }
                }
                else if (stepIndex > 0) {
                    if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                        disable = true;
                    }
                    else if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                        (step.status == 'PENDING' || step.status == 'HOLD')) {
                        disable = false;
                    }
                    else {
                        disable = true;
                    }
                }
            }
            previousStep = step;
        })

        return disable;
    };



    $scope.isUnblockApproverDisable = function (index) {

        var disable = true;

        var previousStep = {};

        $scope.itemunBlockWorkflow[0].WorkflowTracks.forEach(function (step, stepIndex) {

            if (index == stepIndex) {
                if (stepIndex == 0) {
                    if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                        (step.status == 'PENDING' || step.status == 'HOLD')) {
                        disable = false;
                    }
                    else {
                        disable = true;
                    }
                }
                else if (stepIndex > 0) {
                    if (previousStep.status == 'PENDING' || previousStep.status == 'HOLD' || previousStep.status == 'REJECTED') {
                        disable = true;
                    }
                    else if (userService.getSelectedUserDeptID() == step.department.deptID && userService.getSelectedUserDesigID() == step.approver.desigID &&
                        (step.status == 'PENDING' || step.status == 'HOLD')) {
                        disable = false;
                    }
                    else {
                        disable = true;
                    }
                }
            }
            previousStep = step;
        })

        return disable;
    };

    $scope.ApproveVendor = function (vendorID) {

        userService.approveVendorThroughWorkflow({ "customerID": userService.getUserId(), "vendorID": vendorID, "isValid": 1 })
            .then(function (response) {
                if (response.errorMessage != "") {
                    growlService.growl(response.errorMessage, "inverse");
                }
                else if (response.errorMessage == "") {
                    growlService.growl("Vendor Activated Successfully", "success");
                }
            });

    };
    
    /*region end WORKFLOW*/

    $scope.linkVendorId = 0;

    $scope.getvendorId = function (vendorId) {
        $scope.linkVendorId = 0;
        $scope.linkVendorId = vendorId;
        $scope.existingVendors = [];
        $scope.existingVendors.push(vendorId);
        $scope.errorMessage = '';
        $scope.showErrorMessagesForVendors();
    };

    $scope.errorMessage = '';
    $scope.vendorLink = [];

    $scope.SendEmailToVendors = function (workflowID) {
        $scope.errorMessage = '';
        $scope.vendorLink = [];
        if (workflowID > 0)
        {
            var temporaryObj = {
                vendorID: $scope.linkVendorId,
                WF_ID: workflowID
            };

            $scope.vendorLink.push(temporaryObj);

            var params =
            {
                userID: userService.getUserId(),
                vendorIDs: $scope.vendorLink,
                sessionID: userService.getUserToken()
            };
            
            auctionsService.shareemaillinkstoVendors(params)
                .then(function (response) {
                    if (response.errorMessage == "")
                    {
                        angular.element('#registartionLinkPopup').modal('hide');
                        growlService.growl("Email Has Been Sent to Vendor ", "success");
                        $scope.workflowObj.workflowID = 0;
                    }
                });

        } else {
            $scope.errorMessage = "Please Select Atleast Workflow.";
        }

    };


    $scope.showErrorMessagesForVendors = function () {
        $scope.disableVendors = [];
        $scope.disableSave = false;
        $scope.showErrorMessage = '';
        $scope.message = '';
        var vendorsString = '';
        vendorsString = $scope.existingVendors.join(',');

        var params =
        {
            userID: userService.getUserId(),
            vendorIDs: vendorsString,
            sessionID: userService.getUserToken()
        }

        auctionsService.GetSharedLinkVendors(params)
            .then(function (response) {
                //$scope.auctionItemTemporary.emailLinkVendors
                $scope.disableVendors = response;
                
                $scope.disableVendors.forEach(function (item, index) {
                    if (item.vendorID == $scope.existingVendors[0]) {
                        $scope.message = 'Vendor has already received the registration link by';
                        $scope.showErrorMessage = item.errorMessage;
                        $scope.disableSave = true;
                    }
                })
            });

    }

    //$scope.showUnblock = false;

    $scope.displayVendorPopup = function (vendorID) {
        $scope.changeButton = false;
        swal({
            title: "Are you sure?",
            text: "Do you really want to block the vendor?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    var isValid = 1;
                    var params =
                    {
                        customerID: userService.getUserId(),
                        vendorID: vendorID,
                        isValid: isValid,
                        type: 'BLOCK',
                        unblockWfID: 0,
                        sessionID: userService.getUserToken()
                    }
                    userService.blockUnBlockVendor(params)
                        .then(function (response) {

                            if (response.errorMessage) {

                                //$scope.vendorDisplayCollecton.forEach(function (item, index) {
                                //    if (item.userID == response.objectID) {
                                //        $scope.showUnblock = true;
                                //    } 
                                //    return;
                                //})
                                location.reload();
                                growlService.growl(response.errorMessage, "success");
                            } else {
                                growlService.growl(response.errorMessage, "inverse");
                            }
                        });

                } else {
                    // $scope.showUnblock = false;
                }

            });
    };



    $scope.unBlockVendor = function (workflowID) {
        $scope.unblockErrorMessage = '';
        $scope.vendorLink = [];
        if (workflowID > 0) {

            var params =
            {
                customerID: userService.getUserId(),
                vendorID: $scope.unblockVendorId,
                isValid: 0,
                type: 'UN_BLOCK',
                unblockWfID: workflowID,
                sessionID: userService.getUserToken()
            }

            userService.blockUnBlockVendor(params)
                .then(function (response) {

                    //if (response.errorMessage) {
                    //    location.reload();
                    //    growlService.growl(response.errorMessage, "success");
                    //} else {
                    //    growlService.growl(response.errorMessage, "inverse");
                    //}
                    


                });
        } else {
            $scope.unblockErrorMessage = "Please Select Atleast One Workflow.";
        }

    };

    $scope.GetCompanyGSTInfo = function () {
        $scope.companyGSTInfo = [];
        let params = {
            companyId: $scope.newVendor.companyId, //userService.getUserCompanyId()
            sessionid: userService.getUserToken() //userService.getUserCompanyId()
        };
        auctionsService.getCompanyGSTInfo(params)
            .then(function (response) {
                $scope.companyGSTInfo = response;
            });
    };

    $scope.isDuplicateGST = function (gstNumber, vendorCode) {
        let isDuplicate = false;
        vendorCode = vendorCode ? vendorCode : '';
        gstNumber = gstNumber ? gstNumber : '';
        if (gstNumber || vendorCode) {
            var filteredRecords = $scope.companyGSTInfo.filter(function (gstInfo) {
                return gstInfo.gstNumber.toUpperCase() === gstNumber.toUpperCase() && (gstInfo.vendorCode || '').toUpperCase() === vendorCode.toUpperCase();
            });

            if (filteredRecords && filteredRecords.length > 0) {
                isDuplicate = true;
            }
        }

        return isDuplicate;
    };

    $scope.saveCompanyGSTInfo = function () {
        if ($scope.newVendor && $scope.newVendor.companyId) {
            $scope.newGSTInfo.companyId = $scope.newVendor.companyId;
            let params = {
                companygst: $scope.newGSTInfo,
                "user": userService.getUserId(), "sessionid": userService.getUserToken()
            };

            auctionsService.saveCompanyGSTInfo(params)
                .then(function (response) {
                    if (response.errorMessage === "") {
                        growlService.growl("Data saved successfully.", "success");
                        $scope.GetCompanyGSTInfo();
                        $scope.openGSTInfoPopup();
                    } else {
                        swal({
                            title: "Error saving data",
                            text: response.errorMessage,
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Ok",
                            closeOnConfirm: true
                        });
                    }
                });
        }

    };

    $scope.openGSTInfoPopup = function () {
        $scope.newGSTInfo = {
            companyGSTId: 0,
            gstNumber: '',
            companyId: 0,
            gstAddr: '',
            vendorCode: ''
        };
        angular.element('#newGSTInfo').modal('hide');
    };

    $scope.deleteGSTInfo = function (gstInfo) {
        let params = {
            companygst: gstInfo, "sessionid": userService.getUserToken()
        };
        auctionsService.deleteGSTInfo(params)
            .then(function (response) {
                if (response.errorMessage === "") {
                    growlService.growl("Deleted successfully.", "success");
                    $scope.GetCompanyGSTInfo();
                    scope.openGSTInfoPopup();
                } else {
                    swal({
                        title: "Error Deleting data",
                        text: response.errorMessage,
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    });
                }
            });
    };

    $scope.preferredLogin = '';

    $scope.GetPreferredLogin = function () {

        auctionsService.GetPreferredLogin()
            .then(function (response) {
                $scope.preferredLogin = response.errorMessage;
            });
    };

    $scope.GetPreferredLogin();

    $scope.validateUserLogin = function (saveForm) {
        $scope.multipleLoginDetails = [];
        if ($scope.newVendor.email) {
            if ($scope.isEditVendor) {
                $scope.saveVendor(saveForm);
            } else {
                userService.validateUserLogin({ 'email': $scope.newVendor.email }).then(function (response) {
                    if (response && response.length > 0) {
                        $scope.multipleLoginDetails = response;
                        swal({
                            title: "Attach this Id to existing Company Id? If not use new email as it is already exists.",
                            text: 'Company Names:' + _.map(response, 'companyName').join(),
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#F44336",
                            confirmButtonText: "OK",
                            closeOnConfirm: true
                        }, function () {
                            $scope.saveVendor(saveForm);
                        });
                    } else {
                        $scope.saveVendor(saveForm);
                    }
                });
            }
            
        }
    };   
});