﻿prmApp

    .controller('dashboardCtrl', function ($scope, $http, $state, domain, $filter, $window, $log, $stateParams, $timeout, auctionsService, workflowService, fwdauctionsService, userService, SignalRFactory, fileReader, growlService) {

        $scope.cijList = [];
        $scope.userID = userService.getUserId();
        $scope.sessionID = userService.getUserToken();

        $scope.dashboardList = [];

        $scope.DBGetMyPendingApprovals = function () {
            workflowService.DBGetMyPendingApprovals($scope.userID)
                .then(function (response) {
                    $scope.dashboardList = response;

                    $scope.totalItems = $scope.dashboardList.length;

                    console.log($scope.dashboardList);

                })
        }

        $scope.DBGetMyPendingApprovals();

        $scope.totalItems = 0;
        $scope.totalLeads = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 5;
        $scope.maxSize = 5;

        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
        };



    });