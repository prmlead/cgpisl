﻿prmApp
    .controller('workflowPendingApprovalsCtrl', function ($scope, $state, $stateParams, userService, growlService,
        workflowService,
        auctionsService, fileReader, $log, $window) {
        
        $scope.MyPendingApprovals = [];

        $scope.MyPendingApprovals1 = [];

        $scope.MyPendingApprovals2 = [];

        $scope.MyPendingApprovalsSearch = [];

        /*PAGINATION CODE*/
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.maxSize = 5; //Number of pager buttons to show
        $scope.deptIDs = [];
        $scope.desigIDs = [];
        $scope.deptIDStr = '';
        $scope.desigIDStr = '';


        $scope.modules = [
            {
                display: 'QCS',
                value: 'QCS'
            }
            //{
            //    display: 'VENDOR',
            //    value: 'VENDOR'
            //}
        ];
        
        $scope.ListUserDepartmentDesignations = userService.getListUserDepartmentDesignations();
        if ($scope.ListUserDepartmentDesignations && $scope.ListUserDepartmentDesignations.length > 0) {
            $scope.ListUserDepartmentDesignations.forEach(function (item, index) {
                $scope.deptIDs.push(item.deptID);
                item.listDesignation.forEach(function (item1, index1) {
                    if (item1.isAssignedToUser && item1.isValid) {
                        $scope.desigIDs.push(item1.desigID);
                        $scope.desigIDs = _.union($scope.desigIDs, [item1.desigID]);
                    }
                });
            });
            $scope.deptIDStr = $scope.deptIDs.join(',');
            $scope.desigIDStr = $scope.desigIDs.join(',');
        }
        
        $scope.setPage = function (pageNo) {
            $scope.currentPage = pageNo;
        };

        $scope.pageChanged = function () {
            ////console.log('Page changed to: ' + $scope.currentPage);
        };
        /*PAGINATION CODE*/

        $scope.GetMyPendingApprovals = function () {
            workflowService.GetMyPendingApprovals($scope.deptIDStr, $scope.desigIDStr)
                .then(function (response) {
                    $scope.MyPendingApprovals = response;

                    $scope.MyPendingApprovals1 = response;
                    $scope.MyPendingApprovals2 = response;
                    $scope.MyPendingApprovalsSearch = response;
                    /*PAGINATION CODE START*/
                    $scope.totalItems = $scope.MyPendingApprovals.length;
                    /*PAGINATION CODE END*/
                });
        }

        $scope.GetMyPendingApprovals();

        $scope.gotoWFApproval = function (module, moduleID, workflowID,wf) {
            if (module == 'PR')
            {
                var url = $state.href('save-pr-details', { "Id": moduleID });
                $window.open(url, '_blank');
                //var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                //$window.open(url, '_blank');
            }
            else if (module == 'QUOTATION')
            {
                var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'PO') {
                var url = $state.href('workflow-status', { "moduleID": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'JMS') {
                var url = $state.href('save-jms-details', { "Id": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'CL') {
                var url = $state.href('checklistForMMS', { "Id": moduleID, "module": module, "workflowID": workflowID });
                $window.open(url, '_blank');
            }
            else if (module == 'QCS') {
                if (wf.qcsWorkFlow == 'DOMESTIC') {
                    var url1 = $state.href('cost-comparisions-qcs', { "reqID": wf.workflowModuleViewID, "qcsID": moduleID });
                } else {
                    var url1 = $state.href('import-qcs', { "reqID": wf.workflowModuleViewID, "qcsID": moduleID });
                }
                $window.open(url1, '_self');
            }
            else if (module == 'VENDOR') {
                var url2 = $state.href('pages.profile.new-vendor');
                $window.open(url2, '_self');
            }
            else if (module == 'UNBLOCK_VENDOR') {
                var url3 = $state.href('pages.profile.new-vendor');
                $window.open(url3, '_blank');
            }
            else if (module == 'VENDOR_INVOICE') {
                var url4 = $state.href('uploadvendorinvoice', { "ID": wf.workflowModuleViewID });
                $window.open(url4, '_blank');
            }
            
        };

        //$scope.showPopUpClick = function (display, moduleID, module, workflowID) {
        //    $state.go('workflow-status', { moduleID: moduleID, module: module, workflowID: workflowID });
        //};



        $scope.goToModuleView = function (module, moduleViewID) {

            //$stateParams.moduleID, 
            if (module == 'PR') {
                var url = $state.href("save-pr-details", { "Id": moduleViewID });
                window.open(url, '_blank');
            }
            if (module == 'QUOTATION' || module == 'PO') {
                var url = $state.href("view-requirement", { "Id": moduleViewID });
                window.open(url, '_blank');
            }
        };


        $scope.setFilters = function (status) {
            if (status) {
                $scope.MyPendingApprovals = $scope.MyPendingApprovals1.filter(function (pending) {
                    return pending.workflowModule === status;
                });
            } else {
                $scope.MyPendingApprovals = $scope.MyPendingApprovals2;
            }
            
            $scope.totalItems = $scope.MyPendingApprovals.length;
            
        };

        $scope.searchTable = function () {

            if ($scope.searchKeyword) {
                $scope.MyPendingApprovals = _.filter($scope.MyPendingApprovalsSearch, function (item) {
                  
                    return (String(item.reqNumber).includes($scope.searchKeyword)) || String(item.creatorName).toUpperCase().includes($scope.searchKeyword.toUpperCase()) == true;

                   
                });
            } else {
                $scope.MyPendingApprovals = $scope.MyPendingApprovalsSearch;
            }
            $scope.totalItems = $scope.MyPendingApprovals.length;


        };

    });