﻿//(function () { 
prmApp.controller('productAnalysisCtrl', ['$scope', '$state', '$window', '$stateParams', '$filter', 'catalogService', 'userService', 'growlService',
    "catalogReportsServices",
    function ($scope, $state, $window, $stateParams, $filter, catalogService, userService, growlService,
        catalogReportsServices) {

        $scope.stateParamsCatalogId = $stateParams.productId;

        $scope.CovertedDate = '';
        $scope.Name = 'No previous bids';

        $scope.show = false;
        $scope.data = [];
        $scope.categories = [];

        $scope.productData = [{ bidAmount: 265200, createdOn: "28-01-2019 15:57", createdTime: "28-01-2019 15:57", firstName: "Product", lastName: "137", companyName: "Demo vendor 3" },
        { bidAmount: 265200, createdOn: "05-12-2018 19:07", createdTime: "05-12-2018 19:07", firstName: "Product", lastName: "137", companyName: "Demo vendor 1" },
        { bidAmount: 221200, createdOn: "14-11-2018 11:30", createdTime: "14-11-2018 11:30", firstName: "Product", lastName: "137", companyName: "Demo vendor 5" },
        { bidAmount: 221200, createdOn: "16-10-2018 18:37", createdTime: "16-10-2018 18:37", firstName: "Product", lastName: "137", companyName: "Demo vendor 4" },
        { bidAmount: 221200, createdOn: "23-09-2018 19:02", createdTime: "23-09-2018 19:02", firstName: "Product", lastName: "137", companyName: "Demo vendor 2" }];



        $scope.GetDateconverted = function (dateBefore) {

            var date = dateBefore.split('+')[0].split('(')[1];
            var newDate = new Date(parseInt(parseInt(date)));
            $scope.CovertedDate = newDate.toString().replace('Z', '');
            return $scope.CovertedDate;

        };

        //#region Original
        $scope.ProductVsOrgOccupationChart = function (onlyProductInOrg, allOtherProductsInOrg) {
            Highcharts.chart('ProductVsOrgOccupationChart', {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Procurement',
                    colorByPoint: true,
                    data: [{
                        name: 'Product',
                        y: onlyProductInOrg,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Other Products in Org',
                        y: allOtherProductsInOrg
                    }]
                }]
            });
        };

        $scope.ProductVsCatOccupationChart = function (onlyProductInCat, allOtherProductsInCat) {
            //Highcharts.chart('ProductVsCatOccupationChart', {
            //    chart: {
            //        plotBackgroundColor: null,
            //        plotBorderWidth: null,
            //        plotShadow: false,
            //        type: 'pie'
            //    },
            //    title: {
            //        text: ''
            //    },
            //    tooltip: {
            //        pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
            //    },
            //    plotOptions: {
            //        pie: {
            //            allowPointSelect: true,
            //            cursor: 'pointer',
            //            dataLabels: {
            //                enabled: false,            
            //                },
            //                showInLegend: true
            //            }
            //        }
            //    },
            //    series: [{
            //        name: 'Procurement',
            //        colorByPoint: true,
            //        data: [{
            //            name: 'P',
            //            y: notProductWithCat,
            //            sliced: true,
            //            selected: true
            //        },
            //        {
            //            name: 'O.P',
            //            y: productWithCat
            //        }
            //        ]
            //    }]
            //});

            Highcharts.chart('ProductVsCatOccupationChart', {
                credits: {
                    enabled: false
                },
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: ''
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Procurement',
                    colorByPoint: true,
                    data: [{
                        name: 'Product',
                        y: onlyProductInCat,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Other Products in Category',
                        y: allOtherProductsInCat
                    }]
                }]
            });
        }

        $scope.GetProductAnalysis = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "call_code": 1,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductAnalysis(params)
                .then(function (response) {
                    $scope.ProductDetails = response;
                    console.log($scope.ProductDetails);

                    let onlyProductInOrg = ($scope.ProductDetails.PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.ALL_PROD_OCCUPATION)) * 100;
                    let allOtherProductsInOrg = ($scope.ProductDetails.ALL_PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.ALL_PROD_OCCUPATION)) * 100;

                    let onlyProductInCat = ($scope.ProductDetails.PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.CAT_PROD_OCCUPATION)) * 100;
                    let allOtherProductsInCat = ($scope.ProductDetails.CAT_PROD_OCCUPATION / ($scope.ProductDetails.PROD_OCCUPATION + $scope.ProductDetails.CAT_PROD_OCCUPATION)) * 100;

                    console.log(onlyProductInCat);
                    console.log(allOtherProductsInCat);

                    $scope.ProductVsOrgOccupationChart(onlyProductInOrg, allOtherProductsInOrg);
                    $scope.ProductVsCatOccupationChart(onlyProductInCat, allOtherProductsInCat);
                });
        };

        $scope.pad = function (d) {
            return (d < 10) ? '0' + d.toString() : d.toString();
        };

        $scope.GetProductAnalysis();

        $scope.GetProductAnalysis2 = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "call_code": 2,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductAnalysis(params)
                .then(function (response) {
                    $scope.ProductDetails2 = response;
                    $scope.ProductDetails2.BEST_PRICE_DATE = new moment($scope.ProductDetails2.BEST_PRICE_DATE).format("DD-MM-YYYY");
                    $scope.ProductDetails2.LAST_PRICE_DATE = new moment($scope.ProductDetails2.LAST_PRICE_DATE).format("DD-MM-YYYY");
                });
        };

        $scope.GetProductAnalysis2();

        $scope.GetVendorsAndOrders = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetVendorsAndOrders(params)
                .then(function (response) {
                    $scope.VendorsAndOrders = response;
                    $scope.VendorsAndOrders.forEach(function (data, dataIndex) {
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);//new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                    });
                });
        };

        $scope.GetVendorsAndOrders();

        $scope.GetMostOrdersAndVendors = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetMostOrdersAndVendors(params)
                .then(function (response) {
                    $scope.MostOrdersAndVendors = response;
                    $scope.MostOrdersAndVendors.forEach(function (data, dataIndex) {
                        data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                    });
                });
        };

        $scope.GetMostOrdersAndVendors();

        $scope.GetProductRequirements = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductRequirements(params)
                .then(function (response) {
                    $scope.ProductRequirements = response;
                    $scope.ProductRequirements.forEach(function (data, dataIndex) {
                        data.POSTED_DATE = userService.toLocalDate(data.POSTED_DATE);//new moment(data.POSTED_DATE).format("DD-MM-YYYY HH:mm");
                    });
                });
        };

        $scope.GetProductRequirements();

        $scope.GetProductPriceHistory = function () {

            var params = {
                "u_id": userService.getUserId(),
                "prod_id": $stateParams.productId,
                "sessionid": userService.getUserToken()
            };

            catalogReportsServices.GetProductPriceHistory(params)
                .then(function (response) {
                    $scope.ProductPriceHistory = response;
                    $scope.ProductPriceHistory.forEach(function (data, dataIndex) {
                        data.POSTED_DATE = new moment(data.POSTED_DATE).format("DD-MM-YY");
                    })
                    $scope.renderChart($scope.ProductPriceHistory);
                })
        };

        $scope.GetProductPriceHistory();
        $scope.name = [];
        $scope.renderChart = function (ProductPriceHistory) {
            if (ProductPriceHistory.length > 0) {
                $scope.Name = ProductPriceHistory[0].V_COMPANY_NAME;
                $scope.startTime = ProductPriceHistory[0].POSTED_DATE;

                ProductPriceHistory.forEach(function (item, index) {
                    $scope.data.push(item.TOTAL_ITEM_PRICE);
                    $scope.name.push(item.V_COMPANY_NAME);
                    $scope.categories.push(item.POSTED_DATE.toString() + ' (' + item.V_COMPANY_NAME.toUpperCase() + ')');

                });


                $scope.endTime = ProductPriceHistory[ProductPriceHistory.length - 1].POSTED_DATE;
            }

            $scope.chartOptions = {
                credits: {
                    enabled: false
                },
                chart: {
                    width: 530,
                    height: 280
                },
                title: {
                    text: 'Product History Graph'
                },
                xAxis: {
                    categories: $scope.categories,
                    title: {
                        text: 'Time ( Start Time: ' + ($scope.startTime ? userService.toLocalDate($scope.startTime).split(' ')[0] : '') + ' - End Time: ' + ($scope.endTime ? userService.toLocalDate($scope.endTime).split(' ')[0] : '') + ')'
                    }
                },
                yAxis: {
                    title: {
                        text: 'Product Price'
                    }
                },
                series: [{
                    name: 'Prices',
                    data: $scope.data
                }]
            };

            $scope.show = true;
        }



        //#endregion Original

        $scope.goToPPSView = function (reqid, catid, ppsid) {
            var url = $state.href("view-pps", { "reqid": reqid, "catid": catid, "ppsid": ppsid });
            window.open(url, '_blank');
        };

        $scope.goToReq = function (reqid) {
            var url = $state.href("view-requirement", { "Id": reqid });
            window.open(url, '_blank');
        };






        $scope.h = {
            h1: {
                h: "Pending RFQ:",
                t1: "Other than mark as completed  and deleted RFQ's count will be displayed here"
            },
            h2: {
                h: "Pending PO:",
                t1: "If mark as completed is done and if PPS is not created RFQ's count will be displayed here"
            },
            h3: {
                h: "Total PO:",
                t1: "If PPS is created then the count will be displayed here"
            },
            h4: {
                h: "Total Vendors: ",
                t1: "No.of vendors added to that product"
            },
            h5: {
                h: "Number of vendors got orders: ",
                t1: "Total PO"
            },
            h6: {
                h: "Most number of PO to vendors: ",
                t1: "Name — Vendors who received most number of PO count"
            },
            h7: {
                h: "List Of RFQ: ",
                t1: "All requirements posted for this product"
            },
            h8: {
                h: "Price Graph: ",
                t1: "If RFQ's are marked as completed then those vendors price is displayed"
            },
            h9: {
                h: "Best Price: ",
                t1: "Least price for this product"
            },
            h10: {
                h: "Last Purchase Price: ",
                t1: "Latest PO price"
            },
            h11: {
                h: "Product Material Occupying in Total Procurement  : ",
                t1: "1.If  Price type  — Sum of L1 revised item prices (Ignore - Price cap, Initial Not approved vendors, Revised rejected vendors , Other charges ,unconfirmed, not started, started,deleted,regret).",
                t2: "2. If split — Sum of L1 revised item prices with required quantity(In L1 only least item price for which slab item price is least that price is shown) (Ignore - Price cap, Initial Not approved vendors, Revised rejected vendors , Other charges)"
            },
            h12: {
                h: "Category Material Occupying in Total Procurement: ",
                t1: "Category Material Occupying in Total Procurement"
            },
        };



    }]);
