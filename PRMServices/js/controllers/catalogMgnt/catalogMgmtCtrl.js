﻿prmApp
    .controller('catalogMgmtCtrl', ['$scope', 'catalogService', 'userService','auctionsService', 'growlService','fileReader',
        function ($scope, catalogService, userService, auctionsService,growlService, fileReader) {



        // Pagination //
        $scope.loaderMore = false;
        $scope.scrollended = false;
        $scope.page = 0;
        var page = 0;
        $scope.PageSize = 200;
        var totalData = 0;
        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
        $scope.totalCount = 0;
        // Pagination //



          $scope.ExcelProducts = [];
          $scope.compId = userService.getUserCompanyId();
            $scope.catalogCompId = userService.getUserCatalogCompanyId();
            $scope.dataTypes = ['text', 'multiline', 'drop', 'multi'];

            $scope.getcategories = function (IsPaging) {
                var catNodes = [];
                // catalogService.getcategories(userService.getUserCompanyId())
                if (IsPaging === 1) {
                    if ($scope.page > -1) {
                        $scope.page++;
                        $scope.fetchRecordsFrom = $scope.page * $scope.PageSize;
                        $scope.loaderMore = true;
                        $scope.LoadMessage = ' Loading page ' + $scope.fetchRecordsFrom + ' of ' + $scope.PageSize + ' data...';
                        $scope.result = "color-green";
                        if ($scope.totalCount != $scope.companyCatalog.length)
                        {
                            catalogService.getcategories($scope.catalogCompId, $scope.fetchRecordsFrom, $scope.PageSize)
                                .then(function (response) {
                                    //$scope.companyCatalog = response;
                                    response.forEach(function (catItem, catIndex) {
                                        catItem.catNameTemp = catItem.catName.toLowerCase();
                                    });
                                    for (var a in response) {
                                        $scope.companyCatalog.push(response[+a]);
                                    }
                                    if ($scope.companyCatalog && $scope.companyCatalog.length > 0) {
                                        $scope.totalCount = $scope.companyCatalog[0].totalCategories;
                                    }

                                    $scope.data = [{
                                        'compId': 0,
                                        'catId': 0,
                                        'catName': 'Category Index',
                                        'catDesc': '',
                                        'nodes': $scope.companyCatalog,
                                        'catParentId': 0


                                    }];
                                });
                        }
                    }
                } else
                {
                    catalogService.getcategories($scope.catalogCompId, $scope.fetchRecordsFrom, $scope.PageSize)
                        .then(function (response) {
                            response.forEach(function (catItem, catIndex) {
                                catItem.catNameTemp = catItem.catName.toLowerCase();
                            });
                            $scope.companyCatalog = response;

                                if ($scope.companyCatalog && $scope.companyCatalog.length > 0) {
                                    $scope.totalCount = $scope.companyCatalog[0].totalCategories;
                                }

                                $scope.data = [{
                                    'compId': 0,
                                    'catId': 0,
                                    'catName': 'Category Index',
                                    'catDesc': '',
                                    'nodes': $scope.companyCatalog,
                                    'catParentId': 0


                                }];
                            });
                }
            };
          
          $scope.getcategories();

          
          $scope.deleteCategory = function (scope) {
              var nodeData = scope.$modelValue;

              //if (nodeData.subCatCount > 0) {
              //    growlService.growl("need to remove all sub categories to remove this category", "inverse");
              //    return;
              //}
              if (nodeData.nodes.length>0) {
                  growlService.growl("need to remove all sub categories to remove this category", "inverse");
                  return;
              }
              else {
                  var catObj = {
                      catId: nodeData.catId,
                      compId: $scope.catalogCompId,
                      catCode: '',
                      isValid: 0,
                  };
                  var param = {
                      reqCategory: catObj,
                      sessionID: userService.getUserToken()
                  };
                  catalogService.deletecategory(param)
                      .then(function (response) {
                          if (response.errorMessage != '') {
                              growlService.growl(response.errorMessage, "inverse");
                          }
                          else {
                              if (nodeData.catParentId > 0) {
                                  var growText = 'Sub Category removed successfully.';
                              }
                              else {
                                  var growText = 'Category removed successfully.';
                              }
                              growlService.growl(growText, "success");
                              $scope.cancelEdit();
                              $scope.getcategories();
                          }
                      });
              }
              
              

              //var swalText = 'If You Delete Category, its Specific Category also get Deleted Permanently';

              //if (nodeData.parentID > 0) {
              //    var swalText = 'If You Delete SubCategory it will get Deleted Permanently';
              //}
              //else {
              //    var swalText = 'If You Delete Category, its Specific SubCategories also get Deleted Permanently';
              //}

                

              //swal({
              //    title: "Are You Sure!",
              //    text: swalText,
              //    type: "warning",
              //    showCancelButton: true,
              //    confirmButtonColor: "#DD6B55",
              //    confirmButtonText: "Yes",
              //    closeOnConfirm: true
              //},
              
              //function () {
              //catalogService.deletecategory(param)
              //    .then(function (response) {
              //        if (response.errorMessage != '') {
              //            growlService.growl(response.errorMessage, "inverse");
              //        }
              //        else {
              //            var growText = 'If You Delete Category, its Specific Category also get Deleted Permanently';

              //            if (nodeData.parentID > 0) {
              //                var growText = 'Sub Category added successfully.';
              //            }
              //            else {
              //                var growText = 'Category added successfully.';
              //            }


                          
              //            growlService.growl(growText, "success");
              //            $scope.cancelEdit();
              //            $scope.getcategories();
              //        }
              //    });
              //});
              

          };

          $scope.edit = function (scope) {
              $scope.isADDorEDIT = 'EDIT';
              $scope.myEditedScope = scope;
              var nodeData = scope.$modelValue;
              $scope.cid = nodeData.catId;
              $scope.mytitle = nodeData.catName;
              $scope.catdesc = nodeData.catDesc;
              $scope.parentId = $scope.catParentId;
              $scope.addNewNodeEnabled = true;
             
          };
          
          
         

         

          $scope.toggle = function (scope) {
              scope.toggle();
          };

          $scope.moveLastToTheBeginning = function () {
              var a = $scope.data.pop();
              $scope.data.splice(0, 0, a);
          };

          $scope.mytitle = '';
          $scope.catdesc = '';
         // $scope.catname = '';
          $scope.mytitleInputShow = false;
          //$scope.editNodeshow = false;
          $scope.myScope = [];
          $scope.parentNode = '';
          $scope.myEditedScope = [];

          $scope.addNewNodeEnabled = false;
         // $scope.editReqNodeEnable = false;
        //  $scope.saveprop = false;
          
          



          $scope.addItem = function (scope) {
              $scope.isADDorEDIT = 'ADD';
              $scope.mytitleInputShow = true;
              $scope.myScope = scope;             
              var nodeData = scope.$modelValue;
              $scope.parentNode = nodeData.catName;
              $scope.cid = nodeData.catId;
              $scope.pid = nodeData.catId;
              //$scope.catdesc = nodeData.catdesc;
              //$scope.editenablevalue = $scope.parentNode;
              $scope.addNewNodeEnabled = true;
             // $scope.editReqNodeEnable = true;
              
          };

          $scope.errorMessage = '';

          $scope.saveItem = function (mytitle, catdesc) {
            
            $scope.errorMessage = '';

              if (mytitle == null || mytitle == "" || mytitle == undefined) {
                  $scope.errorMessage = 'please fill out all fields';
                  return false;
              }
              if (catdesc == null || catdesc == "" || catdesc == undefined) {
                  $scope.errorMessage = 'please fill out all fields';
                  return false;
              }

              if ($scope.isADDorEDIT == 'ADD')
              {
                  $scope.mytitle = mytitle;
                  $scope.catdesc = catdesc;
                  var nodeData = $scope.myScope.$modelValue;
                  nodeData.nodes.push({
                      id: 0,
                      title: $scope.mytitle,
                      nodes: [],
                      parentID: $scope.pid,
                     
                      catdesc: $scope.catdesc
                      // catname:$scope.catname,


                  });
                 

                   $scope.addNewNodeEnabled = false;
                  


                  $scope.catObj = {
                      catId: 0,
                      compId: $scope.catalogCompId,
                      catCode: '',
                      catDesc: $scope.catdesc,
                      catName: $scope.mytitle,
                      catPath: '',
                      catOrder: 0,
                      catParentId: $scope.pid,
                      isValid: 1,
                      ModifiedBy: userService.getUserToken()
                      //CreatedBy:0,
                  };

                  var param = {
                      reqCategory: $scope.catObj,
                      sessionID: userService.getUserToken(),
                      //companyId: userService.getUserCompanyId(),
                      companyId: $scope.catalogCompId,
                      // CreatedBy: userService.getUserId(),
                      // ModifiedBy: userService.getUserToken(),
                  }

                  catalogService.addcategory(param)
                    .then(function (response) {
                        if (response.errorMessage != '') {
                            growlService.growl(response.errorMessage, "inverse");
                        }
                        else {
                            growlService.growl("category added Successfully.", "success");
                            $scope.getProperties();
                        }
                        $scope.catObjRespose = response;
                        $scope.mytitle = '';
                        $scope.catdesc = '';
                        //$scope.catname = '';
                        
                    });


                  // console.log(nodeData.nodes);
                  //$scope.addcategory = function (scope) {

                  //}

                  //window.location.reload(true);

              }
              else if ($scope.isADDorEDIT == 'EDIT')
              {
                 
                  $scope.mytitle = mytitle;
                  $scope.catdesc = catdesc;
                  var editednodeData = $scope.myEditedScope.$modelValue;
                 
                  $scope.editednodeData = mytitle;
                  $scope.editednodeData = catdesc;

                  $scope.addNewNodeEnabled = false;

                  
                  $scope.catEditObj = {

                      catId: $scope.cid,  //cid is category id
                      catCode: '',
                      catDesc: $scope.catdesc,
                      catName: $scope.mytitle,
                      //catPath: '',
                      //catOrder: 0,
                      catParentId: $scope.pid,
                      isValid: 1,
                      //CreatedBy:0,
                      compId: $scope.catalogCompId,
                      departments: $scope.selectedCatNode.departments
                  };

                  var params = {
                      reqCategory: $scope.catEditObj,
                      sessionID: userService.getUserToken(),
                     // companyId: userService.getUserCompanyId(),
                      companyId: $scope.catalogCompId,

                  }

                  catalogService.updatecategory(params)
                        .then(function (response) {
                            $scope.catObjEditResponse = response;
                            var editednodeData = $scope.myEditedScope.$modelValue;
                           
                            $scope.editednodeData = mytitle;
                            $scope.editednodeData = catdesc;

                        });

                  $scope.updatecategory = function (scope) {

                  }

                 
              }
             
            
                        
              
          }


                  




          $scope.removeNewNode = function (scope) {
              $scope.isADDorEDIT = '';
              $scope.addNewNodeEnabled = false;
              $scope.showUpdatecategory = false;
              $scope.PropertiesRaw = [];
              $scope.Properties = [];
              $scope.isADDorEDIT = '';
              $scope.myEditedScope = [];
              $scope.cid = 0;
              $scope.mytitle = '';
              $scope.catdesc = '';
              $scope.parentId = 0;
          };
         


          
          

          $scope.collapseAll = function () {
              $scope.$broadcast('angular-ui-tree:collapse-all');
          };

          $scope.expandAll = function () {
              $scope.$broadcast('angular-ui-tree:expand-all');
          };

         

          

          
         

          //$scope.showcategory = false;

          //$scope.addCategory = function () {
          //    $scope.showcategory = true;
          //    $scope.Properties = [];
          //}

          //$scope.showUpdatecategory = false;

          //$scope.updatecategory = function () {
          //    $scope.showUpdatecategory = true;
          //    $scope.getProperties();
          //}



          //$scope.showInput = false;

          //$scope.addCat = function () {
          //    $scope.showInput = true;
          //}



          //$scope.saveCat = function () {

          //    $scope.catDeclaration = {
          //        "id": 0,
          //        "title": $scope.catInputstring,
          //        "nodes": []
          //    };

          //    $scope.catalogMgmt.push($scope.catDeclaration);
          //    console.log($scope.catalogMgmt);
          //}

          //$scope.catInputstring = '';






          

          //$scope.cons = function (mytitle)
          //{
          //    console.log(mytitle);
          //    console.log($scope.mytitle);
          //}




          //$scope.isADDorEDIT = '';

        

          //=== START product Attribute====


        //  $scope.companyProducts = [];

        //  $scope.getproductbyid = function () {
        //      catalogService.getproductbyid(0)
        //     .then(function (response) {
        //         $scope.companyProducts = response;
        //     });
        //}


          //$scope.getproductbyid();

         

          

          //$scope.productAttribute = false;
          //$scope.productAttributebtn = function () {
          //    $scope.productAttribute = true;
          //}


          //$scope.attribute = {};

          //$scope.clickedAtrribute = {};

          

          //$scope.selectAttribute = function (attribute) {
          //    //  console.log(attribute);
          //    $scope.clickedAtrribute = attribute;
          //};

          //$scope.updateAttribute = function () {

          //};




         // $scope.editAttribute = function (productAttribute) {
         //     $scope.addnewattributeView = true;
         //     $scope.attribute = productAttribute;

         // };

         // $scope.closeEditAttribute = function () {
         //     $scope.addnewattributeView = false;
         //     $scope.attribute = {


         //     };
         // };



         //$scope.validateData = function(catObj) {
         //     if (catObj.mytitle == null || catObj.mytitle == "") {
         //         $scope.isValidToSubmit = false;
         //     }
         //     else if (catObj.catdesc == null || catObj.catdesc == "") {
         //         $scope.isValidToSubmit = false;
         //     }
             
         // }

          //=== END  product Attribute===











          // START ADD PRODUCT
          

          //$scope.reqProduct = {
          //    prodId: 0,
          //    compId:1,
          //    prodCode: '',
          //    prodName: '',
          //    prodDesc: '',
          //   //prodPath: '',
          //    isValid: 1
          //};

          //$scope.addproduct = function () {

          //    var params = {
          //        reqProduct: $scope.reqProduct,
          //        sessionID: userService.getUserToken()
          //    }

          //    catalogService.addproduct(params)
          //              .then(function (response) {
          //                  if (response.errorMessage != '') {
          //                      //swal
          //                      growlService.growl(response.errorMessage, "inverse");
          //                  }
          //                  else {
          //                      growlService.growl("product Saved Successfully.", "success");
          //                     // $scope.GetProductsByCategory();

          //                      $scope.reqProduct = {
          //                          prodId: 0,
          //                          prodCode: '',
          //                          prodName: '',
          //                          prodDesc: '',
          //                          //prodPath: '',
          //                          isValid: 1
          //                      };
          //                  };

          //              });

          //};

          //$scope.updateproduct = function (reqProduct) {
          //    $scope.addnewdesigView = true;
          //    $scope.reqProduct = companyDepartment;
          //    $scope.designation.sessionID = $scope.sessionID;

          //    document.body.scrollTop = 0; // For Chrome, Safari and Opera 
          //    document.documentElement.scrollTop = 0; // For IE and Firefox
          //};


          // END ADD PRODUCT

          $scope.getProperties = function (catId) {
              $scope.Properties = [];
              catalogService.getProperties($scope.catalogCompId, catId, 1)
                  .then(function (response) {

                      var PropertiesRaw = response;

                      PropertiesRaw.forEach(function (item, index) {
                          var Property = {
                              "propId": item.propId,
                              "propName": item.propName,
                              "propDesc": item.propDesc,
                              "propDataType": item.propDataType,
                              "propChecked": item.propValue == null ? false : true,
                              "propOptions": item.propOptions,
                              "propValue": item.propValue,
                              "propIsValid": item.isValid,
                              "propModified": false
                          }
                          $scope.Properties.push(Property);

                      })

                      //$scope.collapseAll();
                  });
             
          }

          $scope.catAddEditFlag = false;

          $scope.addParentCategory = function (scope) {
             
              $scope.selectedCatNodeType = "Category"
              var selNode = scope.$modelValue;
              $scope.selectedCatNodeTitle = selNode.catName;//"Category";
              $scope.selectedCategoryId = selNode.catId;
              $scope.selectedCatNodeReset = {
                  "catId": 0,
                  "catName": '',
                  "catDesc": '',
                  "catParentId": selNode.catId
              };
              $scope.addCategory(selNode);

          }
          $scope.addSubCategory = function (scope) {
              
              
              $scope.selectedCatNodeType = "Sub Category"
              //if (scope.$modelValue.length > 0) {
              //    var a = scope.$modelValue[0];
              //} else {
              //    var a = scope.$modelValue;
              //}

              var selNode = scope.$modelValue;
              //console.log("selNode111111>>>>>" + scope.$viewValue);
              $scope.selectedCatNodeTitle = selNode.catName;//"Sub Category";
              $scope.selectedCategoryId = selNode.catId;
              $scope.selectedCatNodeReset = {
                  "catId": 0,
                  "catName": '',
                  "catDesc": '',
                  "catParentId": selNode.catId
              };
              $scope.addCategory(selNode);
          }

          $scope.addCategory = function (selNode) {
              $scope.catEditFlag = false;
              $scope.catAddFlag = true;

              $scope.selectedCatNode = {
                  "catId": 0,
                  "catName": '',
                  "catDesc": '',
                  "catParentId": selNode.catId
              };
              
          }

          $scope.saveNewCategory = function (selNode) {
              $scope.selectedCategoryId = selNode.catParentId;
              var catObj = {
                  catId: 0,
                  compId: $scope.catalogCompId,
                  catCode: '',
                  catDesc: selNode.catdesc,
                  catName: selNode.title,
                  catPath: '',
                  catOrder: 0,
                  catParentId: selNode.catParentId,
                  departments: angular.isArray(selNode.departments) ? selNode.departments.join(',') : selNode.departments,
                  isValid: 1,
                  ModifiedBy: userService.getUserId()
              };

              var param = {
                  reqCategory: catObj,
                  sessionID: userService.getUserToken()
              }

              catalogService.addcategory(param)
                  .then(function (response) {
                      if (response.errorMessage != '') {
                          growlService.growl(response.errorMessage, "inverse");
                      }
                      else {
                          if (selNode.catParentId > 0) {
                              growlService.growl("Sub Category added successfully.", "success");
                          }
                          else {
                              growlService.growl("Category added successfully.", "success");
                          }
                          
                          $scope.cancelEdit();
                          $scope.getcategories();
                      }
                  });
          }

          $scope.editCategory = function (scope) {
             
              $scope.catEditFlag = true;
              $scope.catAddFlag = false;
              var selNode = scope.$modelValue;
              $scope.editCategoryData(selNode);
              $scope.selectedCategoryId = selNode.catId;
              $scope.selectedCatNodeReset = {
                  "catId": selNode.catId,
                  "catName": selNode.catName,
                  "catDesc": selNode.catDesc,
                  "catParentId": selNode.catParentId,
                  "departments": selNode.departments

              };

            }
            $scope.getIntArray = function (strText) {
                var intArr = [];
                if (strText) {
                    var strArr = strText.split(',');
                    for (var i = 0; i < strArr.length; i++) {
                        intArr.push(parseInt(strArr[i]));
                    }
                }
                return intArr;
                
            }
          $scope.editCategoryData = function (selNode) {
              $scope.selectedCategoryId = selNode.catId;
              $scope.selectedCatNodeTitle = selNode.catName;
              $scope.selectedCatNode = {
                  "catId": selNode.catId,
                  "catName": selNode.catName,
                  "catDesc": selNode.catDesc,
                  "catParentId": selNode.catParentId,
                  "departments": $scope.getIntArray(selNode.departments) //selNode.departments.split(',')

              };
              
              $scope.getProperties(selNode.catId);
          }
          $scope.saveCategoryDetails = function (selNode) {
              
              $scope.selectedCategoryId = selNode.catId;
              $scope.catEditObj = {

                  catId: selNode.catId,  //cid is category id
                  catCode: '',
                  catDesc: selNode.catDesc,
                  catName: selNode.catName,
                  catParentId: selNode.catParentId,
                  isValid: 1,
                  compId: $scope.catalogCompId,
                  departments: angular.isArray(selNode.departments) ? selNode.departments.join(',') : selNode.departments,
                  ModifiedBy: userService.getUserId()
              };

              var params = {
                  reqCategory: $scope.catEditObj,
                  sessionID: userService.getUserToken(),
                //  companyId: userService.getUserCompanyId(),
                  companyId: $scope.catalogCompId,

              }

              catalogService.updatecategory(params)
                  .then(function (response) {
                      if (response.errorMessage != '') {
                          growlService.growl(response.errorMessage, "inverse");
                      }
                      else {
                          if (selNode.catParentId > 0) {
                              growlService.growl("SubCategory details updated successfully.", "success");
                          } else {
                              growlService.growl("Category details updated successfully.", "success");
                          }
                          
                          $scope.cancelEdit();
                          $scope.getcategories();
                      }
                     
                  });

          }
          $scope.propCheckChanged = function (selectedProp) {
              selectedProp.propModified = true;
              //selectedProp.propChecked = !selectedProp.propChecked;
              selectedProp.propIsValid = selectedProp.propChecked
          }
          $scope.saveProperties = function () {
              $scope.propertyobj = [];
             
              var callsave = false;
              $scope.Properties.forEach(function (item, index) {
                  if (item.propModified) {
                      callsave = true;

                      if (!item && !item.propValue)
                      {
                          item.propValue = '';
                      }


                      $scope.propertyobj.push({
                          entityId: $scope.selectedCategoryId,
                          companyId: $scope.catalogCompId,
                          propId: item.propId,
                          propValue: item.propValue,
                          isValid: item.propIsValid ? 1 : 0,
                          user: userService.getUserId()
                      });
                     
                  }
              })
              if (callsave) {
                  var params = {
                      propertyobj: $scope.propertyobj,
                      sessionId: userService.getUserToken()
                  };
                  catalogService.saveEntityProperties(params)
                      .then(function (response) {
                          if (response.errorMessage != '') {
                              growlService.growl(response.errorMessage, "inverse");
                          }
                          else {
                              //$scope.removeNewNode();
                              growlService.growl("catalogue properties updated successfully.", "success");
                          }

                      });
              }
          }

          $scope.cancelEdit = function () {
              $scope.catEditFlag = false;
              $scope.catAddFlag = false;
              $scope.selectedCatNode = null;
              $scope.Properties = null;
              $scope.selectedCatNodeTitle = null;
          }


          //$scope.nodeColorChange = function () {

          $scope.exportTemplateToExcel = function () {
              var mystyle = {
                  sheetid: 'products',
                  headers: true,
                  column: {
                      style: 'font-size:15px;background:#233646;color:#FFF;'
                  }
              };

              alasql('SELECT "CategoryName" as [CategoryName], "CategoryDepartments" as [CategoryDepartments] INTO XLSX(?,{headers:true,sheetid: "categories", style: "font-size:15px;background:#233646;color:#FFF;"}) FROM ?', ["categories.xlsx", $scope.ExcelProducts]);
          }

          $scope.entityObj = {
              entityName: 'catalogueCategories',
              attachment: [],
              userid: userService.getUserId(),
              sessionID: userService.getUserToken()
          };

          $scope.getFile1 = function () {
              $scope.file = $("#excelquotationproducts")[0].files[0];
              fileReader.readAsDataUrl($scope.file, $scope)
                  .then(function (result) {
                      var bytearray = new Uint8Array(result);
                      $scope.entityObj.attachment = $.makeArray(bytearray);
                      $scope.importEntity();
                  });
          }

          $scope.importEntity = function () {

              var params = {
                  "entity": $scope.entityObj,
                  compId: $scope.catalogCompId,
                  user: userService.getUserId(),
                  sessionId: userService.getUserToken()
              };

              catalogService.importcataloguecategories(params)
                  .then(function (response) {

                      if (response.errorMessage != '') {
                          alert(response.errorMessage);
                      }
                      if (response.repsonseId > 0) {
                          growlService.growl(response.repsonseId + " Categories saved successfully.", "success");
                          location.reload();
                      }
                  })
          }

        $scope.GetCompanyDepartments = function () {
            auctionsService.GetCompanyDepartments(userService.getUserId(), userService.getUserToken())
                .then(function (response) {
                    $scope.companyDepartments = response;
                })
        }
        $scope.GetCompanyDepartments();

      }]);
