prmApp
.factory('SignalRFactory',
    ["$http", "$rootScope", "$location", "Hub", "$timeout", "$q","$log", "signalR",
    function ($http, $rootScope, $location, Hub, $timeout, $q, $log, signalR) {
        function backendFactory(serverUrl, hubName) {
            var isConnected = false;
            var connection = $.hubConnection(signalR);
            var proxy = connection.createHubProxy(hubName);
            proxy.on('checkRequirement', function (req) {
                return req;
            });
            proxy.on('checkComment', function (obj) {
                return obj;
            });
            connection.start().done(function () {
                isConnected = true;
                $log.info("Angular SignalR Service Created");
            });
            return {
                on: function (eventName, callback) {
                    if (connection.state == 1) {
                        proxy.on(eventName, function (result) {
                            $rootScope.$apply(function () {
                                if (callback) {
                                    callback(result);
                                }
                            });
                        });
                    } else {
                        connection.start().done(function () {
                            proxy.on(eventName, function (result) {
                                $rootScope.$apply(function () {
                                    if (callback) {
                                        callback(result);
                                    }
                                });
                            });
                        });
                    }
                },
                off: function (eventName, callback) {
                    proxy.off(eventName, function (result) {
                        $rootScope.$apply(function () {
                            if (callback) {
                                callback(result);
                            }
                        });
                    });
                },
                invoke: function (methodName, args, callback) {
                    if (connection.state == 1) {
                        proxy.invoke(methodName, args)
                            .done(function (result) {
                                $rootScope.$apply(function () {
                                    if (callback) {
                                        callback(result);
                                    }
                                });
                            });
                    } else {
                        connection.start().done(function () {
                            proxy.invoke(methodName, args)
                                .done(function (result) {
                                    $rootScope.$apply(function () {
                                        if (callback) {
                                            callback(result);
                                        }
                                    });
                                });
                        });
                    }
                },
                stop: function () {
                    //////////console.log(connection.stop());
                    connection.stop();
                    isConnected = false;
                },
                getStatus: function () {
                    return connection.state;
                },
                reconnect: function () {
                    connection.start().done(function () {
                        isConnected = true;
                        $log.info("Angular Service Reconnected");
                        return true;
                    });
                },
                connection: connection
            };
        }

        return backendFactory;
}]);