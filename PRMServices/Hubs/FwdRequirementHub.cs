﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.SignalR;
using PRMServices.Models;
using System.Threading.Tasks;
using System.Data;
using PRMServices.SQLHelper;

namespace PRMServices.SignalR
{
    public class FwdRequirementHub : Hub
    {
        public Task JoinGroup(string groupName)
        {
            return Groups.Add(Context.ConnectionId, groupName);
        }

        public Task LeaveGroup(string groupName)
        {
            return Groups.Remove(Context.ConnectionId, groupName);
        }

        public SignalRObj UpdateTime(string parameters)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();
            DataSet ds = new DataSet();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = Convert.ToInt32(listofParams[1]);
            long ticks = Convert.ToInt64(listofParams[2]);
            string sessionID = listofParams[3];
            Response response = new Response();
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            try
            {
                sd.Add("P_REQ_ID", reqID);
                DateTime newTime = DateTime.UtcNow.AddSeconds(ticks);
                sd.Add("P_CLOSE", ticks);
                sd.Add("P_NEW_TIME", newTime);
                sd.Add("P_U_ID", userID);

                ds = sqlHelper.SelectList("fwd_UpdateBidTime", sd);
                ds = prmFwdReqservice.GetRequirementDetailsHub(reqID);
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            FwdRequirement req = new FwdRequirement();
            req = prmFwdReqservice.GetRequirementDataFilter(userID, sessionID, ds);
            payLoad.Add(req);
            req.ErrorMessage += response.ErrorMessage;
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            involvedParties.MethodName = "UpdateTime";
            involvedParties.CallerID = CallerID;
            involvedParties.ErrorMessage = response.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            signalRObj.Inv = involvedParties;
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prmFwdReqservice.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad.Add(req);
            }

            getSignalRObj(req.RequirementID, involvedParties, payLoad);
            return signalRObj;
        }

        public FwdRequirement DeleteRequirement(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            string reason = listofParams[3];

            Response response = new Response();
            PRMFwdReqService prm = new PRMFwdReqService();
            response = prm.DeleteRequirement(reqID, userID, sessionID, reason);
            FwdRequirement req = new FwdRequirement();
            DataSet ds = prm.GetRequirementDetailsHub(reqID);
            req = prm.GetRequirementDataFilter(userID, sessionID, ds);
            req.ErrorMessage += response.ErrorMessage;
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            involvedParties.MethodName = "DeleteRequirement";
            involvedParties.CallerID = CallerID;
            involvedParties.ErrorMessage = response.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            payLoad.Add(req);
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prm.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad.Add(req);
            }
            
            getSignalRObj(req.RequirementID, involvedParties, payLoad);
            return req;
        }

        public SignalRObj UpdateAuctionStartSignalR(RunningAuction auction)
        {
            SignalRObj signalRObj = new SignalRObj();
            List<object> payLoad = new List<object>();
            FwdRequirement req = new FwdRequirement();
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            Response res = prmFwdReqservice.UpdateAuctionStart(auction.AuctionID, auction.CustomerID, auction.AuctionEnds, auction.SessionID, auction.NegotiationDuretion, auction.NegotiationSettings);
            DataSet ds = prmFwdReqservice.GetRequirementDetailsHub(auction.AuctionID);
            req = prmFwdReqservice.GetRequirementDataFilter(auction.CustomerID, auction.SessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            if (req.AuctionVendors != null)
            {
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            }
            involvedParties.MethodName = "UpdateAuctionStartSignalR";
            involvedParties.CallerID = auction.CustomerID;
            involvedParties.ErrorMessage = res.ErrorMessage;
            involvedParties.CustCompID = req.CustCompID;
            req.Inv = involvedParties;
            signalRObj.Inv = involvedParties;
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prmFwdReqservice.GetRequirementDataFilter(vendorID, auction.SessionID, ds);
                payLoad.Add(req);
            }

            getSignalRObj(req.RequirementID, involvedParties, payLoad);
            return signalRObj;
        }

        public FwdRequirement MakeBid(BidObject parameters)
        {
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            FwdRequirement req = new FwdRequirement();
            Response res = prmFwdReqservice.MakeABid(parameters.ReqID, parameters.UserID, parameters.Price, parameters.Quotation,
                parameters.QuotationName, parameters.SessionID, parameters.Tax, parameters.Freightcharges,
                parameters.Warranty, parameters.Payment, parameters.Duration, parameters.Validity,
                parameters.FwdQuotationObject, parameters.Revised, parameters.PriceWithoutTax, parameters.Type,
                parameters.Discount, parameters.ListRequirementTaxes, parameters.OtherProperties);

            req.ErrorMessage = res.ErrorMessage;
            return req;
        }

        public void CheckRequirement(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);
            int CallerID = userID;
            string sessionID = listofParams[2];
            string SubMethodName = string.Empty;
            if (listofParams != null && listofParams.Length > 3)
            {
                SubMethodName = listofParams[3];
            }

            int ReceiverId = -1;
            if (listofParams != null && listofParams.Length > 4)
            {
                if (!string.IsNullOrEmpty(listofParams[4]))
                {
                    ReceiverId = Convert.ToInt32(listofParams[4]);
                }
            }

            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            DataSet ds = prmFwdReqservice.GetRequirementDetailsHub(reqID);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                DataRow row = ds.Tables[0].Rows[0];
                userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;

            }

            FwdRequirement req = prmFwdReqservice.GetRequirementDataFilter(userID, sessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.MethodName = "CheckRequirement";
            involvedParties.SubMethodName = SubMethodName;
            involvedParties.ReceiverId = ReceiverId;
            involvedParties.CallerID = CallerID;
            involvedParties.CustCompID = req.CustCompID;
            involvedParties.ErrorMessage = req.ErrorMessage;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            req.Inv = involvedParties;
            
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prmFwdReqservice.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad.Add(req);
            }

            getSignalRObj(req.RequirementID, involvedParties, payLoad);
        }

        public void RestartNegotiation(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            Response res = prmFwdReqservice.RestartNegotiation(reqID, userID, sessionID);
            DataSet ds = prmFwdReqservice.GetRequirementDetailsHub(reqID);
            FwdRequirement req = prmFwdReqservice.GetRequirementDataFilter(userID, sessionID, ds);
            payLoad.Add(req);
            InvolvedParties involvedParties = new InvolvedParties();
            involvedParties.RequirementID = req.RequirementID;
            involvedParties.CustomerID = req.CustomerID;
            involvedParties.SuperUserID = req.SuperUserID;
            involvedParties.MethodName = "RestartNegotiation";
            involvedParties.CallerID = CallerID;
            involvedParties.CustCompID = req.CustCompID;
            involvedParties.ErrorMessage = req.ErrorMessage;
            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
            req.Inv = involvedParties;
            Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
            Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
            foreach (int vendorID in involvedParties.UserIDList)
            {
                req = prmFwdReqservice.GetRequirementDataFilter(vendorID, sessionID, ds);
                payLoad.Add(req);
            }

            getSignalRObj(req.RequirementID, involvedParties, payLoad);
        }


        public void StopBids(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            Response res = prmFwdReqservice.StopBids(reqID, userID, sessionID);
            if (res.ErrorMessage == "" || res.ErrorMessage == "Failure sending mail.")
            {
                DataSet ds = prmFwdReqservice.GetRequirementDetailsHub(reqID);
                FwdRequirement req = prmFwdReqservice.GetRequirementDataFilter(userID, sessionID, ds);
                payLoad.Add(req);
                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = req.RequirementID;
                involvedParties.CustomerID = req.CustomerID;
                involvedParties.SuperUserID = req.SuperUserID;
                involvedParties.MethodName = "StopBids";
                involvedParties.CallerID = CallerID;
                involvedParties.CustCompID = req.CustCompID;
                involvedParties.ErrorMessage = res.ErrorMessage;
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                req.Inv = involvedParties;
                Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                foreach (int vendorID in involvedParties.UserIDList)
                {
                    req = prmFwdReqservice.GetRequirementDataFilter(vendorID, sessionID, ds);
                    payLoad.Add(req);
                }

                getSignalRObj(req.RequirementID, involvedParties, payLoad);
            }
        }

        public void EndNegotiation(string parameters)
        {
            List<object> payLoad = new List<object>();
            string[] listofParams = parameters.Split('$');
            int reqID = Convert.ToInt32(listofParams[0]);
            int userID = Convert.ToInt32(listofParams[1]);int CallerID = Convert.ToInt32(listofParams[1]);
            string sessionID = listofParams[2];
            PRMFwdReqService prmFwdReqservice = new PRMFwdReqService();
            Response res = prmFwdReqservice.EndNegotiation(reqID, userID, sessionID);
            if (res.ErrorMessage == "" || res.ErrorMessage == "Failure sending mail.")
            {
                DataSet ds = prmFwdReqservice.GetRequirementDetailsHub(reqID);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                }

                FwdRequirement req = prmFwdReqservice.GetRequirementDataFilter(userID, sessionID, ds);
                payLoad.Add(req);
                InvolvedParties involvedParties = new InvolvedParties();
                involvedParties.RequirementID = req.RequirementID;
                involvedParties.CustomerID = req.CustomerID;
                involvedParties.SuperUserID = req.SuperUserID;
                involvedParties.CallerID = CallerID;
                involvedParties.CustCompID = req.CustCompID;
                involvedParties.ErrorMessage = res.ErrorMessage;
                involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                involvedParties.MethodName = "EndNegotiation";
                req.Inv = involvedParties;
                Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                Clients.Group(Utilities.FwdGroupName + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                foreach (int vendorID in involvedParties.UserIDList)
                {
                    req = prmFwdReqservice.GetRequirementDataFilter(vendorID, sessionID, ds);
                    payLoad.Add(req);
                }

                getSignalRObj(req.RequirementID, involvedParties, payLoad);
            }
        }

        public SignalRObj getSignalRObj(int requirementId, InvolvedParties parties, List<object> objArr)
        {
            SignalRObj signalRObj = new SignalRObj();
            signalRObj.Inv = parties;
            signalRObj.PayLoad = objArr.ToArray();
            Clients.Group(Utilities.GroupName + "FWD" + requirementId).checkRequirement(signalRObj);
            return signalRObj;
        }
    }

}