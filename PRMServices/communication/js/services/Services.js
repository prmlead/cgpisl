prmApp.constant('PRMCommunicationServiceDomain', 'communication/svc/PRMCommunication.svc/REST/');
prmApp.constant('signalRFwdHubName', 'fwdRequirementHub');
prmApp.service('PRMCommunicationService', ["PRMCommunicationServiceDomain", "userService", "httpServices",
    function (PRMCommunicationServiceDomain, userService, httpServices) {
        var PRMCommunicationService = this;

        PRMCommunicationService.getCatalogueVendors = function (catalogue) {
            let url = PRMCommunicationServiceDomain + 'getcataloguevendors?catalogue=' + catalogue + '&sessionid=' + userService.getUserToken();
            return httpServices.get(url);
        };


        PRMCommunicationService.SendCommunications = function (params) {
            let url = PRMCommunicationServiceDomain + 'sendcommunication';
            return httpServices.post(url, params);
        };

        
        return PRMCommunicationService;
}]);