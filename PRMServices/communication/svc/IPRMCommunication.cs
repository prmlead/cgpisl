﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using PRMServices.Models;
using PRMServices.models;

namespace PRMServices
{
    [ServiceContract]
    public interface IPRMCommunication
    {
        [OperationContract]
        [WebGet(RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "getcataloguevendors?catalogue={catalogue}&sessionid={sessionID}")]
        List<UserDetails> GetCatalogueVendors(string catalogue, string sessionID);

        [OperationContract]
        [WebInvoke(Method = "POST",
        BodyStyle = WebMessageBodyStyle.WrappedRequest,
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "sendcommunication")]
        Response SendCommunication(List<UserDetails> vendors, string subject, string body, bool sendSMS, bool sendEmail, string sessionID);
    }
}
