﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using PRMServices.SignalR;
using PRMServices.SQLHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.ServiceModel.Activation;
using System.Threading.Tasks;
using System.Web;
using CATALOG = PRMServices.Models.Catalog;



namespace PRMServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class PRMFwdReqService : IPRMFwdReqService
    {
        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
        private PRMServices prmServices = new PRMServices();

        public FwdRequirement GetRequirementData(int reqID, int userID, string sessionID)
        {
            FwdRequirement requirement = new FwdRequirement();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;
                    requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;
                    requirement.IsPOGenerated = isPOGenerated(reqID);
                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;

                    NegotiationSettings NegotiationSettings = new NegotiationSettings();
                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;
                    requirement.ReportReq = row["REPORT_REQ"] != DBNull.Value ? Convert.ToInt32(row["REPORT_REQ"]) : 0;
                    requirement.ReportItemWise = row["REPORT_ITEM_WISE"] != DBNull.Value ? Convert.ToInt32(row["REPORT_ITEM_WISE"]) : 0;
                    requirement.ReqType = row["PARAM_REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["PARAM_REQ_TYPE"]) : string.Empty;
                    requirement.PriceCapValue = row["PARAM_PRICE_CAP_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_PRICE_CAP_VALUE"]) : 0;
                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;
                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;
                    requirement.SuperUser = new User();
                    requirement.PostedUser = new User();
                    requirement.PostedUser.UserInfo = new UserInfo();
                    requirement.SuperUser.FirstName = row["SUPER_USER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_NAME"]) : string.Empty;
                    requirement.SuperUser.Email = row["SUPER_USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_EMAIL"]) : string.Empty;
                    requirement.SuperUser.PhoneNum = row["SUPER_USER_PHONE"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_PHONE"]) : string.Empty;
                    requirement.PostedUser.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    requirement.PostedUser.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    requirement.PostedUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.PostedUser.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.PostedUser.UserInfo.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;
                    requirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;
                    requirement.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
                    requirement.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;
                    requirement.IndentID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;
                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                    requirement.GeneralTC = row["GENERAL_TC"] != DBNull.Value ? Convert.ToString(row["GENERAL_TC"]) : string.Empty;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                    requirement.ContractStartTime = row["CONT_START_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_START_TIME"]) : string.Empty;
                    requirement.ContractEndTime = row["CONT_END_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_END_TIME"]) : string.Empty;
                    requirement.IsContract = row["IS_CONTRACT"] != DBNull.Value ? (Convert.ToInt32(row["IS_CONTRACT"]) == 1 ? true : false) : false;

                    //#CB-0-2018-12-05
                    requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;

                    requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.MinValue;

                    requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                    requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;

                    requirement.LAST_BID_ID = row["LAST_BID_ID"] != DBNull.Value ? Convert.ToInt32(row["LAST_BID_ID"]) : 0;

                    requirement.PrCode = row["PR_CODE"] != DBNull.Value ? Convert.ToString(row["PR_CODE"]) : string.Empty;

                    requirement.IsLockReq = row["LOCK_REQ"] != DBNull.Value ? Convert.ToInt32(row["LOCK_REQ"]) : 0;

                    requirement.PR_ID = row["PR_ID"] != DBNull.Value ? Convert.ToInt32(row["PR_ID"]) : 0;

                    int slot_created_temp = row["IS_SLOT_CREATED"] != DBNull.Value ? Convert.ToInt32(row["IS_SLOT_CREATED"]) : 0;

                    requirement.ShowInspectionValue = slot_created_temp == 1?true:false;

                    requirement.NegotiationSettings = NegotiationSettings;
                    DateTime now = DateTime.UtcNow;
                    requirement.DATETIME_NOW = now;
                    DateTime DATE_CB_END_TIME = Convert.ToDateTime(requirement.CB_END_TIME);
                    long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }


                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }


                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        requirement.TimeLeft = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }


                    PRMSlotService prmSlot = new PRMSlotService();
                    requirement.ListSlotDetails = prmSlot.GetSlotDetails(requirement.RequirementID, sessionID);
                    requirement.ListBookedSlots = prmSlot.GetBookedSlotDetails(requirement.RequirementID, sessionID);

                    SlotBooking BookedSlots = new SlotBooking();
                    if (requirement.ListBookedSlots != null && requirement.ListBookedSlots.Count > 0) {
                        BookedSlots = requirement.ListBookedSlots.Where(v => v.Slot_vendor_id == userID).FirstOrDefault();
                    }
                    if (requirement.ListSlotDetails.Count >0 && BookedSlots != null && BookedSlots.Slot_id > 0) {
                        foreach (SlotDetails slots in requirement.ListSlotDetails) {
                            if (BookedSlots != null && BookedSlots.Slot_id>0) {
                                slots.IsVendorBooked = true;
                            }
                            if (BookedSlots.Slot_id == slots.Slot_id) {
                                slots.VendorSlot = true;
                            }
                            else {
                                slots.VendorSlot = false;
                            }
                        }
                    }



                    foreach (SlotDetails slot in requirement.ListSlotDetails) {
                        if (slot.VENDORS_SLOT_APPROVED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "APPROVED";
                        }
                        else if (slot.VENDORS_SLOT_REJECTED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "REJECTED";
                        }
                        else if (slot.VENDORS_SLOT_CANCELED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "CANCELLED";
                        }
                        else if (slot.VENDORS_SLOT_BOOKED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "BOOKED";
                        }
                        else
                        {
                            slot.VENDOR_SLOT_STATUS = "NA";
                        }
                    }


                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();

                            vendor.PO = new RequirementPO();
                            vendor.ListCurrencies = new List<KeyValuePair>();
                            vendor.RequirementID = requirement.RequirementID;
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;

                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;
                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;
                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;
                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();
                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";
                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;
                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;


                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            //vendor.RevpackingCharges = row1["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES"]) : 0;
                            //vendor.RevinstallationCharges = row1["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES"]) : 0;

                            //vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;
                            //vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;


                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;
                            vendor.Vendor = new User();
                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;
                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;
                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.TechEvalScore = row1["TECH_EVAL_SCORE"] != DBNull.Value ? Convert.ToDecimal(row1["TECH_EVAL_SCORE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;
                            //vendor.PackingCharges = row1["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES"]) : 0;
                            //vendor.PackingChargesTaxPercentage = row1["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.PackingChargesWithTax = row1["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_WITH_TAX"]) : 0;
                            //vendor.InstallationCharges = row1["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES"]) : 0;
                            //vendor.InstallationChargesTaxPercentage = row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.InstallationChargesWithTax = row1["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            vendor.VendorCurrency = row1["CURR_NAME"] != DBNull.Value ? Convert.ToString(row1["CURR_NAME"]) : string.Empty;
                            vendor.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                            //vendor.RevinstallationChargesWithTax = row1["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            //vendor.RevpackingChargesWithTax = row1["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                            // vendor.IsCurrency = row1["IS_CURRENCY"] != DBNull.Value ? (Convert.ToInt32(row1["IS_CURRENCY"]) == 1 ? true : false) : false;

                            //vendor.FREIGHT_CHARGES = row1["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES"]) : 0;
                            //vendor.FREIGHT_CHARGES_TAX_PERCENTAGE = row1["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.FREIGHT_CHARGES_WITH_TAX = row1["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES = row1["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_WITH_TAX = row1["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;

                            vendor.RevPriceCB = row1["REV_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE_CB"]) : 0;
                            vendor.RevVendorTotalPriceCB = row1["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_CB"]) : 0;
                            vendor.TotalRunningPriceCB = row1["VEND_TOTAL_PRICE_RUNNING_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING_CB"]) : 0;
                            //vendor.RevpackingChargesCB = row1["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_CB"]) : 0;
                            //vendor.RevinstallationChargesCB = row1["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_CB"]) : 0;
                            //vendor.VendorFreightCB = row1["VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT_CB"]) : 0;
                            //vendor.RevVendorFreightCB = row1["REV_VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT_CB"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_CB = row1["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_CB"]) : 0;

                            vendor.FREEZE_CB = row1["FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.VEND_FREEZE_CB = row1["VEND_FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["VEND_FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.CB_BID_COMMENTS = row1["CB_BID_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["CB_BID_COMMENTS"]) : string.Empty;

                            vendor.INCO_TERMS = row1["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row1["INCO_TERMS"]) : string.Empty;

                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, 2);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, 2);
                            //vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);
                            vendor.BOOKED_SLOT_DATE = row1["BOOKED_SLOT_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["BOOKED_SLOT_DATE"]) : DateTime.MaxValue;
                            vendor.IS_SLOT_APPROVED = row1["IS_SLOT_APPROVED"] != DBNull.Value ? (Convert.ToInt32(row1["IS_SLOT_APPROVED"])) : -1;
                            
                            if (vendor.BOOKED_SLOT_DATE == DateTime.MaxValue) {
                                vendor.BOOKED_SLOT_DATE = null;
                            }

                            vendor.SLOT_ID = row1["SLOT_ID"] != DBNull.Value ? (Convert.ToInt32(row1["SLOT_ID"])) : -1;
                            vendor.BASE_PRICE = row1["BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["BASE_PRICE"]) : 0;
                            vendor.SLOT_START_DATE = row1["SLOT_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row1["SLOT_START_TIME"]) : DateTime.MaxValue;
                            if (vendor.SLOT_START_DATE == DateTime.MaxValue)
                            {
                                vendor.SLOT_START_DATE = null;
                            }
                            vendor.SLOT_END_DATE = row1["SLOT_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row1["SLOT_END_TIME"]) : DateTime.MaxValue;
                            if (vendor.SLOT_END_DATE == DateTime.MaxValue)
                            {
                                vendor.SLOT_END_DATE = null;
                            }
                            vendor.SLOT_NAME = row1["SLOT_NAME"] != DBNull.Value ? Convert.ToString(row1["SLOT_NAME"]) : string.Empty;
                            //BASE_PRICE

                            KeyValuePair reqCurrency = new KeyValuePair();
                            reqCurrency.Value = requirement.Currency;
                            reqCurrency.Key1 = "REQ_CURRENCY";

                            KeyValuePair vendorCurrency = new KeyValuePair();
                            vendorCurrency.Value = vendor.VendorCurrency;
                            vendorCurrency.Key1 = "VENDOR_CURRENCY";

                            vendor.ListCurrencies.Add(reqCurrency);

                            if (reqCurrency.Value == vendorCurrency.Value)
                            {
                                vendor.SelectedVendorCurrency = reqCurrency.Value;
                            }
                            else
                            {
                                vendor.ListCurrencies.Add(vendorCurrency);
                            }

                            vendorDetails.Add(vendor);
                        }
                    }

                    if (requirement.IsDiscountQuotation == 2)
                    {
                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;
                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }

                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, 2);
                    }

                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.BASE_PRICE == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.BASE_PRICE == 0).ToList());

                    }

                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.BASE_PRICE == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())//item2.revUnitPrice * item2.productQuantity
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);
                    int productSNo = 0;
                    List<FwdRequirementItems> ListRequirementItems = new List<FwdRequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            FwdRequirementItems RequirementItems = new FwdRequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;
                            RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                            RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                            RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                            RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                            RequirementItems.ProductCode = row2["PROD_CODE"] != DBNull.Value ? Convert.ToString(row2["PROD_CODE"]) : string.Empty;
                            RequirementItems.ProductQuotationTemplateJson = row2["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row2["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                            RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;

                            ListRequirementItems.Add(RequirementItems);
                        }

                        requirement.ItemSNoCount = productSNo;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {
                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                            
                            double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;
                            double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;
                            double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                            double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                            double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                            double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                            double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                            double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;
                            
                            double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                            double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                            double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;
                            double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                            double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;

                            string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                            string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                            bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                            string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                            string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                            string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                            double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                            double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;

                            double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                            string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;

                            List<VendorDetails> tempV = new List<VendorDetails>();
                            tempV = v2.Where(v => v != null && v.VendorID == uID).ToList();
                            if (tempV.Count > 0)
                            {
                                v2.Where(v => v != null && v.VendorID == uID).FirstOrDefault().marginRankDiscount += unitdiscount;
                                v2.Where(v => v != null && v.VendorID == uID).FirstOrDefault().marginRankRevDiscount += revunitdiscount;
                            }

                            if (uID == userID)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductBrand = brand;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().OthersBrands = otherBrands;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitPrice = unitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPrice = revunitprice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPrice = revItemPrice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscount = revunitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPriceCB = revItemPriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;
                                
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().CGst = cgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().SGst = sgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IGst = igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().Gst = cgst + sgst + igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitMRP = unitmrp;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitDiscount = unitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().VendorUnits = vendorUnits;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IsRegret = isRegret;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RegretComments = regretComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightCharges = itemFreightCharges;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightTAX = itemFreightTAX;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REDUCTION_PRICE = LAST_BID_REV_UNIT_PRICE - revunitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;
                            }
                        }
                    }

                    requirement.ListFwdRequirementItems = ListRequirementItems;
                    foreach (VendorDetails V in vendorDetails)
                    {
                        int productSNo1 = 0;
                        List<FwdRequirementItems> ListRequirementItems1 = new List<FwdRequirementItems>();
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[2].Rows)
                            {
                                FwdRequirementItems RequirementItems = new FwdRequirementItems();
                                RequirementItems.ProductSNo = productSNo1++;
                                RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;
                                RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                                RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                                RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                                RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;
                                RequirementItems.ProductCode = row2["PROD_CODE"] != DBNull.Value ? Convert.ToString(row2["PROD_CODE"]) : string.Empty;
                                RequirementItems.ProductQuotationTemplateJson = row2["PRODUCT_QUOTATION_JSON"] != DBNull.Value ? Convert.ToString(row2["PRODUCT_QUOTATION_JSON"]) : string.Empty;
                                RequirementItems.CatalogueItemID = row2["CATALOGUE_ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["CATALOGUE_ITEM_ID"]) : 0;


                                ListRequirementItems1.Add(RequirementItems);
                            }
                        }

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[3].Rows)
                            {
                                int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                                int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                                string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                                double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;
                                double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;
                                double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                                double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                                double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;
                                double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                                double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                                double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                                string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                                string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                                bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                                string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                                string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                                string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                                double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                                double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                                double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;
                                double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                                double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                                double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                                double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                                double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                                string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;

                                if (uID == V.VendorID)
                                {
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemPrice = price;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductIDorName = name;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductNo = no;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductDescription = des;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductBrand = brand;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPrice = revItemPrice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitPrice = unitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPrice = revunitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().CGst = cgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().SGst = sgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IGst = igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().Gst = cgst + sgst + igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitMRP = unitmrp;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitDiscount = unitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscount = revunitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().VendorUnits = vendorUnits;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IsRegret = isRegret;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RegretComments = regretComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightCharges = itemFreightCharges;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightTAX = itemFreightTAX;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                    //#CB-0-2018-12-05
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPriceCB = revItemPriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REDUCTION_PRICE = LAST_BID_REV_UNIT_PRICE - revunitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;

                                }
                            }
                        }

                        V.ListFwdRequirementItems = ListRequirementItems1;
                    }
                    
                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {
                        foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())
                        {
                            vendor.Rank = v2.IndexOf(vendor) + 1;
                        }

                        requirement.RequirementVendorsList = v2;

                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2;
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                        }
                    }
                    else
                    {
                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();
                        List<VendorDetails> approvedV2 = new List<VendorDetails>();
                        approvedV2 = v2.Where(v => v.IsQuotationRejected == 0 && v.SumOfMargin > 0).OrderByDescending(v => v.SumOfMargin).ToList();
                        foreach (VendorDetails vendor in v2)
                        {
                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = approvedV2.IndexOf(vendor) + 1;
                            }
                        }

                        requirement.RequirementVendorsList = v2;

                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.Rank).ToList();
                            foreach (VendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ToList();
                        }

                        requirement.AuctionVendors = v2;
                    }


                    int taxSNo = 0;
                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;
                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }

                        requirement.TaxSNoCount = taxSNo;
                    }

                    requirement.ListRequirementTaxes = ListRequirementTaxes;
                }
            }
            catch (Exception ex)
            {
                FwdRequirement req = new FwdRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public Response RequirementSave(FwdRequirement requirement, byte[] attachment)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            int negotationDuration = requirement.NegotiationSettings != null ? Convert.ToInt16(requirement.NegotiationSettings.NegotiationDuration) : 15;
            Response response = new Response();
            string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);

            string reqPosting = "REQ_POSTING";
            string editReq = "EDIT_REQ";

            UserDetails customerDetails = new UserDetails();

            if (requirement.CloneID > 0)
            {
                requirement.RequirementID = 0;
                foreach (var item in requirement.ListFwdRequirementItems)
                {
                    item.ItemID = 0;
                }
            }

            requirement.PostedOn = DateTime.UtcNow;
            try
            {
                Utilities.ValidateSession(requirement.SessionID);
                if (requirement.RequirementID > 0)
                {
                    FwdRequirement oldReq = GetRequirementDataOfflinePrivate(requirement.RequirementID, requirement.CustomerID, requirement.SessionID);

                    int[] vendorsToRemove = GetVendorsToRemove(requirement, oldReq);
                    foreach (int id in vendorsToRemove)
                    {
                        Response res = RemoveVendorFromAuction(id, requirement.RequirementID, requirement.SessionID, "VendoremailForRemoval", requirement);
                    }

                    int[] itemsToRemove = GetItemsToRemove(requirement, oldReq);
                    foreach (int id in itemsToRemove)
                    {
                        Response res = RemoveItemFromAuction(id, requirement.RequirementID, requirement.SessionID, "");
                    }
                }

                string fileName = string.Empty;

                List<Attachment> CustomerListAttachment = new List<Attachment>();
                List<Attachment> VendorListAttachment = new List<Attachment>();

                var filenameTemp = string.Empty;
                if (requirement.MultipleAttachments != null && requirement.MultipleAttachments.Count > 0)
                {
                    foreach (FileUpload fd in requirement.MultipleAttachments)
                    {

                        if (fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                        {

                            var attachName = string.Empty;

                            long tick = DateTime.UtcNow.Ticks;
                            attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName);
                            Utilities.SaveFile(attachName, fd.FileStream);

                            attachName = "req" + tick + "_user" + requirement.CustomerID + "_" + fd.FileName;

                            Response res = Utilities.SaveAttachment(attachName);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }

                            fd.FileID = res.ObjectID;


                            if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = null;
                                var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), requirement.SessionID, attachment, filenameTemp);
                                if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                                {
                                    singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                }

                                CustomerListAttachment.Add(singleAttachment);
                                VendorListAttachment.Add(singleAttachment);
                            }

                        }
                        else if (fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), requirement.SessionID);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }

                            CustomerListAttachment.Add(singleAttachment);
                            VendorListAttachment.Add(singleAttachment);
                        }

                        fileName += Convert.ToString(fd.FileID) + ",";

                    }

                    fileName = fileName.Substring(0, fileName.Length - 1);
                }


                sd.Add("P_REQ_ID", requirement.RequirementID);
                sd.Add("P_REQ_TITLE", requirement.Title);
                sd.Add("P_REQ_DESC", requirement.Description);
                string categoryVal = string.Empty;
                foreach (string category in requirement.Category)
                {
                    categoryVal = category + ",";
                }
                if (!string.IsNullOrEmpty(categoryVal))
                {
                    categoryVal = categoryVal.Substring(0, categoryVal.Length - 1);

                }
                sd.Add("P_REQ_CATEGORY", categoryVal);
                sd.Add("P_REQ_URGENCY", requirement.Urgency);
                sd.Add("P_REQ_BUDGET", requirement.Budget);
                sd.Add("P_REQ_DELIVERY_LOC", requirement.DeliveryLocation);
                sd.Add("P_REQ_TAXES", "");
                sd.Add("P_REQ_PAYMENT_TERMS", requirement.PaymentTerms);
                sd.Add("P_CONT_START_TIME", requirement.ContractStartTime);
                sd.Add("P_CONT_END_TIME", requirement.ContractEndTime);
                sd.Add("P_REQ_SUBCATEGORIES", requirement.Subcategories);
                sd.Add("P_U_ID", requirement.CustomerID);
                sd.Add("P_CLOSED", requirement.IsClosed);
                sd.Add("P_REQ_ATTACHMENT", fileName);
                sd.Add("P_END_TIME", requirement.EndTime);
                sd.Add("P_START_TIME", requirement.StartTime);
                sd.Add("P_DELIVERY_TIME", requirement.DeliveryTime);
                requirement.IncludeFreight = true;
                sd.Add("P_INCLUDE_FREIGHT", Convert.ToInt16(requirement.IncludeFreight));
                requirement.InclusiveTax = true;
                sd.Add("P_INCLUSIVE_TAX", Convert.ToInt16(requirement.InclusiveTax));
                sd.Add("P_REQ_COMMENTS", requirement.ReqComments);
                if (requirement.QuotationFreezTime == DateTime.MaxValue || requirement.QuotationFreezTime == DateTime.MinValue)
                {
                    requirement.QuotationFreezTime = DateTime.MaxValue;
                }

                sd.Add("P_QUOTATION_FREEZ_TIME", requirement.QuotationFreezTime);
                sd.Add("P_REQ_CURRENCY", requirement.Currency);
                sd.Add("P_REQ_TIMEZONE", requirement.TimeZoneID);
                sd.Add("P_REQ_IS_TABULAR", Convert.ToInt16(requirement.IsTabular));
                sd.Add("P_IS_QUOTATION_PRICE_LIMIT", Convert.ToInt32(requirement.IsQuotationPriceLimit));
                sd.Add("P_QUOTATION_PRICE_LIMIT", requirement.QuotationPriceLimit);
                sd.Add("P_NO_OF_QUOTATION_REMINDERS", requirement.NoOfQuotationReminders);
                sd.Add("P_REMINDERS_TIME_INTERVAL", requirement.RemindersTimeInterval);
                sd.Add("P_INDENT_ID", requirement.IndentID);
                sd.Add("P_EXP_START_TIME", requirement.ExpStartTime);
                sd.Add("P_IS_DISCOUNT_QUOTATION", requirement.IsDiscountQuotation);
                int isUnitPriceBidding = 0;
                if (requirement.IsTabular)
                {
                    isUnitPriceBidding = 1;
                }

                sd.Add("P_IS_UNIT_PRICE_BIDDING", Convert.ToInt32(isUnitPriceBidding));

                sd.Add("P_CONTACT_DETAILS", requirement.ContactDetails);
                sd.Add("P_GENERAL_TC", requirement.GeneralTC);
                sd.Add("P_IS_REV_UNIT_DISCOUNT_ENABLE", requirement.IsRevUnitDiscountEnable);
                sd.Add("P_IS_CONTRACT", requirement.IsContract);
                sd.Add("P_PR_CODE", requirement.PrCode);
                sd.Add("P_PR_ID", requirement.PR_ID);
                sd.Add("P_IS_SLOT_CREATED", Convert.ToInt16(requirement.ShowInspectionValue == true ? 1 : 0)); 
                DataSet ds = sqlHelper.SelectList("fwd_RequirementSave", sd);
                Attachment emilAttachmentReqPDF = null;
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (requirement.RequirementID > 0 && requirement.DeleteQuotations)
                    {
                        string deleteQuotations = string.Format(
                            " DELETE FROM fwd_quotations " +
                            " WHERE REQ_ID = {0};" +

                            " UPDATE fwd_auctiondetails SET IS_ACCEPTED = 0, VEND_INIT_PRICE = 0, VEND_RUN_PRICE = 0, QUOTATION_URL = null," +
                            " VEND_TAXES = 0, VEND_TOTAL_PRICE = 0, VEND_TOTAL_PRICE_RUNNING = 0, IS_QUOTATION_REJECTED = -1," +
                            " QUOTATION_REJECTED_REASON = '', VEND_FREIGHT = 0, REV_VEND_FREIGHT = 0, WARRANTY = '', PAYMENT = '', DURATION = ''," +
                            " VALIDITY = '', DISCOUNT = 0, NO_OF_QUOTATION_REMINDERS_SENT = 0, OTHER_PROPERTIES = ''," +
                            " REV_PRICE = 0, REV_VEND_TOTAL_PRICE = 0, GST_NUMBER = '', MULTIPLE_ATTACHMENTS = null," +
                            " IS_ACCEPTED_TC = 0,PACKING_CHARGES = 0,PACKING_CHARGES_TAX_PERCENTAGE = 0,PACKING_CHARGES_WITH_TAX = 0," +
                            " INSTALLATION_CHARGES = 0,INSTALLATION_CHARGES_TAX_PERCENTAGE = 0,INSTALLATION_CHARGES_WITH_TAX = 0, " +
                            " SELECTED_VENDOR_CURRENCY = '', " +
                            " REV_PRICE_CB = 0, " +
                            " REV_VEND_TOTAL_PRICE_CB = 0, " +
                            " REV_VEND_FREIGHT_CB = 0, " +
                            " VEND_TOTAL_PRICE_RUNNING_CB = 0, " +
                            " REV_PACKING_CHARGES_CB = 0, " +
                            " REV_INSTALLATION_CHARGES_CB = 0, " +
                            " VEND_REVISED_TOTAL_PRICE_CB = 0, " +
                            " VEND_FREIGHT_CB = 0, " +
                            " FREEZE_CB = 0, " +
                            " VEND_FREEZE_CB = 0, " +
                            " FREIGHT_CHARGES = 0, " +
                            " FREIGHT_CHARGES_TAX_PERCENTAGE = 0, " +
                            " FREIGHT_CHARGES_WITH_TAX = 0, " +
                            " REV_FREIGHT_CHARGES = 0, " +
                            " REV_FREIGHT_CHARGES_WITH_TAX = 0, " +
                            " PACKING_CHARGES_CB = 0, " +
                            " FREIGHT_CHARGES_CB = 0, " +
                            " REV_FREIGHT_CHARGES_CB = 0," +
                            " INCO_TERMS = '' " +
                            " WHERE REQ_ID = {0};" +

                            " DELETE FROM fwd_quotations " +
                            " WHERE REQ_ID = {0}", requirement.RequirementID);
                        DataSet ds1 = sqlHelper.ExecuteQuery(deleteQuotations);
                    }
                }

                if (response.ObjectID > 0 && requirement.ListSlotDetails.Count > 0 && requirement.ShowInspectionValue)
                {
                    foreach (SlotDetails slotItem in requirement.ListSlotDetails)
                    {
                        PRMSlotService slotResponse1 = new PRMSlotService();
                        if (requirement.CloneID > 0)
                        {
                            slotItem.Slot_id = 0;
                        }
                        slotItem.Slot_entity_id = response.ObjectID;
                        slotItem.Slot_entity_type = "REQ";
                        slotItem.Slot_is_valid = 1;
                        slotItem.Slot_flag = "CREATE";
                        slotResponse1.SaveSlotDetails(slotItem, requirement.SessionID, requirement.CustomerID);
                    }
                }

                string quotationItems = string.Empty;
                if (requirement.ItemsAttachment == null)
                {
                    foreach (FwdRequirementItems Item in requirement.ListFwdRequirementItems)
                    {
                        Response response2 = new Response();
                        response2 = SaveRequirementItems(Item, response.ObjectID, requirement.SessionID, requirement.CustomerID);

                        if (response2.ErrorMessage == "")
                        {
                            if (requirement.IsTabular)
                            {
                                string xml = string.Empty;
                                xml = Utilities.GenerateForwardEmailBody("SaveRequirementItemsXML");
                                xml = String.Format(xml, Item.ProductIDorName, Item.ProductNo, Item.ProductDescription, Item.ProductQuantity, Item.ProductBrand, Item.OthersBrands);
                                quotationItems += xml;
                            }
                        }
                    }
                }
                else
                {
                    DataTable currentData = new DataTable();
                    string sheetName = string.Empty;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        ms.Write(requirement.ItemsAttachment, 0, requirement.ItemsAttachment.Length);
                        using (ExcelPackage package = new ExcelPackage(ms))
                        {
                            currentData = Utilities.WorksheetToDataTable(package.Workbook.Worksheets[1]);
                            sheetName = package.Workbook.Worksheets[1].Name;
                        }
                    }

                    if (sheetName.Equals("RequirementDetails", StringComparison.InvariantCultureIgnoreCase))
                    {
                        int count = 1;
                        foreach (DataRow row in currentData.Rows)
                        {
                            try
                            {
                                Response response2 = new Response();
                                FwdRequirementItems item = new FwdRequirementItems();
                                item.ItemID = (row.IsNull("ItemID") || row["ItemID"] == DBNull.Value || string.IsNullOrEmpty(row["ItemID"].ToString().Trim())) ? 0 : Convert.ToInt32(row["ItemID"].ToString().Trim());
                                item.ProductIDorName = (row.IsNull("ProductName") || row["ProductName"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductName"]).Trim();
                                item.ProductNo = (row.IsNull("ProductNumber") || row["ProductNumber"] == DBNull.Value) ? string.Empty : Convert.ToString(row["ProductNumber"]).Trim();
                                item.ProductDescription = (row.IsNull("Description") || row["Description"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Description"]).Trim();
                                item.ProductQuantity = (row.IsNull("Quantity") || row["Quantity"] == DBNull.Value || string.IsNullOrEmpty(row["Quantity"].ToString().Trim())) ? 0 : Convert.ToDouble(row["Quantity"].ToString().Trim());
                                item.ProductQuantityIn = (row.IsNull("Units") || row["Units"] == DBNull.Value) ? string.Empty : Convert.ToString(row["Units"]).Trim();
                                item.ProductBrand = (row.IsNull("PreferredBrand") || row["PreferredBrand"] == DBNull.Value) ? string.Empty : Convert.ToString(row["PreferredBrand"]).Trim();
                                item.OthersBrands = (row.IsNull("OtherBrands") || row["OtherBrands"] == DBNull.Value) ? string.Empty : Convert.ToString(row["OtherBrands"]).Trim();
                                response2 = SaveRequirementItems(item, response.ObjectID, requirement.SessionID, requirement.CustomerID);
                                count++;
                                if (response2.ErrorMessage == string.Empty)
                                {
                                    if (requirement.IsTabular)
                                    {
                                        string xml = string.Empty;
                                        xml = Utilities.GenerateForwardEmailBody("SaveRequirementItemsXML");
                                        xml = String.Format(xml, item.ProductIDorName, item.ProductNo, item.ProductDescription, item.ProductQuantity, item.ProductBrand, item.OthersBrands);
                                        quotationItems += xml;
                                    }
                                }
                            }
                            catch
                            {
                                response.Message += "Row No. " + count + " has invalid values. Please check the excel sheet you uploaded.";
                                continue;
                            }
                        }
                    }
                }

                if (requirement.AuctionVendors.Count > 0)
                {
                    foreach (VendorDetails vendor in requirement.AuctionVendors)
                    {
                        Response response1 = new Response();
                        response1 = AddVendorToAuction(vendor, response.ObjectID, requirement.SessionID, requirement.CustomerID);
                    }
                }

                customerDetails = prmServices.GetUserDetails(requirement.CustomerID, requirement.SessionID);
                if (requirement.IsSubmit == 1)
                {
                    long ticks1 = DateTime.UtcNow.Ticks;
                    string PDFfileName = string.Empty;
                    int margin = 24;
                    PDFfileName = "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf";
                    filenameTemp = "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf";
                    int flag = 0;
                    List<KeyValuePair<string, DataTable>> dtbl = FwdPdfUtility.MakeDataTableForFwdRequirement(response.ObjectID, requirement, quotationItems, customerDetails, folderPath, flag);
                    FwdPdfUtility.ExportDataTableToPdfForFwdRequirement(dtbl, @folderPath + "reqPDF" + response.ObjectID + "_" + ticks1 + ".pdf", response.ObjectID, requirement, flag);
                    Response responce = Utilities.SaveAttachment(PDFfileName);
                    PDFfileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveFwdAttachments(response.ObjectID, requirement.SessionID, requirement.CustomerID, PDFfileName, 0, "PDFREQUIREMENT");
                    if (PDFfileName != "")
                    {
                        var fileData = Utilities.DownloadFile(PDFfileName, requirement.SessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentReqPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                if (requirement.AuctionVendors.Count > 0)
                {
                    UserInfo customerInfo = prmServices.GetUserNew(requirement.CustomerID, requirement.SessionID);
                    foreach (VendorDetails vendor in requirement.AuctionVendors)
                    {

                        Response response1 = new Response();
                        response1.ObjectID = response.ObjectID;
                        response1.ErrorMessage = "";

                        if (requirement.RequirementID < 1)
                        {
                            requirement.Module = reqPosting;
                        }
                        else if (response1.ObjectID < 0)
                        {
                            requirement.Module = reqPosting;
                        }
                        else
                        {
                            requirement.Module = editReq;
                        }

                        if (ds.Tables[0].Rows[0][1] != null && response1.ErrorMessage == "" && requirement.IsSubmit == 1)
                        {
                            string body = string.Empty;
                            UserInfo user = prmServices.GetUserNew(vendor.VendorID, requirement.SessionID);
                            User altUser = GetAlternateCommunications(response.ObjectID, vendor.VendorID, requirement.SessionID);
                            if (requirement.RequirementID < 1)
                            {
                                if (requirement.CheckBoxEmail == true)
                                {
                                    body = Utilities.GenerateForwardEmailBody("Vendoremail");
                                    body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), customerInfo.Institution, toLocal(requirement.ExpStartTime), user.PhoneNum);
                                    Utilities.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Requirement -ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, toLocal(requirement.QuotationFreezTime), negotationDuration).ConfigureAwait(false);
                                }
                                string body1 = Utilities.GenerateForwardEmailBody("Vendorsms");
                                if (requirement.CheckBoxSms == true)
                                {

                                    body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), customerInfo.Institution, toLocal(requirement.ExpStartTime), user.PhoneNum);
                                    body1 = body1.Replace("<br/>", "");
                                   // Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID).ConfigureAwait(false);
                                }
                            }
                            else
                            {
                                if (response1.ObjectID < 0)
                                {

                                    string ScreenName = "ADD_REQUIREMENT";
                                    if (requirement.CheckBoxEmail == true)
                                    {
                                        body = Utilities.GenerateForwardEmailBody("Vendoremail");
                                        body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), customerInfo.Institution, toLocal(requirement.ExpStartTime), user.PhoneNum);
                                        Utilities.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "You have a new Reqquirement - ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);
                                    }
                                    string body1 = Utilities.GenerateForwardEmailBody("Vendorsms");
                                    if (requirement.CheckBoxSms == true)
                                    {
                                        body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), customerInfo.Institution, toLocal(requirement.ExpStartTime), user.PhoneNum);
                                        body1 = body1.Replace("<br/>", "");
                                       // Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID).ConfigureAwait(false);
                                    }
                                }
                                else
                                {
                                    if (requirement.CheckBoxEmail == true)
                                    {
                                        body = Utilities.GenerateForwardEmailBody("VendoremailForReqUpdate");
                                        body = String.Format(body, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), toLocal(requirement.ExpStartTime));
                                        Utilities.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "Requirement details updated for ReqID: " + response.ObjectID + " Title: " + requirement.Title, body, response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID, emilAttachmentReqPDF, VendorListAttachment, toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);

                                    }
                                    if (requirement.CheckBoxSms == true)
                                    {
                                        string ScreenName = "UPDATE_REQUIREMENT";
                                        string body1 = Utilities.GenerateForwardEmailBody("VendorsmsForReqUpdate");
                                        body1 = String.Format(body1, user.FirstName, user.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), toLocal(requirement.ExpStartTime));
                                        body1 = body1.Replace("<br/>", "");
                                      //  Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, vendor.VendorID, requirement.Module, requirement.SessionID).ConfigureAwait(false);
                                    }
                                }
                            }
                        }
                        if (response1.ErrorMessage != "")
                        {
                            throw new Exception(response1.ErrorMessage);
                        }
                    }
                }

                Attachment emilAttachmentReqPDFCustomer = null;
                if (requirement.IsSubmit == 1)
                {
                    long ticks2 = DateTime.UtcNow.Ticks;
                    string CustomerPDFfileName = string.Empty;
                    CustomerPDFfileName = "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf";
                    filenameTemp = "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf";
                    int flag = 1;
                    List<KeyValuePair<string, DataTable>> dtbl = FwdPdfUtility.MakeDataTableForFwdRequirement(response.ObjectID, requirement, quotationItems, customerDetails, folderPath, flag);
                    FwdPdfUtility.ExportDataTableToPdfForFwdRequirement(dtbl, @folderPath + "CustomerReqPDF" + response.ObjectID + "_" + ticks2 + ".pdf", response.ObjectID, requirement, flag);
                    Response responce = Utilities.SaveAttachment(CustomerPDFfileName);
                    CustomerPDFfileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveFwdAttachments(response.ObjectID, requirement.SessionID, requirement.CustomerID, CustomerPDFfileName, 0, "REQ_PDF_CUSTOMER");
                    if (CustomerPDFfileName != "")
                    {
                        var fileData = Utilities.DownloadFile(CustomerPDFfileName, requirement.SessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentReqPDFCustomer = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                if (requirement.IsSubmit == 1)
                {
                    FwdRequirement req = GetRequirementDataOfflinePrivate(response.ObjectID, requirement.CustomerID, requirement.SessionID);
                    UserInfo customer = prmServices.GetUserNew(req.CustomerID, requirement.SessionID);
                    UserInfo superUser = prmServices.GetSuperUser(req.SuperUserID, requirement.SessionID);
                    string emailBody = string.Empty;
                    emailBody = Utilities.GenerateForwardEmailBody("PostRequirementemail");
                    string emailBody1 = String.Format(emailBody, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), requirement.Currency, toLocal(requirement.ExpStartTime));
                    string emailBody2 = String.Format(emailBody, superUser.FirstName, superUser.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), requirement.Currency, toLocal(requirement.ExpStartTime));
                    Utilities.SendEmail(customer.Email + "," + customer.AltEmail, "Your Requirement has been successfully posted! ReqID: " + response.ObjectID + " Title: " + requirement.Title, emailBody1, response.ObjectID, req.CustomerID, requirement.Module, requirement.SessionID, emilAttachmentReqPDFCustomer, CustomerListAttachment, toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);
                    string body2 = Utilities.GenerateForwardEmailBody("PostRequirementsms");
                    string body4 = String.Format(body2, customer.FirstName, customer.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), requirement.Currency, toLocal(requirement.ExpStartTime));
                    string body3 = String.Format(body2, superUser.FirstName, superUser.LastName, response.ObjectID, requirement.Title, toLocal(requirement.QuotationFreezTime), requirement.Currency, toLocal(requirement.ExpStartTime));
                    body3 = body3.Replace("<br/>", "");
                    body4 = body4.Replace("<br/>", "");
                   // Utilities.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body4.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, req.CustomerID, requirement.Module, requirement.SessionID).ConfigureAwait(false);
                    string bodyTelegram1 = Utilities.GenerateForwardEmailBody("SaveRequirementTelegramsms");
                    string bodyTelegram = String.Format(bodyTelegram1, req.CustomerCompanyName, req.CustFirstName, req.CustLastName, req.Title, req.Description, req.PaymentTerms, req.Budget, req.DeliveryTime, toLocal(requirement.QuotationFreezTime), requirement.Currency, toLocal(requirement.ExpStartTime));
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");
                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = "REQUIREMENT POSTED: REQ ID :" + response.ObjectID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                    Utilities.SendTelegramMsg(tgMsg);

                    if (req.SuperUserID != req.CustomerID)
                    {
                        string ScreenName = "ADD_REQUIREMENT";
                        Utilities.SendEmail(superUser.Email + "," + superUser.AltEmail, "Your Requirement has been successfully posted! ReqID: " + response.ObjectID + " Title: " + requirement.Title, emailBody2, response.ObjectID, req.SuperUserID, requirement.Module, requirement.SessionID, emilAttachmentReqPDFCustomer, CustomerListAttachment, toLocal(requirement.QuotationFreezTime), 15, "QUOTATION_FREEZE").ConfigureAwait(false);
                       // Utilities.SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body3.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), response.ObjectID, req.CustomerID, requirement.Module, requirement.SessionID).ConfigureAwait(false);
                    }
                }

                int remindersCount = 0;
                DateTime inputQuotationFreezeTime = DateTime.UtcNow;
                if (requirement.IsSubmit == 1)
                {
                    remindersCount = requirement.NoOfQuotationReminders;
                    DateTime createdQuotationFreezTime = Convert.ToDateTime(requirement.QuotationFreezTime); ;
                    inputQuotationFreezeTime = Convert.ToDateTime(requirement.QuotationFreezTime);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public FwdRequirement GetRequirementDataOfflinePrivate(int reqID, int userID, string sessionID)
        {
            FwdRequirement requirement = new FwdRequirement();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = reqID;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt16(row["IS_PO_SENT"]) : 0;
                    //requirement.AttachmentName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToBase64String((byte[])row["REQ_ATTACHMENT"]) : string.Empty;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = URL;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;

                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;

                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;

                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;

                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                    DateTime now = DateTime.UtcNow;
                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();

                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;

                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            //vendor.DeliveryTime = row1["DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row1["DELIVERY_TIME"]) : string.Empty;

                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            //vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;

                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;

                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;

                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();

                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";

                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;

                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;
                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            //vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;

                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            //vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;

                            vendor.Vendor = new User();

                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;

                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;

                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;

                            vendor.BASE_PRICE = row1["BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["BASE_PRICE"]) : 0;

                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, 2);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, 2);

                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);

                            vendor.Rank = rank;

                            vendorDetails.Add(vendor);
                            rank++;
                        }
                    }


                    if (requirement.IsDiscountQuotation == 2)
                    {
                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;

                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }


                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, 2);
                    }

                    requirement.AuctionVendors = vendorDetails;
                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());

                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.BASE_PRICE == 0).ToList());

                    }
                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.BASE_PRICE == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            //requirement.Price = vendor1.BASE_PRICE;
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);

                    requirement.CustomerReqAccess = false;

                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }


                    int productSNo = 0;

                    List<FwdRequirementItems> ListRequirementItems = new List<FwdRequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {

                            FwdRequirementItems RequirementItems = new FwdRequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;

                            ListRequirementItems.Add(RequirementItems);
                        }
                    }


                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {

                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;


                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductBrand = brand;
                            }
                        }
                    }

                    requirement.ListFwdRequirementItems = ListRequirementItems;                    
                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {
                        foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())
                        {
                            vendor.Rank = v2.IndexOf(vendor) + 1;
                        }
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2;
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                        }
                    }
                    else
                    {
                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();
                        foreach (VendorDetails vendor in v2)
                        {

                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = v2.IndexOf(vendor) + 1;
                            }
                        }


                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.Rank).ToList();
                            foreach (VendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ToList();
                        }

                        requirement.AuctionVendors = v2;
                    }

                    int taxSNo = 0;
                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;

                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }
                        requirement.TaxSNoCount = taxSNo;
                    }
                    requirement.ListRequirementTaxes = ListRequirementTaxes;

                }
            }
            catch (Exception ex)
            {
                FwdRequirement req = new FwdRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public bool isPOGenerated(int reqID)
        {
            bool response = false;
            try
            {
                string query = string.Format("SELECT count(REQ_ID) FROM POINFORMATION WHERE REQ_ID={0}" +
                                                ";", reqID);
                DataSet ds = sqlHelper.ExecuteQuery(query);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    int result = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (result > 0)
                    {
                        response = true;
                    }
                    else
                    {
                        response = false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return response;

        }

        public Response EndNegotiation(int reqID, int userID, string sessionID)
        {
            int CallerID = userID;
            Response response = new Response();
            string ScreenName = "END_NEGOTIATION";
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                FwdRequirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_EndNegotiation", sd);
                Response response1 = new Response();
                response = Utilities.GetSessionId(req.CustomerID);
                response1 = response;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        InvolvedParties involvedParties = new InvolvedParties();
                        involvedParties.RequirementID = req.RequirementID;
                        involvedParties.CustomerID = req.CustomerID;
                        involvedParties.SuperUserID = req.SuperUserID;
                        involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                        involvedParties.UserRunngPriceList = req.AuctionVendors.Select(x => x.RunningPrice).ToArray();
                        involvedParties.VendorName = req.AuctionVendors.Select(x => x.VendorName).ToArray();
                        involvedParties.MethodName = "EndNegotiation";
                        involvedParties.CallerID = CallerID;
                        involvedParties.CustCompID = req.CustCompID;
                        req.Inv = involvedParties;
                        var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                        context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                        context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                        foreach (int vendorID in involvedParties.UserIDList)
                        {
                            req = GetRequirementData(reqID, vendorID, sessionID);
                            context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                        }

                        FwdRequirement requirement = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);

                        UserInfo customer = prmServices.GetUser(requirement.CustomerID, sessionID);

                        int id1 = involvedParties.UserIDList[0];
                        double UserRunngPriceList = involvedParties.UserRunngPriceList[0];
                       // double minPriceBefore = req.AuctionVendors.Where(x => x.InitialPrice > 0).ToList<VendorDetails>().Min(x => x.InitialPrice);
                        string VendorName = involvedParties.VendorName[0];
                        double SavedAmount = requirement.Savings;
                        DateTime endTime = (DateTime)requirement.EndTime;

                        requirement.Module = ScreenName;

                        if (endTime.Date == DateTime.UtcNow.Date)
                        {
                            string body = Utilities.GenerateForwardEmailBody("CustomeremailForEndNegotiation");
                            body = String.Format(body, customer.FirstName, customer.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);

                            Utilities.SendEmail(customer.Email + "," + customer.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Negotiation is completed successfully", body, reqID, requirement.CustomerID, requirement.Module, sessionID).ConfigureAwait(false);

                            string body1 = Utilities.GenerateForwardEmailBody("CustomersmsForEndNegotiation");
                            body1 = String.Format(body1, customer.FirstName, customer.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);
                            body1 = body1.Replace("<br/>", "");
                           // Utilities.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body1.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, requirement.CustomerID, requirement.Module, response1.Message).ConfigureAwait(false);
                            UserInfo superUser = prmServices.GetSuperUser(requirement.CustomerID, sessionID);
                            string bodySuper = Utilities.GenerateForwardEmailBody("CustomeremailForEndNegotiation");
                            bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);

                            string body2Super = Utilities.GenerateForwardEmailBody("CustomersmsForEndNegotiation");
                            body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, requirement.Title, VendorName, UserRunngPriceList, SavedAmount, reqID);
                            body2Super = body2Super.Replace("<br/>", "");

                            string bodyTelegram = Utilities.GenerateForwardEmailBody("EndNegotiationTelegramsms");
                            bodyTelegram = String.Format(bodyTelegram, superUser.Institution, superUser.FirstName, superUser.LastName, requirement.Title, requirement.Description, SavedAmount);
                            bodyTelegram = bodyTelegram.Replace("<br/>", "");

                            TelegramMsg tgMsg = new TelegramMsg();
                            tgMsg.Message = "END NEGOTITATION: Req ID : " + reqID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                            Utilities.SendTelegramMsg(tgMsg);

                            string subUserID = req.CustomerID.ToString();

                            if (superUser.UserID != subUserID)
                            {
                                Utilities.SendEmail(superUser.Email + "," + superUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Negotiation is completed successfully", bodySuper, reqID, req.CustomerID, requirement.Module, response1.Message).ConfigureAwait(false);
                              //  Utilities.SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, req.CustomerID, requirement.Module, response1.Message).ConfigureAwait(false);                                
                            }

                            foreach (int id in involvedParties.UserIDList)
                            {
                                UserInfo user = prmServices.GetUser(id, sessionID);
                                User altUser = GetAlternateCommunications(reqID, id, sessionID);
                                if (id != id1)
                                {
                                    string body2 = Utilities.GenerateForwardEmailBody("VendoremailForEndNegotiation");
                                    body2 = String.Format(body2, user.FirstName, user.LastName, requirement.Title, reqID);
                                    Utilities.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Negotiation is completed successfully", body2, reqID, id, requirement.Module, response1.Message).ConfigureAwait(false);
                                    string body3 = Utilities.GenerateForwardEmailBody("VendorsmsForEndNegotiation");
                                    body3 = String.Format(body3, user.FirstName, user.LastName, requirement.Title, reqID);
                                    body3 = body3.Replace("<br/>", "");
                                   // Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body3.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, id, requirement.Module, response1.Message).ConfigureAwait(false);                                    
                                }
                                else
                                {

                                    string body2 = Utilities.GenerateForwardEmailBody("Vendor1emailForEndNegotiation");
                                    body2 = String.Format(body2, user.FirstName, user.LastName, requirement.Title, UserRunngPriceList, reqID);
                                    Utilities.SendEmail(user.Email + "," + user.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - Negotiation is completed successfully", body2, reqID, Convert.ToInt32(user.UserID), requirement.Module, response1.Message).ConfigureAwait(false);
                                    string body3 = Utilities.GenerateForwardEmailBody("Vendor1smsForEndNegotiation");
                                    body3 = String.Format(body3, user.FirstName, user.LastName, requirement.Title, UserRunngPriceList, reqID);
                                    body3 = body3.Replace("<br/>", "");
                                  //  Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body3.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(user.UserID), requirement.Module, response1.Message).ConfigureAwait(false);                                    
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public FwdRequirement GetRequirementDataFilter(int userID, string sessionID, DataSet ds)
        {
            FwdRequirement requirement = new FwdRequirement();
            requirement.EntityID = userID;
            try
            {
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    string[] arr = new string[] { };
                    byte[] test = new byte[] { };
                    requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                    requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                    requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                    requirement.Subcategories = row["REQ_SUBCATEGORIES"] != DBNull.Value ? Convert.ToString(row["REQ_SUBCATEGORIES"]) : string.Empty;
                    requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                    requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                    requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                    requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                    requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                    requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                    requirement.Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                    requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                    requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                    requirement.CustFirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.CustLastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                    requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                    int isPOSent = row["IS_PO_SENT"] != DBNull.Value ? Convert.ToInt32(row["IS_PO_SENT"]) : 0;
                    string fileName = row["REQ_ATTACHMENT"] != DBNull.Value ? Convert.ToString(row["REQ_ATTACHMENT"]) : string.Empty;

                    string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                    requirement.POLink = !string.IsNullOrEmpty(POLink) ? POLink : string.Empty;
                    string URL = fileName != string.Empty ? fileName : string.Empty;
                    requirement.AttachmentName = fileName;
                    requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                    requirement.InclusiveTax = row["REQ_INCLUSIVE_TAX"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUSIVE_TAX"]) == 1 ? true : false) : false;
                    requirement.IncludeFreight = row["REQ_INCLUDE_FREIGHT"] != DBNull.Value ? (Convert.ToInt32(row["REQ_INCLUDE_FREIGHT"]) == 1 ? true : false) : false;
                    requirement.SuperUserID = row["SUPER_U_ID"] != DBNull.Value ? Convert.ToInt32(row["SUPER_U_ID"]) : requirement.CustomerID;
                    requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row["SELECTED_VENDOR_ID"]) : requirement.CustomerID;
                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.MinReduceAmount = row["MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["MIN_REDUCE_AMOUNT"]) : 0;
                    requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                    requirement.IsTabular = row["REQ_IS_TABULAR"] != DBNull.Value ? (Convert.ToInt32(row["REQ_IS_TABULAR"]) == 1 ? true : false) : false;
                    requirement.ReqComments = row["REQ_COMMENTS"] != DBNull.Value ? Convert.ToString(row["REQ_COMMENTS"]) : string.Empty;

                    requirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.MaxValue;

                    if (requirement.MinReduceAmount > 0)
                    {
                        requirement.MinBidAmount = requirement.MinReduceAmount;
                    }
                    else
                    {
                        requirement.MinBidAmount = 0;
                    }

                    requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                    requirement.IsStopped = row["IS_STOPPED"] != DBNull.Value ? (Convert.ToInt32(row["IS_STOPPED"]) == 1 ? true : false) : false;
                    requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["SAVINGS"]) : 0;
                    requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                    requirement.TimeZone = row["REQ_TIMEZONE"] != DBNull.Value ? Convert.ToString(row["REQ_TIMEZONE"]) : string.Empty;
                    requirement.IsNegotiationEnded = row["IS_NEGOTIATION_ENDED"] != DBNull.Value ? Convert.ToInt32(row["IS_NEGOTIATION_ENDED"]) : 0;
                    requirement.NegotiationDuration = row["NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["NEGOTIATION_DURATION"]) : string.Empty;
                    requirement.MinVendorComparision = row["VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToInt32(row["VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings NegotiationSettings = new NegotiationSettings();

                    NegotiationSettings.MinReductionAmount = row["UD_MIN_REDUCE_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_MIN_REDUCE_AMOUNT"]) : 0;
                    NegotiationSettings.RankComparision = row["UD_VENDOR_COMPARISION_MIN_AMOUNT"] != DBNull.Value ? Convert.ToDouble(row["UD_VENDOR_COMPARISION_MIN_AMOUNT"]) : 0;
                    NegotiationSettings.NegotiationDuration = row["UD_NEGOTIATION_DURATION"] != DBNull.Value ? Convert.ToString(row["UD_NEGOTIATION_DURATION"]) : string.Empty;

                    requirement.ReqPDF = row["REQ_PDF"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF"]) : 0;
                    requirement.ReqPDFCustomer = row["REQ_PDF_CUSTOMER"] != DBNull.Value ? Convert.ToInt32(row["REQ_PDF_CUSTOMER"]) : 0;

                    requirement.ReportReq = row["REPORT_REQ"] != DBNull.Value ? Convert.ToInt32(row["REPORT_REQ"]) : 0;
                    requirement.ReportItemWise = row["REPORT_ITEM_WISE"] != DBNull.Value ? Convert.ToInt32(row["REPORT_ITEM_WISE"]) : 0;

                    requirement.ReqType = row["PARAM_REQ_TYPE"] != DBNull.Value ? Convert.ToString(row["PARAM_REQ_TYPE"]) : string.Empty;
                    requirement.PriceCapValue = row["PARAM_PRICE_CAP_VALUE"] != DBNull.Value ? Convert.ToDouble(row["PARAM_PRICE_CAP_VALUE"]) : 0;

                    requirement.CustCompID = row["CUST_COMP_ID"] != DBNull.Value ? Convert.ToInt32(row["CUST_COMP_ID"]) : 0;

                    requirement.IsQuotationPriceLimit = row["IS_QUOTATION_PRICE_LIMIT"] != DBNull.Value ? (Convert.ToInt32(row["IS_QUOTATION_PRICE_LIMIT"]) == 1 ? true : false) : false;
                    requirement.QuotationPriceLimit = row["QUOTATION_PRICE_LIMIT"] != DBNull.Value ? Convert.ToInt32(row["QUOTATION_PRICE_LIMIT"]) : 0;
                    requirement.NoOfQuotationReminders = row["NO_OF_QUOTATION_REMINDERS"] != DBNull.Value ? Convert.ToInt32(row["NO_OF_QUOTATION_REMINDERS"]) : 0;
                    requirement.RemindersTimeInterval = row["REMINDERS_TIME_INTERVAL"] != DBNull.Value ? Convert.ToInt32(row["REMINDERS_TIME_INTERVAL"]) : 0;

                    requirement.SuperUser = new User();
                    requirement.PostedUser = new User();
                    requirement.PostedUser.UserInfo = new UserInfo();

                    requirement.SuperUser.FirstName = row["SUPER_USER_NAME"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_NAME"]) : string.Empty;
                    requirement.SuperUser.Email = row["SUPER_USER_EMAIL"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_EMAIL"]) : string.Empty;
                    requirement.SuperUser.PhoneNum = row["SUPER_USER_PHONE"] != DBNull.Value ? Convert.ToString(row["SUPER_USER_PHONE"]) : string.Empty;

                    requirement.PostedUser.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                    requirement.PostedUser.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                    requirement.PostedUser.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                    requirement.PostedUser.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                    requirement.PostedUser.UserInfo.Address = row["ADDRESS"] != DBNull.Value ? Convert.ToString(row["ADDRESS"]) : string.Empty;

                    requirement.IsUnitPriceBidding = row["IS_UNIT_PRICE_BIDDING"] != DBNull.Value ? Convert.ToInt32(row["IS_UNIT_PRICE_BIDDING"]) : 0;

                    requirement.MaterialDispachmentLink = row["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row["MAT_DIS_LINK"]) : 0;
                    requirement.MaterialReceivedLink = row["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row["MR_LINK"]) : 0;

                    requirement.IndentID = row["INDENT_ID"] != DBNull.Value ? Convert.ToInt32(row["INDENT_ID"]) : 0;

                    requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt32(row["IS_DISCOUNT_QUOTATION"]) : 0;
                    requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                    requirement.ContactDetails = row["CONTACT_DETAILS"] != DBNull.Value ? Convert.ToString(row["CONTACT_DETAILS"]) : string.Empty;
                    requirement.GeneralTC = row["GENERAL_TC"] != DBNull.Value ? Convert.ToString(row["GENERAL_TC"]) : string.Empty;

                    requirement.ContractStartTime = row["CONT_START_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_START_TIME"]) : string.Empty;

                    requirement.ContractEndTime = row["CONT_END_TIME"] != DBNull.Value ? Convert.ToString(row["CONT_END_TIME"]) : string.Empty;
                    requirement.IsContract = row["IS_CONTRACT"] != DBNull.Value ? (Convert.ToInt32(row["IS_CONTRACT"]) == 1 ? true : false) : false;

                    requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;

                    requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.MinValue;

                    requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                    requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;

                    requirement.LAST_BID_ID = row["LAST_BID_ID"] != DBNull.Value ? Convert.ToInt32(row["LAST_BID_ID"]) : 0;

                    int slot_created_temp = row["IS_SLOT_CREATED"] != DBNull.Value ? Convert.ToInt32(row["IS_SLOT_CREATED"]) : 0;

                    requirement.ShowInspectionValue = slot_created_temp == 1 ? true : false;

                    requirement.NegotiationSettings = NegotiationSettings;
                    DateTime now = DateTime.UtcNow;
                    requirement.DATETIME_NOW = now;
                    DateTime DATE_CB_END_TIME = Convert.ToDateTime(requirement.CB_END_TIME);
                    long DIFF_CB_END_TIME = Convert.ToInt64((DATE_CB_END_TIME - now).TotalSeconds);

                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }

                    if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.EndTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                    }
                    else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                    {
                        DateTime date = Convert.ToDateTime(requirement.StartTime);
                        long diff = Convert.ToInt64((date - now).TotalSeconds);
                        requirement.TimeLeft = diff;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                    }
                    else if (requirement.EndTime < now)
                    {
                        requirement.TimeLeft = -1;
                        if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && requirement.EndTime < now)
                        {
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                            EndNegotiation(requirement.RequirementID, requirement.CustomerID, sessionID);
                        }
                        else
                        {
                            requirement.Status = Status;
                        }
                    }
                    else if (requirement.StartTime == DateTime.MaxValue)
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                    }
                    if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                    {
                        requirement.TimeLeft = -1;
                        requirement.Status = Status;
                    }

                    if (DIFF_CB_END_TIME > 0)
                    {
                        requirement.CB_TIME_LEFT = DIFF_CB_END_TIME;
                        requirement.TimeLeft = DIFF_CB_END_TIME;
                    }
                    else
                    {
                        requirement.CB_TIME_LEFT = 0;
                        requirement.CB_END_TIME = now;
                    }

                    PRMSlotService prmSlot = new PRMSlotService();
                    requirement.ListSlotDetails = prmSlot.GetSlotDetails(requirement.RequirementID, sessionID);
                    requirement.ListBookedSlots = prmSlot.GetBookedSlotDetails(requirement.RequirementID, sessionID);

                    SlotBooking BookedSlots = new SlotBooking();
                    if (requirement.ListBookedSlots != null && requirement.ListBookedSlots.Count > 0)
                    {
                        BookedSlots = requirement.ListBookedSlots.Where(v => v.Slot_vendor_id == userID).FirstOrDefault();
                    }
                    if (requirement.ListSlotDetails.Count > 0 && BookedSlots != null && BookedSlots.Slot_id > 0)
                    {
                        foreach (SlotDetails slots in requirement.ListSlotDetails)
                        {
                            if (BookedSlots != null && BookedSlots.Slot_id > 0)
                            {
                                slots.IsVendorBooked = true;
                            }
                            if (BookedSlots.Slot_id == slots.Slot_id)
                            {
                                slots.VendorSlot = true;
                            }
                            else
                            {
                                slots.VendorSlot = false;
                            }
                        }
                    }

                    foreach (SlotDetails slot in requirement.ListSlotDetails)
                    {
                        if (slot.VENDORS_SLOT_APPROVED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "APPROVED";
                        }
                        else if (slot.VENDORS_SLOT_REJECTED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "REJECTED";
                        }
                        else if (slot.VENDORS_SLOT_CANCELED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "CANCELLED";
                        }
                        else if (slot.VENDORS_SLOT_BOOKED.Contains(userID.ToString()))
                        {
                            slot.VENDOR_SLOT_STATUS = "BOOKED";
                        }
                        else
                        {
                            slot.VENDOR_SLOT_STATUS = "NA";
                        }
                    }


                    List<VendorDetails> vendorDetails = new List<VendorDetails>();
                    int rank = 1;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row1 in ds.Tables[1].Rows)
                        {
                            VendorDetails vendor = new VendorDetails();
                            vendor.PO = new RequirementPO();
                            vendor.ListCurrencies = new List<KeyValuePair>();
                            vendor.VendorID = row1["U_ID"] != DBNull.Value ? Convert.ToInt32(row1["U_ID"]) : 0;
                            vendor.VendorName = row1["VENDOR_NAME"] != DBNull.Value ? Convert.ToString(row1["VENDOR_NAME"]) : string.Empty;
                            vendor.InitialPrice = row1["VEND_INIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE"]) : 0;
                            vendor.RunningPrice = row1["VEND_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_RUN_PRICE"]) : 0;
                            vendor.TotalInitialPrice = row1["VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE"]) : 0;
                            vendor.TotalRunningPrice = row1["VEND_TOTAL_PRICE_RUNNING"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING"]) : 0;
                            vendor.City = row1["UAD_CITY"] != DBNull.Value ? Convert.ToString(row1["UAD_CITY"]) : string.Empty;
                            vendor.Taxes = row1["TAXES"] != DBNull.Value ? Convert.ToDouble(row1["TAXES"]) : 0;
                            vendor.Rating = row1["RATING"] != DBNull.Value ? Convert.ToDouble(row1["RATING"]) : 0;
                            vendor.Taxes = row1["VEND_TAXES"] != DBNull.Value ? Convert.ToInt32(row1["VEND_TAXES"]) : 0;
                            vendor.CompanyName = row1["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row1["COMPANY_NAME"]) : string.Empty;

                            //vendor.InitialPriceWithOutTaxFreight = row1["VEND_INIT_PRICE_WITHOUT_TAX"] != DBNull.Value ? Convert.ToDouble(row1["VEND_INIT_PRICE_WITHOUT_TAX"]) : 0;
                            //vendor.VendorFreight = row1["VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT"]) : 0;

                            vendor.Warranty = row1["WARRANTY"] != DBNull.Value ? Convert.ToString(row1["WARRANTY"]) : string.Empty;
                            vendor.Payment = row1["PAYMENT"] != DBNull.Value ? Convert.ToString(row1["PAYMENT"]) : string.Empty;
                            vendor.Duration = row1["DURATION"] != DBNull.Value ? Convert.ToString(row1["DURATION"]) : string.Empty;
                            vendor.Validity = row1["VALIDITY"] != DBNull.Value ? Convert.ToString(row1["VALIDITY"]) : string.Empty;

                            vendor.OtherProperties = row1["OTHER_PROPERTIES"] != DBNull.Value ? Convert.ToString(row1["OTHER_PROPERTIES"]) : string.Empty;

                            vendor.IsQuotationRejected = row1["IS_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_QUOTATION_REJECTED"]) : 0;
                            vendor.QuotationRejectedComment = row1["QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.PO = new RequirementPO();

                            vendor.PO.POLink = row1["PO_ATT_ID"] != DBNull.Value ? Convert.ToString(row1["PO_ATT_ID"]) : "0";

                            string fileName1 = row1["QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["QUOTATION_URL"]) : string.Empty;
                            string URL1 = fileName1 != string.Empty ? fileName1 : string.Empty;
                            vendor.QuotationUrl = URL1;

                            string fileName2 = row1["REV_QUOTATION_URL"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_URL"]) : string.Empty;
                            string URL2 = fileName2 != string.Empty ? fileName2 : string.Empty;
                            vendor.RevQuotationUrl = URL2;
                            vendor.RevPrice = row1["REV_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE"]) : 0;
                            //vendor.RevVendorFreight = row1["REV_VEND_FREIGHT"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT"]) : 0;
                            vendor.RevVendorTotalPrice = row1["REV_VEND_TOTAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE"]) : 0;

                            vendor.IsRevQuotationRejected = row1["IS_REV_QUOTATION_REJECTED"] != DBNull.Value ? Convert.ToInt32(row1["IS_REV_QUOTATION_REJECTED"]) : 0;
                            vendor.RevQuotationRejectedComment = row1["REV_QUOTATION_REJECTED_REASON"] != DBNull.Value ? Convert.ToString(row1["REV_QUOTATION_REJECTED_REASON"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.Discount = row1["DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row1["DISCOUNT"]) : 0;
                            vendor.ReductionPercentage = row1["REDUCTION_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["REDUCTION_PERCENTAGE"]) : 0;

                            vendor.Vendor = new User();

                            vendor.Vendor.Email = row1["U_EMAIL"] != DBNull.Value ? Convert.ToString(row1["U_EMAIL"]) : string.Empty;
                            vendor.Vendor.PhoneNum = row1["U_PHONE"] != DBNull.Value ? Convert.ToString(row1["U_PHONE"]) : string.Empty;

                            vendor.PO.MaterialDispachmentLink = row1["MAT_DIS_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MAT_DIS_LINK"]) : 0;
                            vendor.PO.MaterialReceivedLink = row1["MR_LINK"] != DBNull.Value ? Convert.ToInt32(row1["MR_LINK"]) : 0;

                            vendor.LandingPrice = row1["LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["LANDING_PRICE"]) : 0;
                            vendor.RevLandingPrice = row1["REV_LANDING_PRICE"] != DBNull.Value ? Convert.ToDecimal(row1["REV_LANDING_PRICE"]) : 0;
                            vendor.SumOfMargin = row1["PARAM_SUM_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_MARGIN"]) : 0;
                            vendor.SumOfInitialMargin = row1["PARAM_SUM_INITIAL_MARGIN"] != DBNull.Value ? Convert.ToDecimal(row1["PARAM_SUM_INITIAL_MARGIN"]) : 0;
                            vendor.MultipleAttachments = row1["MULTIPLE_ATTACHMENTS"] != DBNull.Value ? Convert.ToString(row1["MULTIPLE_ATTACHMENTS"]) : string.Empty;
                            vendor.GstNumber = row1["GST_NUMBER"] != DBNull.Value ? Convert.ToString(row1["GST_NUMBER"]) : string.Empty;
                            //vendor.PackingCharges = row1["PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES"]) : 0;
                            //vendor.PackingChargesTaxPercentage = row1["PACKING_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.PackingChargesWithTax = row1["PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["PACKING_CHARGES_WITH_TAX"]) : 0;
                            //vendor.InstallationCharges = row1["INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES"]) : 0;
                            //vendor.InstallationChargesTaxPercentage = row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.InstallationChargesWithTax = row1["INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            //vendor.RevpackingCharges = row1["REV_PACKING_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES"]) : 0;
                            //vendor.RevinstallationCharges = row1["REV_INSTALLATION_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES"]) : 0;
                            vendor.VendorCurrency = row1["CURR_NAME"] != DBNull.Value ? Convert.ToString(row1["CURR_NAME"]) : string.Empty;
                            vendor.SelectedVendorCurrency = row1["SELECTED_VENDOR_CURRENCY"] != DBNull.Value ? Convert.ToString(row1["SELECTED_VENDOR_CURRENCY"]) : string.Empty;
                            //vendor.RevinstallationChargesWithTax = row1["REV_INSTALLATION_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_WITH_TAX"]) : 0;
                            //vendor.RevpackingChargesWithTax = row1["REV_PACKING_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_WITH_TAX"]) : 0;
                            //vendor.FREIGHT_CHARGES = row1["FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES"]) : 0;
                            //vendor.FREIGHT_CHARGES_TAX_PERCENTAGE = row1["FREIGHT_CHARGES_TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_TAX_PERCENTAGE"]) : 0;
                            //vendor.FREIGHT_CHARGES_WITH_TAX = row1["FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["FREIGHT_CHARGES_WITH_TAX"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES = row1["REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_WITH_TAX = row1["REV_FREIGHT_CHARGES_WITH_TAX"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_WITH_TAX"]) : 0;

                            vendor.RevPriceCB = row1["REV_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PRICE_CB"]) : 0;
                            vendor.RevVendorTotalPriceCB = row1["REV_VEND_TOTAL_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_TOTAL_PRICE_CB"]) : 0;
                            vendor.TotalRunningPriceCB = row1["VEND_TOTAL_PRICE_RUNNING_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_TOTAL_PRICE_RUNNING_CB"]) : 0;
                            //vendor.RevpackingChargesCB = row1["REV_PACKING_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_PACKING_CHARGES_CB"]) : 0;
                            //vendor.RevinstallationChargesCB = row1["REV_INSTALLATION_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_INSTALLATION_CHARGES_CB"]) : 0;
                            //vendor.VendorFreightCB = row1["VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["VEND_FREIGHT_CB"]) : 0;
                            //vendor.RevVendorFreightCB = row1["REV_VEND_FREIGHT_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_VEND_FREIGHT_CB"]) : 0;
                            //vendor.REV_FREIGHT_CHARGES_CB = row1["REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row1["REV_FREIGHT_CHARGES_CB"]) : 0;

                            //#CB-0-2018-12-05
                            vendor.FREEZE_CB = row1["FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.VEND_FREEZE_CB = row1["VEND_FREEZE_CB"] != DBNull.Value ? (Convert.ToInt32(row1["VEND_FREEZE_CB"]) == 1 ? true : false) : false;
                            vendor.CB_BID_COMMENTS = row1["CB_BID_COMMENTS"] != DBNull.Value ? Convert.ToString(row1["CB_BID_COMMENTS"]) : string.Empty;

                            vendor.INCO_TERMS = row1["INCO_TERMS"] != DBNull.Value ? Convert.ToString(row1["INCO_TERMS"]) : string.Empty;

                            vendor.LandingPrice = Math.Round(vendor.LandingPrice, 2);
                            vendor.RevLandingPrice = Math.Round(vendor.RevLandingPrice, 2);
                            //vendor.LastActiveTime = GetLastActiveTime(sessionID, vendor.VendorID);
                            // vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.LastActiveTime = row1["LAST_ACTIVE_TIME"] != DBNull.Value ? Convert.ToString(row1["LAST_ACTIVE_TIME"]) : string.Empty;
                            vendor.ReductionPercentage = Math.Round(vendor.ReductionPercentage, 3);
                            vendor.BOOKED_SLOT_DATE = row1["BOOKED_SLOT_DATE"] != DBNull.Value ? Convert.ToDateTime(row1["BOOKED_SLOT_DATE"]) : DateTime.MaxValue;
                            vendor.IS_SLOT_APPROVED = row1["IS_SLOT_APPROVED"] != DBNull.Value ? (Convert.ToInt32(row1["IS_SLOT_APPROVED"])) : -1;

                            if (vendor.BOOKED_SLOT_DATE == DateTime.MaxValue)
                            {
                                vendor.BOOKED_SLOT_DATE = null;
                            }
                            vendor.SLOT_ID = row1["SLOT_ID"] != DBNull.Value ? (Convert.ToInt32(row1["SLOT_ID"])) : -1;

                            vendor.BASE_PRICE = row1["BASE_PRICE"] != DBNull.Value ? Convert.ToDouble(row1["BASE_PRICE"]) : 0;
                            vendor.SLOT_START_DATE = row1["SLOT_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row1["SLOT_START_TIME"]) : DateTime.MaxValue;
                            if (vendor.SLOT_START_DATE == DateTime.MaxValue)
                            {
                                vendor.SLOT_START_DATE = null;
                            }
                            vendor.SLOT_END_DATE = row1["SLOT_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row1["SLOT_END_TIME"]) : DateTime.MaxValue;
                            if (vendor.SLOT_END_DATE == DateTime.MaxValue)
                            {
                                vendor.SLOT_END_DATE = null;
                            }
                            vendor.SLOT_NAME = row1["SLOT_NAME"] != DBNull.Value ? Convert.ToString(row1["SLOT_NAME"]) : string.Empty;


                            KeyValuePair reqCurrency = new KeyValuePair();
                            reqCurrency.Value = requirement.Currency;
                            reqCurrency.Key1 = "REQ_CURRENCY";

                            KeyValuePair vendorCurrency = new KeyValuePair();
                            vendorCurrency.Value = vendor.VendorCurrency;
                            vendorCurrency.Key1 = "VENDOR_CURRENCY";

                            vendor.ListCurrencies.Add(reqCurrency);

                            if (reqCurrency.Value == vendorCurrency.Value)
                            {
                                vendor.SelectedVendorCurrency = reqCurrency.Value;
                            }
                            else
                            {
                                vendor.ListCurrencies.Add(vendorCurrency);
                            }

                            vendorDetails.Add(vendor);
                        }
                    }


                    if (requirement.IsDiscountQuotation == 2)
                    {

                        decimal maxInitialMargin = 0;
                        decimal maxClosedMargin = 0;

                        try
                        {
                            maxInitialMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfInitialMargin);
                            maxClosedMargin = vendorDetails.Where(vd => vd.CompanyName != "PRICE_CAP" && vd.CompanyName != "DEFAULT_VENDOR" && vd.IsQuotationRejected == 0).Max(vd => vd.SumOfMargin);
                        }
                        catch
                        {
                            maxInitialMargin = 0;
                            maxClosedMargin = 0;
                        }


                        requirement.Savings = Convert.ToDouble(maxClosedMargin - maxInitialMargin);
                        requirement.Savings = Math.Round(requirement.Savings, 2);
                    }

                    requirement.CustomerReqAccess = false;
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        foreach (DataRow row5 in ds.Tables[5].Rows)
                        {
                            int reqCustomerId = row5["U_ID"] != DBNull.Value ? Convert.ToInt32(row5["U_ID"]) : 0;

                            if (userID == reqCustomerId)
                            {
                                requirement.CustomerReqAccess = true;
                            }
                        }
                    }

                    List<VendorDetails> v2 = new List<VendorDetails>();
                    if (requirement.StartTime > DateTime.UtcNow)
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.InitialPrice == 0).ToList());
                    }
                    else
                    {
                        v2 = vendorDetails.Where(v => v.IsQuotationRejected == 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList();
                        v2.AddRange(vendorDetails.Where(v => v.IsQuotationRejected != 0).Where(v => v.BASE_PRICE > 0).OrderByDescending(v => v.BASE_PRICE).ToList());
                        v2.AddRange(vendorDetails.Where(v => v.BASE_PRICE == 0).ToList());
                    }
                    if (v2.Count > 0)
                    {
                        VendorDetails vendor1 = v2[0];
                        if (vendor1.BASE_PRICE == 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else if (vendor1.TotalInitialPrice < vendor1.TotalRunningPrice && vendor1.TotalInitialPrice != 0)
                        {
                            requirement.Price = vendor1.TotalInitialPrice;
                        }
                        else
                        {
                            requirement.Price = vendor1.TotalRunningPrice;
                        }
                    }
                    foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())
                    {
                        vendor.Rank = v2.IndexOf(vendor) + 1;
                    }
                    if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                    {
                        requirement.AuctionVendors = v2;
                    }
                    else
                    {
                        requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                    }

                    requirement.SelectedVendor = requirement.AuctionVendors.FirstOrDefault(v => v.VendorID == requirement.SelectedVendorID);

                    int productSNo = 0;
                    List<FwdRequirementItems> ListRequirementItems = new List<FwdRequirementItems>();
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[2].Rows)
                        {
                            FwdRequirementItems RequirementItems = new FwdRequirementItems();
                            RequirementItems.ProductSNo = productSNo++;
                            RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                            RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                            RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                            RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                            RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                            RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                            RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                            RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                            RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                            RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                            RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                            RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                            RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;

                            ListRequirementItems.Add(RequirementItems);
                        }
                        requirement.ItemSNoCount = productSNo;
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        foreach (DataRow row2 in ds.Tables[3].Rows)
                        {
                            int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                            int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                            double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                            string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                            string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                            string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                            string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;

                            double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                            double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;

                            double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;

                            double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                            double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                            double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;

                            double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                            double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                            double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                            string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                            string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                            bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                            string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                            string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                            string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                            double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                            double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                            double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                            //#CB-0-2018-12-05
                            double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                            double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                            double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                            double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                            double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                            string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;


                            if (uID == userID)
                            {
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemPrice = price;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductIDorName = name;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductNo = no;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductDescription = des;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductBrand = brand;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPrice = revItemPrice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitPrice = unitprice;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPrice = revunitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().CGst = cgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().SGst = sgst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IGst = igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().Gst = cgst + sgst + igst;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitMRP = unitmrp;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitDiscount = unitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscount = revunitdiscount;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().VendorUnits = vendorUnits;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IsRegret = isRegret;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RegretComments = regretComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightCharges = itemFreightCharges;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightTAX = itemFreightTAX;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                //#CB-0-2018-12-05
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPriceCB = revItemPriceCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REDUCTION_PRICE = LAST_BID_REV_UNIT_PRICE - revunitprice;

                                ListRequirementItems.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;

                            }
                        }
                    }

                    requirement.ListFwdRequirementItems = ListRequirementItems;
                    foreach (VendorDetails V in vendorDetails)
                    {
                        int productSNo1 = 0;

                        List<FwdRequirementItems> ListRequirementItems1 = new List<FwdRequirementItems>();
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[2].Rows)
                            {
                                FwdRequirementItems RequirementItems = new FwdRequirementItems();
                                RequirementItems.ProductSNo = productSNo1++;
                                RequirementItems.ItemID = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;
                                RequirementItems.RequirementID = row2["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row2["REQ_ID"]) : 0;
                                RequirementItems.ProductIDorName = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNo = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescription = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductQuantity = row2["QUANTITY"] != DBNull.Value ? Convert.ToDouble(row2["QUANTITY"]) : 0;
                                RequirementItems.ProductBrand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.OthersBrands = row2["OTHER_BRAND"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRAND"]) : string.Empty;
                                RequirementItems.ProductImageID = row2["IMAGE_ID"] != DBNull.Value ? Convert.ToInt32(row2["IMAGE_ID"]) : 0;
                                RequirementItems.IsDeleted = row2["IS_DELETED"] != DBNull.Value ? Convert.ToInt32(row2["IS_DELETED"]) : 0;
                                RequirementItems.ProductQuantityIn = row2["QUANTITY_IN"] != DBNull.Value ? Convert.ToString(row2["QUANTITY_IN"]) : string.Empty;
                                RequirementItems.SelectedVendorID = row2["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt32(row2["SELECTED_VENDOR_ID"]) : 0;

                                RequirementItems.ProductIDorNameCustomer = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                RequirementItems.ProductNoCustomer = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                RequirementItems.ProductDescriptionCustomer = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                RequirementItems.ProductBrandCustomer = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;
                                RequirementItems.ItemMinReduction = row2["ITEM_MIN_REDUCTION"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_MIN_REDUCTION"]) : 0;
                                RequirementItems.ItemLastPrice = row2["LAST_ITEM_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_ITEM_PRICE"]) : 0;
                                RequirementItems.ITEM_LEVEL_LEAST_PRICE = row2["ITEM_LEVEL_LEAST_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_LEVEL_LEAST_PRICE"]) : 0;
                                RequirementItems.I_LLP_DETAILS = row2["I_LLP_DETAILS"] != DBNull.Value ? Convert.ToString(row2["I_LLP_DETAILS"]) : string.Empty;

                                ListRequirementItems1.Add(RequirementItems);
                            }
                        }

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            foreach (DataRow row2 in ds.Tables[3].Rows)
                            {
                                int uID = row2["U_ID"] != DBNull.Value ? Convert.ToInt32(row2["U_ID"]) : 0;
                                int itemid = row2["ITEM_ID"] != DBNull.Value ? Convert.ToInt32(row2["ITEM_ID"]) : 0;

                                double price = row2["PRICE"] != DBNull.Value ? Convert.ToDouble(row2["PRICE"]) : 0;
                                string name = row2["PROD_ID"] != DBNull.Value ? Convert.ToString(row2["PROD_ID"]) : string.Empty;
                                string no = row2["PROD_NO"] != DBNull.Value ? Convert.ToString(row2["PROD_NO"]) : string.Empty;
                                string des = row2["DESCRIPTION"] != DBNull.Value ? Convert.ToString(row2["DESCRIPTION"]) : string.Empty;
                                string brand = row2["BRAND"] != DBNull.Value ? Convert.ToString(row2["BRAND"]) : string.Empty;

                                double unitprice = row2["UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_PRICE"]) : 0;
                                double revunitprice = row2["REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE"]) : 0;

                                double revItemPrice = row2["REVICED_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE"]) : 0;

                                double cgst = row2["C_GST"] != DBNull.Value ? Convert.ToDouble(row2["C_GST"]) : 0;
                                double sgst = row2["S_GST"] != DBNull.Value ? Convert.ToDouble(row2["S_GST"]) : 0;
                                double igst = row2["I_GST"] != DBNull.Value ? Convert.ToDouble(row2["I_GST"]) : 0;

                                double unitmrp = row2["UNIT_MRP"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_MRP"]) : 0;
                                double unitdiscount = row2["UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["UNIT_DISCOUNT"]) : 0;
                                double revunitdiscount = row2["REV_UNIT_DISCOUNT"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT"]) : 0;
                                string otherBrands = row2["OTHER_BRANDS"] != DBNull.Value ? Convert.ToString(row2["OTHER_BRANDS"]) : string.Empty;
                                string vendorUnits = row2["VENDOR_UNITS"] != DBNull.Value ? Convert.ToString(row2["VENDOR_UNITS"]) : string.Empty;
                                bool isRegret = row2["IS_REGRET"] != DBNull.Value ? (Convert.ToInt32(row2["IS_REGRET"]) > 0 ? true : false) : false;
                                string regretComments = row2["REGRET_COMMENTS"] != DBNull.Value ? Convert.ToString(row2["REGRET_COMMENTS"]) : string.Empty;
                                string itemLevelInitialComments = row2["ITEM_LEVEL_INITIALCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_INITIALCOMMENT"]) : string.Empty;
                                string itemLevelRevComments = row2["ITEM_LEVEL_REVCOMMENT"] != DBNull.Value ? Convert.ToString(row2["ITEM_LEVEL_REVCOMMENT"]) : string.Empty;
                                double itemFreightCharges = row2["ITEM_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_CHARGES"]) : 0;
                                double itemFreightTAX = row2["ITEM_FREIGHT_TAX"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_FREIGHT_TAX"]) : 0;
                                double itemRevFreightCharges = row2["ITEM_REV_FREIGHT_CHARGES"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES"]) : 0;

                                //#CB-0-2018-12-05
                                double revunitpriceCB = row2["REV_UNIT_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_PRICE_CB"]) : 0;
                                double revItemPriceCB = row2["REVICED_PRICE_CB"] != DBNull.Value ? Convert.ToDouble(row2["REVICED_PRICE_CB"]) : 0;
                                double revunitdiscountCB = row2["REV_UNIT_DISCOUNT_CB"] != DBNull.Value ? Convert.ToDouble(row2["REV_UNIT_DISCOUNT_CB"]) : 0;
                                double itemRevFreightChargesCB = row2["ITEM_REV_FREIGHT_CHARGES_CB"] != DBNull.Value ? Convert.ToDouble(row2["ITEM_REV_FREIGHT_CHARGES_CB"]) : 0;

                                double LAST_BID_REV_UNIT_PRICE = row2["LAST_BID_REV_UNIT_PRICE"] != DBNull.Value ? Convert.ToDouble(row2["LAST_BID_REV_UNIT_PRICE"]) : 0;

                                string ProductQuotationTemplateJson = row2["ProductQuotationTemplateJson"] != DBNull.Value ? Convert.ToString(row2["ProductQuotationTemplateJson"]) : string.Empty;


                                if (uID == V.VendorID)
                                {
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemPrice = price;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductIDorName = name;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductNo = no;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductDescription = des;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductBrand = brand;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPrice = revItemPrice;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitPrice = unitprice;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPrice = revunitprice;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().CGst = cgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().SGst = sgst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IGst = igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().Gst = cgst + sgst + igst;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitMRP = unitmrp;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().UnitDiscount = unitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscount = revunitdiscount;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().VendorUnits = vendorUnits;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().IsRegret = isRegret;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RegretComments = regretComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelInitialComments = itemLevelInitialComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemLevelRevComments = itemLevelRevComments;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightCharges = itemFreightCharges;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemFreightTAX = itemFreightTAX;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightCharges = itemRevFreightCharges;

                                    //#CB-0-2018-12-05
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitPriceCB = revunitpriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevItemPriceCB = revItemPriceCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().RevUnitDiscountCB = revunitdiscountCB;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ItemRevFreightChargesCB = itemRevFreightChargesCB;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REV_UNIT_PRICE = LAST_BID_REV_UNIT_PRICE;
                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().LAST_BID_REDUCTION_PRICE = LAST_BID_REV_UNIT_PRICE - revunitprice;

                                    ListRequirementItems1.Where(i => i.ItemID == itemid).FirstOrDefault<FwdRequirementItems>().ProductQuotationTemplateJson = ProductQuotationTemplateJson;

                                }
                            }
                        }

                        V.ListFwdRequirementItems = ListRequirementItems1;
                    }

                    if (requirement.IsDiscountQuotation == 1 || requirement.IsDiscountQuotation == 0)
                    {
                        foreach (VendorDetails vendor in v2.Where(v => v.IsQuotationRejected == 0).OrderByDescending(v => v.BASE_PRICE).ToList())
                        {
                            vendor.Rank = v2.IndexOf(vendor) + 1;
                        }
                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            requirement.AuctionVendors = v2;
                        }
                        else
                        {
                            requirement.AuctionVendors = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.BASE_PRICE).ToList();
                        }
                    }
                    else
                    {
                        v2 = v2.OrderByDescending(v => v.SumOfMargin).ToList();

                        List<VendorDetails> approvedV2 = new List<VendorDetails>();

                        approvedV2 = v2.Where(v => v.IsQuotationRejected == 0 && v.SumOfMargin > 0).OrderByDescending(v => v.SumOfMargin).ToList();

                        foreach (VendorDetails vendor in v2)
                        {

                            if (vendor.IsQuotationRejected == 0 && vendor.SumOfMargin > 0)
                            {
                                vendor.Rank = approvedV2.IndexOf(vendor) + 1;
                            }
                        }

                        if (requirement.CustomerID == userID || requirement.SuperUserID == userID || requirement.CustomerReqAccess == true)
                        {
                            v2 = v2.Where(v => v.IsQuotationRejected == 0).OrderBy(v => v.Rank).ToList();

                            foreach (VendorDetails vendor in requirement.AuctionVendors)
                            {
                                if (vendor.IsQuotationRejected != 0)
                                {
                                    v2.Add(vendor);
                                }
                            }
                        }
                        else
                        {
                            v2 = v2.Where(v => v.VendorID == userID).OrderByDescending(v => v.SumOfMargin).ToList();
                        }

                        requirement.AuctionVendors = v2;
                    }

                    int taxSNo = 0;

                    List<RequirementTaxes> ListRequirementTaxes = new List<RequirementTaxes>();
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        foreach (DataRow row4 in ds.Tables[4].Rows)
                        {
                            RequirementTaxes requirementTaxes = new RequirementTaxes();
                            requirementTaxes.TaxSNo = taxSNo++;
                            int uID = row4["U_ID"] != DBNull.Value ? Convert.ToInt32(row4["U_ID"]) : 0;
                            if (uID == userID || requirement.CustomerReqAccess == true)
                            {
                                requirementTaxes.TaxID = row4["QT_ID"] != DBNull.Value ? Convert.ToInt32(row4["QT_ID"]) : 0;
                                requirementTaxes.TaxName = row4["TAX_NAME"] != DBNull.Value ? Convert.ToString(row4["TAX_NAME"]) : string.Empty;
                                requirementTaxes.TaxPercentage = row4["TAX_PERCENTAGE"] != DBNull.Value ? Convert.ToDouble(row4["TAX_PERCENTAGE"]) : 0;
                                requirementTaxes.IsTaxDeleted = row4["IS_TAX_DELETED"] != DBNull.Value ? Convert.ToInt32(row4["IS_TAX_DELETED"]) : 0;

                                ListRequirementTaxes.Add(requirementTaxes);
                            }
                        }
                        requirement.TaxSNoCount = taxSNo;
                    }
                    requirement.ListRequirementTaxes = ListRequirementTaxes;

                }
            }
            catch (Exception ex)
            {
                FwdRequirement req = new FwdRequirement();
                req.ErrorMessage = ex.Message;
                return req;
            }

            return requirement;
        }

        public DataSet GetRequirementDetailsHub(int reqID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            sd.Add("P_REQ_ID", reqID);
            DataSet ds = sqlHelper.SelectList("fwd_GetRequirementData", sd);
            return ds;
        }

        public Response RestartNegotiation(int reqID, int userID, string sessionID)
        {
            int CallerID = userID;
            string ScreenName = "RESTART_NEGOTIATION";
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_AddTimeToNegotiation", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    List<UserInfo> users = GetUsersInvolved(reqID, "ALLVENDORS");
                    FwdRequirement req1 = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                    foreach (UserInfo user in users)
                    {
                        User altUser = GetAlternateCommunications(reqID, Convert.ToInt32(user.UserID), sessionID);
                        if (user.UserType == "VENDOR")
                        {
                            string body = Utilities.GenerateForwardEmailBody("NegotiationRestartEmail");
                            body = String.Format(body, user.FirstName, user.LastName, reqID, req1.Title);
                            Utilities.SendEmail(user.Email + "," + user.AltEmail, "Negotiation has restarted for the Requirement ID" + reqID, body, reqID, Convert.ToInt32(user.UserID), ScreenName, sessionID).ConfigureAwait(false);
                            string message = Utilities.GenerateForwardEmailBody("NegotiationRestartSms");
                            message = String.Format(message, user.FirstName, user.LastName, reqID, req1.StartTime, req1.Title);
                            message = message.Replace("<br/>", "");
                          //  Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(message.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(user.UserID), ScreenName, sessionID).ConfigureAwait(false);                            
                        }
                    }

                    FwdRequirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                    InvolvedParties involvedParties = new InvolvedParties();
                    involvedParties.RequirementID = req.RequirementID;
                    involvedParties.CustomerID = req.CustomerID;
                    involvedParties.SuperUserID = req.SuperUserID;
                    involvedParties.CallerID = CallerID;
                    involvedParties.CustCompID = req.CustCompID;
                    involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                    var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                    involvedParties.MethodName = "RestartNegotiation";
                    req.Inv = involvedParties;
                    context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                    context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                    foreach (int vendorID in involvedParties.UserIDList)
                    {
                        req = GetRequirementData(reqID, vendorID, sessionID);
                        context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public User GetAlternateCommunications(int reqID, int vendorID, string sessionID)
        {
            User user = new User();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_VENDOR_ID", vendorID);
                DataSet ds = sqlHelper.SelectList("fwd_GetAlternateCommunications", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        user.AltPhoneNum += row["U_PHONE"] != DBNull.Value ? row["U_PHONE"].ToString() : string.Empty;
                        user.AltPhoneNum += ",";
                        user.AltEmail += row["U_EMAIL"] != DBNull.Value ? row["U_EMAIL"].ToString() : string.Empty;
                        user.AltEmail += ",";
                    }
                }
            }
            catch (Exception ex)
            {
                user.ErrorMessage = ex.Message;
            }
            return user;
        }

        public Response StopBids(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_StopBids", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response DeleteRequirement(int reqID, int userID, string sessionID, string reason)
        {
            int CallerID = userID;
            string ScreenName = "DELETE_REQUIREMENT";
            Response response = new Response();
            string msgStep = "Default";
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                FwdRequirement req = GetRequirementDataOfflinePrivate(reqID, userID, sessionID);
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_REASON", reason);
                DataSet ds = sqlHelper.SelectList("fwd_DeleteRequirement", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        InvolvedParties involvedParties = new InvolvedParties();
                        involvedParties.RequirementID = req.RequirementID;
                        involvedParties.CustomerID = req.CustomerID;
                        involvedParties.SuperUserID = req.SuperUserID;
                        involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                        involvedParties.CallerID = CallerID;
                        var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                        involvedParties.MethodName = "DeleteRequirement";
                        involvedParties.CustCompID = req.CustCompID;
                        req.Inv = involvedParties;
                        context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.CustomerID).checkRequirement(req);
                        context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + req.SuperUserID).checkRequirement(req);
                        foreach (int vendorID in involvedParties.UserIDList)
                        {
                            req = GetRequirementData(reqID, vendorID, sessionID);
                            context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID + Utilities.UserIDComponent + vendorID).checkRequirement(req);
                        }

                        msgStep = "Stored Procedure Executed";

                        req.Module = ScreenName;

                        UserInfo user = prmServices.GetUser(userID, sessionID);
                        string body = Utilities.GenerateForwardEmailBody("DeleteRequirementemailCustomer");
                        body = String.Format(body, user.FirstName, user.LastName, reqID);
                        msgStep = "-->>Step 1";
                        Utilities.SendEmail(user.Email + "," + user.AltEmail, "ReqID: " + reqID + " Title: " + req.Title + " - Requirement Deleted Successfully", body, reqID, Convert.ToInt32(user.UserID), req.Module, sessionID).ConfigureAwait(false);


                        string body2 = Utilities.GenerateForwardEmailBody("DeleteRequirementsms");
                        body2 = String.Format(body2, user.FirstName, user.LastName, reqID);
                        body2 = body2.Replace("<br/>", "");
                      //  Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(user.UserID), req.Module, sessionID).ConfigureAwait(false);

                        UserInfo superUser = prmServices.GetSuperUser(req.CustomerID, sessionID);
                        string bodySuper = Utilities.GenerateForwardEmailBody("DeleteRequirementemailCustomer");
                        bodySuper = String.Format(bodySuper, superUser.FirstName, superUser.LastName, reqID);
                        string body2Super = Utilities.GenerateForwardEmailBody("DeleteRequirementsms");
                        body2Super = String.Format(body2Super, superUser.FirstName, superUser.LastName, reqID);
                        body2Super = body2Super.Replace("<br/>", "");
                        string subUserID = req.CustomerID.ToString();
                        if (superUser.UserID != subUserID)
                        {
                            Utilities.SendEmail(superUser.Email + "," + superUser.AltEmail, "ReqID: " + reqID + " Title: " + req.Title + " - Requirement Deleted Successfully", bodySuper, reqID, req.CustomerID, req.Module, sessionID).ConfigureAwait(false);
                         //   Utilities.SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, req.CustomerID, req.Module, sessionID).ConfigureAwait(false);
                        }

                        foreach (int id in involvedParties.UserIDList)
                        {
                            Response res1 = RemoveVendorFromAuction(id, reqID, sessionID, "VendoremailForRemoval", req);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message + msgStep;
            }

            return response;
        }

        public Response UpdateAuctionStart(int reqID, int userID, DateTime date, string sessionID, string negotiationDuration, NegotiationSettings NegotiationSettings)
        {

            string moreInfo = string.Empty;
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                int isValidSession = Utilities.ValidateSession(sessionID);
                List<Attachment> vendorAttachments = new List<Attachment>();
                negotiationDuration = "0 0:35";
                negotiationDuration = negotiationDuration + ":00.0";
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_NEW_TIME", date);
                sd.Add("P_U_ID", userID);
                sd.Add("PARAM_END_TIME", negotiationDuration);
                sd.Add("P_MIN_REDUCE_AMOUNT", NegotiationSettings.MinReductionAmount);
                sd.Add("P_VENDOR_COMPARISION_MIN_AMOUNT", NegotiationSettings.RankComparision);
                sd.Add("P_NEGOTIATION_DURATION", NegotiationSettings.NegotiationDuration);
                DataSet ds = sqlHelper.SelectList("fwd_UpdateAuctionStart", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1].ToString();
                    if (response.ErrorMessage != string.Empty)
                    {
                        response.TimeLeft = -1;
                    }
                    else
                    {
                        long diff = Convert.ToInt64((date - DateTime.UtcNow).TotalSeconds);
                        response.TimeLeft = diff;
                    }
                }

                FwdRequirement requirement = GetRequirementData(reqID, userID, sessionID);
                PRMNotifications prmNotifications = new PRMNotifications();
                Task.Factory.StartNew(() => prmNotifications.ForwardAuctionStartNotification(reqID, userID, sessionID, vendorAttachments));
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + ex.StackTrace;
            }

            return response;
        }

        public List<Comment> GetBidHistory(int reqID, int userID, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<Comment> bidHistory = new List<Comment>();

            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetNegotiationHistory", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        Comment history = new Comment();

                        history.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        history.UserID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : -1;
                        history.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        history.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        history.CreatedTime = row["BID_TIME"] != DBNull.Value ? Convert.ToDateTime(row["BID_TIME"]) : DateTime.MaxValue;
                        history.BidAmount = row["PRICE"] != DBNull.Value ? Convert.ToDouble(row["PRICE"]) : 0;
                        history.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        history.RejectReson = row["REJECT_RESON"] != DBNull.Value ? Convert.ToString(row["REJECT_RESON"]) : string.Empty;
                        bidHistory.Add(history);
                    }
                }
            }
            catch (Exception ex)
            {
                Comment history = new Comment();
                history.ErrorMessage = ex.Message;
                bidHistory.Add(history);
            }

            return bidHistory;
        }

        public Response MakeABid(int reqID, int userID, double price, byte[] quotation, string quotationName, string sessionID,
            double tax, double freightcharges, string warranty, string payment, string duration, string validity,
            List<FwdRequirementItems> quotationObject, int revised, double priceWithoutTax, string type, double discountAmount,
            List<RequirementTaxes> listRequirementTaxes, string otherProperties)
        {
            string moreInfo = string.Empty;
            int CallerID = userID;
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                Utilities.ValidateSession(sessionID);
                long ticks = DateTime.UtcNow.Ticks;
                string fileName = string.Empty;
                if (type == "quotation")
                {
                }
                else
                {
                    sd.Add("P_REQ_ID", reqID);
                    sd.Add("P_PRICE", price);
                    sd.Add("P_U_ID", userID);
                    sd.Add("P_QUOTATION_URL", fileName);
                    sd.Add("P_TAX", tax);
                    sd.Add("P_VEND_FREIGHT", freightcharges);
                    sd.Add("P_WARRANTY", warranty);
                    sd.Add("P_PAYMENT", payment);
                    sd.Add("P_DURATION", duration);
                    sd.Add("P_VALIDITY", validity);
                    sd.Add("P_OTHER_PROPERTIES", otherProperties);
                    DataSet ds = sqlHelper.SelectList("fwd_MakeBid", sd);
                    moreInfo = "STORED PROCEDURE COMPLETED; ";

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                    {
                        response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                        response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                        if (response.ErrorMessage == "Auction is not running anymore.")
                        {
                            response.ErrorMessage = string.Empty;
                        }

                        if (response.ErrorMessage == string.Empty)
                        {
                            SignalRObj signalRObj = new SignalRObj();
                            List<object> payLoad = new List<object>();
                            DataSet ds1 = GetRequirementDetailsHub(reqID);

                            if (ds1 != null && ds1.Tables.Count > 0 && ds1.Tables[0].Rows.Count > 0)
                            {
                                DataRow row = ds1.Tables[0].Rows[0];
                                userID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                            }

                            FwdRequirement req = GetRequirementDataFilter(userID, sessionID, ds1);
                            payLoad.Add(req);
                            InvolvedParties involvedParties = new InvolvedParties();
                            involvedParties.RequirementID = req.RequirementID;
                            involvedParties.CustomerID = req.CustomerID;
                            involvedParties.SuperUserID = req.SuperUserID;
                            involvedParties.CallerID = CallerID;
                            involvedParties.CustCompID = req.CustCompID;
                            involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                            involvedParties.MethodName = "MakeBid";
                            req.Inv = involvedParties;
                            signalRObj.Inv = involvedParties;
                            foreach (int vendorID in involvedParties.UserIDList)
                            {
                                req = GetRequirementDataFilter(vendorID, sessionID, ds1);
                                payLoad.Add(req);
                            }

                            signalRObj.Inv = involvedParties;
                            signalRObj.PayLoad = payLoad.ToArray();

                            try
                            {
                                var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                                context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID).checkRequirement(signalRObj);
                            }
                            catch (Exception ex)
                            {
                                fileName = System.Web.HttpContext.Current.Server.MapPath("/WEBQA/Exceptions.txt");
                                File.WriteAllText(fileName, ex.Message);
                                fileName = "req" + reqID + "_user" + userID + "_" + quotationName;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = moreInfo + ex.Message + " | " + ex.StackTrace;
            }

            return response;
        }

        public Response SaveRunningItemPrice(List<RequirementItems> itemsList, int userID, int reqID, double price,
            double vendorBidPrice, double freightcharges,
            double revfreightCharges, double revpackingCharges, double revinstallationCharges)
        {
            Response response = new Response();
            try
            {
                //int isValidSession = ValidateSession(sessionID, cmd);
                string queryValues = string.Empty;
                int count = 0;
                string delete = string.Format("DELETE FROM fwd_runningitempricetemp WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                string query = delete + "INSERT INTO fwd_runningitempricetemp(ITEM_ID, REQ_ID, U_ID, REV_UNIT_PRICE, " +
                    "REV_ITEM_PRICE, REVICED_PRICE, VEND_REVISED_TOTAL_PRICE, " +
                    "VEND_FREIGHT, REV_UNIT_DISCOUNT,ITEM_REV_FREIGHT_CHARGES," +
                    "REV_FREIGHT_CHARGES, REV_PACKING_CHARGES, REV_INSTALLATION_CHARGES, " +
                    "ProductQuotationTemplateJson) Values";

                foreach (RequirementItems item in itemsList)
                {
                    double prevRevCostPrice = (item.UnitMRP * 100 * 100) / ((item.RevUnitDiscount * 100) + 10000 + (item.RevUnitDiscount * (item.CGst + item.SGst + item.IGst)) + ((item.CGst + item.SGst + item.IGst) * 100));
                    prevRevCostPrice = Math.Round(prevRevCostPrice, 2);
                    queryValues = queryValues + string.Format("({0},{1},'{2}',{3},{4},{5},{6},{7},{8},{9}," +
                        "{10},{11},{12}," +
                        "'{13}'),",
                        item.ItemID, reqID, userID, item.RevUnitPrice, item.RevItemPrice, price, vendorBidPrice, freightcharges, item.RevUnitDiscount,
                        0,
                        revfreightCharges, revpackingCharges, revinstallationCharges,
                        item.ProductQuotationTemplateJson);
                    count++;

                }
                response.ObjectID = count;
                if (queryValues.Length > 0)
                {
                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                    queryValues = queryValues + ";";
                    query = query + queryValues;
                    query = query + string.Format("CALL fwd_SaveRunningItemPriceBulk({0},{1});", userID, reqID);
                    sqlHelper.ExecuteNonQuery_IUD(query);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response UploadQuotation(List<FwdRequirementItems> quotationObject,
            int userID,
            int reqID,
            string sessionID,
            double price, // 5
            double tax,
            double freightcharges,
            double vendorBidPrice,
            string warranty,
            string payment, // 10
            string duration,
            string validity,
            int revised,
            string desFileName,
            double discountAmount, // 15
            List<RequirementTaxes> listRequirementTaxes,
            string otherProperties,
            string gstNumber,
            double packingCharges,
            double packingChargesTaxPercentage, // 20
            double packingChargesWithTax,
            double installationCharges,
            double installationChargesTaxPercentage,
            double installationChargesWithTax,
            double revpackingCharges, // 25
            double revinstallationCharges,
            double revpackingChargesWithTax,
            double revinstallationChargesWithTax,
            string selectedVendorCurrency,
            string uploadType, // 30
            List<FileUpload> multipleAttachments,
            double freightCharges,
            double freightChargesTaxPercentage,
            double freightChargesWithTax,
            double revfreightCharges,
            double revfreightChargesWithTax,
            string INCO_TERMS) // 35
        {
            string countAtt = string.Empty;
            int CallerID = userID;
            string ScreenName = "UPLOAD_QUOTATION";
            string RevScreenName = "UPLOAD_REV_QUOTATION";
            Response response = new Response();
            string moreInfo = string.Empty;
            DataSet ds = new DataSet();
            int customerID = 0;
            FwdRequirement requirement = GetRequirementData(reqID, customerID, sessionID);
            customerID = requirement.CustomerID;
            try
            {
                string quotationItems = string.Empty;
                if (requirement.IsTabular && quotationObject.Where(v => v.ItemPrice < 0).ToList<FwdRequirementItems>().Count > 0)
                {
                    response.ErrorMessage = "Please enter the price of all items before submitting.";
                    return response;
                }

                string multipleFileIDs = string.Empty;
                List<Attachment> CustomerListAttachment = new List<Attachment>();
                List<Attachment> VendorListAttachment = new List<Attachment>();
                List<Attachment> ListAttachment = new List<Attachment>();

                if (multipleAttachments != null && multipleAttachments.Count > 0)
                {
                    foreach (FileUpload fd in multipleAttachments)
                    {
                        if (fd != null && fd.FileStream != null && fd.FileStream.Length > 0 && !string.IsNullOrEmpty(fd.FileName))
                        {
                            var attachName = string.Empty;
                            long tick = DateTime.UtcNow.Ticks;
                            attachName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "REQ_" + reqID + "_USER_" + userID + "_" + tick + "_" + fd.FileName);
                            Utilities.SaveFile(attachName, fd.FileStream);

                            attachName = "REQ_" + reqID + "_USER_" + userID + "_" + tick + "_" + fd.FileName;

                            Response res = Utilities.SaveAttachment(attachName);
                            if (res.ErrorMessage != "")
                            {
                                response.ErrorMessage = res.ErrorMessage;
                            }

                            fd.FileID = res.ObjectID;

                            countAtt += "file ID---;" + fd.FileID;

                            if (fd.FileID > 0)
                            {
                                Attachment singleAttachment = null;
                                var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionID);
                                if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                                {
                                    countAtt += "1---;";
                                    singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                                }

                                CustomerListAttachment.Add(singleAttachment);
                                VendorListAttachment.Add(singleAttachment);
                                ListAttachment.Add(singleAttachment);
                            }

                        }
                        else if (fd != null && fd.FileID > 0)
                        {
                            Attachment singleAttachment = null;
                            var fileData = Utilities.DownloadFile(Convert.ToString(fd.FileID), sessionID);
                            if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                            {
                                countAtt += "2---;";
                                singleAttachment = new Attachment(fileData.FileStream, fileData.FileName);
                            }

                            CustomerListAttachment.Add(singleAttachment);
                            VendorListAttachment.Add(singleAttachment);
                            ListAttachment.Add(singleAttachment);
                        }

                        multipleFileIDs += Convert.ToString(fd.FileID) + ",";

                    }
                    multipleFileIDs = multipleFileIDs.Substring(0, multipleFileIDs.Length - 1);
                }



                string deleteAttachments = string.Format("UPDATE fwd_auctiondetails SET MULTIPLE_ATTACHMENTS = '{2}' WHERE REQ_ID = {0} AND U_ID = {1};",
                    reqID, userID, multipleFileIDs);
                DataSet dataset = sqlHelper.ExecuteQuery(deleteAttachments);
                int loopcount = 0;
                string queryValues = string.Empty;
                int count = 0;
                string delete = string.Format("DELETE FROM fwd_tempquotations WHERE REQ_ID = {0} AND U_ID  = {1};", reqID, userID);
                string query = delete + "INSERT INTO fwd_tempquotations(U_ID, REQ_ID, ITEM_ID, PROD_ID, PROD_NO, DESCRIPTION, BRAND," +
                    " OTHER_BRANDS, VEND_TAXES, WARRANTY, PAYMENT, DURATION, VALIDITY, DISCOUNT, OTHER_PROPERTIES, UNIT_PRICE," +
                    "REV_UNIT_PRICE, PRICE, VEND_INIT_PRICE, VEND_FREIGHT, VEND_TOTAL_PRICE, IMAGE_ID, IS_REVISED," +
                    "C_GST, S_GST, I_GST, UNIT_MRP, UNIT_DISCOUNT, REV_UNIT_DISCOUNT, GST_NUMBER, VENDOR_UNITS, IS_REGRET, " +
                    "REGRET_COMMENTS,ITEM_LEVEL_INITIALCOMMENT,ITEM_LEVEL_REVCOMMENT," +
                    "ITEM_FREIGHT_CHARGES, ITEM_FREIGHT_TAX,PACKING_CHARGES,PACKING_CHARGES_TAX_PERCENTAGE,PACKING_CHARGES_WITH_TAX," +
                    "INSTALLATION_CHARGES,INSTALLATION_CHARGES_TAX_PERCENTAGE,INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES," +
                    "REV_INSTALLATION_CHARGES,SELECTED_VENDOR_CURRENCY,ITEM_REV_FREIGHT_CHARGES," +
                    "REV_INSTALLATION_CHARGES_WITH_TAX,REV_PACKING_CHARGES_WITH_TAX," +

                    "FREIGHT_CHARGES," +
                    "FREIGHT_CHARGES_TAX_PERCENTAGE," +
                    "FREIGHT_CHARGES_WITH_TAX," +
                    "REV_FREIGHT_CHARGES," +
                    "REV_FREIGHT_CHARGES_WITH_TAX, " +
                    "INCO_TERMS, " +
                    "ProductQuotationTemplateJson " +
                    ") Values";

                foreach (FwdRequirementItems quotation in quotationObject)
                {

                    string fileURL = string.Empty;
                    if (quotation.ItemAttachment != null)
                    {
                        long ticks = DateTime.UtcNow.Ticks;
                        fileURL = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "user" + userID + "_" + ticks + quotation.AttachmentName);
                        Utilities.SaveFile(fileURL, quotation.ItemAttachment);
                        fileURL = "user" + userID + "_" + ticks + quotation.AttachmentName;
                        Response res = Utilities.SaveAttachment(fileURL);
                        fileURL = res.ObjectID.ToString();
                        quotation.FileId = Convert.ToInt32(fileURL);
                    }

                    double itemPrice = 0;
                    double unitPrice = 0;

                    if (revised == 0)
                    {
                        itemPrice = quotation.ItemPrice;
                        unitPrice = quotation.UnitPrice;
                        quotation.ItemLevelRevComments = quotation.ItemLevelInitialComments;
                    }
                    else if (revised == 1)
                    {
                        itemPrice = quotation.RevItemPrice;
                        unitPrice = quotation.RevUnitPrice;
                    }
                    //To handle single quote issue, crap code, need to improve.
                    string columns = "({0},{1},{2},~$^{3}~$^,~$^{4}~$^,~$^{5}~$^,~$^{6}~$^," +
                        "~$^{7}~$^,{8},~$^{9}~$^,~$^{10}~$^,~$^{11}~$^,~$^{12}~$^,{13},~$^{14}~$^,{15}," +
                        "{16},{17},{18},{19},{20},{21},{22}," +
                        "{23},{24},{25},{26}," +
                        "{27},{28}," +
                        "~$^{29}~$^,~$^{30}~$^,{31},~$^{32}~$^,~$^{33}~$^,~$^{34}~$^,~$^{35}~$^,~$^{36}~$^,~$^{37}~$^,~$^{38}~$^,~$^{39}~$^,~$^{40}~$^,~$^{41}~$^,~$^{42}~$^," +
                        "~$^{43}~$^,~$^{44}~$^,~$^{45}~$^,~$^{46}~$^,~$^{47}~$^,~$^{48}~$^," +

                        "~$^{49}~$^," +
                        "~$^{50}~$^," +
                        "~$^{51}~$^," +
                        "~$^{52}~$^," +
                        "~$^{53}~$^," +
                        "~$^{54}~$^," +
                        "~$^{55}~$^),";

                    queryValues = queryValues + string.Format(columns,
                        userID, reqID, quotation.ItemID, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription, quotation.ProductBrand,
                        quotation.OthersBrands, tax, warranty, payment, duration, validity, discountAmount, otherProperties, quotation.UnitPrice,
                        quotation.RevUnitPrice, itemPrice, price, freightcharges, vendorBidPrice, quotation.FileId,
                        revised, quotation.CGst, quotation.SGst, quotation.IGst,
                        quotation.UnitMRP, quotation.UnitDiscount, quotation.RevUnitDiscount,
                        gstNumber, quotation.VendorUnits, quotation.IsRegret, quotation.RegretComments, quotation.ItemLevelInitialComments, quotation.ItemLevelRevComments,
                        quotation.ItemFreightCharges, quotation.ItemFreightTAX, packingCharges, packingChargesTaxPercentage, packingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revpackingCharges, revinstallationCharges,
                        selectedVendorCurrency, quotation.ItemRevFreightCharges, revinstallationChargesWithTax, revpackingChargesWithTax,

                        freightCharges,
                        freightChargesTaxPercentage,
                        freightChargesWithTax,
                        revfreightCharges,
                        revfreightChargesWithTax,
                        INCO_TERMS,
                        quotation.ProductQuotationTemplateJson);

                    count++;

                }

                response.ObjectID = count;
                if (!string.IsNullOrEmpty(queryValues))
                {
                    queryValues = queryValues.Replace(@"'", "\\'");
                    queryValues = queryValues.Replace("~$^", "'");
                    queryValues = queryValues.Substring(0, queryValues.Length - 1);
                    queryValues = queryValues + ";";
                    query = query + queryValues;
                    query = query + string.Format("CALL fwd_UploadQuotationBulk({0},{1});", userID, reqID);
                    sqlHelper.ExecuteQuery(query);
                }

                bool isUOMDiffertent = false;
                int isRegret = 0;
                isRegret = quotationObject.Where(x => x.IsRegret == true).Count();

                foreach (FwdRequirementItems quotation in quotationObject)
                {

                    double itemPrice = 0;
                    double unitPrice = 0;

                    if (revised == 0)
                    {
                        itemPrice = quotation.ItemPrice;
                        unitPrice = quotation.UnitPrice;
                    }
                    else if (revised == 1)
                    {
                        itemPrice = quotation.RevItemPrice;
                        unitPrice = quotation.RevUnitPrice;
                    }


                    loopcount = loopcount + 1;

                    double itemUnitDiscount = 0;
                    double itemCostPrice = 0;

                    if (revised == 1)
                    {
                        response.ErrorMessage = "reviced";
                        itemUnitDiscount = quotation.RevUnitDiscount;

                    }
                    else if (revised == 0)
                    {
                        itemUnitDiscount = quotation.UnitDiscount;
                    }

                    itemCostPrice = (quotation.UnitMRP * 100 * 100) / ((itemUnitDiscount * 100) + 10000 + (itemUnitDiscount * (quotation.CGst + quotation.SGst + quotation.IGst)) + ((quotation.CGst + quotation.SGst + quotation.IGst) * 100));

                    if (requirement.IsTabular)
                    {

                        if (requirement.IsDiscountQuotation == 0)
                        {
                            double Gst = (quotation.CGst + quotation.SGst + quotation.IGst);

                            if (quotation.IsRegret == true)
                            {
                                itemPrice = 0;

                                unitPrice = 0;
                                quotation.CGst = 0;
                                quotation.SGst = 0;
                                quotation.IGst = 0;
                                Gst = 0;
                                quotation.UnitMRP = 0;
                                itemUnitDiscount = 0;
                            }



                            if (quotation.ProductQuantityIn != quotation.VendorUnits)
                            {
                                itemPrice = 0;
                                isUOMDiffertent = true;
                            }

                            string xml = string.Empty;
                            xml = Utilities.GenerateForwardEmailBody("QuptationUploadXML");
                            if (isRegret > 0)
                            {
                                xml = Utilities.GenerateForwardEmailBody("QuptationUploadIsRegretXML");
                            }

                            string itemComments = string.Empty;

                            if (revised == 1)
                            {
                                itemComments = quotation.ItemLevelRevComments;
                            }
                            else if (revised == 0)
                            {
                                itemComments = quotation.ItemLevelInitialComments;
                            }

                            double freight = 0;
                            if (revised == 1)
                            {
                                freight = quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
                            }
                            else
                            {
                                freight = quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
                            }

                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription,
                                quotation.ProductQuantity, quotation.ProductBrand, itemPrice, unitPrice, quotation.CGst,
                                quotation.SGst, quotation.IGst, quotation.UnitMRP, itemUnitDiscount, Gst, quotation.VendorUnits, quotation.RegretComments,
                                quotation.ProductQuantityIn, itemComments, selectedVendorCurrency, revinstallationChargesWithTax,
                                revpackingChargesWithTax, freight,

                                revfreightChargesWithTax);
                            quotationItems += xml;

                        }
                        else if (requirement.IsDiscountQuotation == 1)
                        {
                            double freight = 0;
                            if (revised == 1)
                            {
                                freight = quotation.ItemRevFreightCharges + ((quotation.ItemRevFreightCharges / 100) * quotation.ItemFreightTAX);
                            }
                            else
                            {
                                freight = quotation.ItemFreightCharges + ((quotation.ItemFreightCharges / 100) * quotation.ItemFreightTAX);
                            }

                            double reductionPrice = 0;
                            if (revised == 1)
                            {
                                reductionPrice = (quotation.UnitMRP - quotation.RevUnitPrice) * quotation.ProductQuantity;
                            }
                            else
                            {
                                reductionPrice = (quotation.UnitMRP - quotation.UnitPrice) * quotation.ProductQuantity;
                            }

                            var gst = quotation.CGst + quotation.SGst + quotation.IGst;

                            string xml = string.Empty;
                            xml = Utilities.GenerateForwardEmailBody("DiscountQuptationUploadXML");

                            string itemComments = string.Empty;

                            if (revised == 1)
                            {
                                itemComments = quotation.ItemLevelRevComments;
                            }
                            else if (revised == 0)
                            {
                                itemComments = quotation.ItemLevelInitialComments;
                            }

                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductDescription,
                                quotation.ProductQuantity, quotation.ProductBrand, itemPrice, unitPrice, gst,
                                quotation.SGst, quotation.IGst, quotation.UnitMRP, itemUnitDiscount,
                                Math.Round(Convert.ToDouble(reductionPrice), 2), itemComments, selectedVendorCurrency, revinstallationChargesWithTax,
                                revpackingChargesWithTax, freight,

                                revfreightChargesWithTax);
                            quotationItems += xml;
                        }
                        else if (requirement.IsDiscountQuotation == 2)
                        {
                            int decimalPoint = 4;

                            string xml = string.Empty;
                            xml = Utilities.GenerateForwardEmailBody("MarginQuptationUploadXML");
                            xml = String.Format(xml, quotation.ProductIDorName, quotation.ProductNo, quotation.ProductBrand,
                                quotation.ProductQuantity,
                                Math.Round(Convert.ToDouble(itemCostPrice), decimalPoint),
                                quotation.CGst + quotation.SGst + quotation.IGst,
                                Math.Round(Convert.ToDouble(quotation.UnitMRP), decimalPoint),
                                Math.Round(Convert.ToDouble(itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100)), decimalPoint),
                                Math.Round(Convert.ToDouble(quotation.UnitMRP - (itemCostPrice * (1 + (quotation.CGst + quotation.SGst + quotation.IGst) / 100))), decimalPoint),
                                Math.Round(Convert.ToDouble(itemUnitDiscount), decimalPoint));
                            quotationItems += xml;
                        }
                    }
                    else
                    {
                        string xml1 = string.Empty;
                        xml1 = Utilities.GenerateForwardEmailBody("DecQuptationUploadXML");
                        xml1 = String.Format(xml1, requirement.Title, requirement.Description, vendorBidPrice);
                        quotationItems = xml1;
                    }
                }

                string quotationTaxes = string.Empty;
                string folderPath = HttpContext.Current.Server.MapPath(Utilities.FILE_URL);
                List<RequirementTerms> listRequirementTerms = new List<RequirementTerms>();
                string deliveryTermsDays = string.Empty;
                string deliveryTermsPercent = string.Empty;
                string paymentTermsDays = string.Empty;
                string paymentTermsPercent = string.Empty;
                string paymentTermsType = string.Empty;

                deliveryTermsDays = "<td>Days</td><td>N/A</td>";
                deliveryTermsPercent = "<td>Percent</td><td>N/A</td>";

                paymentTermsDays = "<td>Days</td><td>N/A</td>";
                paymentTermsPercent = "<td>Percent</td><td>N/A</td>";
                paymentTermsType = "<td>Type</td><td>N/A</td>";
                if (listRequirementTerms.Count > 0)
                {

                    deliveryTermsDays = "<td>Days</td>";
                    deliveryTermsPercent = "<td>Percent</td>";
                    paymentTermsDays = "<td>Days</td>";
                    paymentTermsPercent = "<td>Percent</td>";
                    paymentTermsType = "<td>Type</td>";
                    foreach (RequirementTerms reqterms in listRequirementTerms)
                    {
                        if (reqterms.ReqTermsType == "DELIVERY")
                        {
                            string xmlfordeliverytermsdays = string.Empty;
                            xmlfordeliverytermsdays = Utilities.GenerateForwardEmailBody("QuptationUploadDeliveryTermsDaysXML");
                            xmlfordeliverytermsdays = String.Format(xmlfordeliverytermsdays, reqterms.ReqTermsDays);
                            deliveryTermsDays += xmlfordeliverytermsdays;
                            string xmlfordeliverytermspercent = string.Empty;
                            xmlfordeliverytermspercent = Utilities.GenerateForwardEmailBody("QuptationUploadDeliveryTermsPercentXML");
                            xmlfordeliverytermspercent = String.Format(xmlfordeliverytermspercent, reqterms.ReqTermsPercent);
                            deliveryTermsPercent += xmlfordeliverytermspercent;
                        }

                        if (reqterms.ReqTermsType == "PAYMENT")
                        {
                            string type = string.Empty;
                            if (reqterms.ReqTermsDays == 0)
                            {
                                type = "OD";
                            }
                            else if (reqterms.ReqTermsDays > 0)
                            {
                                type = "P";
                            }
                            else if (reqterms.ReqTermsDays < 0)
                            {
                                reqterms.ReqTermsDays = -(reqterms.ReqTermsDays);

                                type = "A";
                            }

                            string xmlforpaymenttermsDays = string.Empty;
                            xmlforpaymenttermsDays = Utilities.GenerateForwardEmailBody("QuptationUploadPaymentTermsDaysXML");
                            xmlforpaymenttermsDays = String.Format(xmlforpaymenttermsDays, reqterms.ReqTermsDays);
                            paymentTermsDays += xmlforpaymenttermsDays;

                            string xmlforpaymenttermsPercent = string.Empty;
                            xmlforpaymenttermsPercent = Utilities.GenerateForwardEmailBody("QuptationUploadPaymentTermsPercentXML");
                            xmlforpaymenttermsPercent = String.Format(xmlforpaymenttermsPercent, reqterms.ReqTermsPercent);
                            paymentTermsPercent += xmlforpaymenttermsPercent;

                            string xmlforpaymenttermsType = string.Empty;
                            xmlforpaymenttermsType = Utilities.GenerateForwardEmailBody("QuptationUploadPaymentTermsTypeXML");
                            xmlforpaymenttermsType = String.Format(xmlforpaymenttermsType, type);
                            paymentTermsType += xmlforpaymenttermsType;
                        }

                    }
                }

                Attachment emilAttachmentQuotationPDF = null;

                if (string.IsNullOrEmpty(desFileName))
                {
                    long ticks1 = DateTime.UtcNow.Ticks;
                    string fileName = string.Empty;
                    if (isUOMDiffertent)
                    {
                        vendorBidPrice = 0;
                        price = 0;
                    }

                    string title = requirement.Title;
                    List<KeyValuePair<string, DataTable>> dtbl = new List<KeyValuePair<string, DataTable>>();
                    if (requirement.IsDiscountQuotation == 0)
                    {

                        dtbl = FwdPdfUtility.MakeDataTableFwdQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, selectedVendorCurrency,
                        packingCharges, packingChargesTaxPercentage, packingChargesWithTax, revpackingCharges, revpackingChargesWithTax,
                        installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revinstallationCharges, revinstallationChargesWithTax,
                        freightCharges, freightChargesTaxPercentage, freightChargesWithTax, revfreightCharges, revfreightChargesWithTax,
                        INCO_TERMS);

                    }
                    else if (requirement.IsDiscountQuotation == 1)
                    {

                        //dtbl = FwdPdfUtility.MakeDataTableDiscountQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        //warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        //revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        //paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, selectedVendorCurrency,
                        //packingCharges, packingChargesTaxPercentage, packingChargesWithTax, revpackingCharges, revpackingChargesWithTax,
                        //installationCharges, installationChargesTaxPercentage, installationChargesWithTax, revinstallationCharges, revinstallationChargesWithTax,
                        //freightCharges, freightChargesTaxPercentage, freightChargesWithTax, revfreightCharges, revfreightChargesWithTax,
                        //INCO_TERMS);

                    }
                    else if (requirement.IsDiscountQuotation == 2)
                    {

                        //dtbl = FwdPdfUtility.MakeDataTableMarginQuotation(quotationObject, reqID, customerID, userID, sessionID,
                        //warranty, payment, duration, validity, price, tax, freightcharges, vendorBidPrice, requirement.IsTabular,
                        //revised, quotationTaxes, discountAmount, otherProperties, deliveryTermsDays, deliveryTermsPercent, paymentTermsDays,
                        //paymentTermsPercent, paymentTermsType, folderPath, gstNumber, isRegret, packingCharges,
                        //selectedVendorCurrency, revinstallationChargesWithTax, revpackingChargesWithTax, revfreightChargesWithTax,
                        //INCO_TERMS);

                    }

                    FwdPdfUtility.ExportDataTableFwdQuotationToPdf(dtbl, @folderPath + "reqQuotation" + reqID + userID + "_" + customerID + "_" + ticks1 + ".pdf", response.ObjectID, customerID, userID, sessionID, duration, gstNumber, validity, revised, title, reqID);

                    fileName = "reqQuotation" + reqID + userID + "_" + customerID + "_" + ticks1 + ".pdf";
                    Response responce = Utilities.SaveAttachment(fileName);
                    fileName = responce.ObjectID.ToString();
                    string attachmentsave = SaveFwdAttachments(reqID, sessionID, userID, fileName, revised, "PDFQUOTATION");
                    if (uploadType != "BULK_EXCEL")
                    {
                        var fileData = Utilities.DownloadFile(fileName, sessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentQuotationPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }
                if (uploadType != "BULK_EXCEL")
                {
                    if (!string.IsNullOrEmpty(desFileName))
                    {
                        string uploadattachmentsave = SaveFwdAttachments(reqID, sessionID, userID, desFileName, revised, "PDFQUOTATION");

                        var fileData = Utilities.DownloadFile(desFileName, sessionID);
                        if (fileData.FileStream != null && !string.IsNullOrEmpty(fileData.FileName))
                        {
                            emilAttachmentQuotationPDF = new Attachment(fileData.FileStream, fileData.FileName);
                        }
                    }
                }

                // SignalR

                if (uploadType != "BULK_EXCEL")
                {
                    SignalRObj signalRObj = new SignalRObj();
                    List<object> payLoad = new List<object>();
                    DataSet ds1 = GetRequirementDetailsHub(requirement.RequirementID);
                    FwdRequirement req = GetRequirementDataFilter(requirement.CustomerID, sessionID, ds1);
                    payLoad.Add(req);

                    InvolvedParties involvedParties = new InvolvedParties();
                    involvedParties.RequirementID = req.RequirementID;
                    involvedParties.CustomerID = req.CustomerID;
                    involvedParties.SuperUserID = req.SuperUserID;
                    involvedParties.CustCompID = req.CustCompID;
                    involvedParties.UserIDList = req.AuctionVendors.Select(x => x.VendorID).ToArray();
                    involvedParties.CallerID = CallerID;
                    var context = GlobalHost.ConnectionManager.GetHubContext<FwdRequirementHub>();
                    involvedParties.MethodName = "UploadQuotation";

                    if (revised == 1)
                    {
                        involvedParties.MethodName = "RevUploadQuotation";
                    }
                    req.Inv = involvedParties;

                    signalRObj.Inv = involvedParties;

                    foreach (int vendorID in involvedParties.UserIDList)
                    {
                        req = GetRequirementDataFilter(vendorID, sessionID, ds1);
                        payLoad.Add(req);
                    }

                    signalRObj.Inv = involvedParties;
                    signalRObj.PayLoad = payLoad.ToArray();
                    context.Clients.Group(Utilities.GroupName + "FWD" + req.RequirementID).checkRequirement(signalRObj);

                    // SignalR

                    UserInfo vendor = prmServices.GetUserNew(userID, sessionID);

                    User altUser = GetAlternateCommunications(reqID, userID, sessionID);

                    string emailBody = string.Empty;
                    emailBody = Utilities.GenerateForwardEmailBody("QuotationUploadedVendoremail");
                    emailBody = String.Format(emailBody, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);

                    if (revised == 0)
                    {
                        requirement.Module = ScreenName;
                    }
                    else if (revised == 1)
                    {
                        requirement.Module = RevScreenName;
                    }



                    Utilities.SendEmail(vendor.Email + "," + vendor.AltEmail + "," + altUser.AltEmail, "Your Quotation has been successfully posted for ReqID: " + reqID + " Title: " + requirement.Title, emailBody, requirement.RequirementID, Convert.ToInt32(vendor.UserID), requirement.Module,
                        sessionID, emilAttachmentQuotationPDF, VendorListAttachment).ConfigureAwait(false);
                    string body2 = Utilities.GenerateForwardEmailBody("QuotationUploadedVendorsms");
                    body2 = String.Format(body2, vendor.FirstName, vendor.LastName, response.ObjectID, reqID);
                    body2 = body2.Replace("<br/>", "");

                  //  Utilities.SendSMS(string.Empty, vendor.PhoneNum + "," + vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.RequirementID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                    FwdRequirement requirement1 = GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
                    UserDetails customer = prmServices.GetUserDetails(customerID, sessionID);

                    string bodySuper = Utilities.GenerateForwardEmailBody("QuotationUploadedemail");
                    bodySuper = String.Format(bodySuper, customer.FirstName, customer.LastName, vendor.FirstName, vendor.LastName, requirement1.Title);

                    string body2Super = Utilities.GenerateForwardEmailBody("QuotationUploadedsms");
                    body2Super = String.Format(body2Super, customer.FirstName, customer.LastName, vendor.FirstName, vendor.LastName, requirement1.Title);
                    body2Super = body2Super.Replace("<br/>", "");
                    Utilities.SendEmail(customer.Email + "," + customer.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - " + vendor.FirstName + " " + vendor.LastName + " has uploaded a quotation",
                        bodySuper, reqID, Convert.ToInt32(customer.UserID), requirement.Module, sessionID, emilAttachmentQuotationPDF, VendorListAttachment).ConfigureAwait(false);
                  //  Utilities.SendSMS(string.Empty, customer.PhoneNum + "," + customer.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2Super.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.RequirementID, Convert.ToInt32(customer.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                    string bodyTelegram = Utilities.GenerateForwardEmailBody("QuotationUploadTelegramsms");
                    bodyTelegram = String.Format(bodyTelegram, vendor.Institution, vendor.FirstName, vendor.LastName, requirement.Title, requirement.Description, price, tax, freightcharges, vendorBidPrice);
                    bodyTelegram = bodyTelegram.Replace("<br/>", "");

                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = "QUOTATION UPLOADED FOR THE REQ ID:" + reqID + (bodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                    Utilities.SendTelegramMsg(tgMsg);

                    if (requirement1.SuperUserID != requirement1.CustomerID)
                    {

                        UserInfo superUser = prmServices.GetSuperUser(requirement1.SuperUserID, sessionID);

                        string bodySuperUser = Utilities.GenerateForwardEmailBody("QuotationUploadedemail");
                        bodySuperUser = String.Format(bodySuperUser, superUser.FirstName, superUser.LastName, vendor.FirstName, vendor.LastName, requirement1.Title);

                        string body2SuperUser = Utilities.GenerateForwardEmailBody("QuotationUploadedsms");
                        body2SuperUser = String.Format(body2SuperUser, superUser.FirstName, superUser.LastName, vendor.FirstName, vendor.LastName, requirement1.Title);
                        body2SuperUser = body2SuperUser.Replace("<br/>", "");
                       // Utilities.SendSMS(string.Empty, superUser.PhoneNum + "," + superUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2SuperUser.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), requirement.RequirementID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID).ConfigureAwait(false);
                        Utilities.SendEmail(superUser.Email + "," + superUser.AltEmail, "ReqID: " + reqID + " Title: " + requirement.Title + " - " + vendor.FirstName + " " + vendor.LastName + " has uploaded a quotation",
                            bodySuperUser, reqID, Convert.ToInt32(vendor.UserID), requirement.Module, sessionID, emilAttachmentQuotationPDF, VendorListAttachment).ConfigureAwait(false);

                    }
                }
                else
                {
                    string BULKbodyTelegram = Utilities.GenerateForwardEmailBody("BULKQuotationUploadTelegramsms");
                    BULKbodyTelegram = String.Format(BULKbodyTelegram, reqID, userID);
                    BULKbodyTelegram = BULKbodyTelegram.Replace("<br/>", "");

                    TelegramMsg tgMsg = new TelegramMsg();
                    tgMsg.Message = "QUOTATION UPLOADED FOR THE REQ ID:" + reqID + (BULKbodyTelegram.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]);
                    Utilities.SendTelegramMsg(tgMsg);
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            response.SessionID = sessionID;
            UserInfo user = prmServices.GetUserNew(userID, sessionID);
            response.UserInfo = new UserInfo();
            response.UserInfo = user;
            return response;
        }

        public Response GetRevisedQuotations(int reqID, int userID, string sessionID)
        {
            Response response = new Response();
            try
            {
                FwdRequirement requirement = GetRequirementData(reqID, userID, sessionID);
                Dictionary<int, FwdRequirement> vendorsDictionary = new Dictionary<int, FwdRequirement>();
                foreach (VendorDetails vd in requirement.AuctionVendors)
                {
                    FwdRequirement vendorReq = GetRequirementData(reqID, vd.VendorID, sessionID);
                    vendorsDictionary.Add(vd.VendorID, vendorReq);
                }

                foreach (KeyValuePair<int, FwdRequirement> entry in vendorsDictionary)
                {
                    FwdRequirement req = entry.Value;
                    double priceWithoutTax = 0;
                    double priceWithTax = 0;
                    double gst = 0;
                    double freight = 0;//req.AuctionVendors[0].RevVendorFreight;
                    foreach (FwdRequirementItems item in req.ListFwdRequirementItems)
                    {
                        priceWithoutTax += item.RevItemPrice;
                        gst = item.CGst + item.IGst + item.SGst;
                        priceWithTax = priceWithoutTax * (1 + gst / 100);
                    }
                    if (req.IsNegotiationEnded == 1 && req.AuctionVendors[0].TotalRunningPrice > 0 && req.AuctionVendors[0].CompanyName != "PRICE_CAP")
                    {
                        List<FileUpload> multipleAttachments = new List<FileUpload>();

                        if (req.AuctionVendors[0].MultipleAttachments.Length > 0)
                        {
                            int[] la = Array.ConvertAll(req.AuctionVendors[0].MultipleAttachments.Split(','), int.Parse);
                            foreach (int a in la)
                            {
                                FileUpload fileUpload = new FileUpload();
                                fileUpload.FileID = a;
                                multipleAttachments.Add(fileUpload);
                            }
                        }


                        Response res = UploadQuotation(req.ListFwdRequirementItems, req.AuctionVendors[0].VendorID, reqID, sessionID, priceWithoutTax, gst, 
                            freight, req.AuctionVendors[0].RevVendorTotalPrice, req.AuctionVendors[0].Warranty,
                        req.AuctionVendors[0].Payment, req.AuctionVendors[0].Duration, req.AuctionVendors[0].Validity, 1, "", 
                        req.AuctionVendors[0].Discount, req.ListRequirementTaxes, req.AuctionVendors[0].OtherProperties, 
                        req.AuctionVendors[0].GstNumber, 0,0,0, 0, 0, 0, 0,0,0, 0, 
                        req.AuctionVendors[0].SelectedVendorCurrency, "BULK_EXCEL",
                        multipleAttachments, 
                        0,0, 0,0,0, 
                        req.AuctionVendors[0].INCO_TERMS);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage += ex.Message;
            }

            return response;
        }

        public Response SavePriceCap(int reqID, string sessionID, int userID, string reqType, double priceCapValue, int isUnitPriceBidding, bool IS_CB_ENABLED)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_REQ_TYPE", reqType);
                sd.Add("P_PRICE_CAP", priceCapValue);
                sd.Add("P_IS_UNIT_PRICE_BIDDING", isUnitPriceBidding);
                sd.Add("P_IS_CB_ENABLED", IS_CB_ENABLED);
                DataSet ds = sqlHelper.SelectList("fwd_SavePriceCap", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response QuatationApproval(int reqID, int customerID, int vendorID, bool value, string reason, string sessionID, string action)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                string ScreenName = "";
                if (value == false || reason == "")
                {
                    ScreenName = "VENDOR_QUOTATION_APPROVED";
                }
                else if (value == true && reason != "")
                {
                    ScreenName = "VENDOR_QUOTATION_REJECTED";
                }
                if (value)
                {
                    reason = "Reject Comments-" + reason;
                }
                else
                {

                    reason = "Approved-" + reason;
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", customerID);
                sd.Add("P_VENDOR_ID", vendorID);
                sd.Add("P_VALUE", value);
                sd.Add("P_REASON", reason);
                sd.Add("P_ACTION", action);
                DataSet ds = sqlHelper.SelectList("fwd_QuatationAprovel", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    string query = string.Format("INSERT INTO fwd_negotiationaudit " +
                    "(U_ID, REQ_ID, PRICE, BID_TIME, REJECT_RESON) " +
                    "SELECT U_ID, REQ_ID, REV_VEND_TOTAL_PRICE, UTC_TIMESTAMP, '{2}' " +
                    "FROM fwd_auctiondetails WHERE REQ_ID = {0} AND U_ID = {1};", reqID, vendorID, reason);
                    sqlHelper.ExecuteNonQuery_IUD(query);

                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;

                    FwdRequirement Requirement = GetRequirementDataOfflinePrivate(reqID, customerID, sessionID);
                    UserInfo Customer = prmServices.GetUser(customerID, sessionID);
                    UserInfo Vendor = prmServices.GetUser(vendorID, sessionID);

                    User altUser = GetAlternateCommunications(Requirement.RequirementID, Convert.ToInt32(Vendor.UserID), sessionID);

                    Requirement.Module = ScreenName;

                    if (!value && Requirement.IsDiscountQuotation != 2)
                    {
                        string body = Utilities.GenerateForwardEmailBody("VendoremailForQuotationAprovel");
                        body = String.Format(body, Vendor.FirstName, Vendor.LastName, reqID, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                        Utilities.SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + Requirement.Title + " - Quotation Shortlisted", body, reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);

                        string body2 = Utilities.GenerateForwardEmailBody("VendorsmsForQuotationAprovel");
                        body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqID, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                        body2 = body2.Replace("<br/>", "");
                     //   Utilities.SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);
                    }
                    else if (value && Requirement.IsDiscountQuotation != 2)
                    {
                        string body = Utilities.GenerateForwardEmailBody("VendoremailForQuotationRejection");
                        body = String.Format(body, Vendor.FirstName, Vendor.LastName, reqID, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                        Utilities.SendEmail(Vendor.Email + "," + Vendor.AltEmail + "," + altUser.AltEmail, "ReqID: " + reqID + " Title: " + Requirement.Title + " - Quotation Rejected", body, reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);

                        string body2 = Utilities.GenerateForwardEmailBody("VendorsmsForQuotationRejection");
                        body2 = String.Format(body2, Vendor.FirstName, Vendor.LastName, reqID, Requirement.Title, reason, Customer.FirstName, Customer.LastName);
                        body2 = body2.Replace("<br/>", "");
                      //  Utilities.SendSMS(string.Empty, Vendor.PhoneNum + "," + Vendor.AltPhoneNum + "," + altUser.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, vendorID, Requirement.Module, sessionID).ConfigureAwait(false);
                    }

                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private static string SaveFwdAttachments(int reqID, string sessionID, int userID, string attachmentID, int revised, string fileType)
        {
            IDatabaseHelper sqlHelper = DatabaseProvider.GetDatabaseProvider();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_U_ID", userID);
                sd.Add("P_ATTACHMENT_ID", attachmentID);
                sd.Add("P_IS_REVISED", revised);
                sd.Add("P_FILE_TYPE", fileType);
                DataSet ds = sqlHelper.SelectList("fwd_SaveQuotationAttachment", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    response.ErrorMessage = ds.Tables[0].Rows[0][1] != DBNull.Value ? Convert.ToString(ds.Tables[0].Rows[0][1].ToString()) : string.Empty;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response.ErrorMessage;
        }

        private Response AddVendorToAuction(VendorDetails vendor, int reqID, string sessionID, int userID)
        {
            Response response = new Response();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", vendor.VendorID);
                sd.Add("P_INIT_PRICE", vendor.InitialPrice);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_CREATED_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_AddVendorToAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            return response;
        }

        private Response RemoveVendorFromAuction(int id, int reqID, string sessionID, string templateName, FwdRequirement requirement)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                sd.Add("P_U_ID", id);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_RemoveVendorFromAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                    if (response.ObjectID > 0)
                    {
                        UserInfo user = prmServices.GetUser(id, sessionID);
                        string body = Utilities.GenerateForwardEmailBody("VendoremailForRemoval");
                        body = String.Format(body, user.FirstName, user.LastName, reqID);
                        Utilities.SendEmail(user.Email + "," + user.AltEmail, "ReqID: " + reqID + " - Please ignore the following requirement", body, reqID, Convert.ToInt32(user.UserID), sessionID).ConfigureAwait(false);
                        string body2 = Utilities.GenerateForwardEmailBody("VendorsmsForRemoval");
                        body2 = String.Format(body2, user.FirstName, user.LastName, reqID);
                        body2 = body2.Replace("<br/>", "");
                      //  Utilities.SendSMS(string.Empty, user.PhoneNum + "," + user.AltPhoneNum, System.Web.HttpUtility.UrlEncode(body2.Split(new string[] { "Thank You" }, StringSplitOptions.None)[0]), reqID, Convert.ToInt32(user.UserID), sessionID).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;

        }

        private int[] GetVendorsToRemove(FwdRequirement requirement, FwdRequirement oldReq)
        {
            List<int> oldVendorList = oldReq.AuctionVendors.Select(x => x.VendorID).ToList();
            List<int> newVendorList = requirement.AuctionVendors.Select(x => x.VendorID).ToList();
            int[] removedVendors = oldVendorList.Except(newVendorList).ToList().ToArray();
            return removedVendors;
        }

        private int[] GetItemsToRemove(FwdRequirement requirement, FwdRequirement oldReq)
        {
            List<int> oldItemsList = oldReq.ListFwdRequirementItems.Select(x => x.ItemID).ToList();
            List<int> newItemsList = requirement.ListFwdRequirementItems.Select(x => x.ItemID).ToList();
            int[] removedItems = oldItemsList.Except(newItemsList).ToList().ToArray();
            return removedItems;
        }

        private Response SaveRequirementItems(FwdRequirementItems RequirementItems, int reqID, string sessionID, int userID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                string fileName = string.Empty;
                if (!string.IsNullOrEmpty(RequirementItems.AttachmentBase64))
                {
                    string[] stringSeparators = new string[] { "base64," };
                    string tempAttach = RequirementItems.AttachmentBase64;
                    if (RequirementItems.AttachmentBase64.ToLower().Contains("base64"))
                    {
                        tempAttach = RequirementItems.AttachmentBase64.Split(stringSeparators, StringSplitOptions.None)[1];
                    }

                    RequirementItems.ItemAttachment = Convert.FromBase64String(tempAttach);
                }

                if (RequirementItems.ItemAttachment != null && RequirementItems.ItemAttachment.Length > 0)
                {
                    long tick = DateTime.UtcNow.Ticks;
                    fileName = System.Web.HttpContext.Current.Server.MapPath(Utilities.FILE_URL + "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName);
                    Utilities.SaveFile(fileName, RequirementItems.ItemAttachment);

                    fileName = "req" + tick + "_user" + userID + "_" + RequirementItems.AttachmentName;
                    Response res = Utilities.SaveAttachment(fileName);
                    if (res.ErrorMessage != "")
                    {
                        response.ErrorMessage = res.ErrorMessage;
                    }
                    fileName = res.ObjectID.ToString();
                }
                else if (RequirementItems.ProductImageID > 0)
                {
                    fileName = Convert.ToString(RequirementItems.ProductImageID);
                }

                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_ITEM_ID", RequirementItems.ItemID);
                sd.Add("P_PROD_ID", RequirementItems.ProductIDorName);
                sd.Add("P_PROD_NO", RequirementItems.ProductNo);
                sd.Add("P_PROD_CODE", RequirementItems.ProductCode);
                sd.Add("P_DESCRIPTION", RequirementItems.ProductDescription);
                // sd.Add("P_QUANTITY", RequirementItems.ProductQuantity);
                sd.Add("P_QUANTITY_IN", RequirementItems.ProductQuantityIn);
                sd.Add("P_BRAND", RequirementItems.ProductBrand);
                sd.Add("P_OTHER_BRAND", RequirementItems.OthersBrands);
                sd.Add("P_ITEM_MIN_REDUCTION", RequirementItems.ItemMinReduction);
                sd.Add("P_IMAGE_ID", fileName);
                sd.Add("P_U_ID", userID);
                sd.Add("P_IS_DELETED", RequirementItems.IsDeleted);
                sd.Add("P_LAST_ITEM_PRICE", RequirementItems.ItemLastPrice);
                sd.Add("P_I_LLP_DETAILS", RequirementItems.I_LLP_DETAILS);
                sd.Add("P_CATALOGUE_ITEM_ID", RequirementItems.CatalogueItemID);
                sd.Add("P_HSN_CODE", RequirementItems.HsnCode);

                string json = string.Empty;
                List<CATALOG.ProductQuotationTemplate> listObj = new List<CATALOG.ProductQuotationTemplate>();
                List<CATALOG.ProductQuotationTemplate> jsonToList = new List<CATALOG.ProductQuotationTemplate>();

                CATALOG.ProductQuotationTemplate obj1 = new CATALOG.ProductQuotationTemplate();
                obj1.NAME = "Total";
                obj1.IS_CALCULATED = 1;
                obj1.IS_VALID = 1;

                CATALOG.ProductQuotationTemplate obj2 = new CATALOG.ProductQuotationTemplate();
                obj2.NAME = "Margin 15%";
                obj2.IS_CALCULATED = 2;
                obj2.IS_VALID = 1;

                CATALOG.ProductQuotationTemplate obj3 = new CATALOG.ProductQuotationTemplate();
                obj3.NAME = "Rejection 3%";
                obj3.IS_CALCULATED = 3;
                obj3.IS_VALID = 1;

                CATALOG.ProductQuotationTemplate obj4 = new CATALOG.ProductQuotationTemplate();
                obj4.NAME = "Testing";
                obj4.IS_CALCULATED = 4;
                obj4.IS_VALID = 1;

                CATALOG.ProductQuotationTemplate obj5 = new CATALOG.ProductQuotationTemplate();
                obj5.NAME = "Final FOB";
                obj5.IS_CALCULATED = 5;
                obj5.IS_VALID = 1;

                if (RequirementItems != null && RequirementItems.ProductQuotationTemplateJson != null && RequirementItems.ProductQuotationTemplateJson != "")
                {
                    jsonToList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<CATALOG.ProductQuotationTemplate>>(RequirementItems.ProductQuotationTemplateJson);

                    jsonToList.Add(obj1);
                    jsonToList.Add(obj2);
                    jsonToList.Add(obj3);
                    jsonToList.Add(obj4);
                    jsonToList.Add(obj5);

                    json = JsonConvert.SerializeObject(jsonToList);
                }

                sd.Add("P_PRODUCT_QUOTATION_JSON", json);

                if (RequirementItems.SplitEnabled)
                {
                    sd.Add("P_QUANTITY", 1);
                    sd.Add("P_SPLIT_ENABLED", 1);
                    sd.Add("P_FROMRANGE", RequirementItems.FromRange);
                    sd.Add("P_TORANGE", RequirementItems.ToRange);
                    sd.Add("P_REQ_QTY", RequirementItems.RequiredQuantity);
                }
                else
                {
                    sd.Add("P_QUANTITY", RequirementItems.ProductQuantity);
                    sd.Add("P_SPLIT_ENABLED", 0);
                    sd.Add("P_FROMRANGE", 0);
                    sd.Add("P_TORANGE", 0);
                    sd.Add("P_REQ_QTY", 0);
                }

                DataSet ds = sqlHelper.SelectList("fwd_SaveRequirementItems", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }
            finally
            {
                //cmd.Connection.Close();
            }

            return response;
        }

        private Response RemoveItemFromAuction(int id, int reqID, string sessionID, string templateName)
        {
            Response response = new Response();
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_ITEM_ID", id);
                sd.Add("P_REQ_ID", reqID);
                DataSet ds = sqlHelper.SelectList("fwd_RemoveItemFromAuction", sd);
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0 && ds.Tables[0].Rows[0][0] != null)
                {
                    response.ObjectID = ds.Tables[0].Rows[0][0] != DBNull.Value ? Convert.ToInt16(ds.Tables[0].Rows[0][0].ToString()) : -1;                   
                }
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        private List<UserInfo> GetUsersInvolved(int reqid, string type, int userID = 0)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<UserInfo> test = new List<UserInfo>();
            try
            {
                sd.Add("P_REQ_ID", reqid);
                sd.Add("P_TYPE", type);
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetUsersInvolved", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        UserInfo user = new UserInfo();
                        user.UserID = row["U_ID"] != DBNull.Value ? Convert.ToString(row["U_ID"]) : string.Empty;
                        user.FirstName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        user.LastName = row["U_LNAME"] != DBNull.Value ? Convert.ToString(row["U_LNAME"]) : string.Empty;
                        user.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        user.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        user.UserType = row["U_TYPE"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.PhoneID = row["U_PHONE_TOKEN"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.PhoneOS = row["U_PHONE_OS"] != DBNull.Value ? Convert.ToString(row["U_TYPE"]) : string.Empty;
                        user.AltPhoneNum = row["U_ALTPHONE"] != DBNull.Value ? row["U_ALTPHONE"].ToString() : string.Empty;
                        user.AltEmail = row["U_ALTEMAIL"] != DBNull.Value ? row["U_ALTEMAIL"].ToString() : string.Empty;
                        test.Add(user);
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return test;
        }

        public List<FwdRequirement> GetMyAuctions(int userID, string sessionID)
        {
            List<FwdRequirement> myAuctions = new List<FwdRequirement>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetRequirements", sd);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        FwdRequirement fwdrequirement = new FwdRequirement();

                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        fwdrequirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        fwdrequirement.CustomerID = userID;
                        fwdrequirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        fwdrequirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (fwdrequirement.EndTime == null)
                        {
                            fwdrequirement.EndTime = DateTime.MaxValue;
                        }
                        fwdrequirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (fwdrequirement.StartTime == null)
                        {
                            fwdrequirement.StartTime = DateTime.MaxValue;
                        }
                        fwdrequirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        fwdrequirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.UtcNow && RunPrice > 0)
                        {
                            fwdrequirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        fwdrequirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        fwdrequirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        fwdrequirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        fwdrequirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        fwdrequirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        fwdrequirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        fwdrequirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        fwdrequirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        fwdrequirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        fwdrequirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt16(row["IS_DISCOUNT_QUOTATION"]) : 0;
                        fwdrequirement.NoOfVendorsInvited = row["NO_OF_VENDORS_INVITED"] != DBNull.Value ? Convert.ToInt16(row["NO_OF_VENDORS_INVITED"]) : 0;
                        fwdrequirement.NoOfVendorsParticipated = row["NO_OF_VENDORS_PARTICIPATED"] != DBNull.Value ? Convert.ToInt16(row["NO_OF_VENDORS_PARTICIPATED"]) : 0;
                        fwdrequirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.UtcNow;
                        fwdrequirement.DeliveryTime = row["REQ_DELIVERY_TIME"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_TIME"]) : string.Empty;
                        fwdrequirement.ExpStartTime = row["EXP_START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["EXP_START_TIME"]) : DateTime.UtcNow;
                        fwdrequirement.UserName = row["USER_NAME"] != DBNull.Value ? Convert.ToString(row["USER_NAME"]) : string.Empty;
                        fwdrequirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                        fwdrequirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.UtcNow;
                        fwdrequirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                        fwdrequirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;
                        fwdrequirement.TotalCount = 0;
                        DateTime now = DateTime.UtcNow;
                        if (fwdrequirement.EndTime != DateTime.MaxValue && fwdrequirement.EndTime > now && fwdrequirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(fwdrequirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            fwdrequirement.TimeLeft = diff;
                            fwdrequirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (fwdrequirement.StartTime != DateTime.MaxValue && fwdrequirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(fwdrequirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            fwdrequirement.TimeLeft = diff;
                            fwdrequirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (fwdrequirement.EndTime < now)
                        {
                            fwdrequirement.TimeLeft = -1;
                            if ((Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString()) || Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString())) && fwdrequirement.EndTime < now)
                            {

                                fwdrequirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NegotiationEnded.ToString());
                                EndNegotiation(fwdrequirement.RequirementID, fwdrequirement.CustomerID, sessionID);
                            }
                            else
                            {
                                fwdrequirement.Status = Status;
                            }
                        }
                        else if (fwdrequirement.StartTime == DateTime.MaxValue)
                        {
                            fwdrequirement.TimeLeft = -1;
                            fwdrequirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }
                        if (Status == Utilities.GetEnumDesc<PRMStatus>(PRMStatus.DELETED.ToString()))
                        {
                            fwdrequirement.TimeLeft = -1;
                            fwdrequirement.Status = Status;
                        }

                        if (fwdrequirement.IS_CB_ENABLED)
                        {
                            if (fwdrequirement.CB_END_TIME > now)
                            {
                                fwdrequirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                        }

                        fwdrequirement.AuctionVendors = new List<VendorDetails>();
                        fwdrequirement.CustFirstName = string.Empty;
                        fwdrequirement.CustLastName = string.Empty;
                        //requirement.Status = string.Empty;
                        fwdrequirement.SessionID = string.Empty;
                        fwdrequirement.ErrorMessage = string.Empty;

                        myAuctions.Add(fwdrequirement);
                    }
                }
            }
            catch (Exception ex)
            {
                FwdRequirement fwdrequirement1 = new FwdRequirement();
                fwdrequirement1.ErrorMessage = ex.Message;
                myAuctions.Add(fwdrequirement1);
            }

            return myAuctions;
        }

        public List<FwdRequirement> GetActiveLeads(int userID, string sessionID)
        {
            List<FwdRequirement> myAuctions = new List<FwdRequirement>();
            try
            {
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                DataSet ds = sqlHelper.SelectList("fwd_GetActiveLeads", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        FwdRequirement requirement = new FwdRequirement();
                        string[] arr = new string[] { };
                        byte[] next = new byte[] { };
                        requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt16(row["REQ_ID"]) : -1;
                        requirement.CustomerID = userID;
                        requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        requirement.EndTime = row["END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["END_TIME"]) : DateTime.MaxValue;
                        if (requirement.EndTime == null)
                        {
                            requirement.EndTime = DateTime.MaxValue;
                        }
                        requirement.StartTime = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (requirement.StartTime == null)
                        {
                            requirement.StartTime = DateTime.MaxValue;
                        }
                        requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        requirement.Price = row["VEND_MIN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_PRICE"]) : 0;
                        double RunPrice = row["VEND_MIN_RUN_PRICE"] != DBNull.Value ? Convert.ToDouble(row["VEND_MIN_RUN_PRICE"]) : 0;
                        DateTime start = row["START_TIME"] != DBNull.Value ? Convert.ToDateTime(row["START_TIME"]) : DateTime.MaxValue;
                        if (start < DateTime.UtcNow && RunPrice > 0)
                        {
                            requirement.Price = RunPrice;
                        }
                        string POLink = row["PURCHASE_ORDER_LINK"] != DBNull.Value ? (row["PURCHASE_ORDER_LINK"].ToString()) : string.Empty;
                        requirement.Description = row["REQ_DESC"] != DBNull.Value ? Convert.ToString(row["REQ_DESC"]) : string.Empty;
                        requirement.Category = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]).Split(',') : arr;
                        requirement.Urgency = row["REQ_URGENCY"] != DBNull.Value ? Convert.ToString(row["REQ_URGENCY"]) : string.Empty;
                        requirement.Budget = row["REQ_BUDGET"] != DBNull.Value ? Convert.ToString(row["REQ_BUDGET"]) : string.Empty;
                        requirement.DeliveryLocation = row["REQ_DELIVERY_LOC"] != DBNull.Value ? Convert.ToString(row["REQ_DELIVERY_LOC"]) : string.Empty;
                        requirement.Taxes = row["REQ_TAXES"] != DBNull.Value ? Convert.ToString(row["REQ_TAXES"]) : string.Empty;
                        requirement.PaymentTerms = row["REQ_PAYMENT_TERMS"] != DBNull.Value ? Convert.ToString(row["REQ_PAYMENT_TERMS"]) : string.Empty;
                        requirement.Currency = row["REQ_CURRENCY"] != DBNull.Value ? Convert.ToString(row["REQ_CURRENCY"]) : string.Empty;
                        string Status = row["CLOSED"] != DBNull.Value ? Convert.ToString(row["CLOSED"]) : string.Empty;
                        requirement.CustomerID = row["U_ID"] != DBNull.Value ? Convert.ToInt16(row["U_ID"]) : 0;
                        requirement.SelectedVendorID = row["SELECTED_VENDOR_ID"] != DBNull.Value ? Convert.ToInt16(row["SELECTED_VENDOR_ID"]) : 0;
                        requirement.QuotationFreezTime = row["QUOTATION_FREEZ_TIME"] != DBNull.Value ? Convert.ToDateTime(row["QUOTATION_FREEZ_TIME"]) : DateTime.MaxValue;
                        requirement.IsDiscountQuotation = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt16(row["IS_DISCOUNT_QUOTATION"]) : 0;

                        requirement.CustomerCompanyName = row["COMPANY_NAME"] != DBNull.Value ? Convert.ToString(row["COMPANY_NAME"]) : string.Empty;
                        requirement.IsRevUnitDiscountEnable = row["IS_REV_UNIT_DISCOUNT_ENABLE"] != DBNull.Value ? Convert.ToInt32(row["IS_REV_UNIT_DISCOUNT_ENABLE"]) : 0;

                        requirement.IS_CB_ENABLED = row["IS_CB_ENABLED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_ENABLED"]) == 1 ? true : false) : false;
                        requirement.CB_END_TIME = row["CB_END_TIME"] != DBNull.Value ? Convert.ToDateTime(row["CB_END_TIME"]) : DateTime.UtcNow;
                        requirement.CB_STOP_QUOTATIONS = row["CB_STOP_QUOTATIONS"] != DBNull.Value ? (Convert.ToInt32(row["CB_STOP_QUOTATIONS"]) == 1 ? true : false) : false;
                        requirement.IS_CB_COMPLETED = row["IS_CB_COMPLETED"] != DBNull.Value ? (Convert.ToInt32(row["IS_CB_COMPLETED"]) == 1 ? true : false) : false;
                        int isQuotationRejected = row["IS_DISCOUNT_QUOTATION"] != DBNull.Value ? Convert.ToInt16(row["IS_DISCOUNT_QUOTATION"]) : -1;
                        requirement.TotalCount = 0;
                        DateTime now = DateTime.UtcNow;
                        if (requirement.EndTime != DateTime.MaxValue && requirement.EndTime > now && requirement.StartTime < now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.EndTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());

                        }
                        else if (requirement.StartTime != DateTime.MaxValue && requirement.StartTime > now)
                        {
                            DateTime date = Convert.ToDateTime(requirement.StartTime);
                            long diff = Convert.ToInt64((date - now).TotalSeconds);
                            requirement.TimeLeft = diff;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.NOTSTARTED.ToString());
                        }
                        else if (requirement.EndTime < now)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Status;
                        }
                        else if (requirement.StartTime == DateTime.MaxValue)
                        {
                            requirement.TimeLeft = -1;
                            requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.UNCONFIRMED.ToString());
                        }

                        if (requirement.IS_CB_ENABLED)
                        {
                            if (requirement.CB_END_TIME > now && isQuotationRejected == 0)
                            {
                                requirement.Status = Utilities.GetEnumDesc<PRMStatus>(PRMStatus.STARTED.ToString());
                            }
                        }

                        requirement.AuctionVendors = new List<VendorDetails>();
                        requirement.CustFirstName = string.Empty;
                        requirement.CustLastName = string.Empty;
                        requirement.SessionID = string.Empty;
                        requirement.ErrorMessage = string.Empty;

                        myAuctions.Add(requirement);
                    }
                }

            }
            catch (Exception ex)
            {
                FwdRequirement auction = new FwdRequirement();
                auction.ErrorMessage = ex.Message;
                myAuctions.Add(auction);
            }

            return myAuctions;
        }

        public List<FwdRequirement> GetCompanyLeads(int userID, string searchString, string searchType, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<FwdRequirement> ListRequirement = new List<FwdRequirement>();

            if (string.IsNullOrEmpty(searchString))
            {
                return ListRequirement;
            }

            searchString = "%" + searchString + "%";
            try
            {
                //int isValidSession = ValidateSession(sessionID, cmd);
                sd.Add("P_U_ID", userID);
                sd.Add("P_SEARCH_STRING", searchString);
                sd.Add("P_SEARCH_TYPE", searchType);
                DataSet ds = sqlHelper.SelectList("fwd_GetCompanyLeads", sd);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        FwdRequirement Requirement = new FwdRequirement();
                        Requirement.RequirementID = row["REQ_ID"] != DBNull.Value ? Convert.ToInt32(row["REQ_ID"]) : 0;
                        Requirement.Title = row["REQ_TITLE"] != DBNull.Value ? Convert.ToString(row["REQ_TITLE"]) : string.Empty;
                        Requirement.StringCategory = row["REQ_CATEGORY"] != DBNull.Value ? Convert.ToString(row["REQ_CATEGORY"]) : string.Empty;
                        Requirement.Savings = row["SAVINGS"] != DBNull.Value ? Convert.ToInt32(row["SAVINGS"]) : 0;
                        Requirement.VendorsCount = row["VENDORS_COUNT"] != DBNull.Value ? Convert.ToInt32(row["VENDORS_COUNT"]) : 0;
                        Requirement.IntialLeastVendorPrice = row["INITIAL_PRICE"] != DBNull.Value ? Convert.ToDouble(row["INITIAL_PRICE"]) : 0;
                        Requirement.LeastVendorPrice = row["CLOSING_PRICE"] != DBNull.Value ? Convert.ToDouble(row["CLOSING_PRICE"]) : 0;
                        Requirement.PostedOn = row["REQ_POSTED_ON"] != DBNull.Value ? Convert.ToDateTime(row["REQ_POSTED_ON"]) : DateTime.MaxValue;
                        Requirement.Title = Requirement.Title.ToUpper();
                        ListRequirement.Add(Requirement);
                    }
                }
            }
            catch (Exception ex)
            {
                FwdRequirement Requirement = new FwdRequirement();
                Requirement.ErrorMessage = ex.Message;
                ListRequirement.Add(Requirement);
            }

            return ListRequirement;
        }

        public Response UpdatePriceCap(int uID, int reqID, double price, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            Response response = new Response();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_U_ID", uID);
                sd.Add("P_REQ_ID", reqID);
                sd.Add("P_PRICE", price);
                sqlHelper.SelectList("fwd_UpdateSingleVendorPrice", sd);
                response.ObjectID = uID;
            }
            catch (Exception ex)
            {
                response.ErrorMessage = ex.Message;
            }

            return response;
        }

        public Response BookSlots(SlotBooking sltbook, string sessionID) {
            Response bookedSlots = new Response();

            PRMSlotService bookSlot = new PRMSlotService();
            bookedSlots = bookSlot.BookSlots(sltbook, sessionID);

            return bookedSlots;
        }
        
        public Response ApproveRejectSlot(int flagValue, int vendorID, int reqID, string sessionID,int customerID, int slotID)
        {
            Response bookedSlots = new Response();

            PRMSlotService bookSlot = new PRMSlotService();
            bookedSlots = bookSlot.ApproveRejectSlot(flagValue, vendorID, reqID, sessionID, customerID, slotID);

            return bookedSlots;
        }

        public List<SlotBooking> GetSlotsBySlotId(int slotId, int ReqId, string sessionID)
        {
            SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
            List<SlotBooking> ListSlotBooking = new List<SlotBooking>();
            try
            {
                int isValidSession = Utilities.ValidateSession(sessionID);
                sd.Add("P_SLOT_ID", slotId);
                sd.Add("P_REQ_ID", ReqId);
                DataSet ds = sqlHelper.SelectList("fwd_GetSlotsBySlotId", sd);

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        SlotBooking SlotBook = new SlotBooking();

                        SlotBook.Vendor = new VendorDetails();
                        SlotBook.Vendor.Vendor = new User();

                        SlotBook.Slot_id = row["slot_id"] != DBNull.Value ? Convert.ToInt32(row["slot_id"]) : 0;
                        SlotBook.Vendor.VendorID = row["U_ID"] != DBNull.Value ? Convert.ToInt32(row["U_ID"]) : 0;
                        SlotBook.Vendor.VendorName = row["U_FNAME"] != DBNull.Value ? Convert.ToString(row["U_FNAME"]) : string.Empty;
                        SlotBook.Vendor.Vendor.Email = row["U_EMAIL"] != DBNull.Value ? Convert.ToString(row["U_EMAIL"]) : string.Empty;
                        SlotBook.Vendor.Vendor.PhoneNum = row["U_PHONE"] != DBNull.Value ? Convert.ToString(row["U_PHONE"]) : string.Empty;
                        SlotBook.Vendor.CompanyName = row["COMP_NAME"] != DBNull.Value ? Convert.ToString(row["COMP_NAME"]) : string.Empty;
                        ListSlotBooking.Add(SlotBook);
                    }
                }
            }
            catch (Exception ex)
            {
                SlotBooking SlotBook = new SlotBooking();
                SlotBook.ErrorMessage = ex.Message;
                ListSlotBooking.Add(SlotBook);
            }
            return ListSlotBooking;
        }

        public Response DeleteSlot(int slotID, int entityID, string sessionID)
        {
            Response deleteSlots = new Response();

            PRMSlotService deleteSlot = new PRMSlotService();
            deleteSlots = deleteSlot.DeleteSlot(slotID, entityID, sessionID);

            return deleteSlots;
        }


        public static DateTime toLocal(DateTime? date)
        {
            DateTime convertedDate = DateTime.SpecifyKind(DateTime.Parse(date.ToString()), DateTimeKind.Utc);

            var kind = convertedDate.Kind;

            DateTime dt = convertedDate.ToLocalTime();

            return dt;
        }

    }
}