﻿using System;
using System.IO;
using System.Data;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Collections.Generic;
using System.ServiceModel.Activation;

using OfficeOpenXml;
using PRMServices.Common;
using PRMServices.Models;
using MySql.Data.MySqlClient;
using PRMServices;
using PdfSharp.Pdf;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PRMServices.SQLHelper;
//using PRM.Core.Models.Reports;
using CORE = PRM.Core.Common;

namespace PRMServices
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class PRMAnalysisService : IPRMAnalysisService
    {

        private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();
        private MySQLBizClass sqlHelper = new MySQLBizClass();
        public PRMServices prm = new PRMServices();

        #region Services

        public PurchaserAnalysis GetPurchaserAnalysis(int userID, DateTime fromDate, DateTime toDate, string sessionid)
        {
            PurchaserAnalysis details = new PurchaserAnalysis();
            try
            {
                Utilities.ValidateSession(sessionid);
                SortedDictionary<object, object> sd = new SortedDictionary<object, object>() { };
                sd.Add("P_U_ID", userID);
                sd.Add("P_FROM_DATE", fromDate);
                sd.Add("P_TO_DATE", toDate);
                
                var dataset = sqlHelper.SelectList("cp_GetPurchaserAnalysis", sd);
                if (dataset != null && dataset.Tables.Count > 0)
                {
                    if (dataset.Tables[0].Rows.Count > 0 && dataset.Tables[0].Rows[0][0] != null)
                    {
                        DataRow row = dataset.Tables[0].Rows[0];
                        details.USER_SAVINGS = row["PARAM_USER_SAVINGS"] != DBNull.Value ? Convert.ToDouble(row["PARAM_USER_SAVINGS"]) : 0;
                        details.TOTAL_VOLUME = row["PARAM_TOTAL_VOLUME"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_VOLUME"]) : 0;
                        details.TOTAL_MATERIAL = row["PARAM_TOTAL_MATERIAL"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_MATERIAL"]) : 0;
                        details.TOTAL_RFQ = row["PARAM_TOTAL_RFQ"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_RFQ"]) : 0;
                        details.TOTAL_VENDORS = row["PARAM_TOTAL_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_TOTAL_VENDORS"]) : 0;
                        details.ADDED_VENDORS = row["PARAM_ADDED_VENDORS"] != DBNull.Value ? Convert.ToInt32(row["PARAM_ADDED_VENDORS"]) : 0;
                        details.RFQ_TAT = row["RFQTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["RFQTURNAROUNDTIME"]) : 0;
                        details.QCS_TAT = row["QCSTURNAROUNDTIME"] != DBNull.Value ? Convert.ToDouble(row["QCSTURNAROUNDTIME"]) : 0;
                        details.TOTAL_ITEM_SUM = row["PARAM_TOTAL_ITEM_SUM"] != DBNull.Value ? Convert.ToDouble(row["PARAM_TOTAL_ITEM_SUM"]) : 0;
                    }

                }

            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error in retrieving Data");
            }

            return details;
        }

        #endregion Services


    }

}