﻿prmApp

    .controller('contractSpendCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService",
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.contractSpend = function () {
                var gaugeOptions = {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'solidgauge'
                    },
                   // title: '% Contract Spend',
                    pane: {
                        center: ['50%', '85%'],
                        size: '100%',
                        startAngle: -90,
                        endAngle: 90,
                        background: {
                            backgroundColor:
                                Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
                            innerRadius: '60%',
                            outerRadius: '100%',
                            shape: 'arc'
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    tooltip: {
                        enabled: false
                    },
                    // the value axis
                    yAxis: {
                        stops: [
                            [0.1, '#55BF3B'], // green
                            [0.5, '#DDDF0D'], // yellow
                            [0.9, '#DF5353'] // red
                        ],
                        lineWidth: 0,
                        tickWidth: 0,
                        minorTickInterval: null,
                        tickAmount: 2,
                        title: {
                            y: -70
                        },
                        labels: {
                            y: 16
                        }
                    },
                    plotOptions: {
                        solidgauge: {
                            dataLabels: {
                                y: 5,
                                borderWidth: 0,
                                useHTML: true
                            }
                        }
                    }
                };
                // The speed gauge
                var chartSpeed = Highcharts.chart('contractSpend', Highcharts.merge(gaugeOptions, {
                    title: '% Contract Spend',
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Contract Spend'
                        }
                    },
                    credits: {
                        enabled: false
                    },
                    series: [{
                        name: 'Contract Spend',
                        data: [55],
                        dataLabels: {
                            format:
                                '<div style="text-align:center">' +
                                '<span style="font-size:25px"><strong>Contract Spend</strong></span><br/>' +
                                '<span style="font-size:25px">{y} %</span><br/>' +
                                '<span style="font-size:12px;opacity:0.4">Contract Spend %</span>' +
                                '</div>'
                        },
                        tooltip: {
                            valueSuffix: ' %'
                        }
                    }]

                }));

                // The RPM gauge
                var chartRpm = Highcharts.chart('totalSavings', Highcharts.merge(gaugeOptions, {
                    title: 'Total Savings',
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: 'Total Savings'
                        }
                    },

                    series: [{
                        name: 'Total Savings',
                        data: [25],
                        dataLabels: {
                            format:
                                '<div style="text-align:center">' +
                                '<span style="font-size:25px"><strong>Total Savings</strong></span><br/>' +
                                '<span style="font-size:25px">{y:.1f}</span><br/>' +
                                '<span style="font-size:12px;opacity:0.4">' +
                                'Potential Savings $6.14m' +
                                '</span>' +
                                '</div>'
                        },
                        tooltip: {
                            valueSuffix: ' revolutions/min'
                        }
                    }]

                }));

                // Bring life to the dials
                var point,
                    newVal,
                    inc;

                if (chartSpeed) {
                    point = chartSpeed.series[0].points[0];
                    inc = Math.round((Math.random() - 0.5) * 100);
                    newVal = point.y;

                    if (newVal < 0 || newVal > 200) {
                        newVal = point.y;
                    }

                    point.update(newVal);
                }
                //setInterval(function () {
                //    // Speed
                    

                    
                //}, 2000);
                //// RPM
                    if (chartRpm) {
                        point = chartRpm.series[0].points[0];
                        inc = Math.random() - 0.5;
                        newVal = point.y;

                        if (newVal < 0 || newVal > 5) {
                            newVal = point.y;
                        }

                        point.update(newVal);
                    }

            }
           $scope.contractSpend();


        

            $scope.categoriesBySpend = function () {
                Highcharts.chart('categoriesBySpend', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Top 5 Sub Categories by Spend'
                    },
                    xAxis: {
                        min: 0,
                       
                        categories: ['Copiers And Fax', 'Office Machines', 'Storage and Organisation', 'Tables', 'Telephones']
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        reversed: true
                    },

                    plotOptions: {
                        series: {
                            stacking: 'normal',
                            dataLabels: {
                                enabled: false
                            }
                        }
                    },
                    series: [{
                        showInLegend: false,  
                        data: [2,4, 6, 7, 9]
                    }, {
                            showInLegend: false,  
                        data: [1,3, 5, 9, 10]
                    }]
                });
            }
            $scope.categoriesBySpend();


            $scope.supplierPerMonth = function () {
                Highcharts.chart('supplierPerMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Supplier Per Month'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Supplier in 2019: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                        name: '',
                        data: [
                            ['January', 50],
                            ['February', 43],
                            ['March', 56],
                            ['April', 50],
                            ['May', 47],
                            ['June', 56]
                        ],
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            }
            $scope.supplierPerMonth();

            $scope.savingsPerMonth = function () {
                Highcharts.chart('savingsPerMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Savings Per Month'
                    },
                    //subtitle: {
                    //    text: 'Source: WorldClimate.com'
                    //},
                    xAxis: [{
                        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun'],
                        crosshair: true
                    }],
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value}K',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Total Savings',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, { // Secondary yAxis
                        title: {
                            text: 'Savings %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value} %',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        x: 120,
                        verticalAlign: 'top',
                        y: 100,
                        floating: true,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || // theme
                            'rgba(255,255,255,0.25)'
                    },
                    series: [{
                        name: 'Savings',
                        type: 'column',
                        yAxis: 1,
                        
                        data: [9, 10, 12, 11, 9, 10],
                        tooltip: {
                            valueSuffix: ' %'
                        }

                    }, {
                        name: 'Total Savings',
                        type: 'spline',
                            
                            data: [60, 80, 100, 80, 60, 80],
                        tooltip: {
                            valueSuffix: 'K'
                        }
                    }]
                });
            }
            $scope.savingsPerMonth();
        }
    ]);