﻿prmApp

    .controller('savingsDashBoardCtrl', ["$scope", "$rootScope", "$filter", "$stateParams", "$http", "domain", "fileReader", "$state",
        "$timeout", "auctionsService", "userService", "SignalRFactory", "growlService", "$log", "signalRHubName", "ngDialog",
        "reportingService", "$window", "priceCapServices", "PRMLotReqService", "PRMCustomFieldService", "workflowService", 
        function ($scope, $rootScope, $filter, $stateParams, $http, domain, fileReader, $state, $timeout, auctionsService,
            userService, SignalRFactory, growlService, $log, signalRHubName, ngDialog, reportingService, $window, priceCapServices,
            PRMLotReqService, PRMCustomFieldService, workflowService) {

            $scope.currentUserCompID = userService.getUserCompanyId();
            $scope.currentUserId = +userService.getUserId();
            $scope.currentSessionId = userService.getUserToken();
            $scope.categoryStatsArray = [];
            $scope.categoryStatsArrayTemp = [];
            $scope.monthlyStatsArray = [];
            $scope.monthlyArrayTemp = [];
            $scope.departmentStatsArray = [];
            $scope.departmentStatsArrayTemp = [];


            reportingService.GetCompanySavingStats({ compid: $scope.currentUserCompID, sessionid: $scope.currentSessionId })
                .then(function (response) {
                    if (response) {
                        var categoryStats = response.filter(function (item) {
                            return item.name === 'CATEGORY';
                        });

                        if (categoryStats && categoryStats.length > 0) {
                            $scope.categoryStatsArray = categoryStats[0].arrayPair;
                            $scope.categoryStatsArrayTemp = $scope.categoryStatsArray;
                            //$scope.categoryStatsArray.forEach(function (item, index) {
                            //    item.percentage = (item.decimalVal1) / (item.decimalVal1) * 100;
                            //})
                            //var categoryStatsArray = [];
                            //categoryStats[0].arrayPair.forEach(function (item, itemIndexs) {
                            //    categoryStatsArray.push([item.key1, item.decimalVal]);
                            //});

                            //$scope.savingsFunnel(categoryStatsArray);
                        }

                        var monthlyStats = response.filter(function (item) {
                            return item.name === 'MONTHLY';
                        });

                        if (monthlyStats && monthlyStats.length > 0) {
                            $scope.monthlyStatsArray = monthlyStats[0].arrayPair;
                            var monthlyStatsArrayTEMP = [];
                            monthlyStats[0].arrayPair.forEach(function (item, itemIndexs) {
                                monthlyStatsArrayTEMP.push([item.key1, item.decimalVal]);
                            });

                            $scope.monthlyArrayTemp = $scope.monthlyStatsArray;

                            $scope.savingsbymonth(monthlyStatsArrayTEMP);
                        }

                        var departmentStats = response.filter(function (item) {
                            return item.name === 'DEPARTMENT';
                        });

                        if (departmentStats && departmentStats.length > 0) {
                            $scope.departmentStatsArray = departmentStats[0].arrayPair;

                             $scope.departmentStatsArrayTemp = $scope.departmentStatsArray;
                            //var departmentStatsArray = [];
                            //departmentStats[0].arrayPair.forEach(function (item, itemIndexs) {
                            //    departmentStatsArray.push({ name: item.key1, y: item.decimalVal });
                            //});

                            //$scope.savingsPie(departmentStatsArray);
                        }

                    }
                });

            $scope.savingsFunnel = function (funnelData) {
                Highcharts.chart('savingsFunnel', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'funnel'
                    },
                    title: {
                        text: 'Savings By Categry'
                    },
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b> ({point.y:,.0f})',
                                softConnector: true
                            },
                            center: ['40%', '50%'],
                            neckWidth: '30%',
                            neckHeight: '25%',
                            width: '80%'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    series: [{
                        name: 'Categories',
                        data: funnelData
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                plotOptions: {
                                    series: {
                                        dataLabels: {
                                            inside: true
                                        },
                                        center: ['50%', '50%'],
                                        width: '100%'
                                    }
                                }
                            }
                        }]
                    }
                });
            };

            //$scope.savingsFunnel();

            $scope.savingsPie = function (departmentStatsArray) {
                // Make monochrome colors
                var pieColors = (function () {
                    var colors = [],
                        base = Highcharts.getOptions().colors[0],
                        i;

                    for (i = 0; i < 10; i += 1) {
                        // Start out with a darkened base color (negative brighten), and end
                        // up with a much brighter color
                        colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
                    }
                    return colors;
                }());

                // Build the chart
                Highcharts.chart('savingsPie', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Savings by Department'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    accessibility: {
                        point: {
                            valueSuffix: '%'
                        }
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            colors: pieColors,
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                                distance: -50,
                                filter: {
                                    property: 'percentage',
                                    operator: '>',
                                    value: 4
                                }
                            }
                        }
                    },
                    series: [{
                        name: 'Share',
                        data: departmentStatsArray
                    }]
                });
            };
            


            $scope.targetBar = function () {
                Highcharts.chart('targetBar', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Savings Vs Org Target bar chart'
                    },
                    xAxis: {
                        categories: ['Savings', 'Target']
                    },
                    yAxis: {
                        min: 0,
                        max: 5000,
                        title: {
                            text: 'Gap to Target 162,616(4%)$'
                        }
                        
                    },
                    legend: {
                        reversed: true
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [{
                        showInLegend: false,  
                       name:'',
                        data: [4000,3500]
                    }]
                });
            }
            //$scope.targetBar();

            $scope.savingsbymonth = function (monthlyStatsArray) {
                monthlyStatsArray = [
                    ['January',1660986.54],
                    ['February',2814033.24],
                    ['March',3616009.35],
                    ['April', 575250],
                    ['May', 1913617.5],
                    ['June', 751720.05],
                    ['July', 5357489.16],
                    ['August', 3100780.15],
                    ['September', 236301.35],
                    ['November', 1281.17],
                    ['December', 1926347.62]
                ]

                Highcharts.chart('savingsByMonth', {
                    credits: {
                        enabled: false
                    },
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Savings By Month'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {

                        type: 'category',
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ''
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Savings: <b>{point.y:.1f} millions</b>'
                    },
                    series: [{
                         name: '',
                        data: monthlyStatsArray,
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#FFFFFF',
                            align: 'right',
                            format: '{point.y:.1f}', // one decimal
                            y: 10, // 10 pixels down from the top
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    }]
                });
            };

            $scope.savingsbymonth();

            $scope.filterCategories = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.categoryStatsArray = $scope.categoryStatsArrayTemp;
                }
            };

            $scope.filterDeapartment = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.departmentStatsArray = $scope.departmentStatsArrayTemp;
                }
            };

            $scope.filterMonth = function (search) {

                search = search.toLowerCase();

                if (search) {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp.filter(function (item) {
                        return item.key1.toLowerCase().indexOf(search) >= 0;
                    });
                } else {
                    $scope.monthlyStatsArray = $scope.monthlyArrayTemp;
                }
            };
            
        }
    ]);