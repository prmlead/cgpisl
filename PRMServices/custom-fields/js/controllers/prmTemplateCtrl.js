﻿prmApp
    .controller('prmTemplateCtrl', ["$scope", "$stateParams", "$log", "$state", "$window", "userService", "auctionsService", "storeService", "growlService",
        "PRMCustomFieldService",
        function ($scope, $stateParams, $log, $state, $window, userService, auctionsService, storeService, growlService, PRMCustomFieldService) {

            $scope.userID = userService.getUserId();
            $scope.sessionID = userService.getUserToken();
            $scope.deptID = userService.getSelectedUserDepartmentDesignation().deptID;        
            $scope.prmTemplateList = [];
            $scope.prmTemplateList1 = [];
            $scope.selectedTemplate = {};

            $scope.getPRMTemplateList = function () {
                var params = {
                    "compid": userService.getUserCompanyId(),
                    "sessionid": userService.getUserToken()
                };

                PRMCustomFieldService.GetTemplates(params)
                    .then(function (response) {
                        $scope.prmTemplateList = response;
                        $scope.prmTemplateList1 = response;
                    });
            };

            $scope.getPRMTemplateList();
            
            $scope.goToTemplateFields = function (template) {
                var url = $state.href("template-fields", { "Id": template.TEMPLATE_ID, "name": template.TEMPLATE_NAME });
                window.open(url, '_self');
            };


            $scope.editTemplate = function (template) {
                createTemplateObj();
                $scope.selectedTemplate.TEMPLATE_ID = template.TEMPLATE_ID;
                $scope.selectedTemplate.TEMPLATE_NAME = template.TEMPLATE_NAME;
                $scope.selectedTemplate.TEMPLATE_DESC = template.TEMPLATE_DESC;
                $scope.selectedTemplate.IS_DEFAULT = template.IS_DEFAULT == 1 ? true : false;
            };

            $scope.newTemplate = function () {
                createTemplateObj();
            };

            $scope.saveTemplate = function () {
                angular.element('#showTemplateModal').modal('hide');
                let params = {
                    template: $scope.selectedTemplate
                };

                PRMCustomFieldService.SaveTemplate(params)
                    .then(function (response) {
                        if (response && +response.objectID) {
                            if (!$scope.selectedTemplate.TEMPLATE_ID) {
                                $scope.selectedTemplate.TEMPLATE_ID = response.objectID;
                                $scope.prmTemplateList.push($scope.selectedTemplate);
                            } else {
                                let temp = $scope.prmTemplateList.filter(function (item) {
                                    return item.TEMPLATE_ID === $scope.selectedTemplate.TEMPLATE_ID;
                                });

                                if (temp && temp.length > 0) {
                                    temp[0].TEMPLATE_NAME = $scope.selectedTemplate.TEMPLATE_NAME;
                                    temp[0].TEMPLATE_DESC = $scope.selectedTemplate.TEMPLATE_DESC;
                                }
                            }
                            
                            swal("Success!", "Successfully created.", "success");
                        } else {
                            swal("Error!", "Error saving data.", "error");
                        }
                    });
            };

            $scope.duplicateTemplate = function () {
                let isDuplicate = false;

                let temp = $scope.prmTemplateList.filter(function (item) {
                    return item.TEMPLATE_NAME === $scope.selectedTemplate.TEMPLATE_NAME && item.TEMPLATE_ID !== $scope.selectedTemplate.TEMPLATE_ID;
                });

                if (temp && temp.length > 0) {
                    isDuplicate = true;
                }

                return isDuplicate;
            };

            function createTemplateObj() {
                $scope.selectedTemplate = {
                    TEMPLATE_ID: 0,
                    COMP_ID: userService.getUserCompanyId(),
                    TEMPLATE_NAME: '',
                    TEMPLATE_DESC: '',
                    ModifiedBy: userService.getUserId(),
                    IS_DEFAULT: false
                };
            }
            $scope.searchTable = function () {

                if ($scope.searchKeyword) {
                    $scope.prmTemplateList = _.filter($scope.prmTemplateList1, function (item) {
                        return (item.TEMPLATE_NAME.toUpperCase().indexOf($scope.searchKeyword.toUpperCase()) > -1
                            
                        );
                    });
                } else {
                    $scope.prmTemplateList = $scope.prmTemplateList1;
                }

               

            };

        }]);