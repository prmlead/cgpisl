﻿using PRM.Core.Domain.Configurations.Requirements;
using System.Collections.Generic;

namespace PRM.Services.Configurations
{
    public interface IRequirementFormService
    {
        IList<RequirementForm> GetRequirementForm(int companyId);

        void InsertRequirementFormField(RequirementForm entity);

        void UpdateRequirementFormField(RequirementForm entity);

        void DeleteRequirementFormField(RequirementForm entity);

        RequirementForm GetRequirementFormField(int id);
    }
}