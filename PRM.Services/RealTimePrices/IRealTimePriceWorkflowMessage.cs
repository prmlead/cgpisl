﻿using PRM.Core.Domain.RealTimePrices;
using System;
using System.Collections.Generic;

namespace PRM.Services.RealTimePrices
{
    public interface IRealTimePriceWorkflowMessage
    {
        void SendMailOnProductPriceDrop(string toEmail, string name, IList<RealtimePriceDrop> RealTimePrice, DateTime? dropDate);

        void SendMessageOnDropLastPursched(string toEmail, string name, string productName, decimal diffPrice, decimal price, DateTime? dropDate);

        void SendMessageOnPriceReachedThershlod(string toEmail, string name, IList<RealTimePrice> RealTimePrice, DateTime? dropDate, List<RealTimeSelectedProduct> RealtimeSelectProduct);


    }
}