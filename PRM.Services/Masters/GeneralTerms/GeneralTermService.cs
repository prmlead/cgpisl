﻿using Dapper;
using PRM.Core.Domain.Acl;
using PRM.Core.Domain.Masters.GeneralTerms;
using PRM.Core.Pagers;
using PRM.Data;
using PRM.Services.Acl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Masters.GeneralTerms
{
  public  class GeneralTermService  : IGeneralTermService
    {
        public GeneralTermService(IRepository<GeneralTerm> generalTermRep,
        IDbProvider dbProvider, IAclMappingService aclMappingService)
        {
            _generalTermRep = generalTermRep;
            _dbProvider = dbProvider;
            _aclMappingService = aclMappingService;

        }

        private readonly IRepository<GeneralTerm> _generalTermRep;
        private readonly IAclMappingService _aclMappingService;

        private readonly IDbProvider _dbProvider;
        public void DeleteGeneralTerm(GeneralTerm entity)
        {
            _generalTermRep.Delete(entity);
        }

        public GeneralTerm GetGeneralTermById(int id)
        {

            var entity = _generalTermRep.GetById(id);

            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "GeneralTerm", EntityId = entity.Id });
            if (entity.LimitedToDepartment)
                entity.AllowedDepartments = mapper.Where(e => e.Type == (int)AclMappingType.Department).Select(e => e.Value).ToArray();

            if (entity.LimitedToProject)
                entity.AllowedProjects = mapper.Where(e => e.Type == (int)AclMappingType.Project).Select(e => e.Value).ToArray();

            if (entity.LimitedToUser)
                entity.AllowedUsers = mapper.Where(e => e.Type == (int)AclMappingType.User).Select(e => e.Value).ToArray();

            return entity;
        }

        public IPagedList<GeneralTerm> GetGeneralTermList(Pager pager,int companyId)
        {
            using (var con = _dbProvider.Connection())
            {
                var query = "SELECT SQL_CALC_FOUND_ROWS  * FROM master_generalterm";

                List<String> WhereColumns = new List<string>();

                WhereColumns.Add(string.Format(" Company_Id = {0} ", companyId));
                WhereColumns.Add(string.Format(" Deleted = {0}", 0));

                if (!string.IsNullOrEmpty(pager.Criteria.SearchTerm))
                {
                    WhereColumns.Add(string.Format(" Tttle Like %{0}%", pager.Criteria.SearchTerm));
                    WhereColumns.Add(string.Format(" Terms Like %{0}%", pager.Criteria.SearchTerm));


                }

                if (pager.Criteria.Filter.Any())
                {

                }

                if (WhereColumns.Any())
                    query = query + string.Format(" WHERE {0}", string.Join(" AND ", WhereColumns));

                query = query + " ORDER BY CreatedOnUtc DESC ";

                if (pager.PageIndex >= 0 && pager.PageSize > 0)
                {
                    query = query + string.Format(" LIMIT {0},{1} ", pager.PageIndex * pager.PageSize, pager.PageSize);
                }

                query += " ; SELECT FOUND_ROWS()";

                var multi = con.QueryMultiple(query);

                var lst = multi.Read<GeneralTerm>().ToList();
                int total = multi.Read<int>().SingleOrDefault();

                return new PagedList<GeneralTerm>(lst, pager.PageIndex, pager.PageSize, total);
            }
        }

        public IList<GeneralTerm> GetGeneralTermList(object filter)
        {
            return _generalTermRep.GetList(filter).ToList();
        }

        public void InsertGeneralTerm(GeneralTerm entity)
        {
          var id =  _generalTermRep.Insert(entity);

            entity.Id = id;
            InsertMapping(entity);
        }

        public void UpdateGeneralTerm(GeneralTerm entity)
        {
            _generalTermRep.Update(entity);
            var mapper = _aclMappingService.GetAclMappingList(new { EntityName = "GeneralTerm", EntityId = entity.Id });

            foreach (var map in mapper)
                _aclMappingService.DeleteAclMapping(map);

            InsertMapping(entity);
        }


        private void InsertMapping(GeneralTerm entity)
        {
            if (entity.LimitedToDepartment)
            {
                _aclMappingService.InsertDepartmentMapping(entity.Company_Id, "GeneralTerm", entity.Id, entity.AllowedDepartments);
            }

            if (entity.LimitedToProject)
            {
                _aclMappingService.InsertProjectMapping(entity.Company_Id, "GeneralTerm", entity.Id, entity.AllowedProjects);

            }

            if (entity.LimitedToUser)
            {
                _aclMappingService.InsertUserMapping(entity.Company_Id, "GeneralTerm", entity.Id, entity.AllowedUsers);

            }
        }

    }
}
