﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Logistics
{
    public static  class LogisticQuotationExtension
    {

        public static decimal CalculateVal(decimal val,int valType,int qty)
        {

            decimal result;


            if (valType > 0 && val > 0 && qty > 0)
            {
                result = (val * Convert.ToDecimal(qty));
            }
            else if (val > 0 && qty > 0)
            {
                result = val;
            }
            else
            {
                result = 0;
            }

            return result;

        }
    }
}
