﻿using PRM.Core.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PRM.Services.Common
{
     public static  class ExcelExtension
        {
        public static List<TSource> DataTableToList<TSource>(this DataTable dataTable) where TSource : class, new()
        {
            try
            {
                var dataList = new List<TSource>();

                const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.NonPublic;
                var objFieldNames = (from PropertyInfo aProp in typeof(TSource).GetProperties(flags)
                                     select new
                                     {
                                         Name = aProp.Name,
                                         Type = Nullable.GetUnderlyingType(aProp.PropertyType) ??
                             aProp.PropertyType
                                     }).ToList();
                var dataTblFieldNames = (from DataColumn aHeader in dataTable.Columns
                                         select new
                                         {
                                             Name = aHeader.ColumnName,
                                             Type = aHeader.DataType
                                         }).ToList();
                var commonFields = objFieldNames.Intersect(dataTblFieldNames).ToList();

                foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
                {
                    var aTSource = new TSource();
                    foreach (var aField in commonFields)
                    {
                        PropertyInfo propertyInfos = aTSource.GetType().GetProperty(aField.Name);
                        var value = (dataRow[aField.Name] == DBNull.Value) ?
                        null : dataRow[aField.Name]; //if database field is nullable
                        propertyInfos.SetValue(aTSource, value, null);
                    }
                    dataList.Add(aTSource);
                }
                return dataList;
            }
            catch
            {
                return null;
            }
        }

        public static List<T> ToList<T>(this DataSet datatable) where T : new()
        {
            return (from row in datatable.Tables[0].AsEnumerable()
                    select ConvertDataRow<T>(row)).ToList();
        }

        private static T ConvertDataRow<T>(DataRow row) where T : new()
        {
            var result = new T();

            var type = result.GetType();

            foreach (var fieldInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {


                var columnName = "";

                var dataRowKeyAttribute = fieldInfo.GetCustomAttributes(typeof(DataRowKeyAttribute), true).FirstOrDefault() as DataRowKeyAttribute;
                if (dataRowKeyAttribute != null)
                {
                    columnName = dataRowKeyAttribute.Key;
                }
                else
                {
                    columnName = fieldInfo.Name;
                }

                object value;
                if (row[columnName] is DBNull)
                {

                    value = fieldInfo.PropertyType.GetDefaultValue();
                }
                else
                {
                    value  = Convert.ChangeType(row[columnName], fieldInfo.PropertyType);

                }
                fieldInfo.SetValue(result,value, null);
            }

            return result;
        }

        public static object GetDefaultValue(this Type type)
        {
            // Validate parameters.
            if (type == null) throw new ArgumentNullException("type");

            // We want an Func<object> which returns the default.
            // Create that expression here.
            Expression<Func<object>> e = Expression.Lambda<Func<object>>(
                // Have to convert to object.
                Expression.Convert(
                    // The default value, always get what the *code* tells us.
                    Expression.Default(type), typeof(object)
                )
            );

            // Compile and return the value.
            return e.Compile()();
        }
    }
}
