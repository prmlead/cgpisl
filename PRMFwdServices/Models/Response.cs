﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace PRMFwdServices.Models
{
    [DataContract]
    public class Response : Entity
    {
        int objectId = 0;
        [DataMember(Name = "objectID")]
        public int ObjectID
        {
            get
            {
                return this.objectId;
            }
            set
            {
                this.objectId = value;
            }
        }

        string msg = string.Empty;
        [DataMember(Name = "message")]
        public string Message
        {
            get
            {
                return this.msg;
            }
            set
            {
                this.msg = value;
            }
        }


        [DataMember(Name = "userInfo")]
        public UserInfo UserInfo
        {
            get;
            set;
        }

        long tLeft = 0;
        [DataMember(Name = "timeLeft")]
        public long TimeLeft
        {
            get
            {
                return this.tLeft;
            }
            set
            {
                this.tLeft = value;
            }
        }



        int roleID = 0;
        [DataMember(Name = "roleID")]
        public int RoleID
        {
            get
            {
                return this.roleID;
            }
            set
            {
                this.roleID = value;
            }
        }



    }
}